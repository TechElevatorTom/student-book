# Spring MVC Controllers

The last thing we have to introduce in MVC, is the controller. Controllers play a large role in the [request/response lifecycle](https://docs.spring.io/spring/docs/3.2.x/spring-framework-reference/html/mvc.html).

![MVC Lifecycle](img/mvc.png)

It is through controllers that requests are routed to locate an appropriate action that is capable of generating a response. Controllers have a collection of regular Java methods that return a String that is the name of a JSP. Those methods are called Actions.

## Actions

Actions have a few general responsibilities:

1. Return a String or object that will be used to create the view. This is often the name of a JSP, but in the case of building an API, may be an object that will be converted to JSON.
2. Delegate work to other services or classes capable of retrieving the data model for the view.
3. Ensure that request data is valid. If not, the controller is responsible for returning a proper response indicating so.

Over time we will dig into each of these responsibilities and understand how they fit into the bigger picture.

### Returning JSP Names

When actions return a String, this will be the name of a JSP that will be executed once the controller action is complete.

You can also return an object that can then be converted to JSON. If you want to return JSON rather than a JSP view, then you'll need to add the `@ResponseBody` annotation to the top of the method as well.

``` Java
@ResponseBody
@RequestMapping(path="/books", method=RequestMethod.GET)
public List<Book> getAllBooks() {
    List<Book> books = bookDao.getBooks();

    // Just return the objects and they will be converted to JSON
    // because of the @ResponseBody annotation
    return books;
}
```

--------

In general, the controllers we write will act as mediators that will help us write cleaner code so that we can separate our models from our views. In the next few sections we'll see how controllers can be used to handle specific types of incoming requests (`GET` vs. `POST`).