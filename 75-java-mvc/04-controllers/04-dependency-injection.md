# Dependency Injection

Until now, we've written code in our classes to instantiate other classes. To clarify, when a class such as a controller needs to interact with the database, it is instantiating a DAO whenever and wherever it needs to. 

``` Java
@Controller
public class BooksController {
    @RequestMapping(path="/books/index", method=RequestMethod.GET)
    public String index(ModelMap modelHolder) {

        // create a new data access object
        BooksDao dao = new BooksJdbcDao();

        // retrieve all of the books
        List<Book> books = dao.getBooks();

        // return the books as the model to the view
        modelHolder.put("books", books);
        return "index";
    }
}
```

Despite how clean this code is, it is not easily testable. 

This line `BooksDao dao = new BooksJdbcDao();` can cause some pretty big issues: 

* It is impossible to unit test the controller because it will never fully be isolated. Every time we test it, it will need a real live database to connect to.
* The controller *does too much work*. Even though database code is contained within the DAO, the fact that the controller instantiates the class (with its connection string) implies that work is being spent writing code that doesn't manage HTTP Requests and Responses (the primary responsibility). 
* We've got tightly-coupled code. When we want to change the database that we work with, we need to change the classes our controllers instantiate.

We try to follow a rule called the [Single Responsibility Principle (SRP)](https://en.wikipedia.org/wiki/Single_responsibility_principle).  SRP states that a class should have responsibility **over a single part of the functionality provided by the software**. 

Another principle, the **Dependency Inversion Principle** states that high-level modules should not depend on specific low-level modules. Instead *they should depend on abstractions.* This is where we introduce the concept of dependency injection (DI).

> **Dependency injection (DI)** is a technique for achieving loose coupling between objects and their classes they rely on, aka dependencies. Rather than directly instantiating a dependency, the objects a class needs in order to perform its actions are provided to the class as an input. Most often, classes will declare their dependencies via their constructor, allowing other classes to *inject* in a dependency when constructing the object.

What we are more likely and prefer to see would be the following:

``` Java
@Controller
public class BooksController {

    // Allow Spring to inject a new BooksDao object
    @Autowired
    private BooksDao dao;

    @RequestMapping(path="/books/index", method=RequestMethod.GET)
    public String index(ModelMap modelHolder) {
        // retrieve all of the books
        List<Book> books = dao.getBooks();

        // return the books as the model to the view
        modelHolder.put("books", books);
        return "index";
    }
}
```

Now our `BooksController` is dependent on an `BooksDao`. This is much more abstract. It has no idea if it interfacing with a file, a database, or an API. Instead of referencing specific implementations, classes request abstractions (typically `interfaces`) which are provided when the class is constructed.

For further information on the concept of Dependency Injection, Martin Fowler has written a post called [Inversion of Control containers and the Dependency Injection pattern](https://www.martinfowler.com/articles/injection.html).

## DI Containers

When a system is designed to use DI, it's helpful to have a class dedicated to creating these dependencies. These classes are referred to as dependency injection containers. They may also be referred to as **Inversion of Control (IoC)** containers. The name Inversion of Control comes from the fact that the Controller class is no longer in charge of instantiating its dependencies. Those objects are determined outside of the Controller.

A container is simply a factory that is responsible for various instances of objects upon request. If a class has declared that it has dependencies on abstractions, and the container is configured to create the dependency, then it populates the dependency upon instantiation.

Spring comes with a built in Dependency Injection service out of the box. You can get objects dependency injected by using the `@Autowired` annotation. You also need to specify which objects should be injected using the `@Component` annotation, which we will talk about next.

--------

## Setting Up Dependency Injection

In order to configure your Spring MVC application for DI, you need to do the following:

1. **Create abstractions** in the form of interfaces for all of your lower-level modules.
1. **Setup autowiring for your higher-level modules** that allow dependencies to be injected via the `@Autowired` annotation.
1. **Configure your DI Container** by adding annotations to all the classes that should be dependency injected.

### Creating Abstractions

Let's look at a Movie Example, let's assume a class already exists called `MovieJdbcDao`.

``` Java
public class MovieJdbcDao {

    private JdbcTemplate jdbcTemplate;

    public MovieSqlDal(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    public Movie getById(int id) {
        String getMovieByIdSql = "SELECT * FROM movies WHERE id=?";

        // Make the call to the database to retrieve the data
        // and map it to a Movie object

        return movie;
    }
}
```

In order to abstract this, define an `interface` and call it `MovieDao`.

``` Java
public interface MovieDao {
    Movie getById(int id);
}
```

Now have `MovieJdbcDao` implement the `MovieDao` interface. This way we can say that a `MovieJdbcDao` *is-a* `MovieDao`.

``` Java
public class MovieJdbcDao implements MovieDao {
```

And we add `@Component` above the class declaration to tell Spring that we want it dependency injected into other classes.

``` Java
@Component
public class MovieJdbcDao implements MovieDao {
```

### Adding Constructor Injection

With the abstraction in place, we can declare it as a dependency in our controller.

``` Java
@Controller
public class MovieController {

    // To get an object dependency injected, we just need to
    // have one defined as a `@Component` and then add
    // the `@Autowired` annotation to the variable in the Controller
    @Autowired
    private MovieDao dao; // Will contain a MovieJdbcDao object because
                              // a MovieJdbcDao *is-a* MovieDao

    @RequestMapping(path="/detail", method=RequestMethod.GET)
    public String detail(@RequestParam int id) {
        Movie movie = dao.getById(id);
        return "movie";
    }
}
```

------

## Summary

It may seem like excess work to achieve the same result. Ultimately DI allows us to write *cleaner code*. All of the dependencies are managed by Spring. Any of our controllers can use a DAO if they need to by requesting the abstraction via the `@Autowired` annotation. They can even ask for multiple DAOs if necessary:

``` Java
@Controller
public class MovieController {
    @Autowired
    private MovieDao movieDao;

    @Autowired
    private ActorDao actorDao;

    @Autowired
    private CategoryDao categoryDao;
```

Lastly when we go to test our controllers, we can create [mocks](https://springframework.guru/mocking-unit-tests-mockito/) to simulate how the DAO should work under a test run.