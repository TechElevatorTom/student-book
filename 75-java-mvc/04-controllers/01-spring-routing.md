# Routing

In MVC apps, the controller is the workhorse that does a lot of the work handling the incoming request and determining what needs to be done. Before the controller can jump in and do its job, *the controller* that handles the request must be first identified. This step is called **routing**. 

Spring uses explicit `@RequestMapping` calls to route a request to the correct controller. We will look at how requests are routed based on two main pieces of the request.

- The URL
- The HTTP Method

Given this URL typed into the browser--`http://domain.com/books/index`--Spring will look through all the Controllers and find `@RequestMapping`s that match the path, in this case:

```Java
@RequestMapping(path="/books/index")
```

Spring will then look at the request method and use that to pick a certain controller action. In the above example, that `@RequestMapping` will match all the HTTP methods because none was specified.

> Other parameters provided via the querystring (?) may be used. We'll see how those work in a later section.

> Also note that if two or more `@RequestMapping`s match a given request, your Spring application won't even start up. You will get a message in the console after trying to "Run on server..." that the mappings must be unique.

If you ever get a 404 from a Spring application, you just need to make sure that there is a mapping for the URL **and** the request method that you are trying to connect with.