# Working with Models in Spring MVC

A major benefit of working with an MVC framework is the ability to create a **separation of concerns**. With SOC, we have the ability to separate the application logic (the models) from the way it is presented (the views).

This provides a number of different benefits:

1. The view isn't involved with code that performs logic. Its sole purpose is to output values of properties and methods into HTML.
1. The model doesn't worry how the information will be presented. It could be used to `System.out.println()` or to generate HTML. It could even be used with a mobile application which uses an entirely different rendering engine.

Within the same application we can realize the benefit by creating one view that displays a list of model objects while another page displays the details of a singular object. 

Take Amazon for example:

> If you were to search for *Java Books*, you'll see a page that shows a list of books. Clicking on a given book allows you to view that information at a more detailed level. In this case, the same model gets used between both views.

## What is a Model?

In Java, a model is a simple Java class that represents the data of our app. Models often contain properties. They may also contain validation logic that enforces rules for what type of data can be held in the model.

Here is an example model:

``` Java
public class Book {
   private String isbn;
   private String title;
   private BigDecimal cost;
   private String author;
   private int totalPages;

   public String getIsbn() {
       return isbn;
   }

   public void setIsbn(String isbn) {
       this.isbn = isbn;
   }

   public String getTitle() {
    return title;
    }

    public void setTitle(String title) {
        this.title = title;
   }

    // ... The rest of the getters and setters ... //
}
```

### Using a Model

Models are instantiated and populated from within a controller. In most cases when the application uses a database, the controller uses a data access layer (DAL) to populate the model.

Let's say we had a `BooksController`. Inside it includes an `index` action.

The code for this action would look like:

```java
@Controller
public class BooksController {
    @RequestMapping(path="/books/index", method=RequestMethod.GET)
    public String index(ModelMap modelHolder) {

        // create a new data access object
        BooksDao dao = new BooksJdbcDao();

        // retrieve all of the books
        List<Book> books = dao.getBooks();

        // return the books as the model to the view
        modelHolder.put("books", books);
        return "index";
    }
}
```

Whenever the URL (`http://localhost:xxxxx/books/index`) is requested, the MVC framework will invoke this action. Our code inside will instantiate a new data access object that retrieves the books and returns them as a model to the view. 

> Returning the String "index" from the method tells Spring to show the `/WEB-INF/jsp/index.jsp` view to the user.

When we build the `index.jsp` view, it will have access to everything that was put in the `modelHolder` in the request scope. That means that we can access that data in the JSP using the JSTL tags and EL.

Here's how our `index.jsp` JSP file will look.

```html

<h1>Book Listing</h1>

<p>There are <c:out value="${ books.size() }"/> books that match your search</p>

<div class="books">

<c:forEach var="book" items="${books}">
    <div class="book">
        <h2><c:out value="${ book.title }"/></h2>
        <p>$<c:out value="${ book.cost }"/></p>
        <p>By: <c:out value="${ book.author }"/> (pages: <c:out value="${ book.totalPages }"/>)</p> 
    </div>
</c:forEach>
</div>
```

Hypothetically, if our view were passed a model with 3 books in it, it would look something like this:

```html
<h1>Book Listing</h1>

<p>There are 3 books that match your search</p>

<div class="books">
    <div class="book">
        <h2>Clean Code</h2>
        <p>$20.99</p>
        <p>By: Robert C. Martin (pages: 415)</p>
    </div>
    <div class="book">
        <h2>The Pragmatic Programmer: From Journeyman to Master</h2>
        <p>$15.94</p>
        <p>By: Andrew Hunt (pages: 387)</p>
    </div>
    <div class="book">
        <h2>Lauren Ipsum</h2>
        <p>$14.99</p>
        <p>By: Carlos Bueno (pages: 215)</p>
    </div>
</div>
```

This allows us to create very powerful templates that can output a massive amount of consistent HTML for all of the objects!