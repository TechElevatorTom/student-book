# Spring MVC Validation & Error Handling

Before an app stores data in a database, the data must be validated. Data not only needs to be checked to make sure it holds valid values, but also to ensure it doesn't contain anything that may be a security threat. Validation is necessary although it can be tedious to implement. In a web application, validation often happens on both the client and the server.

We know that with an MVC project, each portion has various responsibilities:

* The model holds the data
* The view displays data to the user
* The controller mediates the request and determines which model/view to display

When it comes to working with validation, these 3 pieces will each play a role to enforce only valid data makes it into the system:

* The model will identify the validation rules to apply
* The controller will enforce the validation rules and determine what to return to the user
* The view will display the validation error messages if there are any

## Model Validation

With Spring MVC, we will use validation annotations as a way to configure what data is acceptable for a given property. It works similar to validation on database columns. Rules can include constraints such as:

* must adhere to a specific data type
* value is required
* value must match a supplied pattern (e.g. phone, credit card, etc.)

Below is a `Movie` model from an app that stores information about movies and TV shows. Most of the properties are required and several string properties have length requirements. Additionally, there's a numeric range restriction in place for the `price` property from 0 to $999.99, along with a custom validation attribute.

``` Java
public class Movie {
    private int id;

    @NotBlank(message="Title is required")
    @Size(max=100, message="Title can not be over 100 characters")
    private String title;

    @PastOrPresent(message="Release date can't be in the future")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    private LocalDate releaseDate;

    @NotBlank(message="Description is required")
    @Size(max=1000, message="Description can not be over 1000 characters")
    private String description;

    @Range(min=0, max=999.99, message="Price must be between $0 and $999.99")]
    private BigDecimal price;

    @NotNull(message="Genre is required")
    private Genre genre;

    private boolean preorder;
    
    // ... getters and setters for all properties ... //
}
```

Simply reading through the model reveals the rules about data for this app, making it easier to maintain the code. Below are other built-in validation attributes:

* `@CreditCardNumber`: Validates the property has a credit card format.
* `@Email`: Validates the property has an email format.
* `@Pattern`: Validates the property matches a given regular expression.
* `@Range`: Validates the property value falls within the given range.
* `@NotBlank`: Validates that something has been entered that is not just whitespace for a String.
* `@Size`: Validates property's size (`Map`, `List`, array or `String`) is between the max and min.
* `@URL`: Validates the property has a URL format.

You can find more of the available validation annotations on the [Hibernate Validator documentation](https://docs.jboss.org/hibernate/stable/validator/reference/en-US/html_single/#chapter-method-constraints).

## Displaying Validation Errors

To show errors on a form, we need to use the Spring Form tag library. These tags are used in place of the standard HTML form and input tags so that Spring can tie the form to the `@ModelAttribute` object in the controller.

If we follow the approach of binding our forms to models then we likely have forms that look similar to this:

``` JSP
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:url var="formActionUrl" value="/movie">
<form:form modelAttribute="movie" action="${formActionUrl}" method="POST">
    <form:input path="title" placeholder="Title" />
    <form:input path="releaseDate" placeholder="yyyy-MM-dd" />
    <form:textarea path="description" placeholder="Description" />
    <form:input path="price" placeholder="Price" />
    <input type="button" value="Submit" />
</form:form>
```

In order to display proper error messages, we need to update our HTML to include `form:errors` tags.

```JSP
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<c:url var="formActionUrl" value="/movie">
<form:form modelAttribute="movie" action="${formActionUrl}" method="POST">
    <form:input path="title" placeholder="Title" />
    <form:errors path="title" />
    <form:input path="releaseDate" placeholder="yyyy-MM-dd" />
    <form:errors path="releaseDate" />
    <form:textarea path="description" placeholder="Description" />
    <form:errors path="description" />
    <form:input path="price" placeholder="Price" />
    <form:errors path="price" />
    <input type="button" value="Submit" />
</form:form>
```

These tags will create placeholders that render an error message (only in the event that an error occurs). In all other cases, they will be ignored and hidden from the user.

## Enforcing Validation

To get the validation to happen, we need to add the `@Valid` annotation to the model in the controller.

Assuming we are still using the Movie model, we also have a controller like this that displays an empty form and then submits data via `POST` request to the database.

``` Java
@Controller
public class MovieController {
    @RequestMapping(path="/movie", method=RequestMethod.GET)
    public String new(ModelMap modelHandler) {
        if( ! modelHolder.contains("movie")) {
            // Add an empty movie object in or the spring form tags will error
            modelHolder.put("movie", new Movie());
        }
    }

    @RequestMapping(path="/movie", method=RequestMethod.POST)
    public String saveNew(@Valid @ModelAttribute Movie movie, BindingResult result, RedirectAttributes redirectAttributes) {
        // Check to see if there were any errors from the @Valid annotation
        if(result.hasErrors()) {
            redirectAttributes.addFlashAttribute(BindingResult.MODEL_KEY_PREFIX + "movie", result);
            redirectAttributes.addFlashAttribute("movie", movie);
            return "redirect:/movie";
        }

        // save movie to the database
        // movieDao.create(movie);

        redirectAttributes.addFlashAttribute("message", "Movie successfully saved.");
        
        return "redirect:/success";
    }
}
```

Spring MVC will only validate a model if it has the `@Valid` annotation on it. The `ModelBinding` object needs to be directly after the model. It will store any messages or errors that happened while Spring was attempting to put the data from the request into the model. If the ModelBinding `hasErrors()`, then we will put that `ModelBinding` object and the movie object into the `RedirectAttributes` and send the user back to the original form.

Whenever there is an error, our controller will handle the presence of errors and return the user to the original view (along with the original model data). There they will see their error messages and original invalid input.