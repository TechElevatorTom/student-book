# Configuration Settings

In most of the DAO code that has been shown, in order to instantiate a DAO (data access object), we've provided the connection string. It is a very normal approach as the connection string is needed in order for the DAO to do its job.

Consider that most companies use separate *environments* (DEV, Quality Assurance, Production - at minimum) to run their applications. In theory each time we *move* our code from one environment to another, our code changes. In reality, code should never be modified once it has been signed off and approved on (including changing a simple constant).

Most programs use *configuration files* to address this. A configuration file, or config file, is used to manage the settings for a program. Values such as file paths, server names, database connections, and credentials, are placed in a configuration file. Then as we need to change what connection string to use, we change these values without modifying the code. When the code gets changed there should be a good reason to do so (e.g. added a new feature, fixed bug, etc.).

In Spring, you can define multiple data sources in the `springmvc-servlet.xml` file. The main one that is defined is the default data source. This is typically the data source used for local development. It is defined in the `springmvc-servlet.xml` using something like this:

``` XML
<bean id="dataSource" class="org.apache.commons.dbcp2.BasicDataSource" destroy-method="close">
  <property name="driverClassName" value="org.postgresql.Driver" />
  <property name="url" value="jdbc:postgresql://localhost:5432/capstone" />
  <property name="username" value="capstone_appuser" />
  <property name="password" value="capstone_appuser1" />

</bean>
```

If you want to different data source on a remote server, you can't just change this XML. This XML file needs to be usable in all environments that the code is deployed to. But we do want to use a different data source on different servers. We can fix that by defining another data source that is only activated when on a certain server. We do this by putting that bean in another profile:

``` XML
<bean id="dataSource" class="org.apache.commons.dbcp2.BasicDataSource" destroy-method="close">
  <property name="driverClassName" value="org.postgresql.Driver" />
  <property name="url" value="jdbc:postgresql://localhost:5432/capstone" />
  <property name="username" value="capstone_appuser" />
  <property name="password" value="capstone_appuser1" />
</bean>

<beans profile="development">
  <bean id="dataSource" class="org.apache.commons.dbcp2.BasicDataSource">
    <property name="url" value="jdbc:postgresql://dev-server:5432/capstone"/>
    <property name="username" value="dev_user"/>
    <property name="password" value="nvfwp3lsw2@#trlf"/>
  </bean>
</beans>
```

If we turn on the development profile Spring will use the data source from the `development` beans and those will override the default beans of the same `id`. In the above example on our local machine, Spring will use the database at `localhost` and on the development machine, Spring will use the database on `dev-server`.