# Deploying a Spring MVC Application

Until now, the applications that you have developed have been deployed to your local computer. If your application needed to be public and accessible over the Internet, it would need to be hosted on a web server. One such way of doing this is to use a web hosting provider.

The following section walks through the steps that would be taken to deploy your application to [Heroku][heroku]. Heroku is a cloud solution that can be used to run many different types of applications, one of which can be a Spring MVC application.

[heroku]:https://www.heroku.com/