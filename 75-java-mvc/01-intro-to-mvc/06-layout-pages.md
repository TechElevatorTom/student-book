# JSP Shared Page Fragments

Web apps frequently contain similar visual elements. In this section, we will cover how to create common layouts so that code can be shared in the many different views of our application.

## What Is a Layout

Most sites have a common layout that provides the user with a consistent experience as they navigate from page to page. A layout often includes an app header, navigation or menu elements, location for content, and a footer.

![Common Layout](https://docs.microsoft.com/en-us/aspnet/core/mvc/views/layout/_static/page-layout.png?view=aspnetcore-2.1)

Other HTML structures such as scripts and stylesheets are used by many pages of an app and shouldn't need to be redefined. Shared JSP fragments provides the ability to define these shared elements so that each view referencing the fragments can reuse them. Shared fragments help us reduce duplicate code in views, following the [Don't Repeat Yourself (DRY) principle](http://deviq.com/don-t-repeat-yourself/).

By convention, JSP fragments usually end in `.jsp` and are kept in a folder called `common`. Typically, common parts of the application are stored in their own files, like `header.jspf` and `footer.jspf`.

## Example Header and Footer Page

An example `header.jsp` looks like this:

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    
    <!-- This title can be added by the JSP that includes this header -->
    <title><c:out value="${ title }"/></title>
</head>

<body>
  <div class="navbar">
    <ul>
      <c:url var="homeUrl" value="/"/>
      <li><a src="${homeUrl}">Home</a></li>

      <c:url var="aboutUrl" value="/about"/>
      <li><a src="${aboutUrl}">About</a></li>

      <c:url var="contactUrl" value="/contact"/>
      <li><a src="${contactUrl}">Contact</a></li>
    </ul>
  </div>
  <div class="container body-content">
```

An example `footer.jspf` would look like this:

``` HTML
    <hr />

    <footer>
      <p>&copy; 2019 - WebApplication1</p>
    </footer>
  </div>

</body>
</html>
```

Notice that this does not include any reference to the main content on the page, just the common elements that would be included on every page. That's because the actual page JSPs would include these files around its content.

### Including Layout Elements from a View

Since the fragments are in their own files, we just need to include them in the right places in our JSPs to use them. For instance, our `index.jsp` could look like this:

``` HTML
<%@ taglib prefix="c"   uri="http://java.sun.com/jsp/jstl/core" %>

<!-- Set the title of the page for the title tag in header.jspf -->
<c:set var="title" value="Index Page"/>
<%@ include file="common/header.jspf" %>

<h1>Welcome to the index page</h1>

<p>When you work with Acme Co. you're guaranteed to deal with the best
	products your money can by. We always stand by our word to deliver
	on time and on budget.
</p>

<%@ include file="common/footer.jspf" %>
```

Using the fragments from before, the full page when generated will have the following HTML:

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Index Page</title>
</head>

<body>
  <div class="navbar">
    <ul>
      <li><a src="/app-path/">Home</a></li>

      <li><a src="/app-path/about">About</a></li>

      <li><a src="/app-path/contact">Contact</a></li>
    </ul>
  </div>
  <div class="container body-content">

<h1>Welcome to the index page</h1>

<p>When you work with Acme Co. you're guaranteed to deal with the best
  products your money can by. We always stand by our word to deliver
  on time and on budget.
</p>

    <hr />

    <footer>
      <p>&copy; 2019 - WebApplication1</p>
    </footer>
  </div>

</body>
</html>
```