# JSP Views

The HTML that is generated and returned to the client comes from Views. In Java we refer to these as Java Server Pages or JSPs for short. These views are stored in files that end in `.jsp` and can be thought of as special HTML files with commands that Java can use to add data to the page.

## What is JSP?

In the beginning Java applications created HTML from a Servlet but this became very tedious and error prone. Java Server Pages were introduced to make it easier to implement dynamic HTML. You do this by inserting Java-like code into HTML pages using JSP tags and JSP Expression Language.

## JSP Expression Language (EL)

The JSP Expression Language (EL) was introduced to provide a more limited scripting language for use in JSPs. Since Views are used just to show data to a user and *not* to be a full programming language like Java, EL was developed to make that display of information easier and cleaner.

EL is very limited in what it can do. These limitations discourage writing complex logic or full Java objects in your HTML files. EL can interact with your other Java code, but is not meant to be a replacement for it.

EL is also easier for non-Java programmers--like strictly front end programmers or designers--to interact with your Java classes and objects without needing to know Java. EL shares similarities with Java syntax but acts in a very different way.

All EL expressions are written in the form `${ ... }`. The expression that is in between the curly brackets is considered the EL expression and is evaluated by the application is extract the value of the expression.

Expressions in EL can be data or logic operators. For example, if I put the following EL expression in my HTML:

``` HTML
<p> ${ content } </p>
```

Then EL will look for a variable called `content` and attempt to put the value of that variable in between the `p` tags.

You can also use simple logic operators in EL expressions:

``` HTML
${ names.length != 0 } <!-- will return true or false -->
```

### Java Beans and EL

EL has some special ways of interacting with Java objects to make it easier to use in your JSPs. For most of our Java objects, we've been adding getter and setter methods to access the properties. We can call these in EL by just using the instance variable names instead of calling the full getter method.

Use:

``` JSP
${ book.title }
```

Instead of:

``` JSP
${ book.getTitle() }
```

This will work for any method on an object that starts with `get` and takes no arguments.

These EL tags are used along with the Java Standard Tag Library to generate HTML in your MVC programs.

## Java Standard Tag Library Tags

The Java Standard Tag Library (JSTL) is a library of HTML-like tags that provide dynamic behavior to JSPs. JSTL tags are the primary way that you add logic to your view. There are a number of different tags that can perform a different action.

You import the JSTL tags into your JSP by adding the following line to the top of the JSP file:

``` HTML
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
```

### `<c:out>`

The `<c:out>` tag is used to put the value of an EL expression into the HTML page while also preventing a type of hack called a [Cross Site Scripting Attack](https://www.owasp.org/index.php/Cross-site_Scripting_(XSS)). If we had a variable called `name`, we could include that in our site's header with:

``` HTML
<h1>Hello, <c:out value="${ name }"/></h1>
```

> #### What's a Cross Site Scripting Attack?
>
> As stated above, the `<c:out>` tag will protect you're application from Cross Site Scripting Attacks. But what is a Cross Site Scripting Attack?
>
> Let's look at the above example again. Let's say that instead of using the `<c:out>` tag to print the name variable to the screen, we just printed it directly to the page.
>
> ``` HTML
> <h1>Hello, ${ name }</h1>
> ```
>
> The above is perfectly allowable and works as you would expect. Most of the time.
>
> If `name` equals `"Joe"`, then the resulting HTML will be:
>
> ``` HTML
> <h1>Hello, Joe</h1>
> ```

> ![Simple Header](img/XSSSimpleHeader.png)
>
> But what if `name` equaled something like `"<em>Joe</em>"`? Now the resulting HTML will be:
>
> ``` HTML
> <h1>Hello, <em>Joe</em></h1>
> ```
>
> ![Emphasized Header](img/XSSEmHeader.png)
>
> We've now allowed the program to embed HTML into our page. If that `name` variable data is coming from the user, that means that we've allowed the user to embed HTML into our page. If a user set their name to `<script src="https://hackerssite.net/js/maliciousscript.js">`, it would lead to this:
>
> ``` HTML
> <h1>Hello, <script src="https://hackerssite.net/js/maliciousscript.js"></h1>
> ```
>
> ![Attacking Header](img/XSSAttackHeader.png)
>
> Where did our name go? Since the name was actually equal to an HTML script tag, the browser didn't show it to the screen but instead loaded and ran the script!
> 
> Once a user can embed HTML in our page, that means that they can also embed JavaScript into our page and once that happens, they can completely change how the page looks and reacts to the user, allowing the hacker to steal user's passwords and other site information.
>
> This often happens when you are using one programming language to write another programming language. We saw this before when trying to use Java to write SQL and needed to protect against SQL Injection Attacks. XSS is a kind of injection attack too, where we are using Java and JSPs to write our HTML and JavaScript. The script tag is perfectly harmless in a Java string, but once it's written to the HTML, it can allow a malicious attack.
>
> So, how does `<c:out>` fix this? You can think of `<c:out>` as putting the content of the variable in the *text* of the page rather than the *HTML* of the page. For instance, if the script tag from above was printed using `<c:out>` instead of just using `${ name }`, it would look more like this:
>
> ``` HTML
> <h1>Hello, &lt;script src="https://hackerssite.net/js/maliciousscript.js"&gt;</h1>
> ```
>
> ![Prevent XSS Attacks via c:out](img/XSSPreventedAttack.png)
>
> Which the browser doesn't see as a script tag at all and instead shows the text in the display back to the user.
>
> Bottom line: Always use `<c:out>` to print out values to the screen. Never use just `${ ... }`.

### `<c:set>`

The `<c:set>` tag is used to create a new page scoped variable, a variable that only exists on the JSP page itself. When creating a new variable this way, you don't need to specify the datatype.

If we wanted to create a new variable and then put it into a header, we could use `<c:set>` like this:

``` HTML
<c:set var="newHeader" value="Welcome to the site" />

<h1><c:out value="${ newHeader }" /></h1>
```

### `<c:if>`

The `<c:if>` tag acts like an if statement with no else clause. It's a simple if that just checks the expression and, if true, adds the contents inside the tag to the HTML page.

``` HTML
<c:if test="${ temperature < 32 }">
    <p>It's below freezing!<p>
</c:if>
```

### `<c:choose>`, `<c:when>`, and `<c:otherwise>`

The `<c:choose>` tag is used when we have a more complex comparison to do. You can think of `<c:choose>` as being used when we need if/else logic and not just a simple if.

`<c:when>` and `<c:otherwise>` are the control structures in the `<c:choose>` tag. You can think of `<c:when>` as the `if` and `else if` structures, while `<c:otherwise>` is the final catch-all `else`. Only one of the sections will be run, just like a regular `if else` statement.

``` HTML
<p>The temperature is : <c:out value="${ temperature }"/></p>
<c:choose>
    
    <c:when test="${ temperature > 90 }">
        It's hot out!
    </c:when>
    
    <c:when test = "${ temperature > 32 }">
        It's a pretty average temperature.
    </c:when>
    
    <c:otherwise>
        Wow, it's cold out.
    </c:otherwise>
</c:choose>
```

The above code would be equivalent to the following in Java:

``` Java
if( temperature > 90 ) {
    System.out.println("It's hot out!");
} else if ( temperature > 32 ) {
    System.out.println("It's a pretty average temperature");
} else {
    System.out.println("Wow, it's cold out.");
}
```

### `<c:forEach>`

The `<c:forEach>` tag is used to create a loop in JSPs. This is helpful for taking an array or collection of data and making it into an HTML table or list. It can also be used to loop a certain number of times.

If we wanted to print out the numbers one through five in a list, we could use the `<c:forEach>` tag like this:

``` HTML
<ul>
    <c:forEach var="i" begin="1" end="5">
        <li><c:out value="${ i }"/></li>
    </c:forEach>
</ul>
```

We can also print out every name in a `names` array using the `<c:forEach>` like so:

``` HTML
<ul>
    <c:forEach var="name" items="${ names }">
        <li><c:out value="${ name }"/></li>
    </c:forEach>
</ul>
```

### `<c:url>`

The `<c:url>` tag is used to generate URLs to resources inside your application. This could be images, JavaScript files, CSS files, or any other file that is in your `WEB-INF` folder that you would want to link to.

If we had an image in our application at `/WEB-INF/img/small.png`, we could link to it in our application like this:

``` HTML
<c:url var="imageUrl" value="/img/small.png"/> <!-- leave off the WEB-INF! -->
<img src="${ imageUrl }" />
```