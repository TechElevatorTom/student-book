# Building Web Applications with Java

When building websites, we have two options:

1. Develop a static site that serves up a single HTML page for every URL.
2. Develop a dynamic web application that generates HTML pages based on the URL the visitor navigates to.

The first option works when our site is sufficiently small and not subject to a lot of change, but it doesn't scale. Imagine how many pages would be needed to support Twitter. Every time a new tweet is published a developer furiously works to write an HTML page allowing other visitors the ability to read it.

The second option, a web application, is used when there is a need to build an up-to-date, always changing website. Web applications consist of application logic (using C#, Java, PHP, etc.) that can both communicate with a database for information retrieval and generate on-the-fly HTML responses. 

The web application framework used depends on the programming language. With Java, we have many choices to choose from, but the most popular framework around is called the Spring Framework. 

## Spring Framework Overview

Spring Framework is a server-side framework that was created in 2003 in response to the complexity of building applications for the web in Java. Web applications developed with the framework will follow an established pattern to provide consistency and agility to quickly develop a web application. 

## What we mean by "Spring"

The term "Spring" means different things in different contexts. It can be used to refer to the Spring Framework project itself, which is where it all started. Over time, other Spring projects have been built on top of the Spring Framework. Most often, when people say "Spring", they mean the entire family of projects. If you visit the link below you can find a list of projects that belong under the "Spring" umbrella. 

https://spring.io/projects 


![Spring Projects](../img/spring-projects.png)