# What Is MVC?

Applications can become quite large. As a result of this, it often becomes difficult to manage the size and complexity as new features emerge or existing requirements change. To address this, software developers rely on design patterns that assist in keeping the code clean and maintainable. One such pattern is called Model-View-Controller (MVC).

MVC is an design pattern that separates an app into three main components:

1. **M**odel 
1. **V**iew
1. **C**ontroller 

As programmers the MVC pattern promotes loose coupling, by helping us create applications so that the logic across our web application can be reused while not allowing any particular part to _do too much_. 

The MVC pattern is not specific to the Spring Framework or Java. There are various frameworks that provide an implementation in many different programming languages.

## Models

Models are classes that represent the data in an app. Model classes define the properties of the data that we need to present and provide the validation rules that must be enforced. Models typically are retrieved from and stored in a database.

## Views

Views are the pieces of our application that are used to display the user interface. A view is a template that is generally built to project model data in one way or another.

## Controllers

Controllers play the mediator role. All incoming web requests coming from a client are routed to a controller that knows how to retrieve model data. The controller then lines it up with the appropriate view template. The output of this process is a web response that is sent back to the client, often in the form of HTML.

## Bringing It Together

If we use Twitter as an analogy:

- A single tweet or a list of tweets are the model.
- A Twitter timeline, a single tweet page, and a twitter thread are all different ways to display model data. 
- When the user navigates to `http://www.twitter.com`, the controller retrieves the Twitter data for that particular user, ensuring that only they have access to it. It is then passed into a timeline view so that the user can see their own personal timeline.

## Separation of Concerns

The MVC pattern helps us create apps that separate its different aspects out.

- UI logic exists only within views. 
- Business rules and validation only exist on models.
- Input logic, determining which data to retrieve exists in the controller.

As a team, this makes it possible for one person to work on the view code without depending on the business logic.

![A Basic MVC Application Flow](../img/mvc_diagram_with_routes.png)

