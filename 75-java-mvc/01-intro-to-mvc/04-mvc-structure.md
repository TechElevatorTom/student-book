# Spring MVC Project Structure

The MVC Project structure differs from the other types of projects that we have seen so far. There are a few important folders & files that have been added (and need to stay).

![Spring MVC Project Structure](../img/spring-starter-project.png)

## Dependencies

When you created the `hellospring` project you did so using the Maven Project wizard. Maven does a few things but one thing it does for you is manage your dependencies. As you learned earlier a dependency is a library (a third party library or one that you created)that you want to include in your project.

These dependencies are defined in `pom.xml` which is in the root of the project. If you were to open that file you should see a list of dependencies that looks similar to this:

``` xml
<dependencies>
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>javax.servlet-api</artifactId>
        <version>3.1.0</version>
        <scope>provided</scope>
    </dependency>
    <dependency>
        <groupId>javax.servlet</groupId>
        <artifactId>jstl</artifactId>
        <version>1.2</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-webmvc</artifactId>
        <version>4.3.4.RELEASE</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-jdbc</artifactId>
        <version>4.3.4.RELEASE</version>
    </dependency>
    <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-databind</artifactId>
        <version>2.8.5</version>
    </dependency>
    <dependency>
        <groupId>org.springframework</groupId>
        <artifactId>spring-test</artifactId>
        <version>4.3.4.RELEASE</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.postgresql</groupId>
        <artifactId>postgresql</artifactId>
        <version>9.4.1212</version>
    </dependency>
    <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-dbcp2</artifactId>
        <version>2.1.1</version>
    </dependency>
    <dependency>
        <groupId>org.bouncycastle</groupId>
        <artifactId>bcprov-jdk16</artifactId>
        <version>1.46</version>
    </dependency>
    <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.hamcrest</groupId>
        <artifactId>hamcrest-core</artifactId>
        <version>1.3</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.mockito</groupId>
        <artifactId>mockito-all</artifactId>
        <version>1.10.19</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.seleniumhq.selenium</groupId>
        <artifactId>selenium-java</artifactId>
        <version>3.0.1</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>org.seleniumhq.selenium</groupId>
        <artifactId>selenium-chrome-driver</artifactId>
        <version>3.0.1</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>info.cukes</groupId>
        <artifactId>cucumber-java</artifactId>
        <version>1.2.4</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>info.cukes</groupId>
        <artifactId>cucumber-spring</artifactId>
        <version>1.2.4</version>
        <scope>test</scope>
    </dependency>
    <dependency>
        <groupId>info.cukes</groupId>
        <artifactId>cucumber-junit</artifactId>
        <version>1.2.4</version>
        <scope>test</scope>
    </dependency>
</dependencies>
```

All of these came from the fact that you selected the Tech Elevator Archetype. If you skipped that step you could start with a blank project but then you would have had to add all of these dependencies yourself. In that list you will see libraries like Spring Framework and Spring MVC.

## Java Resources

The `Java Resources` node contains all your **Java** code. If you look closer you will notice that the same files are in Java Resource/src/main/java and in the folder /src/main/java. This is because `/src/main/java` is the actual folder on the file system. The Java Resource node is just a quick view into all of your Java Resources. 

![Java Resources](../img/java-resources.png)

In Java Resource you will find the following: 

* src/main/java - This folder contains of the Java code for your application. 
* src/main/resources - This folder contains any configuration files or additional resources.
* src/test/java - This folder contains any test classes.
* Libraries - This will contain any libraries (Java or Maven Dependencies) that your project has included. 

## Deployed Resources

The `Deployed Resources` node lists the folders that will be deployed at the root of your application. You will usually see the `webapp` folder which can also be found under `/src/main/webapp`. 

![Deployed Resources](../img/deployed-resources.png)

In here you will find the following:

* **WEB-INF/:** This directory contains all things related to the application that aren’t in the document root of the application. 
* **/WEB-INF/jsp/:** This contains all of our views for our application.
* **springmvc-servlet.xml** This is a configuration file used by Spring Framework.
* **web.xml** Java web applications use a deployment descriptor file to determine how URLs map to servlets, which URLs require authentication, and other information. 

![Deployed Resources](../img/deployed-resources.png)

Again, you could have created this yourself but its cumbersome and something else the Tech Elevator archetype does for you.

## MVC

While some frameworks will force you by convention to place certain types of files in a particular directory Spring Framework does not. As you can see by the default project you created your controller is right in the root of `/src/main/java/com/techelevator/`. 

![Hello Controller](../img/hello-controller.png)

With that said as your project grows it does help to organize your source code so that if other developers know what code is where. These are the 3 components that make up our MVC application. 

### Models

You can create a models package that will contain all of the models used by your application. In Spring models are Plain Old Java Objects (POJO's). At a minimum they often contain the properties that might be displayed on your web page (ie blog posts, books, tweets, authors, etc...)

You are free to organize the contents of this folder however you wish.

### Views

The views are what the users of your application see. As you found out these are stored in `/webapp/WEB-INF/jsp/`. 

### Controllers

You can create a controllers package that will contain all of the controllers used by your application. It is not a requirement but it is good practice to end the filename with the suffix `Controller`. Examples of this will be `MovieController`, `HomeController`, and `UserController`. You will learn more about controllers in a future lesson.

## Target

The target folder is the maven default output folder. When a project is build or packaged, all the content of the sources, resources and web files will be put inside of it, it will be used for construct the artifacts and for run tests.

You can delete all the target folder content with mvn clean command.
