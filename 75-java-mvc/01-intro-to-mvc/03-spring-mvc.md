# Getting Started with Spring MVC

Let's look at how we can create a new Spring MVC application using Eclipse.

## Creating your first Spring MVC Application

With Eclipse open, select **File > New > Project**

![New Project](../img/file-new-project.png)

Complete the **New Project** wizard:

### Select a wizard

In this step you need to select the type of project you will be creating. In this case you will be creating a [Maven](https://maven.apache.org/) Project. 

Maven is going to manage your dependencies for you. A dependency is library or set of code that your project depends on. In the case of The Spring Framework it is made up of many dependencies.

![Maven Project](../img/maven-project.png)

### Create Maven Project

In this step you can leave everything as is and you need to make sure `Create a Simple Project` is not checked. You will learn more about what an archetype is in the next step. This step is also asking where you should save this project to and the default workspace is fine. 

![New Maven Project](../img/maven-project-workspace.png)

### Maven Archetype

In short, [Archetype](https://maven.apache.org/archetype/index.html) is a Maven project templating toolkit. An archetype is defined as an original pattern or model from which all other things of the same kind are made. 

In the first drop down you can select from a catalog (or grouping of archetypes) and then all of the archetypes that belong to that catalog will be listed below. We have pre-installed the Tech Elevator Catalog that contains 3 archetypes. For this project you will be creating a simple MVC project so you can select `te-spring-mvc`.

![Maven Archetype](../img/maven-project-archetype.png)

If you want to install the Tech Elevator Catalog on a personal computer you can download them and learn how to install them from the link below.

https://bitbucket.org/te-curriculum/te-mvn-archetypes/src/master/

### Archetype Parameters

With any Maven project you will have to fill-in the following properties: 

* **GroupId:** uniquely identifies your project across all projects, so we need to enforce a naming schema. A groupId must follow Java's package name rules. This means it must start with a reversed domain name you control. You can create as many subgroups as you want. In this example you we will be using `com.techelevator`

* **ArtifactId:** is the name of the packaged application (jar or war) without the version. If you created it, then you can choose whatever name you want with lowercase letters and no strange symbols.In this example you are creating a project called `hellospring`.

* **Version:** if you distribute it, then you can choose any typical version with numbers and dots (1.0, 1.1, 1.0.1, ...). Don't use dates as they are usually associated with SNAPSHOT (nightly) builds. If it's a third party artifact, you have to use their version number whatever it is, and as strange as it can look. For example, 2.0, 2.0.1, 1.3.1. In this example you can just leave the default of 0.0.1-SNAPSHOT.

* **Package:** You can follow the groupId reverse domain naming convention here. This is the main package that will be created for you. In this example the Tech Elevator Archetype expects it to be `com.techelevator`.

![Maven Parameters](../img/maven-project-params.png)

When you have filled all of those values in click `Finish`.

### Spring MVC Project

When your project has been created you should see a project structure that looks similar to this. 

![Spring MVC Project Structure](../img/spring-starter-project.png)


## Running your new Spring MVC Application

Now that you have your first project created it's time to run it. Right click on your project > Run as > Run on Server. 

![Run on Server](../img/run-on-server.png)

If this is the first time running the project you should see the following dialog. 

![Run on Server Tomcat](../img/run-on-server-tomcat.png)

If you do, just click finish and the application should start up. When your application starts up you should see some information logged to the console and ultimately a line that says `INFO: Server Startup in xxxx ms`.

![Spring Console](../img/run-spring-console.png)

 If anything goes wrong this is the first place you should look to determine what errors were thrown. If your application is running you can open a web browser to the following URL. You will want to update YOUR_NAME to your name. 

http://localhost:8080/hellospring?name=YOUR_NAME

![Run](../img/hello-spring-running.png)
