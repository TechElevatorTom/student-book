# Building POST Forms with Spring MVC

When we first learned about HTTP, we learned there are different request methods. So far we've used `GET` which indicates we are _getting_ a resource. Another type of request that we will go into is an `HTTP POST`.

The `POST` method is used to send data to a server so that it can be _created_ on the server. This may includes: 

* uploading an image
* logging in and creating an account
* submitting a post to a bulletin board

Whatever the reason is, data is altered on the site. There are a few things that set `HTTP POST` requests apart from `HTTP GET` requests:

* `POST` requests are never remembered by the browser
* `POST` requests store content in the request body and have no size limit
* `POST` requests also create the all too common double post scenario


Ever seen this?

![Double Form Post](https://i.stack.imgur.com/dZhrT.png)

We'll see how to fix this later on. Let's jump in.

## Example POST Form

Let's modify our example our example form from our GET example. In this case we want it to be a signup page instead of a search page:

![Simple Form](https://cdn.pbrd.co/images/HJcc1Ca.png)

To get to this page we navigated to `http://www.xyz.com/register`.

To make our form *post* the data, we update the `method` attribute on our `<form>` tag to `post`. We also **keep the action** which tells us where to send the data pointed at `http://www.xyz.com/register`

```html
<c:url var="formUrl" value="/register" />
<form method="POST" action="${formUrl}">
    <label for="name">Name</label>
    <input id="name" type="text" name="name" />
    <label for="email">Email</label>
    <input id="email" type="text" name="email" />
    <input type="submit" />
</form>
```

This is what our controller looks like when its configured to handle a `POST` action.

```Java
@Controller
public class HomeController {

    @Autowired
    private PersonDao dao;
    
    @RequestMapping(path="/register", method=RequestMethod.GET)
    public String registerForm() {
        return "register";
    }

    @RequestMapping(path="/register", method=RequestMethod.POST)
    public String saveRegister(@ModelAttribute Person input, ModelMap modelHandler) {
        // Using name & email, maybe call a DAO to save the Avenger record
        Person person = dao.savePerson(input.getName(), input.getEmail());

        // Return the Success view indicating the avenger was saved
        modelHolder.put("person", person):
        return "success";
    }
}
```

Pay close attention here. We have two Register actions.

1. `public String registerForm() { }`
2. `public String saveRegister(@ModelAttribute Person input, ModelMap modelHandler) { }`

You can see each is mapped to the same URL, but one is specifically for the `GET` HTTP method while the other is for the `POST` HTTP method.

The user loading the form and submitting it will result in two requests.

1. **Request 1** will be a `GET` request and load the first page containing the empty form.
1. **Request 2** occurs when the form is submitted and is a `POST` request which sends the data to the server.


### Post-Redirect-Get

Back to the image at the beginning of the article. Have you ever seen this?

![Double Form Post](https://i.stack.imgur.com/dZhrT.png)

When a web form is submitted via HTTP POST, it is often common that the web user may attempt to refresh the page. This is understandable. Maybe they wish to show updated information on the screen and intend no harm. Certain browsers however, will cause the contents of the original POST request to be resubmitted. This might cause undesirable results (e.g. double signup, double ticketing purchase, etc.).

If the user were to press **continue** then this flow is what would likely occur between the browser and the server.

![Double Form Post Flow](https://upload.wikimedia.org/wikipedia/commons/f/f3/PostRedirectGet_DoubleSubmitProblem.png)

There is a pattern called Post-Redirect-Get (PRG) that can help eliminate this problem. Instead of returning a web page after saving the data, the server responds with a redirect command, telling the user's browser to go directly to another page (i.e. a confirmation page). This allows users to safely refresh the final page without duplicating the POST request.
 
![PRG Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/PostRedirectGet_DoubleSubmitSolution.png/525px-PostRedirectGet_DoubleSubmitSolution.png)


The controller we wrote from above has to be updated. It includes:

1. a new action to support the third request for the success page
1. a redirect from the POST action to the success GET

```Java
@Controller
public class HomeController {

    @Autowired
    private PersonDao dao;
    
    //1. Gets the empty form
    @RequestMapping(path="/register", method=RequestMethod.GET)
    public String registerForm() {
        return "register";
    }

    //2. Handles the Save Form request
    @RequestMapping(path="/register", method=RequestMethod.POST)
    public String saveRegister(@ModelAttribute Person input, ModelMap modelHandler) {
        // Using name & email, maybe call a DAO to save the Avenger record
        Person person = dao.savePerson(input.getName(), input.getEmail());

        // Return the Success view indicating the avenger was saved
        modelHolder.put("person", person):
        return "redirect:/success";
    }

    //3. Load the success view
    @RequestMapping(path="/success", method=RequestMethod.GET)
    public String showSuccessPage() {
        return "success";
    }
}
```

Returning a String starting with `"redirect:"` will tell the Spring to redirect the browser to the URL after the colon. The method itself generates an HTTP response to instruct the browser that it should go to the next URL.

> **REMEMBER** In order for any of these Actions to run, the browser must first submit a request. 3 actions means 3 separate requests. After we redirect the user to the `success` page though, the user can refresh the page to their heart's content and they'll only refresh the 3rd action, eliminating the double form submission.

This ends up being what is meant by Post-Redirect-Get.

1. The form is posted.
2. A redirect response is returned.
3. The browser gets the next page.

------

## Comparing `GET` and `POST`

Before moving on, let's summarize the key differences between `GET` and `POST`.

|   		 | GET 						  |	POST								|
|------------|----------------------------|-------------------------------------|
| Usage		 | Intended for "search" requests | Intended for upload, sending sensitive information, or saving data | 
| Bookmarked | Can be saved as a bookmark | Cannot be bookmarked in the browser |
| Parameters | Parameter data is sent in the URL | Parameter data is sent in the request body |
| BACK button behavior | Requests are re-executed | Browser alerts the user that request data will be re-submitted |
| Restrictions | Only ASCII characters are allowed in the URL | Any kind of character data is allowed |
| Security | Less secure | More secure |
| Form Length | Limited to how many characters can fit in the URL | None |
| Visibility | Visible in the address bar | Parameters are no displayed in the URL |