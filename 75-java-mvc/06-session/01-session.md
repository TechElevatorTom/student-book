# Session

Something that is often surprising when first learning web applications, is that HTTP is referred to as a *stateless protocol*. This means each HTTP request/response is independent and isolated from previous interactions. Imagine having a conversation with someone at a party who can only remember enough to answer your immediate question.

HTTP being stateless has pros and cons (depending on how you look at it). A disadvantage is that it is impossible for a web application to remember anything we ask it to do for an extended period of time (like put items into a shopping cart). This also has a pro though in that the less it needs to remember, the more incoming requests can be served.

That aside, sometimes users need things to be remembered. This is where *session* helps.

## Session

**Session** is the idea of server-side storage of information that is to be persisted throughout a user's interaction with a web application.

Whether we are building an application that needs to store a complex object (e.g. a shopping cart) or a simple object such as a zip code, session should be used. Sessions allow the server the ability to recall information about each client.

Most web applications store session data about their users without the user even acknowledging that this is desired. This occurs with the concept of something called *cookies*. Following is exactly how session works so that a server is able to recall information about a client on demand:

1. A server responds to a client request. In the response a unique **Session ID** is generated. The Session ID is so secure that it cannot be guessed by other users.
2. The responds indicates to the browser that the Session ID should be stored within a *cookie*.
3. For every subsequent request made by the browser, any and all cookies it has for a domain are sent to the server. Included in these cookies is the Session ID, providing enough information for the server to remember the client.

![Session Creation](https://www.whizlabs.com/wp-content/uploads/2015/11/session-tracking.png)


Session state exhibits the following behaviors:

* Cookies are specific to each browser, therefore Session IDs stored in cookies are not shared across browsers.
* Cookies may be deleted when the browser session ends.
* When a request is received with an *expired Session ID* a new session is created.
* The app retains session data for a limited time. When the time expires (timeout) the app clears the data in memory so that it can free up space for a new session with another client.
* There is no default mechanism to inform a web application that the client has closed their browser unless the client manually *logs out* of the web application.

## Saving and Retrieving Data From Session

Working with it is conceptually similar to working with a map.

To get the user's session, just ask for a `HttpSession` object in a controller action's parameters:

``` Java
@RequestMapping(path="/login", method=RequestMethod.POST)
public String performLogin(ModelMap modelHandler, HttpSession session) {
```

To store data into session, type the following (key must be a `String`, but value can be any kind of Java object):

``` Java
session.setAttribute(key, value);
```

To retrieve data from session, you must cast it to the type that you stored it as. You can do that like the following:

``` Java
String value = (String) session.getAttribute(key);
```

And we can do that with any type of object:

``` Java
// To store data - key is a string and cart is a ShoppingCart
session.setAttribute(key, cart);

// To retrieve data
ShoppingCart cart = (ShoppingCart) session.getAttribute(key);
```