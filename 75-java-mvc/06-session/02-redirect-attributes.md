# Redirect Attributes

Now that we have session, we have a way to persist data across separate HTTP requests. A common scenario is that we need to persist data for a short-lived duration. Consider a ticketing purchase flow:

1. The user navigates to a page and completes the information to purchase a ticket.
2. After the user submits the answer, they are taken to a page that displays the summary of their purchase.
3. After submitting the purchase, they are given a confirmation number to print off and keep for reference.

The challenge we end up having is coming up with a way to pass the confirmation number over to the confirmation page. We can use session, but then we should probably clear the data out or else we start to pollute our session with extra data.

Enter **Redirect Attributes**.

## What Are Redirect Attributes

Redirect Attributes are meant to be very short-lived session data. If you find yourself needing data **for the current and subsequent request**, Redirect Attributes are the perfect solution. It is only useful when you are redirecting the user and want the data to still be available. Once you access the data stored in Redirect Attributes, it is automatically cleared.

You can get a handle on a `RedirectAttributes` object just like you would a regular session, by requesting it in the controller action's parameters:

``` Java
@RequestMapping(path="/login", method=RequestMethod.POST)
public String performLogin(ModelMap modelHandler, HttpSession session, RedirectAttributes redirectAttributes) {
```

To set information in `RedirectAttributes`, write the following in your Controller before performing a redirect:

``` Java
redirectAttributes.addFlashAttribute(key, value); // Where key is a string and value is any Object
```

To get information out of RedirectAttributes in the subsequent request, just access it through the `ModelMap` object:

``` Java
// Get the value using the same key
// You may need to cast it
String value = (String) modelHandler.get(key);
```

