



## Parsing Strings

You're probably thinking, "great now I've got a string, but I wanted a number". Unfortunately we can't tell the framework to only accept numbers, however we can  _parse_ the characters they type and try to turn it into a number. 

Fortunately all of the primitive data types offer a `Parse()` method which accepts a string argument and will convert it into the desired data type.

{% if book.language === 'Java' %}

![Integer.parseInt()](resources/java-int-parse.png)

The intellisense for this one looks a bit different than the `nextLine()` method used earlier on. Let's call our attention to two very specific parts:

1. **Return Type** - The return type indicates that this method will return an int.
2. **Arguments** - The intellisense shown indicates that the argument provided **must be a string**. Using anything which cannot be implicitly converted into a string would result in a data mismatch.

> #### Note::Argument Values
>
> The intellisense indicates that we need to pass in a `string` for the program to compile correctly. This means we can pass in:
> 
> 1. a literal string (e.g. `"5"`)
> 2. a string variable 
> 3. a method expression which returns a string (e.g. `input.nextLine()`)

We should also see that in the list of available methods, there are a couple of other methods that are called `parseInt` on the `Integer` class. These are called *overloads*. Overloads are methods that have the same name, but take different arguments, so you can use a different one depending on your needs.

![java-overloads](resources/java-overloads.png)

{% elif book.language === 'C#' %}

![int.parse()](resources/c-int-parse.jpg)

The intellisense for this one looks a bit different than the `ReadLine()` method used earlier on. Let's call our attention to three very specific parts:

1. **Return Type** - The return type indicates that this method will return an int.
2. **Arguments** - The intellisense shown indicates that the argumente provided **must be a string**. Using anything which cannot be implicitly converted into a string would result in a data mismatch.

> #### Note::Argument Values
>
> The intellisense indicates that we need to pass in a `string` for the program to compile correctly. This means we can pass in:
> 
> 1. a literal string (e.g. `"5"`)
> 2. a string variable 
> 3. a method expression which returns a string (e.g. `Console.ReadLine()`)
3. **Overloads** - This last part isn't really a part of the method signature, but the phrase `+ 3 overloads` indicates that there are two other parameter signatures we can pass to call this method. When you're writing code that can call overloads, the UP and DOWN arrows allow you to cycle through the various options. We'll certainly use this feature later on.

![c#-overloads](resources/c-overloads.gif)


{% endif %}


### Now, Back to Our Program

Let's return to our program and add in the code that will prompt the user for the start from, end with, and increment values.

{% if book.language === 'Java' %}

```java
public static void main(String[] args) {

    Scanner input = new Scanner(System.in);

    //greet the user and prompt them to enter a start from, end with, and increment value
    System.out.print("Enter a kilometer value to start at: ");
    String value = input.nextLine();
    int kilometerStart = Integer.parseInt(value);

    System.out.print("Enter a kilometer value to end with: ");
    value = input.nextLine();
    int kilometerEnd = Integer.parseInt(value);

    System.out.print("How many should it increment by: ");
    value = input.nextLine();
    int incrementBy = Integer.parseInt(value);

    System.out.println("Going from " + kilometerStart + "km to " + kilometerEnd + 
        "km in increments of " + incrementBy + "km.");
    
    //print out each value converted into miles from start from to end with 
}
```

{% elif book.language === 'C#' %}

```csharp
static void Main(string[] args) 
{ 
    //greet the user and prompt them to enter a start from, end with, and increment value
    Console.Write("Enter a kilometer value to start at: ");
    string value = Console.ReadLine();
    int kilometerStart = int.Parse(value);

    Console.Write("Enter a kilometer value to end with: ");
    value = Console.ReadLine();
    int kilometerEnd = int.Parse(value);

    Console.Write("How many should it increment by: ");
    value = Console.ReadLine();
    int incrementBy = int.Parse(value);

    Console.WriteLine("Going from " + kilometerStart + "km to " + kilometerEnd + 
        "km in increments of " + incrementBy + "km.");

    //print out each value converted into miles from start from to end with 
}
```
{% endif %}

If we were to run our program this is what it would look like.

    Enter a kilometer value to start at: 0
    Enter a kilometer value to end with: 20
    How many should it increment by: 5
    Going from 0km to 20km in increments of 5km.