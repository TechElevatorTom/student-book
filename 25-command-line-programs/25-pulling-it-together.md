
## Putting It Together

If you're still wondering how some of the methods we wrote the last few days fits into this, great question! Take a look at this same program but with a few things changed towards the end to rely on a method that can convert kilometers to miles.

{% if book.language === 'Java' %}

```java
public static void main(String[] args) {

    Scanner input = new Scanner(System.in);

    //greet the user and prompt them to enter a start from, end with, and increment value
    System.out.print("Enter a kilometer value to start at: ");
    String value = input.nextLine();
    int kilometerStart = Integer.parseInt(value);

    System.out.print("Enter a kilometer value to end with: ");
    value = input.nextLine();
    int kilometerEnd = Integer.parseInt(value);

    System.out.print("How many should it increment by: ");
    value = input.nextLine();
    int incrementBy = Integer.parseInt(value);

    System.out.println("Going from " + kilometerStart + "km to " + kilometerEnd + 
        "km in increments of " + incrementBy + "km.");

    //print out each value converted into miles from start from to end with 
    for (int i = kilometerStart; i <= kilometerEnd; i += incrementBy) {
        double miles = kilometersToMiles(i);
        System.out.println(i + "km is " + miles + "mi.");
    }
}

static public double kilometersToMiles(int kilometers) {
    double miles = kilometers * 0.621371;
    return miles;
}
```
{% elif book.language === 'C#' %}

```csharp
static void Main(string[] args) 
{ 
    //greet the user and prompt them to enter a start from, end with, and increment value
    Console.Write("Enter a kilometer value to start at: ");
    string value = Console.ReadLine();
    int kilometerStart = int.Parse(value);

    Console.Write("Enter a kilometer value to end with: ");
    value = Console.ReadLine();
    int kilometerEnd = int.Parse(value);

    Console.Write("How many should it increment by: ");
    value = Console.ReadLine();
    int incrementBy = int.Parse(value);

    Console.WriteLine("Going from " + kilometerStart + "km to " + kilometerEnd + 
        "km in increments of " + incrementBy + "km.");

    //print out each value converted into miles from start from to end with
    for (int i = kilometerStart; i <= kilometerEnd; i += incrementBy)
    {
        double miles = KilometersToMiles(i); 
        Console.WriteLine(i + "km is " + miles + "mi.");
    } 
}

static public double KilometersToMiles(int kilometers)
{
    double miles = kilometers * 0.621371;
    return miles;
}
```
{% endif %}

All week long you've been implementing the bodies of methods. We took one of those methods and added it to our program. Now we can call it as often as we want. Notice the inside of the for loop calls the `kilometersToMiles()` method using our loop variable `i`. It may seem like more work but practicing an approach like this encourages good coding habits and can  actually makes less work for us later on. Some of the benefits are:

1. **Testable code** - Isolating code into small methods makes it easy to write tests that validate the formula calculates correctly.
2. **Readable code** - Over time code becomes easier to read when it does less work. Our for loop doesn't have to do much with `i` except call a function that converts it into miles. 

It might sound like a broken record by now, but we'll definitely dig into this in the coming weeks as we continue to write many of our own methods.

---

## Next Steps

Programming  becomes fun when you can create things to do for yourself. This program was just a start. There are a lot of things we could do to enhance it with functionality. If you're looking to continue experimenting and adding features consider some of these:

* Run the program infinitely until the user indicates they want to exit.
* Add other conversion units (miles to kilometers, feet to inches, kilometers to yards, etc.).
* Validate user input ensuring they enter positive numbers and it goes from small to large.
* What happens if the user enters something that isn't a number?