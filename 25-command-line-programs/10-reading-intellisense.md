
## Reading the Intellisense

Before we continue on we should take a moment and understand exactly what happened with that read line method.

{% if book.language === 'Java' %}
```java
Scanner input = new Scanner(System.in);
System.out.print("Enter a kilometer value to start at: "); //prints out a message
String userInput = input.nextLine();                   //saves what the user types in userInput
```
{% elif book.language === 'C#' %}
```csharp
Console.Write("Enter a kilometer value to start at: "); //prints out a message
string userInput = Console.ReadLine();                  //saves what the user types in userInput
```
{% endif %}

1. Invoke a method that reads keyboard input up until but not including when they press ENTER.
2. Save the output that method returned into a string variable named `userInput`.

While it does take some getting to know the {{ book.language }} language, how did we know to use a string and not an int?  **Intellisense**. Learning to read intellisense, and realizing it is impossible for all developers to remember everything, is the key to becoming great at writing code.

{% if book.language === 'Java' %}

![Java Intellisense](resources/java-intellisense.png)

{% elif book.language === 'C#' %}

![C# Intellisense](resources/c-intellisense.jpg)

{% endif %}

When hovering over a method, the intellisense window pops up. By reading it, we are able to see these important things:

1. **Return type** - The return type indicates what the method expression evaluates to. If we want to save the value in a variable we must match the return type. If we tried using another type, such as `int`, that would be a data type mismatch. 
2. **Method name** - The method name here indicates exactly what must be typed in order for our program to _invoke_ this method.
3. **Arguments** - Some methods require arguments that are provided, similar to required fields on a web page. If they're not provided (or the data types don't match) then the code won't compile.

Additionally the intellisense often provides a short description that indicates what the method does. Any description or possible exceptions that it can trigger are helpful to us since we didn't write these methods ourselves. We like to think of methods as black boxes. I don't want to know how it works internally, but I need to know what outputs it provides and what, if anything may go wrong.

![Black Box](resources/black-box.jpg)
