
## Converting Kilometers to Miles

The last part, printing out each of the conversion values goes back to what we've learned to love and enjoy so much this week, writing simple arithmetic logic. Writing a lot of repetitive statements and incrementing by the same amount until a value is reached...sounds like a `for` loop.

Remember the structure of the for loop.

```csharp
for(init; condition; increment/decrement) {
    statement or block of code to run when condition is true
}
```

Given all of our recent array practice, it might be burned into our brain that we can only write for loops that start at the beginning and go to the end of an array, incrementing one at a time. 

```csharp
for(int i = 0; i < length; i++) {
    statement or block of code to run when condition is true
}
```

For loops can start wherever and they can increase (or decrease) by however much necessary. With our conversion program we could write a for loop that goes from our start to finish variables.

```csharp
for (int i = kilometerStart; i <= kilometerEnd; i += incrementBy) {
    statement or block of code to run
}
```

This for loop will start the variable `i` at whatever value `kilometerStart` holds. It will run as long as `i < kilometerEnd`. After each iteration `i` will increment by whatever value `incrementBy` holds.


Plugging this into our program, we can now prompt the user for input and display calculated output.

{% if book.language === 'Java' %}

```java
public static void main(String[] args) {

    Scanner input = new Scanner(System.in);

    //greet the user and prompt them to enter a start from, end with, and increment value
    System.out.print("Enter a kilometer value to start at: ");
    String value = input.nextLine();
    int kilometerStart = Integer.parseInt(value);

    System.out.print("Enter a kilometer value to end with: ");
    value = input.nextLine();
    int kilometerEnd = Integer.parseInt(value);

    System.out.print("How many should it increment by: ");
    value = input.nextLine();
    int incrementBy = Integer.parseInt(value);

    System.out.println("Going from " + kilometerStart + "km to " + kilometerEnd + 
        "km in increments of " + incrementBy + "km.");

    //print out each value converted into miles from start from to end with 
    for (int i = kilometerStart; i <= kilometerEnd; i += incrementBy) {
        System.out.println(i + "km is " + i * 0.621371 + "mi.");
    }
}
```

{% elif book.language === 'C#' %}

```csharp
static void Main(string[] args) 
{ 
    //greet the user and prompt them to enter a start from, end with, and increment value
    Console.Write("Enter a kilometer value to start at: ");
    string value = Console.ReadLine();
    int kilometerStart = int.Parse(value);

    Console.Write("Enter a kilometer value to end with: ");
    value = Console.ReadLine();
    int kilometerEnd = int.Parse(value);

    Console.Write("How many should it increment by: ");
    value = Console.ReadLine();
    int incrementBy = int.Parse(value);

    Console.WriteLine("Going from " + kilometerStart + "km to " + kilometerEnd + 
        "km in increments of " + incrementBy + "km.");

    //print out each value converted into miles from start from to end with
    for (int i = kilometerStart; i <= kilometerEnd; i += incrementBy)
    {
        Console.WriteLine(i + "km is " + i * 0.621371 + "mi.");
    } 
}
```

{% endif %}

If this makes sense so far, awesome! You've got a fully fuctioning program now. 