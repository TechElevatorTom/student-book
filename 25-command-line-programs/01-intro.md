# Building Command Line Programs

In this chapter, you'll learn:

* How to read input from the user using Standard I/O (Java) or the Console (C#)
* What are method signatures and how are they used?
* Parsing strings into numeric data types




