## Class Hierarchies

Using inheritance to organize our classes allows us to build a Class Hierarchy of our code. You could think of inheritance as a parent-child relationship.

![Hierarchy of Clocks](resources/class-hierarchy.png)

In the example above, we have a variety of different Clocks that we would like do define in our code. They are arranged here in a hierarchy. Both GrandfatherClock and CuckooClock are types of Clocks. The Clock class would define all the characteristics of what a Clock is--holding the hours, minutes, and seconds, how to advance the time, etc.--and GrandfatherClock and CuckooClock would adopt those characteristics. They could then add their own, like the pendulum of the GrandfatherClock or the little birdy that comes out of the CuckooClock. They may look different, but they are still fundamentally Clocks.

In fact, programmers would say that if a GrandfatherClock inherits from Clock, then a GrandfatherClock object IS A Clock. GrandfatherClock would be called the **subclass** and Clock would be the **superclass**. Same for CuckooClock. CuckooClock would be the **subclass** and Clock would be the **superclass**.

Another thing to notice is that AlarmClock is a **subclass** of Clock but is also a **superclass** of DigitalAlarmClock. This means that DigitalAlarmClock IS A AlarmClock and IS A Clock. It inherits the properties and methods from all of the classes above it in the hierarchy!