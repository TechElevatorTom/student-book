## Subclasses are Their Superclass

Now we can create a new GrandfatherClock in a program.

```java
GrandfatherClock myClock = new GrandfatherClock();

myClock.tick();
myClock.tick();

String currentTime = myClock.getCurrentTime();
```

So we can create a new GrandfatherClock, but what if we wanted to create a new CuckooClock later? To do that, we would have to change our code in two places, but the rest of the code is the same!

```java
CuckooClock myClock = new CuckooClock();

myClock.tick();
myClock.tick();

String currentTime = myClock.getCurrentTime();
```

Now, that's not much change, but programmers are extremely lazy. Let's just admit it. So programmers will figure out a way to change even less code than that. We can make the code more flexible by treating a GrandfatherClock or a CuckooClock as just a plain Clock.

```java
Clock myClock = new CuckooClock();

myClock.tick();
myClock.tick();

String currentTime = myClock.getCurrentTime();
```

Since a GrandfatherClock IS A Clock, then I can save a GrandfatherClock in a Clock variable. Same goes for CuckooClock. This also let's me dynamically choose which kind of clock I want.

```java
Clock myClock;
if(choice.equals('cuckoo')) {
    myClock = new CuckooClock();
} else {
    myClock = new GrandfatherClock();
}

myClock.tick();
myClock.tick();

String currentTime = myClock.getCurrentTime();
```

That's pretty powerful when you think about it. We could have all kinds of different clocks defined but use the same code to work with all of them. As long as our code knows how to work with a Clock object, then it can also work with any object from a class that inherits from Clock.