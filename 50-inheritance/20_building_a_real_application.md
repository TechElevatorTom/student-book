# Building A Real Application

Continuing with the previous section, here is an additional video that updates our bookstore with new products, taking advantage of the inheritance principle.

{% if book.language === 'Java' %}

{% video %}https://www.youtube.com/watch?v=yq6xARZCZxs {% endvideo %}

{% elif book.language === 'C#' %}

{% video %}https://www.youtube.com/watch?v=XT3HmaXi6I4 {% endvideo %}

{% endif %}
