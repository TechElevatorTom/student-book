## Extending Classes

Let's look back at our Clock class from a previous chapter:

{% if book.language === 'Java' %}

```java
// Clock.java
public class Clock {
    private int hour;
    private int minute;
    private int second;

    // Overloaded Constructor
    public Clock() {
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
    }

    // Constructor
    public Clock(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public void tick(int numberOfTimes) {
        for(int i = 1; i <= numberOfTimes; i++) {
            this.tick();
        }
    }

    public void tick() {
        this.second += 1;

        if (this.second >= 60) {
            this.minute += 1;
            this.second = 0;
        }

        if (this.minute >= 60) {
            this.hour += 1;
            this.minute = 0;
        }

        if (this.hour >= 24) {
            this.hour = 0;
        }
    }

    // Getters and setters
    public String getCurrentTime() {
        return String.format("%02d:%02d:%02d", this.hour, this.minute, this.second);
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    public int getSecond() {
        return this.second;
    }
}
```

{% elif book.language === 'C#' %}

```csharp
// Clock.cs
public class Clock
{
    public int Hour { get; private set; }
    public int Minute { get; private set; }
    public int Second { get; private set; }    
    
    public Clock()
    {
        this.Hour = 0;
        this.Minute = 0;
        this.Second = 0;
    }

    public Clock(int hour, int minute, int second)
    {
        this.Hour = hour;
        this.Minute = minute;
        this.Second = second;
    }
}
```

{% endif %}

This class defines some simple behavior for a basic clock and, in fact, every clock that we could ever want would need the above functionality.

But every clock is different too.

Let's say that we want to define some different clocks that make different sounds.

So first, let's define a GrandfatherClock class that will tick and tock every time we call `tick()`.

We could copy and paste all the code from Clock into a new class called GrandfatherClock, but let's not do that. That would be bad and doesn't like up to the DRY principle; Don't Repeat Yourself.

Instead, we can have GrandfatherClock inherit the code from Clock instead.

{% if book.language === 'Java' %}

```java
// GrandfatherClock.java
public class GrandfatherClock extends Clock {
    // Subclasses must redefine parent Constructors
    public GrandfatherClock() {
        super(); // But it can still use all the same code!
    }

    // Constructor
    public GrandfatherClock(int hour, int minute, int second) {
        super(hour, minute, second);
    }
}
```

This is all we have to do to make a new GrandfatherClock class that behaves just like a regular Clock. In Java, we have to reimplement the constructors in the subclasses, but we can still call our superclass's constructors using the `super()` special method. When a subclass calls the `super()` constructor, it will run all the code in that constructor from the superclass.

{% elif book.language === 'C#' %}

```csharp
// GrandfatherClock.cs
public class GrandfatherClock : Clock
{
     // Subclasses must redefine parent Constructors
     // But it can still use all the same code!
    public GrandfatherClock() : base() { }

    // Constructor
    public GrandfatherClock(int hour, int minute, int second)
     : base(hour, minute, second) { }
}
```

This is all we have to do to make a new GrandfatherClock class that behaves just like a regular Clock. In C#, we have to reimplement the constructors if we don't use the default constructor in the superclass. We can still call our superclass's constructor using the `base()` special method. When a subclass calls the `base()` constructor, it will run all the code in that constructor from the superclass.


{% endif %}

So let's go ahead and add a tick tock sound to the GrandfatherClock:

{% if book.language === 'Java' %}

```java
// GrandfatherClock.java
public class GrandfatherClock extends Clock {
    // Subclasses must redefine parent Constructors
    public GrandfatherClock() {
        super(); // But it can still use all the same code!
    }

    // Constructor
    public GrandfatherClock(int hour, int minute, int second) {
        super(hour, minute, second);
    }

    public void tick() {
        super.tick();
        if(getSecond() % 2 == 0) {
            System.out.println("tick");
        } else {
            System.out.println("tock");
        }
    }
}
```

Here we can see the use of the `super` special variable. Once our object is created, we can use the `super` variable to call code from our superclass. We still want the GrandfatherClock's `tick()` method to behave like the Clock's `tick()` method, but with some extra functionality. Our `tick()` method has **overridden** the super `tick()` method. We didn't have to call `super.tick()` and if we didn't, the original tick code wouldn't have run. By defining the code as above, we run the original tick code first and then add in our special functionality for GrandfatherClock.

{% elif book.language === 'C#' %}

{% tabs first="GrandfatherClock.cs", second="Clock.cs" %}
{% content "first" %}

```csharp
// GrandfatherClock.cs
public class GrandfatherClock : Clock
{
     // Subclasses must redefine parent Constructors
     // But it can still use all the same code!
    public GrandfatherClock() : base() { }

    // Constructor
    public GrandfatherClock(int hour, int minute, int second)
     : base(hour, minute, second) { }
    
    public override void Tick() 
    {
        base.Tick();
        if(Second % 2 == 0) {
            Console.WriteLine("tick");
        } else {
            Console.WriteLine("tock");
        }
    }
}
```

{% content "second" %}
```csharp
// Clock.cs
public class Clock
{
    public int Hour { get; private set; }
    public int Minute { get; private set; }
    public int Second { get; private set; }    
    
    public Clock()
    {
        this.Hour = 0;
        this.Minute = 0;
        this.Second = 0;
    }

    public Clock(int hour, int minute, int second)
    {
        this.Hour = hour;
        this.Minute = minute;
        this.Second = second;
    }

    public virtual void Tick()
    {
        /* Clock Tick implementation here */
    }
}
```
{% endtabs %}

Here we can see the use of the `base` special variable. Once our object is created, we can use the `base` variable to call code from our superclass. We still want the GrandfatherClock's `Tick()` method to behave like the Clock's `Tick()` method, but with some extra functionality. Our `Tick()` method has **overridden** the base `Tick()` method (note the use of the `override` keyword. We didn't have to call `base.Tick()` and if we didn't, the original tick code wouldn't have run. By defining the code as above, we run the original tick code first and then add in our special functionality for GrandfatherClock.

> #### Note::Method Overrides
>
> In order for method overriding to work in C#, you need to declare the method as `virtual` in the superclass. This indicates that the subclasses can override it, if necessary.

{% endif %}

Now, let's make a CuckooClock. A CuckooClock will make a Cuckoo sound at every hour.

{% if book.language === 'Java' %}

```java
// CuckooClock.java
public class CuckooClock extends Clock {
    // Subclasses must redefine parent Constructors
    public CuckooClock() {
        super(); // But it can still use all the same code!
    }

    // Constructor
    public CuckooClock(int hour, int minute, int second) {
        super(hour, minute, second);
    }

    public void tick() {
        super.tick();
        if(getSecond() == 0 && getMinute() == 0) {
            System.out.println("cuckoo cuckoo");
        }
    }
}
```

{% elif book.language === 'C#' %}

```csharp
// CuckooClock.cs
public class CuckooClock : Clock
{
     // Subclasses must redefine parent Constructors
     // But it can still use all the same code!
    public CuckooClock() : base() { }

    // Constructor
    public CuckooClock(int hour, int minute, int second)
     : base(hour, minute, second) { }

    public override void Tick() {
        base.Tick();
        if(Second == 0 && Minute == 0) 
        {
            Console.WriteLine("cuckoo cuckoo");
        }
    }
}
```

{% endif %}

And now we have two new clocks, sharing all the functionality of Clock but with special features all their own.