# Inheritance

Another of our OOP principles is Inheritance, the act of having one class adopt the properties and methods of another class. This prevents code duplication and allows us to share code across classes while having the source code live in only one class file.