## Determining the Length of an Array

Let's say we wanted to get the last element in the array.

    int[] testScores = new int[10];

    // ... populate the array

    int lastValue = testScores[9];

It's cheating though since we know that 9 is going to be the last index. What if someone changed the size of the array and made it smaller? Then our code would trigger an exception because 9 is out of range.

As programmers, we should strive to avoid hard-coding numeric values into our program. Fortunately we have a formula for calculating the last index at runtime ,`length - 1`. Most programming languages provide a way to access this value for a given array.

{% if book.langauge === 'Java' %}

Each array has a property, `.length` that allows you to retrieve the size of the given array.

    int size = testScores.length;

That syntax is often used when referencing the last value


```java
int[] testScores = new int[10];

// ... populate the array

int lastValue = testScores[testScores.length - 1];
int secondToLastValue = testScores[testScores.length - 2];
```

{% elif book.language === 'C#' %}

Each array has a property, `.Length` that allows you to retrieve the size of the given array.

    int size = testScores.Length;

That syntax is often used when referencing the last value

```csharp
int[] testScores = new int[10];

// ... populate the array

int lastValue = testScores[testScores.Length - 1];
int secondToLastValue = testScores[testScores.Length - 2];
```

{% endif %}



Remember how expressions are evaluated? The `length - 1` expression evaluates first and evaluates to 9. That 9 is then used when the program runs to retrieve the actual last value from the array.
