
## Elements & Indexes

Let's say that we have our array of integers called `testScores`. How do we talk about it?

![An array of test scores](resources/testscores.png)

First of all, its important to know that each item inside of an array is referred to as an **element**. Our array that we created holds 10 elements.

When we want to reference a particular element in a discussion, we indicate which one by referring to it as "_the element at index X_". Notice that the first index starts at 0. In programming, counting often starts at 0 and therefore our first element is at index 0.

> #### Info::Why Zero?
> 
> Indexes start with zero because of the way arrays are stored in memory. Think of the variable `testScores` as a reference to a memory block where the array starts. Once we get to that memory block, index 0 actually means offset 0 blocks from the starting point. Index 1 means offset 1 block and so on. 

Whenever we need to determine the last element in an array, the calculation `length - 1` can be used. Here the length of the array is 10 and therefore the last index is 9.

### Accessing Elements Within an Array

To access a particular element in an array to read from or assign to it a new value, "index notation" is used. The index of the element you are trying to access is enclosed within a pair of `[]`.

    int[] testScores = new int[10];

    testScores[0] = 85; // update the value at index 0 to 85
    testScores[1] = 96; // update the value at index 1 to 96
    testScores[2] = 80; // update the value at index 2 to 80
    testScores[3] = 98; // ...
    testScores[4] = 89;
    testScores[5] = 70;
    testScores[6] = 93;
    testScores[7] = 84;
    testScores[8] = 66;
    testScores[9] = 96;

Retrieving a value from an element in the array works like reading the value of a variable, assuming you know the index. 

    int[] testScores = new int[10];

    // ... populate array

    int aaronsScore = testScores[0];

`aaronsScore` now holds a copy of the value stored at index 0 in the `testScores` array.

> #### Note::Out of Range
>
> Remember that arrays have fixed capacity. If you try to access an item which beyond the edge of the array boundary your program will receive an exception.