## Array Basics

When we want to work with collections of values we use arrays. Rather than creating multiple variables, we create a single variable name that has the possibility to represent multiple values. Instead of worrying whether the test score is Bea's or Sam's (Sam 1 or Sam 2), we interact with the variable as a collection of scores. 

### Declaring and Initializing an Array

Here's the syntax for declaring a new array

    int[] testScores;

This command declares a new variable, called `testScores`. Its data type is an array of integers.

Once the array is declared, it needs to be initialized. 

    testScores = new int[10];

We've just initialized our array, `testScores`, to hold 10 integers.

There are three key points to be aware of when working with arrays.

1. Arrays represent sequential collections of items. There is a beginning and an end.
1. All values in the array ***must be the same datatype***.
1. The size of the array must be defined when initializing the array. Once created, the size is 'fixed'.

Often you'll see programmers write the above two lines on one statement

    int[] testScores = new int[10];

Arrays are not limited to integers. You can create arrays to hold any datatype.

    string[] names = new string[5]; // an array of strings
    bool[] switches = new bool[10]; // an array of boolean values

{% if book.language === 'C#' %}

If you know the values of your array, you can provide those values at the time of initialization.

    int[] testScores = new int[] { 85, 96, 80, 98, 89, 70, 93, 84, 66, 96 };

With this above form, the compiler determines the number of items that will be added and generates the full syntax seen in the preceding examples.

{% endif %}

> #### Info::Initializing an Array of Unknown Size
>
> While it is required to define the size of the array when you initialize it, you do not need to _know the size_. As long as the size can fit inside of an integer you can initialize an array using an integer variable.
>
> ```
> int someRandomNumber = 42;
>   
> // ...
>
> int[] testScores = new int[someRandomNumber];
> ```
>
> This code still creates an array and uses whatever value `someRandomNumber` holds at the time of the array initialization.

This is our first example of working with a reference type and in it we're seeing the `new` keyword for the first time. We'll cover it in a later section, but for now, when you see the `new` keyword realize that it is *creating new memory* to hold your data.
