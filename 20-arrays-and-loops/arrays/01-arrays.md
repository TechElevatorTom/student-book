# Arrays

## What Do Arrays Help Solve?

In the previous lession, [Variables and DataTypes](../../05-introduction-to-programming/01-intro.md), you learned how to create variables and assign values to them. Using what you learned in there, your first inclination might be to write the following bit of code to calculate the average test score for a group of students.

```java
int aaronScore = 80;
int beaScore = 98;
int billScore = 89;
int greerScore = 70;
int kyleScore = 93;
int margoScore = 84;
int peteScore = 66;
int zedScore = 96;
double averageScore = (aaronScore + beaScore + billScore + greerScore + 
                    kyleScore + margoScore + peteScore + zedScore) / 8.0;
```

This will work, but it is a bit awkward for a couple of reasons. First, there's an awful lot of typing.  Second, it's difficult to keep everything straight. What if you forgot to include the scores for the two Sams in the class? You can add them to the code easy enough.

```java
int samScore = 85;
int samScore = 96;
.
.
.
double averageScore = (samScore + samScore + aaronScore + beaScore + billScore + greerScore + 
                        kyleScore + margoScore + peteScore + zedScore) / 10.0;
```

However, the second `samScore` causes a "duplicate variable" compiler error. But you're a clever programmer and figure out another way -- change the variable names!

```java
int student01 = 85;    // Sam
int student02 = 96;    // Sam
int student03 = 80;    // Aaron
int student04 = 98;    // Bea
int student05 = 89;    // Bill
int student06 = 70;    // Greer
int student07 = 93;    // Kyle
int student08 = 84;    // Margo
int student09 = 66;    // Pete
int student10 = 96;    // Zed
double averageScore = (student01 + student02 + student03 + student04 + student05 + 
                    student06 + student07 + student08 + student09 + student10) / 10.0;
```

Then again, 10 students sounds like a pretty small class.  A more realistic size is probably 25 ... or more. Changing the variable names doesn't really address the issue of too much typing, or lessen the potential confusion caused by so many variables. Having to add 15 more students only exacerbates the problem. Additionally, it is a bad practice to use sequential numbering for naming variables.

Fortunately, there is an alternative, an array.