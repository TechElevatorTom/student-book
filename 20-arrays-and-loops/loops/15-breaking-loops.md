
### Breaking a Loop

If at any point during the loop execution you need it to end without executing the remaining iterations, the `break` keyword is used. 

Let's write our code to look through our array of test scores to see if it contains a perfect score.

{% if book.language === 'Java' %}

```java
int[] testScores = new int[10];

// ... array is populated

boolean hasPerfectScore = false;

for(int i = 0; i < testScores.length; i++) {
    if (testScores[i] == 100) { 
        hasPerfectScore = true;
        break;      // stop looking for a perfect score if we've found one
    }
}

System.out.println("Contains perfect score " + hasPerfectScore);
```

{% elif book.language === 'C#' %}

```csharp
int[] testScores = new int[10];

// ... array is populated

bool hasPerfectScore = false;

for(int i = 0; i < testScores.Length; i++) 
{
    if (testScores[i] == 100) 
    { 
        hasPerfectScore = true;
        break;      // stop looking for a perfect score if we've found one
    }
}

Console.WriteLine("Contains perfect score " + hasPerfectScore);
```

{% endif %}

Unfortunately there is no crystal ball that tells us if the array contains a perfect score. In order to be sure, we need to look at every single element in the array. If we were to find a perfect score before the end of the array, we would want to stop looking. That is where `break` comes in. 

If we didn't break, then we'd look through the remainder of the array when we have no need to. That's a lot like continuing to look for your keys in every room of your house after you've already found them.

The other keyword often used with loops is `continue`. When used inside of a loop, the code within the loop body is skipped and the iterator executes next.