
### Writing a For Loop

The for loop is defined using the following syntax:

    for (<initializer>; <condition>; <iterator>) {
        <body>
    }

The loop is made up of three very important pieces:

* *initializer* - the statement (or set of statements) that set the initial state. This is executed one time *before* the loop begins.
* *condition* - before running the body of the loop, the expression is evaluated.
* *iterator* - a statement (or set of statements) that execute at the end of each pass through the loop.

The loop continues forever until the condition evaluates to false.

If we wanted to print "Hello World!" five times using a for loop, it would look like this:

{% if book.langauge === 'Java' %}

```java
for(int i = 1; i <= 5; i++) {
    System.out.println("Hello World!");
}

```

{% elif book.language === 'C#' %}

```csharp
for(int i = 1; i <= 5; i++) {
    Console.WriteLine("Hello World!");
}

```

{% endif %}

Here's how the three parts match up:

* *initializer* - `int i = 1;` - we declare a local loop variable that is named `i`. Its initial value is 1 and it is used to track which iteration of the loop we are on. 
* *condition* - `i <= 10` - before running the body of the loop, we check to make sure `i` is less than 10.
* *iterator* - `i++` - after each iteration of the loop, we increment the value of `i` by 1. This syntax is equivalent to `i = i + 1`. The condition is then evaluated again and if it remains true, the body executes.

![A for loop animation run step-by-step](resources/for-loop-execution.gif)

> #### Info::Increment & Decrement Operators
>
> There are many different ways to increment a variable. 
>
> ```
> i = i + 1;  
> i += 1;
> i++;
> ```
>
> On the flip-side, there is a short-hand syntax for decrementing a variable as well.
>
> ```
> i = i - 1;  
> i -= 1;
> i--;
> ```
