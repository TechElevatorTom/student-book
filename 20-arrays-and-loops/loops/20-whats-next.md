## What's Next

We've seen that arrays are pretty helpful for managing collections of data. One of their limitations though is their inabilty to adjust by growing or shrinking their capacity to handle a different number of values. There are ways that we'll see this get easier with the addition of the Collections framework. That said, how would you solve some of the following problems:

* Add a new item to the end of an already full array?
* Insert an item into the middle of an array?
* Remove an item from an array?

