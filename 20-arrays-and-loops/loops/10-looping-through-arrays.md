## Looping Through an Array

If we were to create a for loop that goes through our array of test scores, it might look like this:

{% if book.langauge === 'Java' %}

```java
int[] testScores = new int[10];
// ... array is populated

for(int i = 0; i < testScores.length; i++) {
    int value = testScores[i];   
}
```

{% elif book.language === 'C#' %}

```csharp
int[] testScores = new int[10];
// ... array is populated

for(int i = 0; i < testScores.Length; i++)
{
    int value = testScores[i];   
}

```
{% endif %}


Again looking at the three parts of the loop:

* *initializer* - `int i = 0;` - `i` will be used to track which index of the array we are looking at. Since arrays start at index 0, so do we. 
* *condition* - `i < testScores.length` - before running the body of the loop, we check to make sure `i` is less than the length of the array to avoid an out of range exception.
* *iterator* - `i++` - after we look at an element at `i`, we want to look at the next one `i + 1`.


> #### Info::For Loops
>
> Make sure you practice writing a lot of for loops by hand. Most whiteboard problems involve for loops and you will find that writing code by hand is more challenging than typing it. It's best to prepare and be confident writing some of the fundamental C#/Java syntax.

----

### Calculating the Average Test Score

Using our array of test scores, lets write some code that can be used to loop through the array and calculate the average. We have a few steps that we need to consider:

1. Add up the sum of the scores within the array
1. Divide the sum by the number of scores we're averaging

{% if book.language === 'Java' %}

```java
int[] testScores = new int[10]; // our array
int sum = 0; // the sum of all our scores

for(int i = 0; i < testScores.length; i++) {
    sum = sum + testScores[i];  // add each score to the sum 
}

int average = sum / testScores.length;
```

{% elif book.language === 'C#' %}

```csharp
int[] testScores = new int[10]; // our array
int sum = 0; // the sum of all our scores

for(int i = 0; i < testScores.Length; i++)
{
    sum = sum + testScores[i];  // add each score to the sum 
}

int average = sum / testScores.Length;
```

{% endif %}

In this case we know how big the array is because we still see it. More realistically we will write this kind of code within methods where we didn't explicitly create the array. 

{% if book.language === 'Java' %}

```java
private int getAverage(int[] testScores) {

    int sum = 0; // the sum of all our scores

    for(int i = 0; i < testScores.length; i++) {
        sum = sum + testScores[i];  // add each score to the sum 
    }

    int average = sum / testScores.length;
    
    return average;
}
```

{% elif book.language === 'C#' %}

```csharp
private int GetAverage(int[] testScores) {

    int sum = 0; // the sum of all our scores

    for(int i = 0; i < testScores.Length; i++) {
        sum = sum + testScores[i];  // add each score to the sum 
    }

    int average = sum / testScores.Length;
    
    return average;
}
```

{% endif %}

#### Scope

Notice that we have our `sum` variable declared outside of the `for` loop. If you remember from the previous topic, variables once declared are _in scope._ That variable remains in scope until the end of the block when it is discarded and goes _out of scope_.

Given that we want our `sum` variable to hold its value, we need to declare it in advance of the loop. Any variable declared within the loop is reinitialized with each itearation of the loop.
