## Strings

The first object to discuss is objects from the String class. To create a new String object, use the `"` syntax.

    String myName = "joe";

Earlier, you learned that new objects are created with the `new` keyword, but Strings are special because they are frequently used. So, Java and C# allow you to create new String objects with the `"` literal.

The above code creates a new String object with the contents of `"joe"` on the Heap and assigns its address to the newly declared `myName` variable.

The `myName` variable now acts like a handle on the actual object. You can get to the object's methods by using the dot (.) operator.

{% if book.language === 'Java' %}
    int lengthOfTheString = myName.length();
{% elif book.language === 'C#' %}
    int lengthOfTheString = myName.Length;
{% endif %}

There are other useful String methods to discuss.

{% if book.language === 'Java' %}

### length()

The length returns how many characters are in the String.

```
String name = "joe";
int lengthOfName = name.length(); // Will return 3
```

### substring()

The `substring()` method gets a small subset of an existing String. The parameters are:

1. The index to start the substring
2. An optional index to end the substring. If not given, it goes to the end of the String

```
String fullName = "Java Jones";
String firstName = fullName.substring(0, 4); // <- Will equal "Java"
String lastName = fullName.substring(5); // <- Will equal "Jones"
```

### contains()

The `contains()` method sees if a String contains another String. The parameters are:

1. The String to search for

```
String fullName = "Java Jones";
boolean hasJavaInIt = fullName.contains("Java"); // Will equal true
```

### startsWith() and endsWith()

The `startsWith()` and `endsWith()` methods work like the `contains()` method, but they only check to see if the String starts or ends with the given parameter.

```
String fullName = "Java Jones";
boolean startsWithJava = fullName.startsWith("Java"); // Will equal true
```

### indexOf()

The `indexOf()` method looks through a String, finds the first occurrence of another String, and returns back at what index the String was found.

```
            // 0123456789
String name = "Java Jones";
int firstJFound = name.indexOf("J"); // Will return 0
int firstLetterOfJones = name.indexOf("Jones"); // Will return 5
```

### replace()

The `replace()` method returns a new String with the specified replacements done.

```
String name = "Java Jones";
String nameWithReplacements = name.replace("Java", "Justin");
    // nameWithReplacements will equal "Justin Jones"
    // name will still equal "Java Jones"
```

> #### Warning::Immutability
>
> Strings are immutable objects. Immutable means that it is impossible to change its value once it's created. Using the `replace()` method always generates a new string.

### equals() and equalsIgnoreCase()

These two methods compare the current String object to a String object parameter and return true or false. `equals()` returns true if the characters are exactly the same; `equalsIgnoreCase()` ignores whether a letter is upper or lower case.

```
String name = "Joe";
String otherName = "joe";
boolean exactlyTheSame = name.equals(otherName); // Will be false
boolean closeToTheSame = name.equalsIgnoreCase(otherName); // Will be true
```

### split() and String.join()

These methods turn a String into an array of Strings by splitting on a character in the String and turn an array of Strings back into a single String.

Both of these methods work off of a delimiter, which is a character or set of characters that mark off different sections of a String.

```
String fullString = "Joe;Mark;Josh;Craig";
String[] separateNames = fullString.split(";");
    // separateNames will contain ["Joe", "Mark", "Josh", "Craig"]
String togetherNames = String.join(",", separateNames);
    // togetherNames will equal "Joe,Mark,Josh,Craig"
```







{% elif book.language === 'C#' %}

### Length

The length returns how many characters are in the String.

```
string name = "han";
int lengthOfName = name.Length; // Will return 3
```

### Substring()

The `Substring()` gets a small subset of an existing String. The parameters are:

1. The index to start the substring
2. An optional length of the string to retrieve. If not given, goes to the end of the String

```
string fullName = "Bill Brown";
string firstName = fullName.Substring(0, 4); // <- Will equal "Bill"
string lastName = fullName.Substring(5); // <- Will equal "Brown"
```

### Contains()

The `Contains()` method sees if a string contains another string. The parameters are:

1. The String to search for

```
string fullName = "Bill Brown";
bool hasBillInIt = fullName.Contains("Bill"); // Will equal true
```

### StartsWith() and EndsWith()

The `StartsWith()` and `EndsWith()` methods work like the `Contains()` method, but they only check to see if the String starts or ends with the given parameter.

```
string fullName = "Bill Brown";
bool startsWithBill = fullName.StartsWith("Bill"); // Will equal true
```

### IndexOf()

The `IndexOf()` method looks through a String, finds the first occurrence of another String, and returns back at what index the String was found. If nothing is found, it returns -1.

```
            // 0123456789
string name = "Bill Brown";
int firstBFound = name.IndexOf("B"); // Will return 0
int firstLetterOfBrown = name.IndexOf("Brown"); // Will return 5
```

### Replace()

The `Replace()` method returns a new String with the specified replacements done.

```
string name = "Bill Brown";
String nameWithReplacements = name.Replace("Bill", "William");
    // nameWithReplacements will equal "William Brown"
    // name will still equal "Bill Brown"
```

> #### Warning::Immutability
>
> Strings are immutable objects. It is impossible to change its value once its created. Using the `Replace()` method always generates a new string.

### ToLower() and ToUpper()

These two methods returns a new String with the contents lower or upper-cased.

```
string name = "jOe";
string lowered = name.ToLower(name); // Will be "joe"
string uppered = name.ToUpper(name); // Will be "JOE"
```

### Split() and String.Join()

These methods allow you to turn a String into an array of Strings by splitting on a character in the String and turn an array of Strings back into a single String.

Both of these methods work off of a delimiter, which is a character or set of characters that mark off different sections of a String.

```
string fullString = "Joe;Mark;Josh;Craig";
string[] separateNames = fullString.split(";");
    // separateNames will contain ["Joe", "Mark", "Josh", "Craig"]
string togetherNames = String.Join(",", separateNames);
    // togetherNames will equal "Joe,Mark,Josh,Craig"
```

{% endif %}
