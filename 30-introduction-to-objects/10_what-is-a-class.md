### What is a Class?

So, how do you make an object? Technically, you don't write objects in your code. Objects only exist when your code is running because an object is an in-memory data structure. To make objects, you must write classes.

A class is a grouping of variables and methods in a source code file that generates objects. It's important to know what a class is before you begin writing classes and using objects.

A class is to an object like a blueprint is to a house. A class defines what an object will be like once the object is created.

![Blueprints and Houses](resources/blueprint-house.png)

Imagine a blueprint that defines where the bathtub will be. You can't take a shower in that tub because it's a drawing on a piece of paper. But if you build a house out of that blueprint, you can use the tub whenever you want.

In fact, you can build as many houses as you want from that blueprint, and each will have its own shower. It's important that each house has its own bathtub and not share the same one between them.

The same is true for objects. Imagine that you have a class in your code that models a Person. This Person class has a variable to hold a first name and a last name.

```
class Person {
    String firstName;
    String lastName;
}
```

That's your blueprint. You've created a class that defines two variables in it. But it's not an object just yet; it's a blueprint for one. To create an object from it, there are a couple of steps to complete.