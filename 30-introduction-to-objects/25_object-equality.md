### Object Equality

Typically, when you wanted to see if two variables are equal, you used the `==` operator. However, `==` works on the *value* of the variable. It behaves differently depending upon whether or not the variable is a value type or a reference type.

For value types, `==` checks to see if the two values stored in the variables are the same.

For reference types, `==` checks to see if the two variables hold the same address. This may not be what you want. In fact, it's often not what you want at all.

{% if book.language === 'Java' %}

Below are some examples using strings, which are objects, and by extension, reference types in Java.

    String lowerCaseName = new String("java");
    String anotherName = lowerCaseName;
    lowerCaseName == anotherName;

Does `lowerCaseName == anotherName` return true? Yes, it does. Both of those variables hold the same address, so `==` returns true here. What about the example below?

    String lowerCaseName = new String("java");
    String anotherName = new String("java");
    lowerCaseName == anotherName;

Now does `lowerCaseName == anotherName` return true? The two objects contain the same word *but they aren't the same object*. So `==` returns false here. Remember, when working with objects, `==` compares the addresses of the objects, and these two variables point to two different objects in memory.

If you don't want to compare addresses but want to compare the *contents* of two objects, you need to use something other than `==`. Thankfully, Java has a standard method that lets you do that. If you want to compare two objects to see if they are equivalent, use the `.equals()` method of the object.

    String lowerCaseName = new String("java");
    String anotherName = new String("java");
    lowerCaseName.equals(anotherName);

`.equals()` asks if these two objects are similar. For objects, this is a kind of equivalence you want to check.

{% elif book.language === 'C#' %}

Below are some examples using strings, which are objects, and by extension, reference types.

    int[] array = new int[] {1, 3, 5, 7};
    int[] anotherArray = new int[] {1, 3, 5, 7};
    array == anotherArray;

Now does `array == anotherArray` return true? You can see that the two objects contain the same numbers *but they aren't the same object*! So `==` returns false here. Remember, when working with objects, `==` compares the addresses of the objects, and these two variables point to two different objects in memory.

The code below shows how strings deal with object equality.

    string lowerCaseName = "c#";
    string anotherName = "c#";
    lowerCaseName == anotherName;

How about now? Does `lowerCaseName == anothername` return true? It does this time. The developers of the .NET framework were kind enough to create what is referred to as _overload the operator_. The `==` takes on a different behavior when used between two string references. Instead of comparing their memory addresses, it compares the value of the strings at those addresses.

For all other objects, if you don't want to compare addresses but want to compare the *contents* of two objects, you need to use something other than `==`. Thankfully, C# has standard method that lets you do that. If you want to compare two objects to see if they are equivalent, use the `.Equals()` method of the object. It requires a little work on your side and won't do what you want it to out-of-the-box for arrays. This is discussed in future chapters.

{% endif %}



> #### Note::Ask the Right Equivalence
>
> For value types (primitives), use the `==` operator to check if two things are equal.
>
{% if book.language === 'Java' %}
> For objects, use the `.equals()` operator to check if two things are equal.
{% elif book.language === 'C#' %}
> For objects, use the `.Equals()` operator to check if two things are equal.
{% endif %}
