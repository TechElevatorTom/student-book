# Introduction to Objects Objectives

In this chapter, you'll learn:

* What is an object in programming?
* What's the difference between objects and classes?
* How are objects created and used?
* How are objects stored in the computer and how are objects different from primitives?
* What is the String class and how do we use objects from it?