## Objects in Memory

One important thing to understand about objects is how they are stored in memory. Usually, you wouldn't need to dive down into the hardware and see *how* things are working, but for objects, especially in Java, this can be really important.

The next topic of discussion is the Stack.

### The Stack

When an application runs, there is a memory space set up for it called the Stack. **The Stack** is a set amount of memory that stores some of the basic values. When you create a new `int`, the system knows to reserve exactly 4 bytes (32 bits) of space for it. So if you say:

    int a = 0;

Then the system creates a 4 byte spot in the Stack, calls it `a`, and puts the value of 0 in it.

If you call:

    double price = 5.23;

Then the system creates an 8 byte spot in the Stack, calls it `price`, and puts 5.23 in it. This happens on the Stack because you know the exact size of the variables.

For all primitive values, this works great. You know exactly what size primitive values are, or can be, so these can be stored on the Stack all day long. But what about this:

    int[] numbers;

How big can `numbers` be? You know `int`s are 4 bytes each, but now many `int`s are the in `numbers`? You don't know. So, arrays can't be stored on the Stack.  You have to put them in a different, more dynamic memory structure. That structure is called the Heap.

### The Heap

**The Heap** is the memory space where arrays and objects go. 

In the above example, you declared a variable to hold an `int[]`. This variable is still created in the Stack, but the array itself is created in the Heap.

    int[] numbers = new int[10];

How do you know it's in the Heap? Because of the `new` operator. That's the operator that reserves a new hunk of memory on the Heap.

So, the array is created on the Heap and then the *memory address* of it is stored in the variable `numbers` on the Stack. When you use the variable `numbers` in your code, it holds a memory address only. Then, you have to tell it to go over to the array in the Heap using either the dot operator (`.`) or brackets (`[]`).

So all variables live on the Stack, but their values might not.

### So what?

Data types that keep their values on the Stack, like `int` and `double`, are called primitives or value types.

Data types that keep their values on the Heap, like arrays and objects, are called reference types.

So what does this mean for you? Take a look at the following lines of code:

    int a = 10;
    int b = a;
    b = 4;

What value is stored in `a` after you run that last line of code? If you understand that these variables hold values, you should be able to figure out that `a` is still `10`. You set `a` to `10`. You then set `b` to the same value that's in `a`. So now, `b` is `10`. Then you set `b` to `4`. The value of `a` doesn't change and is still `10`.

Take a look at the code below:

    int[] a = new int[] {10, 20, 30};
    int[] b = a;
    b[0] = 100;

Now, what's the value stored in `a`?

Would you believe `[100, 20, 30]`?

To understand this, you need to understand how `=` really works.

> #### Warning::The `=` works on the value
>
> The `=` takes the value from one variable and puts it into another variable.
>
> For reference variables, the value stored in the variable is a memory address and that memory address transfers to the new variable.

In the block of code above, you declare a new variable called `a` that contains the address to an `int[]`. You then create a new `int[]` on the Heap and put its address into `a`.

Next, you put that same address from `a` into the declared variable `b`. These two variables now point to the exact same `int[]` in the Heap. The `=` didn't copy the array; it just took the address from `a` and put it into `b`.

That last line does something interesting. When your language sees a dot (`.`) or a bracket (`[`) - also called dereference operators - it takes a look at the address in the variable. Then, it jumps over that location in the Heap and goes to the array or object that's stored over there. So, `b[0]` jumps the code over to the Heap and looks at the first element in the array over there. You then say `b[0] = 100` which sets that element's value to `100`.

So, did the array that `a` points to change? Yes, it did. Because `a` and `b` are now pointing to the same array in the Heap. A change using one variable shows up in the other variable too.

It's important to remember if you are using a type that is pass by value (primitives) or a type that is pass by reference (arrays and objects) in your code.