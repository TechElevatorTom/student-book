## What is an Object?

### Layers of Abstraction

When writing programs, you already know that you need to break your solutions down into step-by-step actions to solve problems. If you need to find the largest number in an array, you might write something like this:

```
int largestElement = 0;
for(int i = 0; i < arrayOfNumbers.length; i++) {
    if(arrayOfNumbers[i] > largestElement) {
        largestElement = arrayOfNumbers[i];
    }
}
// largestElement will have the largest number from the array
```

You have looked at problems like this, as well as methods that let you encapsulate that code into something reusable:

```
public int getLargestElement(int[] arrayOfNumbers) {
    int largestElement = 0;
    for(int i = 0; i < arrayOfNumbers.length; i++) {
        if(arrayOfNumbers[i] > largestElement) {
            largestElement = arrayOfNumbers[i];
        }
    }
    return largestElement;
}

int largestFromFirst = getLargestElement(new int[] {1, 2, 3, 4, 5});
// largestFromFirst == 5
int largestFromSecond = getLargestElement(new int[] {300, 240, 550, 129});
// largestFromSecond == 550
```

Grouping this code into a method makes it possible to reuse it rather than rewriting it. This kind of structure makes code easier to work with and saves time.

It also means that you can work at a higher layer of abstraction. Putting the algorithm for finding the largest number inside of a method helps to solve more complex problems with less and more obvious code:

```
public boolean isLargestElementEven(int[] arrayOfNumbers) {
    return getLargestElement(arrayOfNumbers) % 2 == 0;
}
```

The question that remains now is: if methods are good, are more methods better?

### Objects

Object-Oriented Programming is about organizing your problem-solving into objects. An object is an in-memory data structure that combines state and behavior into a usable and useful abstraction. In other words, objects are a collection of variables and methods that make your job easier.

Objects make your job easier in several ways:

1. They're modular

   Breaking your programming into objects allows you to section off your code into pieces. You can treat these pieces as a mini-program that you can test and write separately from other parts of your application. This makes it easier to think about and plan each part of the whole.

2. They hide information

   Most people frown upon hidden information, but in programming, it's a good thing. Objects are used through their methods and other parts of the program that use the object don't know, or want to know, how the object's methods are actually coded. It doesn't matter how `getLargestNumber()` is implemented; what matters is that it produces the largest number. In fact, you might find a much faster algorithm to solve `getLargestNumber()` and can replace all the code in it whenever you want. As long as it takes an `int[]` and returns a `int`, you can change the code within that method all day long.

3. They make it easy to reuse code

   If you build your code using objects, reusing that code becomes easy - simply create another object. Because objects are build to be modular and handle just one small function of your application, that makes them very useful for many different purposes.

4. They are pluggable and testable

   Because the objects are accessible via very defined methods, testing the object is just a matter of calling those methods and making sure they're doing what you expect them to do. It is also easier to swap out one object for another in your application. To do so, write another object that has the same method signatures. Then, you have a drop-in replacement for what was already there.

For these reasons and more, programming with objects - commonly called Object-Oriented Programming - is a smart idea when building a complex program in Java or C#.
