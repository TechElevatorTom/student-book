### Creating Objects

Creating a new object in your code goes through three expressions. They are usually, but not always, contained in the same statement of code.

1. Declaration - A declaration associates a new variable name with a defined data type. You've seen this before when working with variables.
2. Instantiation - Using `new` creates a new object in-memory and returns the address to it. You've seen `new` with arrays before, and here it's doing the same thing - making a new object for you to use.
3. Initialization -  Using a constructor sets some initial values on the created object's instance variables. Essentially, it starts an object off with a certain state.

The example below shows these three expressions at work:

```
                            // Bedrooms, Bathrooms, Siding
House houseAt443WinstonSt = new House(3, 2.5, "Cornflower Blue");
```

This line creates a new House object and puts a reference to that new in-memory object into the variable `houseAt443WinstonSt`. The section below summarizes each piece:

**Declaration**
: `House houseAt443WinstonSt` is the declaration.  It creates a new variable called `houseAt443WinstonSt` that holds a `House` object. The data type of the `houseAt443WinstonSt` variable is `House`.

**Instantiation**
: `new House()` is the instantiation. This is where you tell the runtime that you want it to create a new object of type `House`. That creates a new object in heap memory from the class `House` and then returns the address of that memory location to save it in a variable.

**Initialization**
: `House(3, 2.5, "Cornflower Blue")` is the initialization. This starts the object off with some initial values in its instance variables. This creates the object, sets all of its values, and gets it ready for use right away.

These are three expressions that make up one very common statement - the creation and initialization of a new object and putting it into a variable where it can be accessed.