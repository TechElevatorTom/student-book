# Anatomy of a URL

URL stands for Uniform Resource Locator, or in other words, the web address of an online resource, i.e. a web site or document. A basic URL is broken down into the following parts.

## URL Basic Parts

![URL Basic Parts](img/basic_url_parts.png)

* The **protocol** determines how your browser should communicate with a web server. The two most common protocols are HTTP & HTTPS. 
    * HTTP stands for Hypertext Transfer Protocol
    * HTTPS stands for Hypertext Transfer Protocol Secure. 
* A **sub-domain** is a sub-division of the main domain name. For example, blog.techelevator.com and book.techelevator.com are sub-domains of the the domain name techelevator.com.
* A **domain name** is a unique reference that identifies a web site on the internet, for example techelevator.com. 
* The **TLD (Top Level Domain)** refers to the last part of the domain such as .net, .com, .gov, etc.

## URL Advanced Parts

A more advanced url might look like this and contain a few more parts. 

![URL Basic Parts](img/advanced_url_parts.png)

* A **Port** number is rarely visible in URLs but always required. When declared in a URL it comes right after the TLD, separated by a colon. When it's not declared and in most cases where the protocol is http, port 80 is used. For https (secure) requests port 443 is used.
* The **Path** typically refers to a file or directory on the web server, e.g. /directory/index.html.
* The **Query String Parameters**  is commonly found in the URL of dynamic pages (ones which are generated from database or user-generated content) and is represented by a question mark followed by one or more parameters. For example the full url to search for Tech Elevator on Google is https://www.google.com/search?source=hp&q=tech+elevator but the query is just `?source=hp&q=tech+elevator`
* A **Fragment** is an internal page reference, sometimes called a named anchor. 


## Common Network Protocols

You learned that the two most common networking protocols are HTTP & HTTPS. There are actually a lot of protocols that we use for a variety of applications. It is important to note that you won't use your browser for all of these protocols. For instance if you are using SMTP or POP for mail you might be using email software like Microsoft Outlook. Below is a list of common networking protocols, their default port, and what they are used for.


| Protocol  | Port  | Description |
|-----------|-------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| HTTP      | 80    | Hypertext Transfer Protocol (HTTP) is the underlying protocol used by the internet and defines how messages are formatted, transmitted, and what actions servers and browsers should take in response to various commands.|
| HTTPS     | 443   | Hypertext Transfer Protocol Secure (HTTPS) is the secure version of HTTP. |
| FTP       | 20/21 | FTP can be defined as a standard network protocol which is especially used to transfer files from one host (machine/ operating system) to another host over a TCP/ IP based network. | 
| SSH       | 22    | SSH allows for remote command-line login and remote execution. | 
| Telnet    | 23    | Telnet is the primary method used to manage network devices at the command level. Unlike SSH, Telnet does not provide a secure connection, but it provides a basic unsecured connection.|
| SMTP      | 25    | SMTP is used for two primary functions. It is used to transfer email from source to destination between mail servers and it is used to transfer email from end users to a mail system.|
| SMTPS     | 465   | Secure version of SMTP |
| DNS       | 53    | Domain name system is used to convert the domain name to IP address. There are root servers, TLDs and authoritative servers in the DNS hierarchy.|
| POP3      | 110   | The Post Office Protocol is one of the two main protocols used to retrieve mail from the internet. It is very simple as it allows the client to retrieve complete content from the server mail box and deletes contents from the server.|
| POP3      | 995   | Secure version of POP3 |
| IMAP      | 143   | Internet Message Access Protocol (IMAP) is another main protocol that used to retrieve mail from a server. IMAP does not delete the content from the mail box of the server.|
| IMAP      | 993   | Secure version of IMAP |
