# Request & Response

In the previous lesson you learned about the basics of a request and a response. When you type an address in your browser you will request a document from the web server through HTTP. The web server software will locate that document (or return a 404 if not found) and then return it to you, also through HTTP.

![Web Server](img/web-server.svg)

## HTTP & HTTPS

When you visit a website using the HTTP protocol you are using it over port 80. Remember from an earlier lesson that even though you don't see the port in the Url it is there. When you visit a website over HTTPS you are using port 443 even though it isn't explicitly defined in the Url.

### HTTP

Hypertext Transfer Protocol (HTTP) is an application-layer protocol for transmitting hypermedia documents, such as HTML. It was designed for communication between web browsers and web servers, but it can also be used for other purposes. HTTP follows a classical client-server model, with a client opening a connection to make a request, then waiting until it receives a response. HTTP is a stateless protocol, meaning that the server does not keep any data (state) between two requests. Though often based on a TCP/IP layer, it can be used on any reliable transport layer; that is, a protocol that doesn't lose messages silently, such as UDP.

### HTTPS

HTTPS (HTTP Secure) is an encrypted version of the HTTP protocol. It usually uses SSL or TLS to encrypt all communication between a client and a server. This secure connection allows clients to safely exchange sensitive data with a server, for example for banking activities or online shopping.

## HTTP Request

If you open up your browsers Devtools > Network and then visit https://www.techelevator.com you will see the following information under Request. 

![Request Headers](img/request_headers.png)

These are the key parts to the request.

### Http Methods

HTTP defines a set of request methods to indicate the desired action to be performed for a given resource. Although they can also be nouns, these request methods are sometimes referred as HTTP verbs. 

| Method  | Definition                                                                                                                                |
|---------|-------------------------------------------------------------------------------------------------------------------------------------------|
| POST    | The POST method is used to submit an entity to the specified resource, often causing a change in state or side effects on the server.     |
| GET     | The GET method requests a representation of the specified resource. Requests using GET should only retrieve data.                         |
| PUT     | The PUT method replaces all current representations of the target resource with the request payload.                                      |
| PATCH   | The PATCH method is used to apply partial modifications to a resource.                                                                    | 
| DELETE  | The DELETE method deletes the specified resource.                                                                                         |

When you type a URL into your browser window, it executes an HTTP GET. By convention, GET is a safe method, because the server takes no further action other than simply returning the requested information.

### Path

The path for the current request is `/` which is the root of the application. You will notice that there is no specific document being requested (e.g. index.html, home.html, etc...) so how does the server handle this? It is now up to the web or application servers software to determine what the default document is and serve that up. If the default document is `index.html` then the following paths are identical: 

* /
* /index.html

If you were to click on the navigation button Learn to Code you will see the path defined as `/learn-to-code`. Again it is up to the server to define the default document. In application servers you might have a list of default documents and order becomes important. In the following example the server will first look to find a Java Server Page (.jsp) if it can't find one it will default to `index.html`

* index.jsp
* index.html


## HTTP Response

After the server processes your request it will send back a response. The response contains some key elements that are covered below. If you open up your browsers Devtools > Network and then visit https://www.techelevator.com you will see the following information under Response. 

![Response Headers](img/response_headers.png)

### HTTP Response Status Codes

HTTP response status codes indicate whether a specific HTTP request has been successfully completed. Responses are grouped in five classes: informational responses, successful responses, redirects, client errors, and servers errors. 

#### Status Code Categories

Status codes are broken down into different categories. In the table below we have labeled a code as 200.x. This means that any status code that falls in the 200 range is a success message. This helps because even if you don't know what that specific message is, you know what type of response it is. 

| Status Code | Description               |
|-------------|---------------------------|
| 100.x       | Information Responses     |
| 200.x       | Successful Responses      |
| 300.x       | Redirection Messages      |
| 400.x       | Client Error Responses    |
| 500.x       | Server Error Responses    | 


Below is a list of common status codes and descriptions from each category. 

| Code                     | Description                                                                                                                                              |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| 100 Continue             | This interim response indicates that everything so far is OK and that the client should continue with the request or ignore it if it is already finished.|
| 200 OK                   | The request has succeeded. The information returned with the response is dependent on the method used in the request.                                    |
| 201 Created              | The request has succeeded and a new resource has been created as a result of it. This is typically the response sent after a POST request, or after some PUT requests. |
| 202 Accepted             | The request has been received but not yet acted upon. It is non-committal, meaning that there is no way in HTTP to later send an asynchronous response indicating the outcome of processing the request. It is intended for cases where another process or server handles the request, or for batch processing.  |
| 204 No Content           | There is no content to send for this request, but the headers may be useful. The user-agent may update its cached headers for this resource with the new ones. |
| 301 Moved Permanently    | This and all future requests should be directed to the given URI.                                                                                        |
| 304 Not Modified         | Indicates the resource has not been modified since last requested. Typically, the HTTP client provides a header like the If-Modified-Since header to provide a time against which to compare. Using this saves bandwidth and reprocessing on both the server and client, as only the header data must be sent and received in comparison to the entirety of the page being re-processed by the server, then sent again using more bandwidth of the server and client.|
| 400 Bad Request          | The request cannot be fulfilled due to bad syntax.                                                                                                       |
| 401 Unauthorized         | Similar to 403 Forbidden, but specifically for use when authentication is possible but has failed or not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authentication and Digest access authentication. |
| 403 Forbidden            | The request was a legal request, but the server is refusing to respond to it. Unlike a 401 Unauthorized response, authenticating will make no difference.|
| 404 Not Found            | The requested resource could not be found but may be available again in the future. Subsequent requests by the client are permissible.|
| 409 Conflict             | Indicates that the request could not be processed because of conflict in the request, such as an edit conflict. |
| 500 Internal Server Error     | The request cannot be fulfilled due to bad syntax.                                                                                           |
| 503 Service Unavailable       | The server is currently unavailable (because it is overloaded or down for maintenance). Generally, this is a temporary state.                |


It needs to be repeated, you're never going to memorize all of the status codes. Just knowing what each category (200.x,400.x,etc) is really going to help you understand what is going on with the request.


### Content Type

In responses, a Content-Type header tells the client what the content type of the returned content actually is. This can be any resource such as an HTML Document, image or any other type of resource. In the case of this response the header contained the following

```bash
content-type: text/html; charset=UTF-8
```

This response is returning an HTML document with a character set of UTF-8. To display an HTML page correctly, a web browser must know which character set (character encoding) to use.



