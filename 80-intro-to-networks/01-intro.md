# Introduction to Networks

An important concept of software development is having a fundamental understanding of how the internet works. If someone shows you a long url can you identify the different parts of that url? If someone asked you to explain what happens when you type in a url to your browser, would you be able to? These are the questions you are going to be able to answer when you finish this lesson.  

In this lesson you are going to learn

* Anatomy of a Url
    * The basic and advanced parts of a url
* What happens when you visit a URL
    * Domain Name
    * IP Addresses
        * IPv4
        * IPv6
        * Domain name to IP Address
        * Checking your local IP Address
    * Domain Name Servers
        * What is DNS
        * Registering a domain name
* Servers
    * Web Servers
    * Application Servers
    * Static vs Dynamic
* Request & Response
    * HTTP & HTTPS 
    * HTTP Request
    * HTTP Response
* Hosting
    * Self Host
    * Static Hosting
    * Shared Hosting Provider
    * Dedicated Host
    * Cloud Hosting