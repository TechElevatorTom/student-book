# Hosting Providers

In this section you will learn about the different types of hosting providers as well as some recommendations for each. If you remember from the servers section you learned about static vs dynamic hosting. You need to have a very clear answer of what type of application you are building before selecting a hosting provider.

![Website Hosting Providers](img/website-hosting.png)

## Self Host

Self-hosting is when you are hosting a website on a computer that you own. This could be a personal laptop, a computer with a little more power sitting in your closet or your own server sitting in a data center somewhere close.

This could be a static or dynamic website but it will be up to you to provision everything from networking to web server software. This is less popular today because of the inexpensive or even free at times hosting options out there. If you like to learn and tinker with things this is a great way to understand everything that goes into hosting a website.

## Shared Hosting Provider

Shared hosting refers to a service where many websites reside on the same web server. If you have ever had roommates you will know that sharing the cost of living with others can reduce your costs and is a great place to start. You aren't stuck there forever and when the time comes you can move out and get your own place. 

You can think of shared hosting in the same manner. While there are some pros like cost & availability there are some cons to shared hosting. For instance, if there are 100 customers on one shared hosting service and one customers website or application causes the server to crash then all of the customers websites will go down. 

It's up to you to weigh the pros and cons to decide if this is the right choice for you.

**Shared Hosting Providers**

* [bluehost](https://www.bluehost.com/)
* [HostGator](https://www.hostgator.com/)
* [DreamHost](https://www.dreamhost.com/)


## Dedicated Host

Shared hosting isa great place to start but as your website needs begin to grow moving to a dedicated host can be a great solution. A dedicated host means that you get an entire server to yourself. There's no sharing of CPU, RAM or bandwidth, which means your website stays responsive at all times.

Just like each of the providers you have seen so far there are some pros and cons. The pro is you no longer have to worry about other customers bringing down your website for undefined amounts of time. The con of course is having your own dedicated server will cost more. 

The other thing you need to think about with dedicated servers is that it is up to you to manage the server. If you need something installed or configured it's on you. Usually this happens through ssh on a linux box so being familiar with that is important. It is also up to you to perform any operating system and security updates. Some people enjoy the fact that they get to control every aspect of this hosting and some people like to be hands off, it's up to you to decide your preference.

**Dedicated Server Hosting Providers**

* [Linode](https://www.linode.com/)
* [Digital Ocean](https://www.linode.com/)
* [WebFaction](https://www.webfaction.com/)

## Cloud Host

There is a bit of a crossover here because many of the dedicated server hosting providers are moving their services to the cloud. Cloud hosting services provide hosting on virtual servers instead of an actual tangible server. 

It use to be that there were data centers and if you wanted your own server you would customize your own personal server and someone at the data center would install and provision this server for you. This was not only very expensive but very time consuming. 

In the world, virtual servers can be spun up in minutes. When you decide that you don't want it any longer, it can be disposed of. No more large investments or long contracts. If you want a new application out there right now, click a button.

**Cloud Hosting**

* [Amazon Web Services](https://aws.amazon.com/)
* [Google Cloud](https://cloud.google.com/)
* [Microsoft Azure](https://azure.microsoft.com/en-us/)


## Static Hosting

If your website is nothing more than static pages you will have some really great options for hosting. The pricing at this level is really inexpensive and in some cases even free. 

You learned that static websites are nothing more than HTML/CSS/JavaScript. It is important to understand that this is what the final output of your website needs to be but this doesn't mean if you have 10 pages you need to write 10 different HTML pages. 

Static Site Generators (SSGs) are becoming more and more popular and it allows you to create some pretty awesome static websites using frameworks like React & Vue.

**Static Hosting Providers**

* [Netlify](https://www.netlify.com/)
* [Amazon Web Services (AWS)](https://docs.aws.amazon.com/AmazonS3/latest/dev/WebsiteHosting.html)
* [Google Cloud](https://cloud.google.com/storage/docs/hosting-static-website)

