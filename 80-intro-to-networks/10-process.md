# What happens when you visit a URL. 

In this lesson we are going to discuss what happens when you type in a URL in your web browser. You might not realize it but there is a lot happening behind the scenes and its important to understand what is going on. If you understand the details of what is happening in the background it becomes much easier to debug issues. This is something that might come up in technical interviews so please revisit this when preparing for an interview.

## Domain Name

In the previous lesson you learned the different parts of a url and what a domain name is. When you type in a domain name (techelevator.com) in the browser, the first thing that your computer does is send a query to the DNS system and get back the host IP Address. 

![Domain Name](img/domain_name_te.png)

An IP Address will uniquely identify each computer on the internet. IP Addresses are hard to remember and in the case of the new format (IPv6) they are impossible, so we link a domain name to an IP Address for ease of use. 

## IP Address

**IP** stands for Internet Protocol and **Address** refers to the unique location of a computer on the internet. There are two standards for IP address

* IP Version 4 (IPv4)
* IP Version 6 (IPv6)

All computers with IP addresses have an IPv4 address and many are using the IPv6 address system as well. 

* IPv4 is 32 bits allowing for 4,294,967,296 addresses.
    * Sample IPv4 Address: 192.168.51.81
* IPv6 is 128 bits allowing for 3.4 * 10<sup>38</sup> addresses. </div>
    * Sample IPv6 Address: fe80:0000:0000:0000:c0b5:30b7:3fd7:e2a4 Two colons (::) represent consecutive sets of zeroes: fe80::c0b5:30b7:3fd7:e2a4

![IP Address Formats](img/ip_addresses.png)

The previous version, IPv4, uses a 32-bit addressing scheme to support 4.3 billion devices, which was thought to be enough. However, the growth of the internet, personal computers, smartphones and now Internet of Things (IoT) proves that the world needed more addresses. 

Fortunately, the Internet Engineering Task Force (IETF) recognized this 20+ years ago. In 1998 it created IPv6, which instead uses 128-bit addressing to support approximately 340 trillion trillion (or 2 to the 128th power, if you like). Instead of the IPv4 address method of four sets of one- to three-digit numbers, IPv6 uses eight groups of four hexadecimal digits, separated by colons.


### Domain Name -> IP Address

If you wanted to find out the IP Address for a specific website you can do so by using the command prompt. If you're on Windows you can open a Command Prompt and if you're on mac you can open up Terminal If you type in `ping` and the domain name in question you should get an answer back. 

#### Windows

```bash
C:\>ping www.techelevator.com       
         pinging www.techelevator.com [198.185.159.145] with 32 bytes of data:       
         Reply from 198.185.159.145: bytes=32 time=101ms TTL=124       
         Reply from 198.185.159.145: bytes=32 time=100ms TTL=124       
         Reply from 198.185.159.145: bytes=32 time=120ms TTL=124       
         Reply from 198.185.159.145: bytes=32 time=120ms TTL=124
```

#### macOS

```bash
vega ~ $ ping www.techelevator.com
PING ext-sq.squarespace.com (198.185.159.145): 56 data bytes
64 bytes from 198.185.159.145: icmp_seq=0 ttl=252 time=51.515 ms
64 bytes from 198.185.159.145: icmp_seq=1 ttl=252 time=44.944 ms
64 bytes from 198.185.159.145: icmp_seq=2 ttl=252 time=38.120 ms
64 bytes from 198.185.159.145: icmp_seq=3 ttl=252 time=37.953 ms
^C
--- ext-sq.squarespace.com ping statistics ---
4 packets transmitted, 4 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 37.953/43.133/51.515/5.601 ms
```

>On macOS you will need to use ctrl+c to stop pinging the server.

### How do I check my IP Address

You just learned how to figure out what the IP Address is of a website but what about your computer? As you learned earlier every single computer on the internet has an IP Address, not just servers.

#### Windows

If you are on windows you can run the command [ipconfig](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/ipconfig)

```bash
ipconfig
```

#### macOS

If you are on macOS you can run the command [ifconfig](https://ss64.com/osx/ifconfig.html)

``` bash
en0: flags=8863<UP,BROADCAST,SMART,RUNNING,SIMPLEX,MULTICAST> mtu 1500
	ether f0:18:98:39:5f:23
	inet6 fe80::1889:31e0:7d46:cd64%en0 prefixlen 64 secured scopeid 0x6
	inet 192.168.1.14 netmask 0xffffff00 broadcast 192.168.1.255
	nd6 options=201<PERFORMNUD,DAD>
	media: autoselect
	status: active
```

* ether is your IPv6 address
* inet is your IPv4 address


### Localhost

Localhost is the hostname that means this computer. If you were to `ping localhost` from the command line you will find out that the IP Address is `127.0.0.1`. 

```bash
vega ~ $ ping localhost
PING localhost (127.0.0.1): 56 data bytes
64 bytes from 127.0.0.1: icmp_seq=0 ttl=64 time=0.033 ms
64 bytes from 127.0.0.1: icmp_seq=1 ttl=64 time=0.055 ms
64 bytes from 127.0.0.1: icmp_seq=2 ttl=64 time=0.054 ms
64 bytes from 127.0.0.1: icmp_seq=3 ttl=64 time=0.104 ms
^C
--- localhost ping statistics ---
4 packets transmitted, 4 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 0.033/0.061/0.104/0.026 ms
```

 It is used to access the network services that are running on the host via the loopback network interface. This allows you to develop applications using locally so anytime you see localhost know that this means you are on your own computer. You can now purchase this shirt and wear it proudly.

 ![No place like home](img/no_place_like_home.jpg)


## Domain Name Servers (DNS)

Domain Name Servers (DNS) are the Internet's equivalent of a phone book. They maintain a directory of domain names and translate them to Internet Protocol (IP) addresses. This is necessary because, although domain names are easy for people to remember, computers or machines, access websites based on IP addresses. 

You learned this earlier but it's important enough to cover again. Each domain name can point to a single IP Address

* techelevator.com -> 198.185.159.144
* github.com -> 192.30.253.112
* reddit.com -> 151.101.1.140

Each sub-domain can also point to their own IP Address:

```bash
vega ~ $ ping www.techelevator.com
PING ext-sq.squarespace.com (198.185.159.145): 56 data bytes
64 bytes from 198.185.159.145: icmp_seq=0 ttl=252 time=43.730 ms
```

```bash
vega ~ $ ping mail.techelevator.com
PING mail.techelevator.com (198.71.232.3): 56 data bytes
64 bytes from 198.71.232.3: icmp_seq=0 ttl=252 time=44.693 ms
```

```bash
vega ~ $ ping book.techelevator.com
PING book.techelevator.com (198.74.54.82): 56 data bytes
64 bytes from 198.74.54.82: icmp_seq=0 ttl=252 time=46.346 ms
```

So if you want create a new website how do you get a new domain name and configure it? This is where a domain name registrant comes into play.

### Domain Name Registration

If you want a new domain name for your website our application you can **lease** one from a DNS Registrar. You read that correctly, you don't purchase domain names, you lease them. You enter into a contract to own that domain for x number of years. If you fail to renew the domain name someone else can come in lease the domain name.

Earlier you learned about Top Level Domains (TLD). You have undoubtedly come across these before but didn't know that they were called this. You are probably familiar with the well known ones like .com, .net, .gov, .edu, etc but you might be surprised how many there actually are. The Internet currently has 1,535 top-level domains (TLDs) as of February 5, 2019. 

If you want to register a domain name you can use one of the following services. There are many more than just these but this represents a few of the popular ones. 

* [Google](https://domains.google/)
* [GoDaddy](https://www.godaddy.com/)
* [Hover](https://www.hover.com/)

The domain name just gives you the right to the name. As you saw earlier you can have different sub-domains (www.techelevator.com,mail.techelevator.com,book.techelevator.com) point to different IP Addresses. The way that you do this is through managing the DNS records. Once you have leased a domain name you can go into the Domain Name Registrants software and manage your DNS records.

Before you can point your domain name to an application you need somewhere to host it and in the next lesson we will take a look at some terminology and options.

