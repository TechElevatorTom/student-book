# Web Servers vs Application Servers

Now that you understand what happens when you type a domain name into your browser, it's time to look at the missing piece of this puzzle. You learned that you can register a domain name and configure DNS for that domain name but where do you point it to? This is where hosting and hosting providers come into play. 

In this section you will learn about the differences between a web server and application server. From there you will learn what the different types of hosting providers are and some examples of each. After this you should have everything you need to setup your own website.

## Web Servers

A **Web Server** refers to the hardware and software that work together to serve up static content. On the hardware side, a web server is a computer that stores web server software and a website's files (e.g. HTML documents, images, CSS, JavaScript). 

On the software side, a web server includes several parts that control how web users access hosted files, at minimum an HTTP server. An HTTP server is a piece of software that understands URLs (web addresses) and HTTP (the protocol your browser uses to view webpages). It can be accessed through the domain names (like mozilla.org) of websites it stores, and delivers their content to the end-user's device.

When you type in a domain name a DNS lookup is made to detrmine the IP Address of the host. At that time an HTTP Request is sent to the host address. When the request reaches the correct web server (hardware) the HTPP server (software) accepts the request, finds the requested resource and sends it back to the browser, also through HTTP.

![Web Server](img/web-server.svg)


### Static vs Dynamic

To publish a website, you need either a static or a dynamic web server.

A static web server, or stack, consists of a computer (hardware) with an HTTP server (software). We call it "static" because the server sends its hosted files "as-is" to your browser.

A dynamic web server consists of a static web server plus extra software, most commonly an application server and a database. We call it "dynamic" because the application server updates the hosted files before sending them to your browser via the HTTP server.

For example, to produce the final webpages you see in the browser, the application server might fill an HTML template with contents from a database. Sites like MDN or Wikipedia have many thousands of webpages, but they aren't real HTML documents, only a few HTML templates and a giant database. This setup makes it easier and quicker to maintain and deliver the content.

## Application Servers

You just learned about the static vs dynamic websites. If you are going to create a dynamic website like Wikipedia that contains thousands of pages than you will need some additional software and this is why application servers exist. 

From a hardware standpoint they don't need to be any different but usually will be more powerful to handle larger throughput. To serve up Java Server Side Applications you will typical see software like Apache Web Server & Apache Tomcat installed. On the Windows side with .NET you will typically see Internet Information Server (IIS) installed. 

This software allows you, the developer, the ability to build out these dynamic applications. 

