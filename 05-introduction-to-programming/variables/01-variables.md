# Variables

Typical programs, like web applications need the ability to store and manipulate data. For example, many websites have the ability to send a password reset e-mail. Programmers, being an inherently lazy bunch, try and write these applications with as little repetitive code as possible. Building a password reset flow for every single user would be a waste of time. Programmers, in their effort to work less will use a variable to store the user's e-mail address. This allows their code to be written in a generic and reusable manner.

Here are a few points about variables that will be covered in the following sections:
* a variable must identify what type of data it will store
* a variable must be declared before it is used
* variables must have a unique name given when declared