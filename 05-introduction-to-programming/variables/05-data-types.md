# Data Types

Every variable, when created, is defined with a data type. A data type classifies the variable and indicates the type of data that a particular variable can hold. 

In C# and Java there are two different classifications of data types: primitive(java)/value(C#) data type and reference data type. A primitive data type stores a simple value (e.g. `42`, `true`, or `'a'`) that consumes a fixed amount of memory. A reference type allows complex unknown structures of data to be created by the program.

## Common Data Types

| C#        | Range                                      |   | Java      | Range                           |
|-----------|--------------------------------------------|---|-----------|---------------------------------|
| `bool`    | true or false                              |   | `boolean` | true or false                   |
| `byte`    | 0 to 255                                   |   | `byte`    | -127 to 127                     |
| `char`    | U+0000 to U+FFFF ('a', 'b', etc.)          |   | `char`    | \u000 to \uffff ('a', 'b', etc.)|
| `int`     | -2^31 to 2^31                              |   | `int`     | -2^31 to 2^31                   |
| `float`   | -3.4×10^38 to 3.4×10^38                    |   | `float`   | -3.4*10^38 to 3.4*10^38         |
| `double`  | ±5.0 × 10^−324 to ±1.7 × 10^308            |   | `double`  | ±5.0 × 10^−324 to ±1.7 × 10^308 |
| `long`    | -2^63 to 2^63                              |   | `long`     | -2^63 to 2^63                   |
| `decimal` | (-7.9×10^28 to 7.9×1028) / (10^0 to 10^28) |   |           | &nbsp;                          |

Many of these data types deal with similar types of data (integer, boolean, and floating point). Their difference is in the ranges of values they hold. For example:

* `int` and `long` both deal with integers, `long` can hold significantly larger numbers
* `float` and `double` both store decimal points, `double` offers higher precision

> ####Info::Strings
>
> `string` does not fall in the list of primitive types. It falls under the category of a reference type.

For a reference of all C# value types view the [MSDN documentation](https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/keywords/value-types).

For a reference of all Java primitive types view the [Java Docs](https://docs.oracle.com/javase/tutorial/java/nutsandbolts/datatypes.html).
