# Constants

Consider the following program where two variables, days and seconds are declared and used later in an arithmetic expression.

```csharp
int days = 2;
int seconds = 40;

// ....

int numberOfHoursAwake = 24 * days;
int numberOfFrames = 24 * seconds;
```

While 24 is the same number in both statements, they serve completely different meaning. In programming we refer to these as _magic numbers_. A magic number in programming is a unique value whose meaning is based on context.

Above, 24 refers to the number of hours in a day and the number of frames per second. When writing a program, it is strongly recommended to create a constant so that we do not have to infer the meaning of 24. This constant allows us to give the number a name so that it may be referenced throughout our code.

Using that same idea above, consider the following program that uses constants to clearly indicate the meaning of 24 in a block of code.

{% tabs first="C#", second="Java" %}
{% content "first" %}


```csharp
int days = 2;
int seconds = 40;
const int NumberOfHoursPerDay = 24;
const int NumberOfFramesPerSecond = 24;

// ....

int numberOfHoursAwake = NumberOfHoursPerDay * days;
int numberOfFrames = NumberOfFramesPerSecond * seconds;
```

{% content "second" %}


```java
int days = 2;
int seconds = 40;
final int NUMBER_OF_HOURS_PER_DAY = 24;
final int NUMBER_OF_FRAMES_PER_SECOND = 24;

// ....

int numberOfHoursAwake = NUMBER_OF_HOURS_PER_DAY * days;
int numberOfFrames = NUMBER_OF_FRAMES_PER_SECOND * seconds;
```

{% endtabs %}
 