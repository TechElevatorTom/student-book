# Giving Values to Variables

After a variable has been declared, it can be assigned a value. Variables can be assigned three different types of values:
* literal values (e.g. `42`, `true`, or `a`) depending on the data type 
* variable values where the value of one variable is given to the value of another variable
* evaluated values from of an _expression_

The assignment operator `=` is used when giving a value in an assignment statement.

```csharp
int numberOfPeople;             //declare an integer called numberOfPeople
double gallonsOfGas;            //declare a double called gallonsOfGas
bool isRaining;                 //declare a bool(ean) called isRaining
bool isCloudy;


numberOfPeople = 16;            //assign the integer 16 to the numberOfPeople variable
gallonsOfGas = 3.26;            //assign the double 3.26 to the gallonsOfGas variable
isRaining = true;               //assign the boolean true to the isRaining variable
firstLetter = 'a';              //assign the character 'a' to the firstLetter variable

isCloudy = isRaining;           //copy the value from isRaining into the isCloudy variable
```

If the variable appears on the right-hand side of the operator, then its value is being retrieved and used to copy into another variable. 

> #### Hint::Reading Code
>
> When reading code, it might help to read the right-hand side of the assignment statement first. Once you've determined what the value will be used, use the `=` to indicate you are assigning a value to the variable on the left-hand side.


You may often see variables declared _and_ assigned within the same line of code.

```
int numberOfPeople = 16;
double gallonsOfGas = 3.26;
bool isRaining = true;
```

**Remember that there is a difference between declaring and assigning a value**.