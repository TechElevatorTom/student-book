# Naming Variables

> Always give your variables meaningful names.
>
> - _The Golden Rule of Programming_

 Code that has complex variable naming or uses short abbreviations that are difficult to decipher is not well received in the developer community. Even if it's your own code, there isn't a chance you'll remember what you meant when you're under the gun trying to fix an error in production on a codebase you worked on over a year ago.

While variable naming is an art, when it comes to creating names, there are some best practices:
* follow camel-casing conventions: first word is lowercase, subsequent words have the first letter capitalized
* use pronounceable names for variables
* prefer names over single characters
* use names that describe WHAT the variable does not HOW it does it
* avoid creating multiple variables that are variations of the same name, this creates confusion
* with booleans use names that start with is, has, was, etc. and avoid using a double negative

**Good Variable Names**
* `numberOfStudents` for an int
* `averageCostOfGas` for a double
* `isAvailable` for a boolean

**Not Good Variable Names**
* `nbr`
* `name` followed by `name2`, `name3`, etc.
* `numberOfStudents` followed by `number_of_students_`

