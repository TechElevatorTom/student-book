# Declaring a Variable

Before using a variable, it must first be declared. The declaration statement for a variable looks like:

```
data-type identifier;
```

**Example:**

```
int numberOfPeople;             //declare an integer called numberOfPeople
double gallonsOfGas;            //declare a double called gallonsOfGas
bool isRaining;                 //declare a bool(ean) called isRaining
char firstLetter;               //declare a char called firstLetter
```

Once a variable has been declared within a given scope, it cannot be redeclared. The following code would create a compilation error if written.

```
{
    int numberOfPeople;     //legal

    //...

    int numberOfPeople;     //numberOfPeople is already in scope and cannot be declared again
}
```