# An Introduction to Variables & Data Types

By the end of this chapter you should have an understanding of:

* How variables are declared and used in code
* How arithmetic expressions are evaluated in code
* Understand how data types can be converted