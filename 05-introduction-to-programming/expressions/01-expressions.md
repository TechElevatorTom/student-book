# Expressions

Besides assigning values to a variable using literals and other variables, another way is to use an expression. Expressions are statements that get evaluated and produce a single result which can then be used in another statement.

An example that we may be familiar with could be:

```
200 / 10
```

We know that this arithmetic expression yields a single value, 20. In a program it could be used in a similar way.

```
int milesPerGallon = 200 / 10;      //200 / 10 is evaluated to 20 which is then saved into the variable.
```

As we go forward, expressions will take on many forms. For now we will see how arithmetic expressions can be used with variables in programs.