# Arithmetic Expressions

There is usually a need for simple calculations when our programs run. In order to do that, we have access to the following arithmetic operators:

Assume: A is equal to 15 and B is equal to 2.

| Operator | Description                        | Example    |
|----------|------------------------------------|------------|
| `+`      | Adds two operands                  | A + B = 17 |
| `-`      | Subtracts two operands             | A - B = 13 |
| `*`      | Multiplies two operands            | A * B = 30 |
| `/`      | Divides two operands               | A / B = 7  |
| `%`      | Finds the remainder after division | A % B = 1  |

These operators can be combined with `()` to group parts of the expression together.

```csharp
int a = 15;
int b = 2;
int result;

result = a + b;     //expression is evaluated to 17 and copied into result
result = a - b;     //expression is evaluated to 13 and copied into result
result = a * b;     //expression is evaluated to 30 and copied into result
result = a / b;     //expression is evaluated to 7 and copied into result
result = a % b;     //expression is evaluated to 1 and copied into result   

//In the below statement, a is multiplied with b first. The output is divided
//by the current value of the result variable. This final value is assigned
//into the result value. 
result = (a * b) / result;

// Translates to
//  result = (15 * 2) / result;
//      result = 30 / result;
//      result = 30 / 1;
//      result = 30;    
```


