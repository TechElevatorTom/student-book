# Literal Suffixes

Java and C# are pretty smart about figuring out which data type is used during an assignment operation. However there are some scenarios where it gets stuck and isn't sure what to do, normally when you're working with floating point numbers. As an example look below to see the code that doesn't compile.

```csharp
//float f = 3.26;       //won't compile, compiler assumes 3.26 is a double and wants an explicit conversion
float f = 3.26F;        //adding 'F' tells the compiler this is a literal float value
```

For a reference to other literal suffixes used in Java and C# [go here](https://www.codeproject.com/Articles/689009/Numeric-Literals-in-Java-and-in-Csharp).