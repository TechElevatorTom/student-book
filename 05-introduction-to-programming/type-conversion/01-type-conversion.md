# Type Conversion

When a value is moved from one type to another, the compiler becomes interested in what we are doing. It worries about whether or not the operation being performed will result in a loss of data. Each conversion operation is considered in terms of _widening_ (smaller data type to larger data type) and _narrowing_ (larger data type to smaller data type) of value ranges.

As an example, converting from an `int` into a `long` can take place implicitly and requires no additional code. When a value is narrowed (e.g. `double` to `int`) you will be required to explicitly indicate this is your intention.  How to perform type conversion is shown in the below code blocks.

The following code block will not compile because it is missing an explicit type conversion.

```csharp
double calculatedScore = 87.6;
int finalGrade = calculatedScore;
```

The syntax for a type conversion is:

```csharp
datatype identifier = (dataType)valueToConvert;

double calculatedScore = 87.6;
int finalGrade = (int)calculatedScore;
```

> #### Caution::Inferring Data Types
>
> Realistically you'll run into type conversions the most when you're trying to perform arithmetic between two data types. As a rule, the runtime will try its best to be efficient despite what data type you use on the left-hand side of the statement.
>
> ```csharp
> int score1 = 85;
> int score2 = 93;
> int score3 = 75;
> 
> // (score1 + score2 + score3) / 3;    yields the integer 84
> // (score1 + score2 + score3) / 3.0;  yields the double 84.33333
>
> double averageScore = (score1 + score2 + score3) / 3;   //84
> double averageScore = (score1 + score2 + score3) / 3.0; //84.33339
```