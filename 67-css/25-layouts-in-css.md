# Layouts in CSS

It used to be that layouts for HTML pages were handled by the HTML side of web pages. At first, a lot of the layout was done using HTML tables and splitting the content up into different cells on the page. Eventually, layout was handled more by using `<div>`s and then trying to position those `<div>`s properly on the page using floats, alignments, and positioning. Then CSS libraries like Bootstrap came along to try and fix the layout issues in CSS. These became very popular and solved the issue as best they could.

But now there are better tools for handling layouts than ever before in CSS and they are being supported by more and more browsers as they improve. The two main tools that you will be looking at are Flexbox and Grid. Both of these are rather new additions to CSS but are already being adopted by and added to all the major browsers.

The Flexible Box Module, or Flexbox, is designed to help aligning and laying out items. Flexbox is useful for any interface layout that needs to nicely organize a group of like objects, like a Twitter feed, Amazon list of products, or a Pinterest board.

The CSS Grid Layout, or Grid, is designed to allow dividing a page into major regions and then assigning elements to those regions. This removes the need for floats, trying to align elements, or writing a bunch of extra CSS and HTML just to make sure things are laid out properly. It also makes responding to screen size changes--from desktop to phone to tablet--extremely easy.

Using Flexbox and Grid together can build very complex and stable layouts very quickly. You'll look at Grid first, since that is mainly used for laying out entire pages, and then Flexbox, which can be used to layout sections of a page.