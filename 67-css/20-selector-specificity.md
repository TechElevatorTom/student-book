# CSS Selector Specificity

When setting up all these rules for elements, it's important to understand that if multiple rules apply to a single element, the browser has special rules to figure out which styles should actually be applied. These special rules are called specificity.

The first thing to know is, the browser will load rules in the order that it sees them. This means that a rule that is loaded last will win over a rule that is loaded earlier, the later styles will override the earlier styles. This is called [Cascading](https://developer.mozilla.org/en-US/docs/Web/CSS/Cascade).

But the selectors used in the rules can also matter. The more specific a rule is, the higher priority it will have over other rules no matter where or when they were defined. Each selector has a priority assigned to it and they are listed in the order of least specific to most specific below:

1. Type selectors are the least specific
2. Class selectors, attribute selectors and pseudo-classes
3. ID selectors are the most specific

So, if you write a CSS rule and it doesn't seem to be applying the way you expected it to, check to make sure you don't have a more specific rule written elsewhere that overrides it. The easiest way to do that is to check your browser's development tools, which will show you all the applied and overridden rules for any element that you select.

You can read more about [specificity on MDN](https://developer.mozilla.org/en-US/docs/Web/CSS/Specificity).