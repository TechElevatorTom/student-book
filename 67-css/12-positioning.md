# Positioning

By default, all elements want to get into the flow of the page using the `display` and box model rules that you saw earlier. However, there are ways to dynamically position any element on the page by taking it out of that flow and defining an exact position for an element. You can do this with the `position` style.

By default, all elements have a position of `static`. Static tells the browser to just put the element in the page normally and react to scrolling by the user as if the element is stuck, statically, to the page. If you don't set a position for an element, this is the position value that it will have.

There are other positioning value that you can use to get different results.

## `relative`

Setting the `position` value to `relative` will offset the element from its normal location. Once an element is set to `relative`, you can also then set `top`, `bottom`, `right`, or `left` to move the element away from those directions.

### Example

``` CSS
div.icon {
    position: relative;
    top: 40px;
    left: 50px;
}
```

This will make a `div` with a class of `icon` shifted 40 pixels down and 50 pixels to the right. This might sound strange, but the `top` indicates how far to move it from its original top coordinate--effectively moving it down--and `left` indicates how far to move it from its original left coordinate--effectively moving it to the right.

Another thing to note, even though the element is moved, the browser will still keep all other elements in the same position as if it was still there, essentially reserving its space. This can create a blank space on the page where the element would have been.

## `absolute`

Setting the `position` value to `absolute` lets us take the element out of the normal flow of the website and place it where ever we want in the parent element. Once you define an element to have `position: absolute`, you can define `top`, `bottom`, `right`, and `left` to say where in the parent element you want the element set. Here, the `top` will define how far from the top edge of the parent element you want the element set and `left` will define how from the left edge of the parent element you want the element set.

Elements positioned with `absolute` are taken out of the normal flow. The browser will not reserve a blank spot for this element and as far as the other elements are concerned, it is as if it was never on the page.

## `fixed`

Setting the `position` to `fixed` lets us permanently fix the element to a location on the screen. This position could be well outside of the parent element that this element is in. If you imagine the page being like a piece of paper under a sheet of glass, `absolute` sticks the element to the paper while `fixed` sticks the element to the glass. Even when the page is scrolled, the element will still stay in the exact same place. Here `top`, `bottom`, `left`, and `right` refer to the browser window location, not the page itself. Not setting a location will fix the element to wherever it would normally show up on the page.

## `sticky`

Setting the `position` to `sticky` lets the element toggle between `relative` and `fixed`. This means if you have an element that you want to live in the page, but if the user scrolls away from it, it can become something that follows the user as they scroll through the site. This can be useful if you have navigation that you want to be in the page normally, until the user scrolls down and then the navigation sticks to the top of the screen. Some news sites do this with videos, where the video is displayed normally on the page until the user scrolls down and the video "sticks" to the bottom right corner.

You can read more about these positioning types and try out some examples on the [MDN page for positioning](https://developer.mozilla.org/en-US/docs/Web/CSS/position).