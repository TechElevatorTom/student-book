# CSS Grid Layout, or Grid

Grid is a fairly recent addition to CSS, but is already adopted by 88% of users' browsers. Grid provides the ability to split an HTML page into rows and columns and then assign elements of that page into the "grid" that is created.

Grid is typically used to split up the major sections of a page--like the header, footer, nav, and main content--and then assigning elements to those sections. The browser will then calculate and figure out how everything will fit and how big it all should be.

In these examples, you'll look at using the most powerful feature of Grid for designing layouts--grid template areas.

## Grid Template Areas

The idea behind grid template areas is that you will define a grid of rows and columns that your page should be split up into and then assign elements from your HTML into those grid areas.

![Grid Template Areas](img/dddgrid-template-areas.svg)

### Defining the Grid

The first thing that you will do is define the grid. Imagine there is a page with the following HTML body:

``` HTML
<body>
  <header><h1>Welcome!</h1></header>
  <aside>This is the sidebar</aside>
  <main>
    <h2>Main Content</h2>
    <p>
      This is our main content.
    </p>
  </main>
  <footer>Copyright 2019 by Me</footer>
</body>
```

Which, by default, looks like this:

![Default HTML](img/default-html.png)

If you want to lay this out like the picture above, you'll have to define a grid in which to lay it out in first. The above example has three rows and four columns. To define that, write CSS to tell the `<body>` tag to use a grid layout and have four equal sized columns.

```CSS
body {
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
}
```

`1fr` means "one fractional unit". In other words, you are splitting up the screen into four fractional units, each one being ¼ of the body.

Just making these changes makes your page look like this:

![HTML page with Grid](img/default-grid.png)

The content now seems to be laid out horizontally rather than vertically. If you turn on the grid outline in the browser, you'll see why:

![HTML page with Grid outlined](img/default-grid-with-lines.png)

You can see that the four child elements in the body have automatically been assigned into one of the columns. If nothing else is defined for the template, this is the default behavior.

But what you really want is to have the header stretch across the top, the footer below that and the content and the sidebar next to each other in the middle. In order to do that, you'll want to define a grid area template.

You define the template in CSS, defining grid areas, and naming them so that you can insert your content into them.

You do that by defining the `grid-template-areas` property and setting it to a string template. Each string is a row in your grid and each word is a column in that row.

``` CSS
body {
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-areas:
    "header header header header"
    "content content . sidebar"
    "footer footer footer footer";
}
```

If you define the same name to each column in a row, that means that that element will span that entire row. If you put a `.` in one of the areas, that means that area is empty.

You then have to give these grid names to your elements that are in the `body` element.

```CSS
body {
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr;
  grid-template-areas:
    "header header header header"
    "content content . sidebar"
    "footer footer footer footer";
}

main {
  grid-area: content;
}

header {
  grid-area: header;
}

footer {
  grid-area: footer;
}

aside {
  grid-area: sidebar;
}
```

Now you can see your page is laid out within this template:

![HTML page with laid out Grid elements](img/laid-out-grid.png)

### Grid Gap

You might also want to space out the grid elements a little more also. You can do this by adding padding or margins to your elements, but it's much easier to just add some space between the columns and rows instead. You can do this by adding this to your defined grid:

```CSS
body {
  ...
  grid-gap: 10px;
}
```

This will add a little buffer in between each of the cells of the grid.

![HTML with Grid Gap](img/grid-with-gap.png)

You can see that it doesn't affect any content that spans across the grid, but will separate the different grid items from the others.

### More Information

This is the basic information that you need to know to get started with grid. You can read more at the following resources:

1. [MDN documentation for getting started with Grid](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Grid_Layout/Basic_Concepts_of_Grid_Layout)
2. [CSS Tricks Complete Guide to Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
3. [CSS Grid Garden](http://cssgridgarden.com/)

