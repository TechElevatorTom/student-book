# Default Look and the Box Model

By default, browsers have defined how certain HTML elements are supposed to look and act on the page. For instance, `<em>` elements will italicize text that is within it. `<strong>` elements will bold text that is within it. These styles can be overridden by CSS, but first, look at how some of these elements act by default.

## Block, Inline, and Inline-Block

When the browser reads in the HTML, it must interpret that code and make a visual representation of it on the screen, laying everything out in a consistent manner. How does it do that? Does it make it up on the fly?

The browser has some display rules that it applies to our HTML, depending on the type of element we're using.

There are three primary kinds of display rules:

- `block`
- `inline`
- `inline-block`

### Block

A *block element* always starts on a **new line** and takes up the **full width** available (stretches out to the left and right as far as it can).

Examples of block level elements:
- `<div>`
- `<h1>` - `<h6>`
- `<p>`
- `<form>`
- `<header>`
- `<footer>`
- `<section>`

### Inline

An *inline element* does not start on a new line and only takes up **as much width as necessary**. It stays *inline* to the text that it's contained in. Inline elements **accepts margin and padding** while ignoring height and width.

Examples of inline elements:
- `<span>`
- `<a>`
- `<img>`
- `<em>`
- `<strong>`

### Inline-Block

An **inline-block element** is very similar to inline but **it allows you to set a width and height**. This can be useful for certain elements that you want to change the size of, but don't want it to start on a new line.

Examples of inline-block elements:
- `<select>`
- `<button>`

### Example of Elements Together

If the following HTML was rendered in a browser:

``` HTML
<h1>Header Level 1</h1>
<p> This is some text that we are putting on the page. It has some <em>inline</em> elements as well.

This isn't actually another paragraph.</p>
<p>But this is another paragraph</p>

This is before a <h2>Header Level 2</h2> but the header is on its own line.
```

It would look like this:

![Box Model Example](img/box-model-screenshot.png)

As you can see, the headers and `<p>` elements get their own "line" while the `<em>` element stays inline with the text. With this information, you can predict how a page will look just by looking at the raw code and knowing if an element is by default inline or block.

You can also change an element's display rule any time you want by writing CSS to do that. For instance, if you want all your `<div>` elements to display as `inline-block` instead of the default `block`, you could write the following CSS to make that happen:

``` CSS
div {
    display: inline-block;
}
```

## The Box Model

Elements also have some default properties. Some of them were talked about above, like margin, padding, height, and width. These properties show up in the browser view like this:

![Image of Box Model](https://www.w3schools.com/css/box-model.gif)

Every element is displayed as a rectangular box. There may be rounded corners defined for some that make then look rounded, but the browser treats them all as rectangular. The content, padding, border, and margin are used to calculate the amount of space that an element takes up.

You'll see that every element has a couple of very specific parts to the box model.

- Margin is the space **outside an element**. It does not affect the size of the box but affects other content that interacts with the box.
- Border is the **edge of an element**. Most elements don't have a visible border, but you can add one with CSS.
- Padding is the space **inside an element**. It's the space between the element's content and the border.

A good analogy would be a piece of paper. Lined paper typically has a border to it, the margins are the spaces outside the border and the padding is soft, fluffy space you give between your writing and the border.