# Flexible Box Module, or Flexbox

Flexbox is a layout option in CSS that allows the flexible laying out of items on a page and dynamically handling alignment and the space between the items without a lot of calculation on the part of the web developer.

Flexbox has been around for a couple of years now and is being supported in about 95% of users' browsers.

Flexbox is not typically used to layout an entire page, but more to layout a group of like elements on a part of the page. You'll look at some of the more common uses for Flexbox, including laying out a form.

## Laying Out a Form with Flexbox

Laying out a form in a pleasing manner has always been difficult in HTML. It used to be that you would have to build an HTML table to hold all of the form fields in order to have everything line up properly, or use a library like Bootstrap that had handled all of the form layout issues with lots and lots of cross browser tested CSS.

Flexbox makes laying out and aligning form elements a lot easier.

Imagine you have the following form:

```HTML
<form>
  <div class="form-group">
  	<label for="first-name">First Name</label> 
  	<input id="first-name" type="text">
  </div>
  
  <div class="form-group">
  	<label for="last-name">Last Name</label>
  	<input id="last-name" type="text">
  </div>
  
  <div class="form-group">
  	<label for="email">Email</label>
  	<input id="email" type="text">
  </div>
  
  <div class="form-group">
  	<button>Submit</button>
  </div>
</form>
```

By default, this form will look like this:

![Default Style for Form](img/default-form.png)

Not very nice looking. What you would really like is to have the labels all lined up and right justified and the form fields all lined up. It would also be nice to have the submit button on the far right.

### Making the Form Use Flexbox

The first thing you'll want to do is make the form itself use Flexbox to layout the items vertically. Flexbox can be applied to any element and then you define how you want the items within that element to be arranged.

For this form, you want the items to be arranged vertically with one on top of the other. In Flexbox, that's called a `column` layout. By default, things will be arranged horizontally, or in a `row` layout.

Write in the CSS for the `form` element to use Flexbox and for the items to be in `column` layout:

```CSS
form {
  margin: 10px;
  display: flex;
  flex-direction: column;
}
```

![Form using Flexbox](img/default-flex.png)

The items are now trying to take up the entire width of the page, making the submit button stretch across the screen. You can tell the flex layout to center our items instead by adding this CSS:

``` CSS
form {
  margin: 10px;
  display: flex;
  flex-direction: column;
  align-items: center;
}
```

![Form with Flex and centered](img/flex-column.png)

The `align-items` property is used to tell the Flexbox container how to layout the items inside of it. `center` says to have them only take up the space they need and center them inside the container.

Now you want to arrange the elements inside the `form`. For that, you will turn each `label` element into a flex layout as well, using the row direction and aligning them:

``` CSS
.form-group {
  padding: 5px;
  width: 400px;
  display: flex;
  align-items: center;
}
```

![Form with fields Flexed](img/flex-with-padding.png)

It's getting there. Now you want to tell the label and input fields in those `.form-group` divs to arrange themselves nicely. You'll do that by setting size constraints on the `label` and `input` elements.

To do that, you're going to add some attributes that are special to items within a `flex` container. They are:

1. `flex-basis`
2. `flex-grow`
3. `flex-shrink`

### `flex-basis`

Flex basis is the default size of a flex item. This is the size the element wants to be. Often the `flex-basis` will be based off of the item's set `width`, but can also be set directly as the basis.

### `flex-grow`

Flex grow is an indication to the flex container that this item can grow bigger than it's basis. If it is a positive number, that means that the item can grow to fill the available space within the flex container. The `flex-grow` number is a ratio. If all of your items have the same number, then they will grow to the same size. If one has `1` and another has `2`, then the first will take up one third of the container and the other will take up two thirds of the container. If you don't want the item to grow, set this value to `0`.

### `flex-shrink`

Just like flex grow, flex shrink lets the container know that this item can shrink in relation to the other items in the container. If there is not enough space in the container to layout all the items, it will take away space from any item that has a positive number in the `flex-shrink` property, taking away space from the basis. If you don't want an item to get smaller than its basis, set this value to `0`.

For our form, you want the labels to take up about a third of the available space and the input elements to take up about two thirds of the available space in each `form-group`. You also want each `label` to start off at a default size of about `100px`. You can write the CSS for that like so:

``` CSS
.form-group label {
  padding: 0 5px;
  text-align: right;
  flex-grow: 1;
  flex-basis: 100px;
}

.form-group input {
  flex-grow: 2;
  flex-basis: 200px;
}
```

As you can see, you can also right align the label text and put a little bit of horizontal padding on it to make it look better. You now have something that looks like this:

![Form with Flex Aligned Fields](img/flex-with-grow.png)

You still have a submit button that is off to the left. You can move it to the right by adding this CSS:

``` CSS
.form-group button {
  /* have the left margin take up as much space as it can */
  margin-left: auto;
}
```

![Form with Right Side Button](img/flex-with-aligned-button.png)

By using Flexbox to handle laying out this form, it took very few lines of code to get it looking good and aligned. It also makes the form very flexible in the future as well. If you add more fields to this form, the new fields will be automatically laid out to match.

![Form with New Fields](img/flex-with-more-fields.png)

## More Information

To look up more information on Flexbox, visit these resources:

1. [MDN Documentation for Basic Concepts of Flexbox](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout/Basic_Concepts_of_Flexbox)
2. [MDN Documentation for Flexible Box Layout](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Flexible_Box_Layout)
3. [CSS Tricks: Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)

