# HTML Structure and Common Tags

[HTML (HyperText Markup Language)](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Getting_started) is a declarative language used to communicate to browsers, search engines, and other web applications. HTML is mainly used for transferring content to browsers and telling the browsers how to arrange that content to the user.

Every HTML page has the same basic structure:

``` HTML
<! DOCTYPE html>
<html>
    <head>
        <title>Name of the page as it shows in the tab</title>
        <!-- other extra information about the content -->
    </head>
    <body>
        <!-- information that actually shows on the page -->
    </body>
</html>
```

All of the HTML page contents must be inside the `<html>` tag and every `<html>` tag has two tags inside of it: `<head>` and `<body>`.

## `<head>`

The `<head>` tag contains all the things that describe the web page, things like the page's name as it should be shown in a browser tab, author information, linked CSS or JavaScript files, etc. This is the place where the page's metadata lives and none of this will show up on the actual page in the browser.

## `<body>`

The `<body>` tag contains everything that should show up as content on the page itself. This will be where the `<h1>`, `<p>`, and `<div>` tags go. If you want it to show up on the screen for the user, it goes in the `<body>`.

The body is typically split up into sections as well. HTML5 has defined new HTML tags that define these sections:

- `<header>` - The header of the page. This is where the site's branding and other common elements live.
- `<footer>` - The footer of the page. This can hold common navigation and copyright information.
- `<nav>` - The navigation for the site. This can be used on the main navigation or even page sub navigation.
- `<main>` - This tag defines the main content for *this* page.

These sections are then put together to define all the content for the page.

## Further Reading

- [More information on structuring a web page](https://developer.mozilla.org/en-US/docs/Learn/HTML/Introduction_to_HTML/Document_and_website_structure)