# HTML and CSS Overview

As part of the pre-work for this bootcamp you dove into HTML and CSS as one of the requirements. Now it's time to put that work to good use!

In this section, you're going to review all of the HTML and CSS you learned and dive deeper into how these technologies work to create the visual aspect of your web applications. You'll cover these topics:

- How HTML is structured to be readable by the browser
- How the browser interprets HTML
- How the browser chooses what CSS rules to apply
- How the HTML is converted into visual elements on the screen

And you'll learn these important topics:

- The most common HTML tags
- The Box Model of HTML layout
- CSS Selectors used by CSS (and, we'll see in the future, JavaScript and testing frameworks)
- Cascading of Stylesheets and specificity
- Default HTML element positioning