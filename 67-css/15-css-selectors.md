# CSS Selectors

Cascading StyleSheets (CSS) are used to change the default styles of HTML elements in the browser. You can redefine colors, borders, widths, heights, animation, fonts, display styles, etc. In order to define these new rules, you first have to select the elements that you want to select. But first, you need to put CSS on a page.

## CSS Rules

First, CSS rules are made up of a property and a value to set that value to. For instance, if you wanted to change an element's background color to red, you could write a rule like this:

``` CSS
background-color: red;
```

A full gamut of CSS rules can be found on the [MDN page for CSS](https://developer.mozilla.org/en-US/docs/Web/CSS/Reference)

Rules can also have a selector. The selector will tell the browser what elements you want to style. For instance, the following will change the background color to red for all `<div>`s on the page:

``` CSS
div {
    background-color: red;
}
```

## Adding CSS

CSS can be added to a web page in three different ways. Really, only one of them is used in actual projects.

You can add CSS inline to an element using its `style` attribute:

``` HTML
<div style="background-color: red">
```

This is frowned upon and is typically only used as a last resort. It makes the code very brittle. If you did this on a number of `<div>` tags on the site and then later wanted to change the color to something else, you would have to change that in every location. That's a lot of duplicated code and leaves a lot of room for error.

You can also add a section to the `<head>` of your page and add all of your CSS in one place:

```HTML
<style>
    div {
        background-color: red;
    }
    span {
        text-decoration: underline;
    }
</style>
```

This allows you to keep all of your CSS in one place, but it's still hard coded into one page and can't be reused across other pages without copy and pasting. And copying and pasting would just cause more duplicated code.

The last way to include CSS, and the way it's used 99% of the time, is to create a CSS file separate from the HTML that you link into the HTML.

``` HTML
<link href="style.css" rel="stylesheet" type="text/css">
```

This `style.css` file can now be included on every page on your site and most browsers will only load it from the server once, caching it for future use and saving load times for your future pages.

> #### Always create a separate file for your CSS
>
> As a rule of thumb, always create a separate file that contains the CSS for your site. This, again, is how 99% of the CSS on the web is written and will keep all your CSs and rules in one place and save you a lot of headache.

## CSS Selectors

CSS rules are applied by selecting HTML elements from the page and applying specified styles to them. HTML elements are selected with what are called CSS selectors. CSS Selectors select certain elements depending on a couple of the features that the elements have. There are a couple of different kinds of selectors and they can be mixed and matched to select only the elements that you need. Their syntax and purpose is described below.

### Universal Selector

``` CSS
* {
    color: red;
}
```

The Universal selector selects *every* element. The universal selector is just a single `*`.

### Element Selector

``` CSS
div {
    color: red;
}
```

The Element, or Type, selector selects all HTML elements of a certain type. The selector `div` will select all the `div` elements on a page. `input` would select all the `input` elements on the page. The element selector will be just a single word with no other attached characters.

### Class Selector

``` CSS
.warning {
    color: red;
}
```

The Class selector selects all elements that have a certain `class` attribute. The example above would match all of these elements:

``` HTML
<div class="warning">...</div>
<span class="warning">...</span>
<p class="small warning">...</p>
```

Class selectors always start with a `.`.

### ID Selector

``` CSS
#demo {
    color: red;
}
```

The ID selector selects the element that has an attribute with the given `id`. The example above would match this element:

``` HTML
<section id="demo">...</section>
```

ID selectors always start with a `#`.

> #### IDs should be unique
>
> In your HTML document, no two elements should have the same ID. IDs are meant to be unique in HTML. There's nothing that strictly enforces this, but certain things won't work if you reuse an ID anywhere. Fair warning!

### Attribute Selector

``` CSS
input[type=text] {
    color: red;
}
```

The Attribute Selector selects elements that have the defined attribute. You could think of this as a long hand version of the class and id selectors, but for any attribute that you have on an element. This type of selector can get pretty fancy and you can read more about it on the [MDN site for Attribute Selector](https://developer.mozilla.org/en-US/docs/Web/CSS/Attribute_selectors). Attribute selectors will always have `[]` in them.

### Pseudo Class Selector

``` CSS
:hover {
    color: red;
}
```

The Pseudo Class Selector selects elements based on dynamic and changing state from user interaction. Some examples are `:hover`, `:focus`, `:active`, and location states like `:last-child` and `:first-of-type`. You can read more about them at the [MDN site for Pseudo Class Selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/Pseudo-classes). Pseudo class selectors always begin with a `:`.

## Combinators

Another powerful feature of CSS selectors is that they can be combined in order to specify exactly the elements you want from the page. There are a number of different combinators that you can use to narrow down your selection.

### Combo Combinator

``` CSS
button[type=submit].danger {
    background-color: red;
}
```

You can combine selectors into very specific selections by just combining them together. The above uses an element selector, an attribute selector, and a class selector to apply a red background to any `button` element that has an attribute of `type` equal to `submit` that also has a class of `danger`. There can be no spaces between any of these parts if you want them all to apply to the same element.

### Multiple Combinator

``` CSS
button, input[type=checkbox], select {
    border: 2px;
}
```

You can have a set of styles apply to different elements by separating those elements with a comma. The above styles apply to all `button`s *and* all checkboxes *and* all `select` elements.

### Descendant Combinator

``` CSS
div.aside p {
    color: blue;
}
```

Descendant combinators will select all the elements that are inside another element. The rule above will apply to any `p` element that is anywhere within a `div` element that has a class of `aside`. For example, the styles will apply to the following `p` elements.

``` HTML
<div class="aside">
    <p>Styles will apply to this paragraph</p>
    <div class="section">
        <p>Styles will apply to this paragraph also because it is still inside the div.aside</p>
    </div>
</div>
```

### Child Combinator

``` CSS
div.aside > p {
    color: blue;
}
```

Child combinators will select all the elements that are *direct children* of another element. The rule above will apply to any `p` element that is directly under a `div` element that has a class of `aside`. For example, the styles will apply to only the first `p` element.

``` HTML
<div class="aside">
    <p>Styles will apply to this paragraph</p>
    <div class="section">
        <p>Styles will *NOT* apply to this paragraph because it is not a direct child of div.aside</p>
    </div>
</div>
```

### Sibling Combinators

There are also sibling combinators that select elements next to other elements in your page, but these are rarely used. You can read more about them on the [MDN page for CSS Selectors](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Selectors#Combinators).