## Exception Object

While the `catch` clause can be used without any type of argument after it, **this is not recommended**. 

{{book.language}} provides access to an exception object with additional detail about the exception that was thrown. Exceptions are similar to classes and they use properties to represent their current state.

They are built on the principle of inheritance.

{% if book.language === 'Java' %}

    Object
        Throwable
            Exception
                RuntimeException
                    IllegalArgumentException
                        NumberFormatException
                    ArithmeticException
                    NullPointerException
                    IndexOutOfBoundsException
                    ...

The number of built-in exception classes is far too many to be able to show here, so this is just a sample. Intellisense will tell you which exceptions you will need to watch for when calling a method.

Using the example from the previous section, we'll catch a `NumberFormatException`, which is used when a call to one of the `parse` methods can't complete.

```java
try {    
    System.out.print("Enter a number: ");
    String s = input.nextLine();
    int result = Integer.parseInt(s);

    System.out.println("The number typed in is " + result + ".");
} catch(NumberFormatException e) {
    System.out.println("An error parsing the input.");
}

System.out.println("Thank you.");
```

**Example (w/Valid Input)**

    Enter a number: 2
    The number type in is 2.
    Thank you.

**Example (w/Invalid Input)**

    Enter a number: two
    An error occurred parsing the input.
    Thank you.

Our `catch` block is configured to look for a specific type of exception. It will catch anything that is of type `NumberFormatException` or inherits from `NumberFormatException`.

{% elif book.language === 'C#' %}

    Object
        Exception
            SystemException
                ArgumentException
                DivideByZeroException                
                FormatException
                InvalidCastException
                IOException
                ...


The number of built-in exception classes is far too many to be able to show here, so this is just a sample. The MSDN documentation has the full Exception hierarchy available.

> #### Info::MSDN Documentation
>
> The .NET framework is heavily documented on MSDN's website. Within the documentation you can see what exceptions any of the pre-existing methods can throw and under what circumstances.

Using the example from the previous section, we'll catch a `FormatException`, which is used when a call to one of the `Parse` methods can't complete.

```csharp
try
{    
    Console.Write("Enter a number: ");
    string s = Console.ReadLine();
    int result = int.Parse(s);

    Console.WriteLine($"The number typed in is {result}.");
}
catch(FormatException e)
{
    Console.WriteLine("An error parsing the input.");
}

Console.WriteLine("Thank you.");
```

**Example (w/Valid Input)**

    Enter a number: 2
    The number type in is 2.
    Thank you.

**Example (w/Invalid Input)**

    Enter a number: two
    An error occurred parsing the input.
    Thank you.

Our `catch` block is configured to look for a specific type of exception. It will catch anything that is of type `FormatException` or inherits from `FormatException`.

{% endif %}

Notice this looks like a method call. `e` is now treated like a parameter and represents the object that contains all of the detail about the exception that occurred. 

**TLDR**
1. You should catch exceptions when you have a good understanding of how you wish to recover from it.