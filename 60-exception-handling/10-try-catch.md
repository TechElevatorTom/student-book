## Try / Catch

When we need to write code where there is a possibility of an exception, we use the `try` and `catch` keywords to handle the potential error. 


{% if book.language === 'Java' %}

```java
try {    
    System.out.print("Enter a number: ");
    String s = input.nextLine();
    int result = Integer.parseInt(s);

    System.out.println("The number typed in is " + result + ".");
} catch(Exception ex) {
    System.out.println("An error occurred.");
}

System.out.println("Thank you.");
```
{% elif book.language === 'C#' %}

```csharp
try
{    
    Console.Write("Enter a number: ");
    string s = Console.ReadLine();
    int result = int.Parse(s);

    Console.WriteLine($"The number typed in is {result}.");
}
catch
{
    Console.WriteLine("An error occurred.");
}

Console.WriteLine("Thank you.");
```

{% endif %}

**Example (w/Valid Input)**

    Enter a number: 2
    The number type in is 2.
    Thank you.

**Example (w/Invalid Input)**

    Enter a number: two
    An error occurred.
    Thank you.

When an exception is thrown in code, the runtime finds the nearest `catch` statement to handle the exception. If the current method doesn't have a try-catch block, the exception *bubbles up*, and looks for a `catch` block in the calling method. If an exception occurs, the remaining code in the `try` block is ignored. 

In the event that there isn't any exception, the `try` block completes successfully and the `catch` block is ignored.

> #### Note::Variable Scope
>
> Try-catch blocks work with variable scope the same way that other blocks do. If you need a variable to be available to your entire try-catch block, you'll need to declare it in advance.
