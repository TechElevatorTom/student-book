
## Exception Properties

{% if book.language === 'Java' %}

The [Exception](https://docs.oracle.com/javase/9/docs/api/java/lang/Exception.html) class is the base class from which all exceptions inherit. 

It contains the following properties that help make understanding this exception easier.

| Property Name | Description                                                                           |
|---------------|---------------------------------------------------------------------------------------|
| Cause    | Used to create and preserve a series of exceptions caught during exception handling.  |
| Message       | Provides detail about the cause of the exception.                                     |
| StackTrace    | Contains a *stack trace* that can be used to determine where the error occurred.      |

Most of the classes that inherit from `Exception` don't add additional properties or members. They simply inherit from `Exception` to allow as specific of error handling as possible.

{% elif book.language === 'C#' %}

The [Exception](https://docs.microsoft.com/en-us/dotnet/api/system.exception) class is the base class from which all exceptions inherit. 

It contains the following properties that help make understanding this exception easier.

| Property Name | Description                                                                           |
|---------------|---------------------------------------------------------------------------------------|
| Data          | A dictionary that holds abritary data in key-value pairs.                             |
| HelpLink      | May hold a URL to a help file with information about the exception.                   |
| InnerException| Used to create and preserve a series of exceptions caught during exception handling.  |
| Message       | Provides detail about the cause of the exception.                                     |
| StackTrace    | Contains a *stack trace* that can be used to determine where the error occurred.      |

Most classes that inherit from `Exception` don't add additional properties or members. They simply inherit to allow as specific of error handling as possible.

{% endif %}

Let's look at two particular properties to demonstrate their usefulness.

------

### Message

The message property describes the exception in further detail. 

* Most exception messages explain *the reason for the exception*. They aren't guaranteed to be user friendly.
* With custom exceptions, you can set your own message.

------

### Stack Trace

A stack trace is a list of method calls the application had pending when the exception was thrown. As a developer who has troubleshooted many production issues without seeing the original error take place, I can't emphasize the significance of having a stack trace.

If we look at a sample Java stack trace (C# looks very similar) we can see its value.

    Exception in thread "main" java.lang.NullPointerException
            at com.example.myproject.Book.getTitle(Book.java:16)
            at com.example.myproject.Author.getBookTitles(Author.java:25)
            at com.example.myproject.Bootstrap.main(Bootstrap.java:14)

The stack trace acts like a trail of breadcrumbs so that we can determine where in our code the `NullPointerException` occurred. If you start with the top of the list of "at..." this indicates the name of the class, method, and the line number that caused the `NullPointerException`.

In this case it's:

    at com.example.myproject.Book.getTitle(Book.java:16)

We should use this by observing what file it references it and look directly at the line. In this case the error occurred in `Book.java` on line 16.

```java
15   public String getTitle() {
16      System.out.println(title.toLower());
17      return title;
18   }
```

A `NullPointerException` occurs when you try to invoke a property or method on an object whose value is null. It looks like `title` may have been null.

> ####Caution::Stack Traces Give a Lot Away
>
> Stack Traces can be dangerous if observed by customers or users in production. Error screens should be used to show when something goes wrong. If a malicious user were able to see a stack trace it gives information that can help compromise your application. They can see the technology used, libraries your code relies on, and internal naming conventions that might make it easier for them to exploit.

**TLDR**
1. Read the stack trace, it'll save you time.
