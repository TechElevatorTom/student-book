# Exception Handling

{{book.language}} includes the capability to help you deal with any unexpected or exceptional situations while your program is running. The practice of anticipating these anomalies and addressing them is called *exception handling*.

When users interact with applications, we should expect them to make mistakes. If we don't, you're in for a surprise. In their defense, they should expect our application to indicate that something went wrong, not display a cryptic error message.

When applications implement exception handling, they allow us:
* the ability to display specific error messages
* to notify other programmers that our code did not finish execution

Without proper exception handling, exceptions go uncaught and can potentially reveal information about our program that we don't want seen by users. 

There are a few terms when it comes to working with exception handling.

1. Exceptions are *raised* or *thrown*.
1. Control of the program stops abruptly and the exception bubbles up until it is *caught*.
1. A *caught* exception handles the error and displays the message to the user or logs it. 

Following are the approaches to implementing exception handling in your programs.
