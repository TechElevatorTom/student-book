
## Custom Exceptions

You're not limited to working with the system exception classes built into {{book.language}}. If your application needs additional detail, you can create a custom exception.  **This is often the case when you are releasing code for other developers to incorporate.** 

Consider a hypothetical vending machine. When the customer decides to purchas an item a number of things could go wrong:
1. The item is *out of stock*.
2. The customer has *insufficient funds*.
3. The customer selects an *invalid slot*.
4. The vending machine experienced a *mechanical dispensing error*.

Rather than returning `null` or nothing at all because ambigious, it is possible to throw an exception. The user interface part of our program can determine the best way to display a message to the user, letting them know something went wrong.

---- 
### Creating a Custom Exception

{% if book.language === 'Java' %}
```java
public class InsufficientFundsException extends Exception {
    public InsufficientFundsException() { super(); }
    public InsufficientFundsException(String message) { super(message); }
    public InsufficientFundsException(String message, Exception inner) { super(message, inner); }
}
```
{% elif book.language === 'C#' %}

```csharp
public class InsufficientFundsException : Exception
{
    public InsufficientFundsException() : base() { }
    public InsufficientFundsException(string message) : base(message) { }
    public InsufficientFundsException(string message, Exception inner) : base(message, inner) { }
}
```

**Best Practices for Custom Exceptions:**

1. All exceptions inherit from `Exception`.
1. It is a convention to end all custom exceptions with the word `Exception`.
1. The custom exception should define at least these three constructors:
    1. A default constructor
    1. A constructor that accepts the message property.
    1. A constructor that sets the message property and the inner exception.

Once an exception is created it can be thrown (e.g. `throw new InsufficientFundsException("balance is short $25.72");`) and and caught with a `catch` block (e.g. `catch(InsufficientFundsException ex)`).

{% endif %}



