
# Raising Exceptions

In addition to handling exceptions you can raise your own exceptions using the `throw` keyword. 

The most common scenario in which we want to throw our own exception is when our code cannot complete its functionality. This may be due to a null parameter, a valid not in a valid range, or any other possible reason.

As an example, our Clock constructor allowed you to pass any integer as the hour, minute, and second. We need to indicate when a value is not within the acceptable value range. If we find one, we can throw an exception.

{% if book.language === 'Java' %}

```java
public class Clock {
    public Clock(int hour, int minute, int second) {
        if (hour > 24 || hour < 0) 
            throw new IllegalArgumentException("Parameter must be between 0 and 24", "hour");
        if (minute > 60 || minute < 0) 
            throw new IllegalArgumentException("Parameter must be between 0 and 60", "minute");
        if (second > 60 || second < 0) 
            throw new IllegalArgumentException("Parameter must be between 0 and 60", "second");

        this.Hour = hour;
        this.Minute = minute;
        this.Second = second;
    }
}
```
{% elif book.language === 'C#' %}

```csharp
public class Clock
{
    public Clock(int hour, int minute, int second) 
    {
        if (hour > 24 || hour < 0) 
            throw new ArgumentException("Parameter must be between 0 and 24", "hour");
        if (minute > 60 || minute < 0) 
            throw new ArgumentException("Parameter must be between 0 and 60", "minute");
        if (second > 60 || second < 0) 
            throw new ArgumentException("Parameter must be between 0 and 60", "second");

        this.Hour = hour;
        this.Minute = minute;
        this.Second = second;
    }
}
```
{% endif %}
