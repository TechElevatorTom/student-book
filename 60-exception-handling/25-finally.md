## Finally

While most memory management is handled for us in {{book.language}} sometimes we need to dispose of resources when we're done with them.

For example:
* databases have a finite number of connections, our applications need to close connections when done with them
* files are "locked" when opened, prohibiting other applications from seeing or making modifications
* releasing lower-level graphics resources

We know when exceptions are thrown, the statements in the `catch` block are called and our program never returns to the `try` block. The `catch` block might return from the method itself or throw additional exceptions. Its possible that code after the try-catch may never run and resources are never cleared out.

{% if book.language === 'Java' %}

```java
try {
    // Code that may throw an exception
} catch(IOException ex) {
    // Code that catches the exception
}

// Code that is not guaranteed to execute
```
{% elif book.language === 'C#' %}

```csharp
try
{
    // Code that may throw an exception
}
catch(IOException ex)
{
    // Code that catches the exception
}

// Code that is not guaranteed to execute
```

{% endif %}

The {{book.language}} provides the `finally` block that can be added to a try-catch. Statements inside of a `finally` block will run regardless if an exception occurred or not.

{% if book.language === 'Java' %}

```java
try {
    // Code that may throw an exception
} catch(IOException ex) {
    // Code that catches the exception
} finally {
    // Code that executes whether the exception
    // is thrown or not.
}
```

{% elif book.language === 'C#' %}

```csharp
try
{
    // Code that may throw an exception
}
catch(IOException ex)
{
    // Code that catches the exception
}
finally
{
    // Code that executes whether the exception
    // is thrown or not.
}
```

{% endif %}

