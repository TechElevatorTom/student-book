
# Defensive Programming

If it seems like exception handling is awesome, it is! Exceptions help us avoid scenarios which may worry our users that something really bad just happened.

Still, we can't rely on the runtime to tell us when there is a problem. If the exception can be avoided, through *defensive programming* it should be at all costs. 


Here are a few ways to avoid exceptions

{% if book.language === 'Java' %}

-----

## OutOfBoundsException

Prevent the `OutOfBoundsException` by confirming that your index is within the bounds of the array or list first.

-----

## ArithmeticException

Prevent the `ArithmeticException` by making sure that your divisor is not equal to 0 first.

-----

## NullPointerException

Prevent a `NullPointerException` by checking if the reference object you are acting on is `!= null` before calling any properties or methods on it.

-----

## NumberFormatException

When you try and parse a string into an `int`, `bool`, `double`, or `decimal`, you open up room for error. The user or programmer may not have a valid value that can be parsed. 

------

## IllegalArgumentException

Use user-interface validation first to inform the user they typed in an invalid value and to try again. This doesn't remove the necessity for an `IllegalArgumentException`, but it cuts down on the occurrences.

{% elif book.language === 'C#' %}

-----

## IndexOutOfRangeException

Prevent the `IndexOutOfRangeException` by confirming that your index is within the bounds of the array or list first.

-----

## DivideByZeroException

Prevent a `DivideByZeroException` by making sure that your divisor is not equal to 0 first.

-----

## NullReferenceException

Prevent a `NullReferenceException` by checking if the reference object you are acting on is `!= null` before calling any properties or methods on it.

-----

## FormatException

When you try and parse a string into an `int`, `bool`, `double`, or `decimal`, you open up room for error. The user or programmer may not have a valid value that can be parsed. 

Instead use `TryParse(string s, out value)` to first check to see if the value can be parsed. Once you've confirmed it can, a second argument is provided containing the parsed value.

------

## ArgumentOutOfRangeException

Use user-interface validation first to inform the user they typed in an invalid value and to try again. This doesn't remove the necessity for an `ArgumentOutOfRangeException`, but it cuts down on the occurrences.

{% endif %}

