# Catching Multiple Exceptions

It might be necessary to expect a number of different exceptions could occur with the code. The syntax for a try-catch statement allows one or more `catch` blocks.

Doing this allows us to address each of the exceptions in different ways.

{% if book.language === 'Java' %}
```java
try {
    // Perform some work (e.g. open a file and read all of the text)
} catch(FileNotFoundException e) {
    // A File not found exception would be handled here.
} catch(IOException e) {
    
}
```
{% elif book.language === 'C#' %}

```csharp
try 
{
    // Perform some work (e.g. open a file and read all of the text)
}
catch(FileNotFoundException e)
{
    // A File not found exception would be handled here.
}
catch(IOException e)
{
    // A general IOException would be handled here.
}
```

{% endif %}

When the exception occurs from within the `try` block, it immediately stops and looks for the nearest `catch` block that matches the exception type. If you define the more generic exception instead of specific exception first, you risk losing the ability to execute exception-specific code.

**TLDR**
1. It is a best practice to catch a specific type of exception.
2. Place your catch blocks in order of most specific to least specific.
3. Avoid catching the base `Exception` class.
