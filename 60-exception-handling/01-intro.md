# Handling Program Errors

Eventually we have to face a reality:
1. Our users will type "two" when we ask for a number, "yes" when we ask for a boolean, and anything else that we can't anticipate.
1. We're going to make a mistake in our own code that we didn't expect.

Thanks to the compiler the second one is less likely. When it comes to syntax errors or a data type mismatch, our IDE will happily indicate something is wrong with our code. Fortunately our users never see these. These are called compiler errors.

Runtime errors are different. They occur when the code is running. The IDE and the compiler won't detect logical errors ahead of time for us. Runtime errors happen for any number of reasons:
* we go out of bounds when working with an array
* the input can't be parsed into a valid data type
* the e-mail address already exists in the database
* the server is offline or overloaded 
* the file we try to open doesn't exist

Some of these can be limited through *defensive programming* but as programmers, it is our job to make sure that the ones that cannot be prevented don't negatively affect our user's experience.

The last thing that we want our users to see is this error message. They're likely to think something malicious has happened to them:

    Unhandled Exception: System.FormatException: Input string was not in a correct format.
    at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number,
    NumberFormatInfo info, Boolean parseDecimal)
    at System.Number.ParseInt32(String s, NumberStyles style, NumberFormatInfo info)
    at BobsProgram.Main(String[] args)

In this section you will learn:
* how to handle exceptions with `try`, `catch`, and `finally`
* what is revealed within a stack trace and how it helps you troubleshoot your code
* throwing built-in and custom exceptions
* guarding against exceptions through defensive programming

    






    