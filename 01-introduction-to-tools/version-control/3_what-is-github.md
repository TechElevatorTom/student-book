# Git & GitHub

## Git

> Git is a version control system for tracking changes in computer files and coordinating work on those files among multiple people. It is primarily used for source code management in software development, but it can be used to keep track of changes in any set of files.
>
> — *[Wikipedia](https://en.wikipedia.org/wiki/Git)*

One of the most widely used version control systems in the word today is Git. It can be found online at the core of most open-source projects and internal in many different organizations. Even the software that developers use to write and test their code (IDEs) offer built-in integrations with Git.

---

## GitHub, BitBucket...What's the Difference?

Once you start using Git on your computer, all of the changes you make to a repository are tracked and saved on a local database. Working only with this local database still poses some risks, mainly the possibility of losing your computer or damaging the hard drive. You would be lucky to be able to recover this data. For this a solution exists that allows you to copy your repositories from your local computer to a remote hosted location in the cloud.

GitHub and BitBucket offer remote repository hosting. As a developer you would update the remote repository with your code by *pushing your changes* to it. On the flip side, you can download new code from a remote repository, code that a teammate wrote, by *pulling the changes* onto your computer.

You'll find that GitHub tends to be more prevalent amongst open source developer communities while BitBucket may be used more often in corporate settings. They both offer the same capability but usually have differences in pricing and how they manage public & private repositories.