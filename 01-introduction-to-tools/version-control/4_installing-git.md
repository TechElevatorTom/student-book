# Installing the Git Client

A git client provides the interface that allows developers the ability to contribute and manage a git repository. While some developers prefer applications with a flashy user interface, others prefer to work with the command line where they have the highest level of control. 

Below are the instructions to install the git command line tools.

{% tabs first="Windows", second="OS X" %}

{% content "first" %}
1. Visit the [Git Website](https://git-scm.com/downloads) and download the **Git Bash for Windows** client.
2. Run the **Git Bash for Windows** installer. Leave all of the default settings as they currently are.
3. Once the installation is complete, a new program called **Git Bash** will be installed.

{% content "second" %}
1. From a Mac OS X terminal type the command `git`.

{% endtabs %}

---

## Configuring Git

Once git has been installed, it needs to be configured with a name and e-mail address so that it can associate modifications to a user.

From a command line execute the following commands:

```shell
git config --global user.name <name>
git config --global user.email <email>
```