# Pushing & Pulling Code

Changes that you make on your local repository can be copied to the remote repository with the `git push` command.

The `git push` command takes two arguments:

* A remote name, for example `origin`. `origin` is the alias automatically given to the original remote repository
* A branch name, for example `master`.

The entire command may look like:

```
git push <remote-name> <branch-name>
```

**Pulling Changes**

Whether you are working on separate computers (one at home, one at work) or with different team members, you are going to need a way to incorporate the changes they push into your local repository. The `git pull` command instructs git to fetch commits from a remote repository.

The `git pull` command looks like:

```shell
git pull <remote-name> <branch-name>
```

> #### Info::Pull Before You Push
>
> Git won't let you push your changes to a remote repository if the remote repository has changes that you haven't incorporated into yours yet.

---

# Forks and Upstream Repositories

In a standard open-source project (and some company configurations) you generally have two remote repositories, `origin` and `upstream`. `upstream` refers to the original repository that you have _forked from_. A **fork** is a copy of a repository that you own so that you can freely experiment with changes without affecting the original project. The _forked repository_ (your copy) is referred to as `origin` by default.

Forks are most commonly used for the following reasons:

**Propose changes to someone else's project**

A great example of using a fork is for bug fixes. Rather than logging an issue, you can:

* Fork the repository.
* Make the fix.
* Submit a _pull request_ to the project owner.

If the work is approved, it might be pulled into the original repository!

**Use someone else's project as a starting point**

By sharing code, developers can create better, more reliable software. When a repository is marked public, it can be forked and used in the source code for other projects.


![Upstream & Origin Remotes](resources/images/upstream-origin.png)


