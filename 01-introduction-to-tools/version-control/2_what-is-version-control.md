# Using Version Control

Whether you work as a team of one or a team of many, on many software projects protecting the source code is a must. Version control is software often used to help track the changes over time to a codebase. Version control systems are a category of software tools that help teams manage changes to source code over time. They offer *three major benefits*:

1. Teams can _go back in time_ when a bug occurs and compare different versions of the source code.
2. Sharing changes between team members is made simple, guaranteeing everyone has the latest copy.
3. Team members can develop code in parallel through the use of branches and merge them together when complete.

Watch the below video to see how version control can benefit any type of collaborative team.

{% video %}https://vimeo.com/41027679 {% endvideo %}