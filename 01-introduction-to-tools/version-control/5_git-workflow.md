# The Git Workflow

![Git Workflow](resources/images/git-workflow.jpg)

Working with Git requires you to follow a very simple workflow. This workflow starts from getting the code copied and running on your machine to distributing changes to other developers on the team. It is broken down into the following steps:

1. Initialize a local repository or clone a remote repository onto your computer to work with.
1. Make edits to the repository contents by modifying its files.
1. Stage modified files for Git to mark which are included in the next commit.
1. Commit staged changes, providing a clear message indicating what modifications were saved.
1. Push the changes to the server for the rest of the team and pull their changes into your local repository.