# Committing Changes

When the staged files are ready to be recorded, the `git commit` command is used. This command records all staged files into the local repository. It creates a *snapshot* of what changed in each of the staged files from the previous version. This is like creating a save point in a video game where someone can go back to a previous version of the code as it existed at a point in time. 

The `git commit` command is used in conjunction with an argument `-m` that allows the committer to specify a *commit message*. The commit message is extremely important. To someone not present at the time the code changed, it is a clear statement indicating what changed with this commit. It's also a permanent record in the repository, so make sure your commit message stays clean!

```shell
git commit -m "Added iOS 12 support."
```

The effect of a `git commit` command is that an entry similar to below is added to the log. Commits can be viewed with the `git log` command.

```
commit 1787389986db4df29c916f14f297f51dda86208f
Author: Tim Cook <tim@apple.com>
Date:   Thu Nov 16 22:31:51 2017 -0500

    Added iOS 12 support.
```