

# Staging Changes

Git has three main states that a file can exist in:

1. **Modified** - the file has been changed but not saved to your local database yet
1. **Staged** - the file is marked in its current version to go into the next commit
1. **Committed** - the data is safely stored in your local database

The `git status` command can be used to check the status of all modified & staged files within the repository. As a developer, it becomes second nature to use the `git status` command to continually see which files have been staged or modified.

The `git add` command can be used to stage a modified file.  

## Stage Explicit File(s)

```
git add <file-name> [<file-name>, ...]
```

This version supports explicit file names to be identified when staging files. Multiple files can be staged at the same time by separating the names with spaces. The command `git add foo.txt bar.txt` stages two files: `foo.txt` and `bar.txt`.

## Stage Explicit Directory

```
git add <directory-name> [<directory-name>, ...]
```

This version allows you to specify a directory to have its contents staged. Git is intelligent enough to detect which files inside the directory have been recently modified.


## Stage All Changes

```
git add .
```

This version allows developers to stage the contents of *this directory and all directories beneath it*. It can save time from the other more tedious `git add` commands.

> #### Caution::Careful with `git add .`
>
>  It is important to realize what your current working directory is before running this command. If you aren't located in the root of the repository you may only stage some of the changes. On the flip side, if you are located in the root and run this command then you will stage every addition, modification, and deletion to modified files in the repository.