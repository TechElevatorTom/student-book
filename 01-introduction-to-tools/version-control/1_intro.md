# Version Control Objectives

By the end of this section you should have an understanding of:

* Be able to install a Git client on your machine
* Clone a Git repository
* Stage and commit changes into a local repository
* Push and pull changes to a remote repository