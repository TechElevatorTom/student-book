# Creating a Git Repository

In order to begin working with an existing repository, a developer needs the remote URL so that the repository can be cloned to the computer as a local repository. A remote URL is git's way of identifying "a place where code is stored". This URL may reference a location on GitHub, BitBucket, or another server. On GitHub it is usually found on the home screen of the repository and in the format of `https://github.com/username/repository-name.git` (it always ends with .git).  With this URL, a new command `git clone` is run in the command line to download a copy of the repository.

```
git clone <remote-url>
```

Once a remote repository has been *cloned*, a developer has the entire code base and is free to begin making changes.

> #### Info::Clone Location
>
> When you use the `git clone` command, git will automatically create a folder for you in the location of your current working directory. Be careful not to perform this command inside of an existing git repository.
