# What is a shell

A shell is a character-based interface to the operating system. The shell is also known as the "terminal", or the "command line". You tell the operating system what to do by typing commands inside the shell. These commands can be as mundane as asking the operating system for the current date, or as sophisticated as telling the operating system to search for files containing a particular word, moving those files to a new location, and then emailing you a short report of the files found and moved. 

Programmers, and other computer professionals tend to prefer the shell because of its flexibility. While each command typically performs a single limited function, they can be joined to build complex tasks, and can even be scripted to create complete applications. Describing all the possibilities is well beyond the scope of this short document, however, several frequently used commands to navigate and work with the file system are covered.

The most popular shell is named `bash`. It is available on the OS X, Windows, and Linux operating systems, and is the shell we use at Tech Elevator.

## Launching a Shell

#### Windows

There are a number of "shells" that can be launched on a Windows computer. One of the most common ones is the Windows Command Prompt.

This is launched by opening the Start Menu and typing `cmd`. The command prompt allows you to run a number of common windows style commands (e.g. `dir`, `cd`, etc.) however this is not the same as a Unix shell.

The equivalent for the Unix-based shell can be opened through a program called Git Bash. This can also be launched by opening the Start Menu and typing `git bash` until the Git Bash application shows up. Because it is integrated with Git and supports many of the common commands we will leverage, we will find ourselves in Git Bash the majority of the cohort.

#### OS X

Many programmers drag a copy of the Terminal icon to the Dock so it can be easily launched by simply pulling up the Dock and clicking.  If you prefer a mouse-less method which doesn't require any additional prep like dragging an icon to the Dock, you can make use of Spotlight Search. Hit `⌘-spacebar` to bring up Spotlight Search, and type "terminal" in the Spotlight Search. Spotlight Search will begin searching for the nearest match from the first character you type. The search narrows as you enter more characters.  You may stop typing when the Terminal icon appears. Simply press `return` to launch Terminal.  

![Spotlight Search: Terminal](resources/images/spotlight-search-terminal.png)