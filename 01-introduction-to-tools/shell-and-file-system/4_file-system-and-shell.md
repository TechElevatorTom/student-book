# The File System and the Shell

While GUI Tools such as OS X' Finder, and Windows' File Explorer let you move around your computer's files and folders in a point and click interface, there is also another way to navigate your computer.

The command line interface is the OG computer UI. Using it lets you walk through your computer's file structure and perform actions very quickly. It is also the UI used to program actions on your computer and is very important to understand for all software developers.



## Navigating the File System

Navigating around the system requires knowledge of three commands: `cd`, `pwd`, and `ls`.

### Changing Directories

Changing directories is done via the `cd` command.

```bash
cd /Users/me/Documents
```

This command tells the computer to go to the root folder, `/`, then into the `Users` folder, then the `me` folder inside of that and finally the `Documents` folder in that. This style of using `cd` is using an *absolute path*. An absolute path always starts at the beginning of the file system - at root - and then specifies each that to take from there.

But you might already be in a folder. If you run the command above, you enter the `/Users/me/Documents` folder. What if you want to go to the `/Users/me` folder instead? You could type out the full path again, like:

```bash
cd /Users/me
```

*OR* you could use a *relative* path. If you're already in `/Users/me/Documents`, you can go back one directory by typing:

```bash
cd ..
```

The special `..` directory used above means "go to my parent". In this case, if you run this from inside `/Users/me/Documents`, the parent becomes `/Users/me`.

What if you're in `/Users/me/Documents` and you type:

```bash
cd ../..
```

This sends you to `/Users`. In other words, it sends you to your parent's parent.

There is another special directory to remember, and that is the `~` directory. The `~` directory always takes you to your Home folder. If my Home folder is `/Users/joe`, then typing:

```bash
cd ~
```

takes me straight to my Home folder and:

```bash
cd ~/Documents
```

takes me to `/Users/joe/Documents`.

Now we know how to get somewhere, but how do we know where we are?

### Print Working Directory

We can find out where we are in the file system by using the `pwd` command:

```bash
pwd

/Users/joe
```

That's it. `pwd` just tells you where you are as an absolute path.

But how do we find out what's here?

### List Directory Contents

To find out what is in the current folder, use the command `ls`:

```bash
ls

05-introduction-to-programming introduction-to-tools
Definitions.md                 linear-data-structures
README.md                      logical-branching
SUMMARY.md                     non-linear-data-structures
arrays-and-loops               package-lock.json
book.json                      package.json
command-line-programs          styles
introduction-to-objects
```

`ls` lists out all of the folders and files (sometimes color coded) that are in the current folder. This lets you navigate easier and make sure that you have what it is you're looking for.

If you want more details on those files and folders, you can pass some command line arguments to ls, like this:

```bash
ls -al

total 64
drwxr-xr-x  19 joe  staff   608 Jan 14 10:36 .
drwx------+ 30 joe  staff   960 Jan  2 14:13 ..
drwxr-xr-x  15 joe  staff   480 Jan 14 10:41 .git
-rw-r--r--   1 joe  staff   907 Dec 15 14:47 .gitignore
drwxr-xr-x   6 joe  staff   192 Dec 15 14:47 05-introduction-to-programming
-rw-r--r--   1 joe  staff  2755 Jan 14 10:36 Definitions.md
-rw-r--r--   1 joe  staff   526 Jan 14 10:36 README.md
-rw-r--r--   1 joe  staff  5618 Jan 14 10:36 SUMMARY.md
drwxr-xr-x   5 joe  staff   160 Jan 14 10:36 arrays-and-loops
-rw-r--r--   1 joe  staff   289 Jan 14 10:36 book.json
drwxr-xr-x   9 joe  staff   288 Jan 14 09:05 command-line-programs
drwxr-xr-x  10 joe  staff   320 Jan 14 09:05 introduction-to-objects
drwxr-xr-x   5 joe  staff   160 Jan 14 09:05 introduction-to-tools
drwxr-xr-x  10 joe  staff   320 Jan 14 10:36 linear-data-structures
drwxr-xr-x   8 joe  staff   256 Jan 14 09:05 logical-branching
drwxr-xr-x   8 joe  staff   256 Jan 14 10:36 non-linear-data-structures
-rw-r--r--   1 joe  staff   291 Jan 14 09:05 package-lock.json
-rw-r--r--   1 joe  staff   412 Jan 14 09:05 package.json
drwxr-xr-x   3 joe  staff    96 Jan 14 10:36 styles
```

Using the two flags `-al`, which stands for `all` and `long`, gives us many more details on the children of this folder and shows files that are typically hidden (start )with a .).

## Working with Files and Folders

When we want to work with the files and folders on our computers, there are a couple of useful commands that we'll need to know.

### Moving Files and Folders

To move a file or a folder to a different place, use the `mv` command. It's first argument is the file or folder you want to move and the second argument is where to move it to.

For example, if I wanted to move a file called `readme.txt` from my home folder to my `Documents` folder, I could do it like this:

```bash
mv /Users/joe/readme.txt /Users/joe/Documents/
```

An easier way might be:

```bash
mv ~/readme.txt ~/Documents/
```

This command is also how you rename things. If I wanted to rename `readme.txt` to `info.txt`, I could do that like this:

```bash
mv readme.txt info.txt
```

### Copying Files and Folders

You can also copy files and folders, putting a duplicate in another location without getting rid of the original.

To copy a file, use the `cp` command like this:

```bash
cp readme.txt info.txt
```

After this runs, you still have your original `readme.txt`, but you have a new file called `info.txt` with the same exact content.

To copy a folder, you need to add a `-r` to the command:

```bash
cp -r Documents Documents_backup
```

This duplicates the `Documents` folder *and all of its contents*, so be very careful running it on a folder with a lot of content.

#### Making Folders

You can also make an empty folder at any time with the `mkdir` command:

```bash
mkdir my_stuff
```

