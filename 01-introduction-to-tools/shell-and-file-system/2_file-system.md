# File System

All modern operating systems such as OS X, Windows, and Linux have file systems. They are an important component in any operating system, and while they may differ from each other in implementation and capabilities across the platforms, they all share the same basic concept of organizing the raw data recorded on a storage medium into files and folders.

## Files, Folders, and Hierarchical Structure

### Files

Files are the basic building blocks of any file system. In general, most users understand that documents, spreadsheets, pictures, and songs are files, even if they don't always think of them as such.  Files have names and various attributes such as the date and time they were created, a size, and permissions limiting who can access them. Files can be copied, moved, renamed, and even deleted.

### Folders

Folders, sometimes referred to as directories, hold files. In fact, a file cannot exist outside of a folder. ***All files reside in some folder***. In addition to files, folders may also contain other folders, a.k.a., subfolders. The subfolders, in turn, may hold other files and folders. The nesting of folders can be virtually unlimited (within the constraints of a give file system), and means the file system has a hierarchical structure.

<!-- Link to stylized image of folders and files -->

Like files, folders have names and many of the same attributes as folders.  Folders can also be copied, moved, renamed, and deleted.

### Special Folders

There are a number of special folders present in all modern file systems. In addition to the built-in folders such as Desktop, Documents, Downloads, Movies, Music, and Pictures, there are two key folders you will work with as a programmer.

#### Root

The root folder is the ***absolute*** beginning of any file system. It is the base folder that holds all other files and folders, either directly, or indirectly.

In OS X and Linux, the root is referred to as `/`, and contains files and folders from one to many disks, or other storage media.

Under Windows, the root is associated with drive-letters such as `C:\` or `D:\`. Unlike OS X and Linux, there is not necessarily a single ***root*** in the Windows file system, each drive-letter may be thought of as having its own root.

> We won't spend much time on Windows drive-letters during the cohort in preference of the `/` of Unix-based operating systems like OS X and Linux.  See, [File System and the Shell](4_file-system-and-shell.md).

#### Home

The Home folder is the folder associated with a user's login name where the special folders mentioned earlier reside. The majority of files and folders the user works with and creates are located here.

## GUI Tools

Both Windows and OS X provide GUI tools to explore and manage their respective file systems. These tools allow users to easily locate, copy, move, delete, and rename files and folders. While useful, most computer professionals prefer to use Shell commands to accomplish these tasks (See, [File System and the Shell](4_file-system-and-shell.md))

### File Explorer (formerly "Windows Explorer")

![](resources/images/file-explorer-icon.png)

There are several ways to launch File Explorer from anywhere in Windows 10. 

The second way is to hit `⊞-E`

> `⊞` is used to represent the Windows Key ![](resources/images/windows-key.png)

The third is to hit `⊞-S` and type 

![](resources/images/windows-search-file-explorer.png)

<!-- Link to Windows Explorer video -->

<!-- Link to Windows Explorer pdf -->

### OS X Finder

The most typical way of launching the Finder is to click on its icon in the Dock. Another available method is to hit ⌘-spacebar and type "finder" in the Spotlight Search. The Spotlight Search searches for the nearest match from the first character you type. The search narrows as you enter more characters. You may stop typing when the Finder icon appears. Simply press return to launch Finder.

![](resources/images/spotlight-search-terminal.png)

Two-minute video overview of the OS X Finder.

{% video %}https://www.youtube.com/watch?v=zx4iAJ5J04A {% endvideo %}

<!-- Link to Finder pdf -->


