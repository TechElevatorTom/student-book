# File System and Shell Objectives

Files and folders are one of the great, unsung achievements in computing. For most of us, they're  documents, speadsheets, pictures, and songs. Many of us don't even think of them as files as we share a photo with the tap of a finger. Programmers, on the other hand, have a much more direct relationship with files and folders. We work with them on a daily basis, and we need to know how to locate and manipulate them.

By the end of this section you should have an understanding of:

* The basic File System, and its hierarchical structure of files and folders.
* What a shell is, and how to launch it.
* How to navigate through the File System from inside the shell.
* Creating, copying, moving, and removing files and folders using the command line.

