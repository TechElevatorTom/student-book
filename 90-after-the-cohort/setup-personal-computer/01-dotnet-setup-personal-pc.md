# .Net on Personal Windows 10 PC

The following instructions are for setting up the Tech Elevator .Net development environment on a Windows 10 PC (laptop or desktop). They are primarily intended for students who wish to recreate the Tech Elevator .Net environment on their personal computer after the cohort ends.

## Basic Installation

### Hardware Requirements

The following are the recommended **minimums**.

* Intel i5 (or comparable 64-bit CPU)
* 8GB RAM 
* 128GB Disk Space Available

### Software

General information and advice for installing the software.<br/>
* Windows 10 Home or better is required.
* Step-by-step installation instructions are not provided. Most installations now-a-days are straightforward affairs of reviewing the options, taking the defaults, and clicking "Next" repeatedly until finished. Any specific installation instructions for a product are called out below as necessary.
* The URLs listed under each product may change at any time, be prepared to google for new URLs if the old ones don't work.
* If offered a choice between 32-bit and 64-bit versions of software, **always choose 64-bit**.

**Chrome and/or Firefox**<br/>
https://www.google.com/chrome/<br/>
https://www.mozilla.org/en-US/firefox/<br/>
It's your choice which browser to install. You might even consider both.<br/><br/>
Recommended extensions/add-ons:
* Vue.js devtools &#8212; Available for both Chrome and Firefox.

**.Net Core**<br/>
https://dotnet.microsoft.com/download/dotnet-core<br/>
Download and install the "recommended" version of .Net Core. At the time of this writing, the "recommended" version is .Net Core 2.2.106 when using Visual Studio 2017.<br/>

**Visual Studio 2017 (Community)**<br/>
https://visualstudio.microsoft.com/vs/community/<br/>
You should install the following workloads:
* .Net desktop development
* ASP&#46;Net and web development <!--- Escaped period keeps markdown from treating ASP-dot-Net as a link -->
* Azure development
* Data storage and processing
* Node.js

Recommended individual components (in addition to the many default components already checked):
* Class Designer

Recommended extensions (added after Visual Studio installed):
* Live Share - Microsoft.VisualStudio.LiveShare.vsix

**Visual Studio Code**<br/>
https://code.visualstudio.com/download<br/>
Recommended extensions (added after Visual Studio Code installed):
* Code Spell Checker - streetsidesoftware.code-spell-checker
* Debugger for Chrome - msjsdiag.debugger-for-chrome
* ESLint - dbaeumer.vscode-eslint
* Live Server - ritwickdey.liveserver
* Live Share - ms-vsliveshare.vsliveshare
* Markdown PDF - yzane.markdown-pdf
* Vetur - octref.vetur

**SqL Server 2017 (Express)**<br/>
https://www.microsoft.com/en-us/sql-server/sql-server-editions-express<br/>
Select the "Basic" installation type for the install.

**SqL Server Management Studio**<br/>
https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms<br/>
Download and install the "GA" release of Sql Server Management Studio.

**Git**<br/>
https://git-scm.com/downloads<br/>
Download and install the latest Windows version.

**node / npm**<br/>
https://nodejs.org/en/download/<br/>
Download and install the latest LTS Windows version of node (includes npm).

**Vue CLI**<br/>
https://cli.vuejs.org/<br/>
Install the latest release of Vue CLI using npm. Specific instructions can be found on the Vue CLI site.

**Postman**<br/>
https://www.getpostman.com/downloads/<br>
Download and install the latest Windows version of Postman.

**Fiddler**<br/>
https://www.telerik.com/download/fiddler<br/>
Download and install the latest Windows version of Fiddler.

## Completing Installation
There are no additional steps needed to complete setting up the Tech Elevator .Net development environment beyond the installation of the software listed above.



