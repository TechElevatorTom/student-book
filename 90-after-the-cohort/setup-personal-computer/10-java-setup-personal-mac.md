# Java on Personal Mac

The following instructions are for setting up the Tech Elevator Java development environment on a personal Mac (MacBook, iMac, Mac mini). They are primarily intended for students who wish to recreate the Tech Elevator Java environment on their personal computer after their cohort ends.

## Basic Installation

### Hardware Requirements

The following are the recommended **minimums**.

* Intel i5
* 8GB RAM
* 128GB Disk Space Available

### Software

General information and advice for installing the software.<br/>
* OS X 10.9 Mavericks or higher is required.
* Step-by-step installation instructions are not provided. Most installations now-a-days are straightforward affairs of reviewing the options, taking the defaults, and clicking "Next" repeatedly until finished. Any specific installation instructions for a product are called out below as necessary.
* The URLs listed under each product may change at any time, be prepared to google for new URLs if the old ones don't work.
* If offered a choice between 32-bit and 64-bit versions of software, **always choose 64-bit**.

**Chrome and/or Firefox**<br/>
https://www.google.com/chrome/<br/>
https://www.mozilla.org/en-US/firefox/<br/>
It's your choice which browser to install. You might even consider both.<br/><br/>
Recommended extensions/add-ons:
* Vue.js devtools &#8212; Available for both Chrome and Firefox.

**Java Development Kit (JDK), Standard Edition, 1.8 or higher**<br/>
https://www.oracle.com/technetwork/java/javase/downloads/index.html<br/>
Download and install the **Mac OS X x64** version of the ***JDK***, not the JRE. Tech Elevator has standardized on JDK 1.8.

**Eclipse**<br/>
https://www.eclipse.org/downloads/<br/>
Download and launch the latest version of the Eclipse Installer. Once running, the installer offers several Eclipse packages to choose from, select *"Eclipse IDE for Enterprise Java Developers"*. 

**Note**, *the installer requires Java in order to run*, **make sure the JDK has been installed**.

**Visual Studio Code**<br/>
https://code.visualstudio.com/download<br/>
Recommended extensions (added after Visual Studio Code installed):
* Code Spell Checker - streetsidesoftware.code-spell-checker
* Debugger for Chrome - msjsdiag.debugger-for-chrome
* ESLint - dbaeumer.vscode-eslint
* Live Server - ritwickdey.liveserver
* Live Share - ms-vsliveshare.vsliveshare
* Markdown PDF - yzane.markdown-pdf
* Vetur - octref.vetur

**Apache Maven**<br/>
https://maven.apache.org/download.cgi<br/>
You should simply download the "binary zip archive" of Maven for now. Additional setup instructions are provided under *"Completing Installation"*.

**Apache Tomcat**<br/>
https://tomcat.apache.org/download-90.cgi<br/>
You should simply download the "Core zip" Tomcat for now. Additional setup instructions are provided under *"Completing Installation"*.

**Postgres.app**<br/>
https://www.enterprisedb.com/downloads/postgres-postgresql-downloads<br/>
Download and install the latest Postgre.app.

**DBVisualizer**<br/>
https://www.dbvis.com/download/10.0<br/>
Since the JDK is already installed, download and install the "macOS" version of DBVisualizer **without Java VM**.

**Git**<br/>
https://git-scm.com/downloads<br/>
Download and install the latest Mac version.

**node / npm**<br/>
https://nodejs.org/en/download/<br/>
Download and install the latest LTS "macOS Installer" version of node (includes npm)..

**Vue CLI**<br/>
https://cli.vuejs.org/<br/>
Install the latest release of Vue CLI using npm. Specific instructions can be found on the Vue CLI site.

**Postman**<br/>
https://www.getpostman.com/downloads/<br/>
Download and install the latest Mac version of Postman

## Completing Installation
Now that all required software has been installed, or at least downloaded, it's time to finish things up.

### Maven and Tomcat

Open the Terminal, and navigate to your home directory. Once there, perform the following commands.

```
cd ~
cd mkdir dev-tools
cd dev-tools
unzip ~/Downloads/apache-maven-version-number-bin.zip
unzip ~/Downloads/apache-tomcat-version-number-windows-x64.zip
```
where `version-number` is a placeholder for the actual version number of Maven and Tomcat respectively. For example:
`apache-maven-3.6.0-bin.zip` and `apache-tomcat-9.0.19.zip`.  The last two commands will "unzip" Maven and Tomcat into your `dev-tools` folder.

### Path

Now that Maven and Tomcat have been "unzipped" into `dev-tools`, it's time to add Maven to the Path. There are several ways to accomplish this, the easiest is to simply run the following command in the Terminal.

```
$ echo 'export PATH=~/dev-tools/apache-maven-version-number/bin:$PATH' >> ~/.bashrc
```

Once again, `version-number` is a placeholder for the actual version number of Maven. You need to substitute the actual version number for the placeholder prior to running the command in the Terminal.

### TE Archetype Installation

Staying in the Terminal, enter the following commands to clone and register the Tech Elevator Archetypes.

```
$ cd ~/dev-tools
$ git clone https://bitbucket.org/te-curriculum/te-mvn-archetypes
$ cd te-mvn-archetypes/te-basic
$ mvn install
$ cd ../te-spring-jdbc
$ mvn install
$ cd ../te-spring-mvc
$ mvn install
```

**Note**, the install must be evoked from within each archetype folder. Also, verify each Maven installation ends with a `[INFO] Build Success` message.

You may close the Terminal after you have installed all the Tech Elevator Archetypes.

### Eclipse Maven and Tomcat Integration

Assuming Eclipse has already been installed, launch it. Once Eclipse is running, go to Preferences under the Eclipse menu, and navigate to Maven &rarr; Archetypes. Click "Add Local Catalog".

![Preferences, Maven, Archetypes, Add Local Catalog](images/mac-preferences-maven-archetypes-add-local-catalog.png)

A secondary dialog opens, browse to the `te-archetype-catalog.xml` file under the `dev-tools/te-mvn-archetypes` folder. Enter any description you care to.

![Local Archetype Catalog](images/mac-local-archetype-catalog.png)

Click "OK" when done.

Next, navigate to the Server &rarr; Runtime Environments under Preferences, and click the Add button.

![Preferences, Server, Runtime Environments](images/mac-preferences-server-runtime-environments-add.png)

A wizard will open, select Apache Tomcat 9 from the list of runtime environments, make sure the checkbox is checked.

![Select Tomcat 9 Runtime.](images/mac-select-tomcat-9.png)

Click "Next" to move onto the last panel in the wizard.

Browse to the Tomcat folder under `dev-tools` and click "Finish" when done.

![Browse to Tomcat Folder](images/mac-browse-to-tomcat-folder.png)

Click "Apply and Close" once you have been returned to the Preferences dialog.

 **Note**, you will need to repeat these steps for both Maven and Tomcat any time you switch to a "new" workspace folder under Eclipse. Eclipse records preferences on a workspace-by-workspace basis.

This completes the installation of the Tech Elevator Java environment on your PC.
