# Java on Personal Windows 10 PC

The following instructions are for setting up the Tech Elevator Java development environment on a Windows 10 PC (laptop or desktop). They are primarily intended for students who wish to recreate the Tech Elevator Java environment on their personal computer after their cohort ends.

## Basic Installation

### Hardware Requirements

The following are the recommended **minimums**.

* Intel i5 (or comparable 64-bit CPU)
* 8GB RAM
* 128GB Disk Space Available

### Software

General information and advice for installing the software.<br/>
* Windows 10 Home or better is required.
* Step-by-step installation instructions are not provided. Most installations now-a-days are straightforward affairs of reviewing the options, taking the defaults, and clicking "Next" repeatedly until finished. Any specific installation instructions for a product are called out below as necessary.
* The URLs listed under each product may change at any time, be prepared to google for new URLs if the old ones don't work.
* If offered a choice between 32-bit and 64-bit versions of software, **always choose 64-bit**.

**Chrome and/or Firefox**<br/>
https://www.google.com/chrome/<br/>
https://www.mozilla.org/en-US/firefox/<br/>
It's your choice which browser to install. You might even consider both.<br/><br/>
Recommended extensions/add-ons:
* Vue.js devtools &#8212; Available for both Chrome and Firefox.

**Java Development Kit (JDK), Standard Edition, 1.8 or higher**<br/>
https://www.oracle.com/technetwork/java/javase/downloads/index.html<br/>
Download and install the **Windows x64** version of the ***JDK***, not the JRE. Tech Elevator has standardized on JDK 1.8.

**Eclipse**<br/>
https://www.eclipse.org/downloads/<br/>
Download and launch the latest version of the Eclipse Installer. Once running, the installer offers several Eclipse packages to choose from, select *"Eclipse IDE for Enterprise Java Developers"*. 

**Note**, *the installer requires Java in order to run*, *make sure the JDK has been installed*.

**Visual Studio Code**<br/>
https://code.visualstudio.com/download<br/>
Recommended extensions (added after Visual Studio Code installed):
* Code Spell Checker - streetsidesoftware.code-spell-checker
* Debugger for Chrome - msjsdiag.debugger-for-chrome
* ESLint - dbaeumer.vscode-eslint
* Live Server - ritwickdey.liveserver
* Live Share - ms-vsliveshare.vsliveshare
* Markdown PDF - yzane.markdown-pdf
* Vetur - octref.vetur

**Apache Maven**<br/>
https://maven.apache.org/download.cgi<br/>
You should simply download the "binary zip archive" of Maven for now. Additional setup instructions are provided under *"Completing Installation"*.

**Apache Tomcat**<br/>
https://tomcat.apache.org/download-90.cgi<br/>
You should simply download the "Core 64-bit Windows zip" Tomcat for now. Additional setup instructions are provided under *"Completing Installation"*.

**PostgreSQL**<br/>
https://www.enterprisedb.com/downloads/postgres-postgresql-downloads<br/>
Download and install the latest Windows version of PostgreSQL from Enterprise DB.  It is recommended you uncheck StackBuilder when installing. Also, set the `postgres` admin account's password to `postgres1` as this is the password used in most Tech Elevator exercises and capstones.

**Note**, If you are familiar with Postgres.app, it is only available for the Mac. Windows requires a standard PostgreSQL install.

**DBVisualizer**<br/>
https://www.dbvis.com/download/10.0<br/>
Since the JDK is already installed, download and install the Windows version of DBVisualizer **without Java VM**.

**Git**<br/>
https://git-scm.com/downloads<br/>
Download and install the latest Windows version.

**node / npm**<br/>
https://nodejs.org/en/download/<br/>
Download and install the latest LTS Windows version of node (includes npm)..

**Vue CLI**<br/>
https://cli.vuejs.org/<br/>
Install the latest release of Vue CLI using npm. Specific instructions can be found on the Vue CLI site.

**Postman**<br/>
https://www.getpostman.com/downloads/<br/>
Download and install the latest Windows version of Postman

## Completing Installation

Now that all required software has been installed, or at least downloaded, it's time to finish things up.

### Maven and Tomcat

Open File Explorer, and navigate to where you downloaded Maven and Tomcat. Right-click on Maven, and Extract All.

![File Explore, Maven, and Extract All](images/pc-file-explorer-maven-extract-all.png)

A secondary dialog will open prompting you to choose a destination for the extraction. Browse to your home directory (`C:\Users\your-username`), and select the `dev-tools` folder. (Click "New Folder" in Browse dialog to create the `dev-tools` folder if one is not already in your home.)

![Extract Maven into dev-tools](images/pc-maven-extract-into-dev-tools.png)

Click the "Extract" button when ready.

Repeat for Tomcat by extracting it into the `dev-tools` folder in a similar manner once you are done with Maven.

After extracting Maven and Tomcat, you should have folders for each under your dev-tools folder.
```
C:\Users\your-username\dev-tools/apache-maven-version-number
C:\Users\your-username\dev-tools/apache-tomcat-version-number
```
where `version-number` is a placeholder for the actual version number of Maven and Tomcat respectively. For example:
`Maven 3.6.0` and `Tomcat 9.0.19`.

### Path

Now that Maven and Tomcat have been extracted into `dev-tools`, it's time to add Maven to the Path. Start by opening the System Properties dialog, and clicking the "Environment Variables" button. This will open the "Environment Variables" dialog, select "Path" under "User variables" and click the "Edit" button. (The System Properties dialog can be opened from the Control Panel under System & Security -> System -> Advanced system settings.)

![System Properties, Environment Variables, Path, and Edit](images/pc-sys-props-env-vars-path-edit.png)

The "Edit Environment Variable" dialog will open, click the "Browse" button and navigate to the `bin` folder under Maven in the `dev-tools` folder (`C:\Users\your-username\dev-tools\apache-maven-version-number\bin`).

![Browse to Maven Path](images/pc-browse-to-maven-path.png)

Click "OK" repeatedly to dismiss all the dialogs opened to add Maven to the Path..

### TE Archetype Installation

These next steps are easiest from the command line, open a Command Prompt (Run as Administrator), navigate to `C:\Users\your-username\dev-tools`, and clone the Tech Elevator Archetypes from the Tech Elevator Bitbucket repository.

```
> git clone https://bitbucket.org/te-curriculum/te-mvn-archetypes
```

Once the archetypes have been cloned, you will need to install them in your local Maven repository. Beginning with the `te-basic` archetype, perform the following commands.

```
> cd te-mvn-archetypes\te-basic
> mvn install
> cd ..\te-spring-jdbc
> mvn install
> cd ..\te-spring-mvc
> mvn install
```

**Note**, the install must be evoked from within each archetype folder. Also, verify each Maven installation ends with a `[INFO] Build Success` message.

You may close the Command Prompt after you have installed all the Tech Elevator Archetypes.

### Eclipse Maven and Tomcat Integration

Assuming Eclipse has already been installed, launch it. Once Eclipse is running, go to Preferences under the Windows menu, and navigate to Maven &rarr; Archetypes. Click "Add Local Catalog".

![Preferences, Maven, Archetypes, Add Local Catalog](images/pc-preferences-maven-archetypes-add-local-catalog.png)

A secondary dialog opens, browse to the `te-archetype-catalog.xml` file under the `dev-tools/te-mvn-archetypes` folder. Enter any description you care to.

![Local Archetype Catalog](images/pc-local-archetype-catalog.png)

Click "OK" when done.

Next, navigate to the Server &rarr; Runtime Environments under Preferences, and click the Add button.

![Preferences, Server, Runtime Environments](images/pc-preferences-server-runtime-environments-add.png)

A wizard will open, select Apache Tomcat 9 from the list of runtime environments, make sure the checkbox is checked.

![Select Tomcat 9 Runtime.](images/pc-select-tomcat-9.png)

Click "Next" to move onto the last panel in the wizard.

Browse to the Tomcat folder under `dev-tools` and click "Finish" when done.

![Browse to Tomcat Folder](images/pc-browse-to-tomcat-folder.png)

Click "Apply and Close" once you have been returned to the Preferences dialog.

 **Note**, you will need to repeat these steps for both Maven and Tomcat any time you switch to a "new" workspace folder under Eclipse. Eclipse records preferences on a workspace-by-workspace basis.

This completes the installation of the Tech Elevator Java environment on your PC.
