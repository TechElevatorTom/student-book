# Overloaded Constructors

In the following example, we create an overloaded constructor that allows the programmer to create a clock with default values or to specify their own values when instantiating a new clock.

{% if book.language === 'Java' %}

{% tabs first="Clock.java", second="Program.java" %}
{% content "first" %}
```java
// Clock.java
public class Clock {
    private int hour;
    private int minute;
    private int second;

    // Overloaded Constructor
    public Clock() {
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
    }

    // Constructor
    public Clock(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public void tick(int numberOfTimes) {
        for(int i = 1; i <= numberOfTimes; i++) {
            this.tick();
        }
    }

    public void tick() {
        this.second += 1;

        if (this.second >= 60) {
            this.minute += 1;
            this.second = 0;
        }

        if (this.minute >= 60) {
            this.hour += 1;
            this.minute = 0;
        }

        if (this.hour >= 24) {
            this.hour = 0;
        }
    }

    // Getters and setters
    public String getCurrentTime() {
        return String.format("%02d:%02d:%02d", this.hour, this.minute, this.second);
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    public int getSecond() {
        return this.second;
    }
}
```

{% content "second" %}
```java
// Program.cs
public static void main(String[] args) {
    Clock grandfather = new Clock(11, 17, 42);

    // Prints "Current time is 11:17:42"
    System.out.println("Current time is " + grandfather.getCurrentTime());

    Clock watch = new Clock();

    // Prints "Current time is 00:00:00"
    System.out.println("Current time is " + watch.getCurrentTime());
}
```
{% endtabs %}

{% elif book.language === 'C#' %}

{% tabs first="Clock.cs", second="Program.cs" %}
{% content "first" %}
```csharp
// Clock.cs
public class Clock
{
    public int Hour { get; set; }
    public int Minute { get; set; }
    public int Second { get; set; }    
    
    public Clock()
    {
        this.Hour = 0;
        this.Minute = 0;
        this.Second = 0;
    }

    public Clock(int hour, int minute, int second)
    {
        this.Hour = hour;
        this.Minute = minute;
        this.Second = second;
    }
}
```

{% content "second" %}
```csharp
// Program.cs
public static void Main(string[] args)
{
    // Creates a new clock initialized at 11:17:42
    Clock grandfather = new Clock(11, 17, 42); 

    // Creates a new clock initialized at 00:00:00
    Clock watch = new Clock();
}
```
{% endtabs %}

{% endif %}
