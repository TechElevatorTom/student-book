# Building A Real Application

Each of these different concepts can be difficult to find ways in which they can be applicable. To that end, we've also prepared a video series that go with the content of the next few chapters to put these concepts in perspective.

{% if book.language === 'Java' %}

{% video %}https://www.youtube.com/watch?v=yuDRo7mnytA {% endvideo %}

{% elif book.language === 'C#' %}

{% video %}https://www.youtube.com/watch?v=BxGFiPd0YDY {% endvideo %}

{% endif %}
