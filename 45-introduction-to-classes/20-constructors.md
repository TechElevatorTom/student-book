# Constructors

When classes are defined, you want to make sure it can be instantiated into a useable state. That being, when the new statement is used to create a new object we should be able to provide any arguments our class needs to allow usability immediately after. This is done through something called a constructor.

A constructor is a special method of a class that initializes an object of that type. It is pretty easy to spot a constructor. All constructors have the same name as the class that they are in.

Here you'll see our Clock class has a Clock constructor allowing users the ability to set the clock to a specific time when instantiated.

{% if book.language === 'Java' %}

{% tabs first="Clock.java", second="Program.java" %}
{% content "first" %}
```java
// Clock.java
public class Clock {
    private int hour;
    private int minute;
    private int second;

    // Constructor has same name and spelling of class.
    // Notice there isn't a return type with constructors.    
    public Clock(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }
}
```
{% content "second" %}
```java
// Program.java
public static void main(String[] args) {
    // Creates a new clock initialized at 11:17:42
    Clock grandfather = new Clock(11, 17, 42); 

    // Creates a new clock initialized at 16:11:01
    Clock watch = new Clock(16, 11, 1); 
}
```
{% endtabs %}

{% elif book.language === 'C#' %}

{% tabs first="Clock.cs", second="Program.cs" %}
{% content "first" %}
```csharp
// Clock.cs
public class Clock
{
    public int Hour { get; set; }
    public int Minute { get; set; }
    public int Second { get; set; }    

    // Constructor has same name and spelling of class.
    // Notice there isn't a return type with constructors.    
    public Clock(int hour, int minute, int second)
    {
        this.Hour = hour;
        this.Minute = minute;
        this.Second = second;
    }

    public void Tick()
    {
        this.Second += 1;

        if (this.Second >= 60)
        {
            this.Minute += 1;
            this.Second = 0;
        }

        if (this.Minute >= 60)
        {
            this.Hour += 1;
            this.Minute = 0;
        }

        if (this.Hour >= 24)
        {
            this.Hour = 0;
        }
    }    
}
```

{% content "second" %}
```csharp
// Program.cs
public static void Main(string[] args)
{
    // Creates a new clock initialized at 11:17:42
    Clock grandfather = new Clock(11, 17, 42); 

    // Creates a new clock initialized at 16:11:01
    Clock watch = new Clock(16, 11, 1);
}
```
{% endtabs %}

{% endif %}


