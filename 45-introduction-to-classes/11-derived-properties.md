# Derived Properties

Sometimes we want properties to represent state of an object, but we need it to be dependent on other properties. For example a full name is derived or generated using the combination of "First Last".

Let's add a property that lets our clock return the current time (e.g. "01:10:42"). We don't need a fourth property with its own setter. Creating one allows the possibility that the value it returns contradics the value stored in the hour, minute, and second properties.

A derived property will let us generate the value by relying on the other properties within the class.

{% if book.language === 'Java' %}

{% tabs first="Clock.java", second="Program.java" %}
{% content "first" %}
```java
// Clock.java
public class Clock {
    private int hour;
    private int minute;
    private int second;

    public String getCurrentTime() {
        return this.hour + ":" + this.minute + ":" + this.second;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    public int getSecond() {
        return this.second;
    }
}
```
{% content "second" %}
```java
// Program.java
public static void main(String[] args) {

    Clock grandfather = new Clock();
    
    // Set the time to 12:22:32
    grandfather.setHour(12);
    grandfather.setMinute(22);
    grandfather.setSecond(32);    

    // Prints "Current time is 12:22:32
    System.out.println("Current time is " + grandfather.getCurrentTime());
}
```
{% endtabs %}

{% elif book.language === 'C#' %}

{% tabs first="Clock.cs", second="Program.cs" %}
{% content "first" %}
```csharp
// Clock.cs
public class Clock
{
    public int Hour { get; set; }
    public int Minute { get; set; }
    public int Second { get; set; }

    // Derived Property, getter with no setter
    public string CurrentTime
    {
        get
        {
            return $"{this.Hour}:{this.Minute}:{this.Second}";
        }
    }
}
```
{% content "second" %}
```csharp
// Program.cs
public static void Main(string[] args)
{
    Clock clock = new Clock();
    
    // Set the time to 12:22:32
    clock.Hour = 12;
    clock.Minute = 22;
    clock.Second = 32;    

    // Prints "Current time is 12:22:32
    Console.WriteLine($"Current time is {clock.CurrentTime}");
}
```
{% endtabs %}

{% endif %}