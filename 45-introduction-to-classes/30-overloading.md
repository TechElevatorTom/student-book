# Overloading Methods & Constructors

If we want to provide users of our class different options when they instantiate new objects or invoke our methods we can create an *overloaded method* or *overloaded constructor*.

As a programmer when you overload a method or constructor, you add flexibility to your class so that users can leverage different inputs when interacting with it. 

The rules of an overloaded method are pretty short:

1. Overloaded methods must **have the same name**.
1. Overloaded methods must **have the same return type** (does not apply to constructors).
1. Overloaded methods must **differ in the number of parameters and/or parameter types**.



