# Classes

Programmers often need to solve problems that the 
{{book.language}} language doesn't have an out-of-the-box solution for. E-commerce sites need "shopping carts." Social media sites require "user profiles."

To create programs like these, programmers need to model this data and represent it as a custom data type. Using classes, they can define what characteristics and behaviors a data type has. Think of a clock, for example:

* a clock has a current time, based on an hour, minute, and second
* a clock can be set to a specific time when it is "plugged in"
* clocks track time by ticking one second at a time

Defining characteristics and behaviors within a class helps programmers write clean code. Code remains in its place, and can be reused at any time.  Programmers can create as many objects from a blueprint as needed, and the objects still follow the rules.

Now, you will build your own application from an object-based design perspective. First, you will create **classes**, which serve as blueprints that indicate what variables and methods an object has.  Variables and properties represent an object's **state**, and methods indicate an object's **behaviors**.

To review the differences between classes and objects, visit the [Introduction to Objects](../introduction-to-objects/01_intro.md) chapter.

It takes time to become proficient in object modeling.  This tutorial covers the basics.

## Creating Classes

The following example shows how a class is created.

{% if book.language === 'Java' %}

```java
// Clock.java
public class Clock {

}
```

{% elif book.language === 'C#' %}

```csharp
// Clock.cs
public class Clock
{

}
```
{% endif %}

The keyword `class` allows you to create your own data type.

## Creating an Instance of a Class

To declare variables, you might type the following:

    int count = 18;
    string name = "Bill";

To declare a new instance of your class, type something similar:

    Clock clock = new Clock();

This code declares a variable of type Clock, named `clock`. The next part of the expression `new Clock()` allocates memory to hold a new instance of the Clock class. The `clock` variable stores a reference to that memory location.

> ####Info::Reference Types
>
> Remember to use the `new` keyword when working with reference types. Each variable references a different memory location on the heap where the object resides.

