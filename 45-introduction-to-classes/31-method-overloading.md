# Method Overloading

In the following example, we create an overloaded method that allows the programmer to "tick" the clock a number of times. 


{% if book.language === 'Java' %}

{% tabs first="Clock.java", second="Program.java" %}
{% content "first" %}
```java
// Clock.java
public class Clock {
    private int hour;
    private int minute;
    private int second;

    // Constructor
    public Clock(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    // Overloaded method, relies on other tick method
    // but allows users to pass an int to say how many
    // times it ticks
    public void tick(int numberOfTimes) {
        for(int i = 1; i <= numberOfTimes; i++) {
            this.tick();
        }
    }

    public void tick() {
        this.second += 1;

        if (this.second >= 60) {
            this.minute += 1;
            this.second = 0;
        }

        if (this.minute >= 60) {
            this.hour += 1;
            this.minute = 0;
        }

        if (this.hour >= 24) {
            this.hour = 0;
        }
    }

    // Getters and setters
    public String getCurrentTime() {
        return String.format("%02d:%02d:%02d", this.hour, this.minute, this.second);
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    public int getSecond() {
        return this.second;
    }
}
```

{% content "second" %}
```java
// Program.cs
public static void main(String[] args) {
    Clock grandfather = new Clock(11, 17, 42);
    
    // tick the clock 5 times
    grandfather.tick(5);// time will now be 11:17:47
    
    // tick the clock 1 time
    grandfather.tick(); // time will now be 11:17:48

    // Prints "Current time is 11:17:48
    System.out.println("Current time is " + grandfather.getCurrentTime());
}
```
{% endtabs %}

{% elif book.language === 'C#' %}

{% tabs first="Clock.cs", second="Program.cs" %}
{% content "first" %}
```csharp
// Clock.cs
public class Clock
{
    public int Hour { get; private set; }
    public int Minute { get; private set; }
    public int Second { get; private set; }

    public Clock(int hour, int minute, int second)
    {
        this.Hour = hour;
        this.Minute = minute;
        this.Second = second;
    }    

    // Overloaded method, relies on other Tick method
    public void Tick(int numberOfTimes)
    {
        for (int i = 1; i <= numberOfTimes; i++)
        {
            this.Tick();    // calls Tick method below
        }
    }

    public void Tick()
    {
        this.Second += 1;

        if (this.Second > 60)
        {
            this.Minute += 1;
            this.Second = 0;
        }

        if (this.Minute > 60)
        {
            this.Hour += 1;
            this.Minute = 0;
        }

        if (this.Hour > 24)
        {
            this.Hour = 0;
        }
    }    
}
```
{% content "second" %}
```csharp
// Program.cs
public static void Main(string[] args)
{
    // Creates a new clock initialized at 11:17:42
    Clock grandfather = new Clock(11, 17, 42); 

    grandfather.Tick(5); // <-- changes time to 11:17:47
    grandfather.Tick();  // <-- changes time to 11:17:48
}
```
{% endtabs %}

{% endif %}