# Properties

Let's say we want our class to have some different characteristics. Since its a clock, it should represent what the current hour, minute, and second is. Properties are used to let us add *state* to the class. Each individual instance will keep track of its own separate state.

The following syntax adds properties to a class.

{% if book.language === 'Java' %}

{% tabs first="Clock.java", second="Program.java" %}
{% content "first" %}
```java
// Clock.java
public class Clock {
    private int hour;
    private int minute;
    private int second;

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    public int getSecond() {
        return this.second;
    }
}
```
{% content "second" %}
```java
// Program.java
public static void main(String[] args)
{

    Clock grandfather = new Clock();
    
    // Set the time to 12:22:32
    grandfather.setHour(12);
    grandfather.setMinute(22);
    grandfather.setSecond(32);    
}
```
{% endtabs %}


> ####Note::Coding Convention
>
> In Java, all properties are accessed through methods called Getters and Setters. These are named based on the property they are for with either the word `get` or `set` before them.

We've defined three properties of type int named: hour, minute, and second. This will allow us to set these properties in our class and they will be stored throughout the lifetime of each instance.


{% elif book.language === 'C#' %}

{% tabs first="Clock.cs", second="Program.cs" %}
{% content "first" %}
```csharp
// Clock.cs
public class Clock
{
    public int Hour { get; set; }
    public int Minute { get; set; }
    public int Second { get; set; }
}
```
{% content "second" %}
```csharp
// Program.cs
public static void Main(string[] args)
{
    // Instantiate a new instance of a clock
    Clock clock = new Clock();
    
    // Set the time to 12:22:32
    clock.Hour = 12;
    clock.Minute = 22;
    clock.Second = 32;    
}
```
{% endtabs %}



> ####Note::Coding Convention
>
> In C#, properties are written following the Pascal Case naming conventions.

We've defined three properties of type int named: Hour, Minute, and Second. This will allow us to set these properties in our class and they will be stored throughout the lifetime of each instance.

C# properties work similar to variables except when written the ability to retrieve or assign the value is indicated with a getter and/or a setter. They keyword `get` and `set` are placed within a pair of { and } braces. This approach is called automatic properties.

Another alternative way of declaring properties in C# involves creating something called a backing field. This field is often a private variable declared as part of of the class.

```csharp
public class Clock
{
    private int hour;
    public int Hour
    {
        get { return this.hour; }
        set { this.hour = value; }
    }    
}
```

With this example, the "Hour property" is used to allow access to retrieve or set the value of the "hour variable". The data resides in the hour variable and the property acts as an access control mechanism. 

This full property syntax allows us the ability to manage the value of our properties with greater control (e.g. we could only permit values within a given range of 1 and 24). In actuality, the first approach with automatic properties, generates the second approach when compiled.

{% endif %}

> ####Note::A note about `this`
>
> The `this` keyword is a special type of reference variable available within each class (and its members). It allows code within our classes to have a variable that refers to "this specific instance".

You may notice the usage or public and private. Those are called access modifiers and we will go over that in one of the following sections.


