# Methods

When we need an object to "do something" or perform a behavior, methods can be used.

In the case of our clock, it can *tick*. When we invoke the tick method, it will change the state of the class by ticking forward one second at a time.

In the following code sample we show how to add a Tick method that changes the state of the object.

{% if book.language === 'Java' %}

{% tabs first="Clock.java", second="Program.java" %}
{% content "first" %}
```java
// Clock.java
public class Clock {
    private int hour;
    private int minute;
    private int second;

    // tick() method changes the value of hour, minute, and second
    // void means nothing is returned
    public void tick() {
        this.second += 1;

        if (this.second >= 60) {
            this.minute += 1;
            this.second = 0;
        }

        if (this.minute >= 60) {
            this.hour += 1;
            this.minute = 0;
        }

        if (this.hour >= 24) {
            this.hour = 0;
        }
    }

    public String getCurrentTime() {
        return String.format("%02d:%02d:%02d", this.hour, this.minute, this.second);
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    public int getSecond() {
        return this.second;
    }
}
```

{% content "second" %}
```java
// Program.cs
public static void main(String[] args) {
    Clock grandfather = new Clock();
    
    // Set the time to 12:22:32
    grandfather.setHour(12);
    grandfather.setMinute(22);
    grandfather.setSecond(32);

    grandfather.tick(); // time will be 12:22:33
    grandfather.tick(); // time will be 12:22:24

    // Prints "Current time is 12:22:34
    System.out.println("Current time is " + grandfather.getCurrentTime());
}
```
{% endtabs %}

{% elif book.language === 'C#' %}

{% tabs first="Clock.cs", second="Program.cs" %}
{% content "first" %}
```csharp
// Clock.cs
public class Clock
{
    public int Hour { get; set; }
    public int Minute { get; set; }
    public int Second { get; set; }

    // Tick() method changes the value of hour, minute, and second
    // void means nothing is returned
    public void Tick()
    {
        this.Second += 1;

        if (this.Second >= 60)
        {
            this.Minute += 1;
            this.Second = 0;
        }

        if (this.Minute >= 60)
        {
            this.Hour += 1;
            this.Minute = 0;
        }

        if (this.Hour >= 24)
        {
            this.Hour = 0;
        }
    }
}
```

{% content "second" %}
```csharp
// Program.cs
public static void Main(string[] args)
{
    Clock clock = new Clock();

    clock.Hour = 11;
    clock.Minute = 23;
    clock.Second = 30;
        
    clock.Tick(); // <-- changes time to 11:23:31
    clock.Tick(); // <-- changes time to 11:23:32
}
```
{% endtabs %}

{% endif %}

Using methods allows us to encapsulate the logic needed to make our clock function.  Encapsulation allows us to restrict access to the internal mechanism of how the class works by hiding or protecting its variables so that the values remain consistent and don't contradict each other.

Rather than having the user of the clock change the second, minute, and hour manually, the clock will tick and internally manage when the values need to increment by one or rollover to zero. Ideally this limits the risk of bugs that appear in our code.

> ####Note::Void Return Type
>
> Some methods don't have a return value. Those methods indicate so in the method signature with `void`.
