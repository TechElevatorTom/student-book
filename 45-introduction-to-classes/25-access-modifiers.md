# Access Modifiers

The keywords public and private are two of the most common access modifiers in the {{ book.language }} language. Access modifiers are used to control visbility to methods and properties to the rest of our program.

## public

The modifier keyword `public` is used to mark something as publicly accessible, meaning it can be used or invoked by external callers. 

By making our Clock class public, `public class Clock` any other program that references our code can instantiate it.

By marking a property or method public, we are indicating that it can be used by any external caller. An external caller may be another program outside of our program or a class within our own application.

## private

The `private` access modifier is used to restrict access to type members. Private variables and methods are only visible from within the class that the member is declared.

{% if book.language === 'Java' %}

In our clock example, our class variables hour, minute, and second are all designated `private.` If we try and access them through an instance of our class, we will receive a compile error. 

This allows us to hide our implementation away from users or outside callers of the class. Because its only exposed through the getter and setter methods, we now have the ability to restrict the values and/or access for the variable.

{% tabs first="Clock.java", second="Program.java" %}
{% content "first" %}
```java
// Clock.java
public class Clock {
    private int hour;
    private int minute;
    private int second;

    public Clock(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

     public void setHour(int hour) {
        if(hour >= 1 && hour <= 24) {
            this.hour = hour;
        }
    }

    public void setMinute(int minute) {
        if(minute >=0 && minute < 60) {
            this.minute = minute;
        }
    }

    public void setSecond(int second) {
        if(second >=0 && second < 60) {
            this.second = second;
        }
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    public int getSecond() {
        return this.second;
    }
}
```
{% content "second" %}
```java
// Program.java
public static void main(String[] args) {
    // Creates a new clock initialized at 11:17:42
    Clock grandfather = new Clock(11, 17, 42); 

    grandfather.hour = 12; //<-- not allowed
    grandfather.setHour(12);
}
```
{% endtabs %}

{% elif book.language === 'C#' %}

In this example we modify our properties as `private set` so that their values can only be changed within the class.

{% tabs first="Clock.cs", second="Program.cs" %}
{% content "first" %}
```csharp
// Clock.cs
public class Clock
{
    public int Hour { get; private set; }
    public int Minute { get; private set; }
    public int Second { get; private set; }

    public Clock(int hour, int minute, int second)
    {
        this.Hour = hour;
        this.Minute = minute;
        this.Second = second;
    }    

    public void Tick()
    {
        this.Second += 1;

        if (this.Second >= 60)
        {
            this.Minute += 1;
            this.Second = 0;
        }

        if (this.Minute >= 60)
        {
            this.Hour += 1;
            this.Minute = 0;
        }

        if (this.Hour >= 24)
        {
            this.Hour = 0;
        }
    }    
}
```
{% content "second" %}
```csharp
// Program.cs
public static void Main(string[] args)
{
    Clock clock = new Clock();
    clock.Hour = 1; // <-- can't access Hour because it does not have a public setter
}
```
{% endtabs %}

{% endif %}

If it helps, think about classes as something seen from the outside, not internally. Public is what we want the other programmers to see. It should be reserved for things that are useful to help someone perform a task.

Knowing there might be a lot of code to make things work under the hood, its not necessary for programmers to see everything. In those cases we keep things private so that they are only accessible from within the class.

