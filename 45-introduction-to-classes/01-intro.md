# Introduction to Classes

We started working with the primitive data types and they can be disappointingly simplistic. Arrays add some new capability but they're fixed in size. Fortunately the collections framework provides us an opportunity to work with more complex objects created from class definitions. 

Still, what if we wanted to create our own data type? Like the time on a clock?

    int hour = 11;
    int minute = 23;
    int second = 42;


That works but if we wanted our program to store multiple clocks, it could become difficult.

{% if book.language === 'Java' %}

```java
int[] hours = new int[] { 11, 8 };
int[] minutes = new int[] { 23, 17 };
int[] seconds = new int[] { 42, 58 };
```

{% elif book.language === 'C#' %}

```csharp
List<int> hours = new List<int>() { 11, 8 };
List<int> minutes = new List<int>() { 23, 17 };
List<int> seconds = new List<int>() { 42, 58 };
```

{% endif %}

This becomes a real issue as we need to ensure all of the values remain in sync and that we don't inadvertently update `hours[0]` and `minutes[1]`.

Here we will learn how to create our own data type that has these properties, like hour and minute. Just like a clock, our data type can tick, counting up second by second.

In order to do this, we'll use something called classes. This chapter will focus on the mechanics of creating classes. Additionally you will learn:

* How a class is defined
* Class variables, methods, and properties
* How to create a constructor
* The difference between `public` and `private`

At the end of the chapter is a video, belonging to a three-part series that gives an example of using classes to build an application.