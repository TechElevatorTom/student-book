## Assignment Statement

An assignment sets the value in a storage location denoted by a variable name.


## Boolean Expression

An expression using comparison or logical operators and the evaluated value is a boolean result.

## Branch

A branch represents an independent line of development separate from the main development.

## Camel Case

A naming convention where the first letter starts with a lower-case letter and each subsequent word starts with a capital letter.

## Code Smell

A code smell is any symptom in the code of a program that possibly indicates there is a deeper problem, violation of fundamental design principles, or an impact to design quality. Duplicate code is often an indication of a code smell. 

## Constant

A constant is a value that cannot be altered during execution of the program.

## Constructor

A constructor is a special method of a class that initializes an object of that type. Constructors have the same name as the class and can be used to set the values to default values or user-defined values.


## Declaration Statement

A declaration statement introduces a new variable by providing a data type and a name that it can be referenced by.

## Exception

An anomaly or exceptional condition that requires special processing.

## Instantiate

Instantiation is the creation of an instance of an object from a template such as a class.

## Integrated Development Environment (IDE)

A software application that unifies several programming tools commonly used by programmers to develop software. An IDE normally consists of a source code editor with intelligent code completion, build and run automation, and a debugger.

## Interfaces

An interface is a contract. It specifies how a software component could be used by indicating what behavior it needs to implement but not how.

## Layer of Abstraction

A concept that sits on top of a more basic concept that makes is easier to solve problems. For example, a programming language like Java or C# is a layer of abstraction on top of the C programming language. C, in turn, is a layer of abstraction on top of the underlying CPU and hardware the program is running on.

## Local Repository

A local repository refers to a copy of a git repository that resides on your computer.

## Loosely Coupled

Loose coupling reduces the dependencies that one class has on another so that it depends on it to the least extent possible.

## Method Signature

Part of the method declaration that indicates the return type, name of the method, and input parameters.

## Overloaded Method

Method overloading allows a class to have several methods with the same name for flexibility. In order for it to be valid though, the parameter signatures must be different between each overloaded method.

## Pascal Case

A naming convention where the first letter starts with an upper-case letter and each subsequent word starts with a capital letter.

## Platform

A platform is the basic hardware and software on which applications can be run.

## Remote Repository

A remote repository is a copy of a git repository located on a centralized server. Team members push commits to the remote repository when they are ready to share with the rest of the team.

## Scope

Scope refers to the visibility of variables. If code can access a variable, it is _in scope_. If code cannot access a variable, it is _out of scope_.

## Tightly Coupled

Tightly coupled code tends to make changes in one module have a ripple effect throughout the other modules due to inter-module dependency. Tightly coupled code is not flexible.