# Listening for Events in JavaScript

Reacting to events in JavaScript will require three things from you:

1. A DOM element that you want to listen to events on
2. A specific event that you want to listen to
3. A function that holds the logic that you want to execute

You already know how to select DOM elements. But how do you know what events you can listen for on a certain element?

## Types of Events

All DOM elements can receive the following events:

1. Mouse Events
    1. `click` - a user has clicked on the DOM element
    2. `dblclick` - a user has double clicked on the DOM element
    3. `mouseover` - a user has moved their mouse over the DOM element
    4. `mouseout` - a user has moved their mouse out of the DOM element

Input elements, like `<input>`, `<select>`, and `<textarea>`, also trigger these events:

1. Input Events
    1. `keydown` - a user pressed down a key (including shift, alt, etc.) while on this DOM element
    2. `keyup` - a user released a key (including shift, alt, etc.) while on this DOM element
    3. `change` - a user has finished changing the value of this input element
    4. `focus` - a user has selected this input element for editing
    5. `blur` - a user has unselected this input element for editing

Form elements have these events:

1. Form Events
    1. `submit` - a user has submitted this form using a submit button or by hitting Enter on a text input element
    2. `reset` - a user has reset this form using a reset button

There are many more events that can be listened for but these are the ones you will be using 99% of the time. You can find more at the [MDN documentation for events](https://developer.mozilla.org/en-US/docs/Web/Events).

## Adding Event Handlers to DOM Elements

Imagine that you want to react to a user clicking on a button. When they click the button, you're going to change the text in an `<h1>` to be different. The HTML for this might look something like this:

```html
<h1 id="greeting">Hello</h1>
<button id="change-greeting">Change Greeting</button>
```

First, write a JavaScript function that will do the logic of what you want.

```javascript
function changeGreeting() {
    let greetingHeader = document.getElementById('greeting');
    greetingHeader.innerText = 'Goodbye';
}
```

Now you'll want to get the DOM element you want to listen to the event on.

```javascript
let changeButton = document.getElementById('change-greeting');
```

Then you attach the function to run whenever the button gets clicked.

```javascript
changeButton.addEventListener('click', (event) => {
    changeGreeting();
});
```

And that's the extent of event handling.

## Event Handling using Anonymous Functions

You could also get the same functionality as above by attaching an anonymous function as the event listener instead of calling a named function:

```javascript
changeButton.addEventListener('click', (event) => {
    let greetingHeader = document.getElementById('greeting');
    greetingHeader.innerText = 'Goodbye';
});
```

These two examples are functionally the same, but you might have reasons to use one over the other.

> #### Notice::Best Practice and Tech Elevator Convention
>
> You will see in all of the examples that the event listeners are structured as two different functions. Tech Elevator highly encourages you to follow this convention too.
>
> The structure will be that you first write your named function that will handle the functionality. This is called the **event handler**.
>
> ```javascript
> function changeGreeting() {
>  let greetingHeader = document.getElementById('greeting');
>  greetingHeader.innerText = 'Goodbye';
> }
> ```
>
> And then call that in an `addEventListener` call. This is called the **event listener**.
>
> ```javascript
> let changeButton = document.getElementById('change-greeting');
> changeButton.addEventListener('click', (event) => {
>  changeGreeting();
> });
> ```
>
> So the event listener is listening for the event and, when the event happens, calls the event handler to handle the logic for the event.
>
> This is best practice because it makes the event handler easy to test. It is just a normal JavaScript function that you already know how to unit test and it makes your code more flexible and able to handle HTML structure changes in the future.

## The Event Object

All event handlers will receive an object when the event is triggered called the Event Object. This object holds a number of important properties that you can use to get information about what the event was.

To use this object, you can write your event handlers to take a parameter that will hold the event object:

```javascript
(event) => {
    ...
}
```

With this parameter in your function, you have access to an object that has details of the triggered event, including:

| Property     | Found In    | Purpose   |
|----------|---------|--------|
| currentTarget | All events | Holds the element that the event was triggered on, ie. the button clicked or the select box that changed |
| clientX       | Mouse events | The X coordinate on the screen of the click |
| clientY       | Mouse events | The Y coordinate on the screen of the click |
| altKey, metaKey, ctrlKey, shiftKey | Mouse and Keyboard events | A boolean on whether the specified key was pressed down during the event |
| key           | Keyboard events | The key that was pressed, taking the Shift key into account. Arrow keys show up as 'ArrowRight', 'ArrowDown', 'ArrowLeft', and 'ArrowUp'|

More information on Events and information in Event objects can be found on the [MDN page for the UIEvent object](https://developer.mozilla.org/en-US/docs/Web/API/UIEvent).