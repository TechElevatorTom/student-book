# The Browser Event Model

Browser events work in what is commonly called a Publish and Subscribe manner. Publish and Subscribe is a programmatic way to pass messages between different parts of a system while keeping those different parts decoupled from each other, meaning that the parts don't have to know about each other, they just need to know which messages to watch out for.

Publishing means that a part of the system can put messages out for other parts to act on and Subscribe means that a part can listen for certain messages to be published and perform logic because of it.

Back to a browser example, when a user clicks on a link, the UI can publish--or in browser speak, trigger--a 'click' event on that link and another part of the browser can subscribe to--or in browser speak, listen for--that event and load the link as a new page.

This is called an event driven interface and most GUI applications follow this model in some way or another. With this model, you're not looking to run your program's logic from start to finish but you are going to write your program to listen for user triggered events and then run a small piece of logic in response to it. That logic can range anywhere from adding a class to a table row to saving a form full of user data to a back-end system.

## Where do events happen?

In the browser, events are always attached to DOM elements. You can listen to events on links, buttons, input elements, tables, table rows and any other DOM element on the page.

When you want to listen for an event in your JavaScript, you will first need to select the DOM element that you want to listen for events on. Then you will attach function to the DOM element that you want triggered when the event happens.

## What are the events?

When an event is triggered, that event might have some information that gets sent along with it. If it is a mouse event, you will get the X and Y coordinates of where it happened. If it is a keyboard event, you will get the key that was pressed. This information can allow you to create very powerful user interface interactions with your JavaScript.

In the next section, you'll see how to listen for events from the browser and run your own JavaScript logic when they happen.