# Event Handling in JavaScript

When users interact with a web page--by scrolling, moving their mouse over elements, or clicking on forms or buttons--we can trigger certain JavaScript logic to respond to those events. In JavaScript, this is called Event Handling.

Built into the browser is a system that constantly notifies and processes events that are happening while the page is loaded. This includes browser events, like the DOM being successfully loaded or state changes in the display of the page, and user events, like mouse clicks or key presses. This system is open enough that we can write JavaScript to listen for certain events and have our logic run as well when things happen. We can also write JavaScript that triggers certain events to mimic browser activity or create our own custom events to add more functionality to our application.

In the following chapters, we will be exploring the browsers event model and how it handles and triggers events that we can listen for in our application.