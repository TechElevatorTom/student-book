# Conditional Code

There are a number of different choices available to programmers who want their code to take various paths based on a `true` or `false` condition.

The most common approach developers will use to allow their code to take various paths uses `if`, `if/else`, or `if/else if/else`.

With each of the following examples any usage of the word `condition` can be replaced by a boolean expression that can consist of a simple boolean variable or a more complex one evaluated from comparison and logical operators.

## Single Conditions with `if`

The format for an `if` condition follows:

```
if (condition) {
    statement or block we perform if condition is true
}
```

## Two Paths with `if/else`

The format for an `if/else` condition follows:

```
if (condition) {
    statement or block we perform if condition is true
} else {
    statement or block we perform if condition is not true
}
```



> #### Caution::Don't add conditions for `else`
>
> Notice that the else keyword does not have any type of condition following it. There is no need to add a condition because `else` always executes if the condition in the `if` section is `false`.  

## Multiple Paths using `if/else if/else`

If you need to provide more than two paths for the code to take then an `if/else if` code branch can be used. With the `if/else if`, each time you write `if` then you need to supply an additional condition that indicates if the following code block should execute.

Once Java or C# runs into the first true condition, it executes the following block of code. After that block, the program resumes after the last conditional block.

```
if (condition) { 
    statement or block we do if the first condition is true
} else if (other condition) { 
    statement or block we do if the first condition is false
    and the second condition is true
} else if (other condition 2) { 
    statement or block we do if the first two conditions are false
    and the third condition is true
} else {
    statement or block we do if all the above conditions are false
}
```

> #### Note::`Else` not required
>
> The `else` is not required. It is only useful if you want a default option in the event that all conditions before are `false`.

&nbsp;

> #### Info::Prioritize your conditional checks
>
> When chaining multiple `if/else if` blocks make sure to put your most exclusive option first. Programs stop at the first condition they find to be true. If there are less exclusive options first, then there is a risk that the code block you thought would run does not. Consider the classic FizzBuzz problem which has programmers return 'Fizz' if a number is divisible by 3, 'Buzz' if the number is divisible by 5, and 'FizzBuzz' if the number is divisible by 3 and 5.
>
> ```
> int n = 15;
>
> if (n % 5 == 0) {
>   // Fizz
> } else if (n % 3 == 0) {
>   // Buzz  
> } else if (n % 3 == 0 && n % 5 == 0) {
>   // FizzBuzz    
> }
> 
> ```
>
> The program will never find FizzBuzz. As soon as `15 % 5  == 0` is evaluated and true, the remaining conditions are never evaluated and the result will be Fizz.

## Choosing between two values

There's another type of conditional operator that acts a lot like an if statement but is just used to select between two different values. This operator is called a Ternary Operator. It looks something like this:

``` java
    conditional ? true_value : false_value
```

This may look odd at first but this `if` shorthand can come in handy when formatting data to print to a user.

The way the ternary works is that first the condition is checked. If the condition is true, the operator returns the value after the `?`, but if it's false, it returns the value after the `:`. You won't use this often, but it becomes very handy in a statement like the following:

{% if book.language === "Java" %}
``` java
    int numOfBananas = ...;
    String message = 
        "We have " + numOfBananas + " banana" + (numOfBananas != 1 ? "s" : "");
```
{% elif book.language === 'C#' %}
``` csharp
    int numOfBananas = ...;
    string message = 
        "We have " + numOfBananas + " banana" + (numOfBananas != 1 ? "s" : "");
```
{% endif %}

Which will only put an "s" on banana if these isn't just one banana remaining.