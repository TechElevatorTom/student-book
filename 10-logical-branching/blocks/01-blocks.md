
# Blocks

Each of the sections of code that follow an `if` or an `else` statement are called blocks. Blocks use a pair of braces **{..}** to enclose single or multiple lines of code. While they may seem to make life hard for us early on they actually are very useful by enhancing code readability and dealing with something called variable scope.

The below code is enclosed in a block that only executes when the condition is true. **Note the indentation that occurs when you write code inside of a code block.**

```
if (condition) { 
    //statement or block of code to run when condition is true    
}
```

## Scope

In C# and Java, you can declare a variable at any point in a block, but you **must** declare it before you use it. Once declared, the variable is _in scope_. Variables are in scope until the end of the block when they are discarded and go _out of scope_.

## Nested Blocks

Whether you have complex conditional logic or you are mixing multiple loops with conditions, blocks are often nested. Each nested block can declare and use its own set of local variables:


```csharp
{ //outer block
    int i;
    { //inner block
        int j;
    }
}
```

The variable `j` has the scope of the inner block. Statements within the inner block can use both `i` and `j`. Statements in the outer block can only use `i`. If the following code was written a compilation error would occur because `j` would be out of scope.

```csharp
{
    int i;
    {
        int j;
    }
    j = 33; //<--- not allowed, variable out of scope
}
```

C# and Java also won't allow a variable in an inner block have the same name as a variable in an outer block. This will also create a compilation error.

```csharp
{
    int i;
    {
        int i;  //<-- not allowed, variable already in scope
    }
}
```