# Logical Operators

The comparison operators are limited in their ability to compare values between two different types. When a boolean expression needs to take two or more considerations into account to evaluate a boolean value (e.g. is the number negative AND is it even) then the logical operators are used.

The logical operators are: AND (`&&`), OR (`||`), XOR (`^`), and NOT (`!`). These operators work with one or more boolean operands to evaluate a boolean expression and yield `true` or `false.` 

## NOT `!`

When placed in front of a boolean value, the logical not operator negates the value of the boolean operand.

## AND `&&`

If both of the operands on each side of the `&&` are `true`, the result of the expression is `true`. If one of them is `false`, the result is `false`. Consider the variable called `width` used in the following expression:

```
(width >= 0.5) && (width <= 5.0)
```

This expression is true if the `width` is between 0.5 and 5.0.

## OR `||`

If either of the operands on each side of the `||` are `true`, then the result of the expression is also `true`. The expression is only `false` when both operands are `false`. For example consider the variable called `width` used in the following expression:

```
width < 0.5 || width > 5.0
```

This expression is true if the `width` is less than 0.5 or greater than 5.0.

## XOR `^`

If one operand in the expression is `true` and the other operand is `false`, then the result of the expression is `true`. In an exclusive-or whenever both operands have the same value, then the expression is `false`.

This expression is not used as often as the logical AND and logical OR.

## Logical Operator Table

The following table can be used to sum up the different logical operators

| `A`       | `B`       | `!A`  | `A && B` | <code>A &#124;&#124; B</code>      | `A ^ B` |
|-----------|-----------|-------|----------|------------------------------------|---------|
| **TRUE**  | **TRUE**  | FALSE | TRUE     | TRUE                               | FALSE   |
| **TRUE**  | **FALSE** | FALSE | FALSE    | TRUE                               | TRUE    |
| **FALSE** | **TRUE**  | TRUE  | FALSE    | TRUE                               | TRUE    |
| **FALSE** | **FALSE** | TRUE  | FALSE    | FALSE                              | FALSE   |

Using one of our previous hypothetical examples, if we wanted to find out if a number was negative AND even, it could be written like so:

{% if book.language === "Java" %}
```java
int number = 5;

boolean isItEven = (number % 2 == 0);      //true if number is even
boolean isItNegative = (number < 0);       //true if number is less than but not equal to 0

boolean answer = isItEven && isItNegative; //true if isItEven is true and isItNegative is true
```
{% elif book.language === 'C#' %}
```csharp
int number = 5;

bool isItEven = (number % 2 == 0);      //true if number is even
bool isItNegative = (number < 0);       //true if number is less than but not equal to 0

bool answer = isItEven && isItNegative; //true if isItEven is true and isItNegative is true
```
{% endif %}