# Comparison Operators

As programmers when we create applications we often deal with simple arithmetic comparisons to allow our website visitors to make a choice or present them with different options. For example, we may want to:

* calculate if a number is odd or even to create alternating row colors in web pages
* validate that there are enough tickets available before allowing a user to reserve a seat for an event
* check to see if the user has not been to the site within 90 days to show them a reminder

The following comparison operators allow us to make these comparisons.

| Operator | Meaning                    |
|----------|----------------------------|
| `==`     | Equal To                   |
| `!=`     | Not Equal To               |
| `>`      | Greater Than               |
| `<`      | Less Than                  |
| `>=`     | Greater Than or Equal To   |
| `<=`     | Less Than or Equal To      |

When used in an expression, these comparison operators always evaluate to a boolean `true` or `false` value. For example:

{% if book.language === "Java" %}
```java
int number = 5;

boolean isItFive = (number == 5);      //true if number is '5'
boolean isItEven = (number % 2) == 0;  //true if number is evenly divisible by '2'
boolean isItNegative = (number < 0);   //true if number is less than but not equal to 0
```
{% elif book.language === 'C#' %}
```csharp
int number = 5;

bool isItFive = (number == 5);      //true if number is '5'
bool isItEven = (number % 2) == 0;  //true if number is evenly divisible by '2'
bool isItNegative = (number < 0);   //true if number is less than but not equal to 0
```
{% endif %}