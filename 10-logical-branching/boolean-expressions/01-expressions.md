
# Boolean Expressions

In the previous chapter we learned an expression always evaluates to a single value. Outside of arithmetic expressions, one of the other common types of expressions is a boolean expression. This type of expression always evaluates to `true` or `false`.

There are two common ways that boolean expressions are built:
* comparison operators (to compare two values)
* logical operators (to create relationships between one or more boolean values)

