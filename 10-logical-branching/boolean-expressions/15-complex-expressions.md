# Reading Complex Expressions

In programming, it is often likely that you will encounter many different combinations of the above comparison and logical operators used in an expression. Boolean expressions can be combined.

Assume two integers `a` and `b` are used in the following expression.

{% if book.language === "Java" %}
```java
int a, b;
boolean output = (a >= 10 && a <= 20) || (b >= 10 && b <= 20);  
```
{% elif book.language === 'C#' %}
```csharp
int a, b;
bool output = (a >= 10 && a <= 20) || (b >= 10 && b <= 20);  
```
{% endif %}

In the above expression `output` is only true if `a` or `b` is between 10 and 20. Let's give `a` the value 8 and `b` the value of 13 and see how the expression is simplified step-by-step. 

First the value for the variables are replaced within the expressions.
{% if book.language === "Java" %}
```java
boolean output = (8 >= 10 && 8 <= 20) || (13 >= 10 && 13 <= 20);
```
{% elif book.language === 'C#' %}
```csharp
bool output = (8 >= 10 && 8 <= 20) || (13 >= 10 && 13 <= 20);
```
{% endif %}

Next, the following comparison operations are evaluated, simplifying the expression:
* `8 >= 10` is `false`
* `8 <= 20` is `true`
* `13 >= 10` is `true`
* `13 <= 20` is `true`

{% if book.language === "Java" %}
```java
boolean output = (false && true) || (true && true);                  
```
{% elif book.language === 'C#' %}
```csharp
bool output = (false && true) || (true && true);                  
```
{% endif %}

After the comparison operations, then the logical expressions are evaluated:
* `false && true` yields `false`
* `true && true` yields `true`.

{% if book.language === "Java" %}
```java
boolean output = (false) || (true);                  
```
{% elif book.language === 'C#' %}
```csharp
bool output = (false) || (true);                  
```
{% endif %}

Lastly `false || true` is evaluated and the final value is `true`. The complex expression from a few steps back is simplified into:

{% if book.language === "Java" %}
```java
boolean output = true;                  
```
{% elif book.language === 'C#' %}
```csharp
bool output = true;
```
{% endif %}