

# Methods

In programming, methods are used to break programs up into small repeatable units of code. They allow programmers to create blocks of code that perform a single action or calculation where the answer only changes based on the inputs provided to it. In later lessons we will explore this further but as a start it is important that we begin to read and understand the  two parts of a method: the method signature and the method body.

The components of a method signature are:

* the return type
* the method name
* the parameter list

![Method Signature](../resources/method-signature.png)

## Return Type

Methods return to the code that invoked it when it completes all the statements in the method, reaches a return statement, or runs into an error (we call them exceptions), whichever occurs first.

A method's return type is declared in the method signature. Within the body of the method, the return statement is used to return a value of that type. 

We will see this later, but any method declared `void` doesn't return a value. It does not need to contain a return statement, but it may do so in order for the method to end early. It will look like this.

```
return;
```

If you try to return a value from a method that is declared void, you will get a compiler error.

Any method that is not declared void must contain a return statement with a corresponding return value, like this:

```
return returnValue;
```

The data type of `returnValue` must match the data type declared in the method signature.

## Method Name

The method name is a clear name indicating what type of action or calculation the method performs when invoked. It is similar to variable names in that unclear variable names don't provide any meaning to what they actually hold. Here unclear method names mislead the programmer into knowing what the method actually does. 

In Java, method names are camel case, whereas C# methods are pascal case.

## Parameter List

Parameters act like variables. They have a data type, a name, and their value can be obtained by using it in a code statement or expression. When methods are created, parameter lists indicate what inputs are required for the method to run. 

When we write the bodies of methods, we don't necessarily know their values of parameters like we do with variables. This is challenging because we have to think of them just as their data type without ever knowing their specific value. For example consider the following method:

```
public int MultiplyBy(int multiplicand, int multiplier) { 
    int result = multiplicand * multiplier;

    return result;
}
```

This method indicates that it accepts two parameters and returns an integer. The first parameter acts as the multiplicand and the second, the multiplier.

As we will see in later parts of the cohort, we invoke these methods by calling their name and passing in parameters.

```
int output = MultiplyBy(3, 4);  //invokes MultiplyBy method. 3 is the multiplicand, 4 is multiplier
// ...
// ...
output = MultiplyBy(9, 2);      //invokes MultiplyBy method. 9 is the multiplicand, 2 is multiplier
```

Each time a program calls the MultiplyBy method it _passes_ values for the multiplicand and the multiplier. Every time the MultiplyBy method uses those values to calculate a new result.