## Stack

For some collections, throwing things into a list is not enough. Programmers may need to structure a collection to support a feature or another part of the program that they are writing. 

Another common type of collection is the stack. Stacks are last-in, first-out data structures (LIFO). The idea with stacks is that you can push items onto a stack and pop them off from the top first. It might be easy to visualize a stack of pancakes. You can pile them as high as you want, but you always eat the last pancake added to the stack on the top.

![Mmmmm, Pancakes](resources/pancakes.jpg)

For those familiar with the [_Tower of Hanoi_](https://en.wikipedia.org/wiki/Tower_of_Hanoi), it is a mathematical puzzle game in which the player needs to move all of the disks from the first rod to the last rod by moving one disk at a time. The game is solvable by working with three different stacks that you use to hold the disks. At no point in time do you interact with a disk on the stack if there are others on top of it.

> #### Info::Real World Usage
>
> Stacks are most often used when applications need to implement an undo feature. All of the actions the user performs are added to a stack. When the user decides to undo an operation, the most recent action is popped off the top of the stack.

Like lists, the stack is internally backed with an array. A stack class manages this array so that items are only added and removed from a particular position.

### Creating a Stack

{% if book.language === 'Java' %}

Declaring a new stack consists of the same three expressions to create any other object.

```java
Stack<String> names = new Stack<String>();
```

Again, the element type can be anything you want. Here, you create a stack that holds DateTime objects and another one that holds integers.

```java
Stack<DateTime> calendar = new Stack<DateTime>();
Stack<Integer> sequence = new Stack<Integer>();
```

{% elif book.language === 'C#' %}

Declaring a new stack consists of the same three expressions to create any other object.

```csharp
Stack<string> names = new Stack<string>();
```

Again, the element type can be anything you want. Here, you create a stack that holds DateTime objects, and another one that holds integers.

```csharp
Stack<DateTime> calendar = new Stack<DateTime>();
Stack<int> sequence = new Stack<int>();
```

{% endif %}

###  Adding Items to the Stack


{% if book.language === 'Java' %}

Stacks use a `.push()` method instead of the usual `.add()` method. This adds an item to the last available position in the underlying array.

```java
Stack<String> names = new Stack<String>();

names.push("C3PO");  // names now contains C3PO
names.push("R2-D2"); // names now contains R2-D2, C3PO
names.push("BB-8");  // names now contains BB-8, R2-D2, C3PO
```

{% elif book.language === 'C#' %}

Stacks use a Push method instead of the usual Add method. This adds an item to the last available position in the underlying array.

```csharp
Stack<string> names = new Stack<string>();

names.Push("C3PO");  // names now contains C3PO
names.Push("R2-D2"); // names now contains R2-D2, C3PO
names.Push("BB-8");  // names now contains BB-8, R2-D2, C3PO
```

{% endif %}

### Removing Items from the Stack


{% if book.language === 'Java' %}

The method for removing items is Pop. This always removes the last item that was added to the stack.

```java
Stack<String> names = new Stack<String>();

// ... BB-8, R2-D2, and C3PO are pushed onto the stack

String name = names.pop();  // name contains BB-8 since that was the last name placed on the stack 
                            // and names contains R2-D2, C3PO
```

If you try and pop an empty stack, an `EmptyStackException` occurs. If it doesn't sound good, it probably isn't. It's best to check the `.size()` to make sure at least one item exists before calling `.pop()`.

Most languages that support a stack have a way to check the next item. To do this, call the `.peek()` method.

{% elif book.language === 'C#' %}

The method for removing items is Pop. This always removes the last item that was added to the stack.

```csharp
Stack<string> names = new Stack<string>();

// ... BB-8, R2-D2, and C3PO are pushed onto the stack

string name = names.Pop();  // name contains BB-8 since that was the last name placed on the stack 
                            // and names contains R2-D2, C3PO
```

If you try and Pop an empty stack, an `InvalidOperationException` occurs. If it doesn't sound good, it probably isn't. It's best to check the Count property to make sure at least one item exists before calling Pop.

> ####Note::What Count Property?
>
> Collections all have something in common. They all implement an interface called `ICollection<T>` which requires that every collection must have a `Count` property. An interface is a contract that you will explore further as you get into object-oriented programming.

Most languages that support a stack have a way to check the next item. To do this, call the Peek method.

{% endif %}

### Looping Through a Stack

Stacks don't support a way of accessing specific elements by index. Doing so would let you go to the nth item directly. If you want to look at each item, you need to use a foreach iterator to go through the stack.

{% if book.language === 'Java' %}

```java
Stack<String> names = new Stack<String>();

//... push names on to the stack

for(String name : names) {
    // The name is not removed from the stack.
    System.out.println(name);
}
```

When you enumerate using a foreach loop, you can read items, but you can't modify the overall collection. If you want to look at each item and remove it with pop, you'll need a different way.

```java
Stack<String> names = new Stack<String>();

//... push names on to the stack

while (names.size() > 0)
{    
    String name = names.pop(); // the next item is removed from the stack
    System.out.println(name);    
}
```
{% elif book.language === 'C#' %}


```csharp
Stack<string> names = new Stack<string>();

//... push names on to the stack

foreach (string name in names)
{
    // The name is not removed from the stack.
    Console.WriteLine(name);
}
```

When you enumerate using foreach, you can read items, but you can't modify the overall collection. If you want to look at each item and remove it with Pop, you'll need a different way.

```csharp
Stack<string> names = new Stack<string>();

//... push names on to the stack

while (names.Count > 0)
{    
    string name = names.Pop(); // the next item is removed from the stack
    Console.WriteLine(name);    
}
```

{% endif %}

> ####Info::How Is This Useful
>
> Junior developers rarely need to create a stack while on the job.  Developers spend most of their time working with something called the **call stack**. The call stack stores information about all of the active methods a program executes. You'll see it more when you learn about exception handling.