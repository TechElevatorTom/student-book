## List

The list is by far the most popular collection type used in the {{book.language}} language. Like an array, a list serves as a general purpose type-safe collection, which allows it to manage any data type.  Also, like an array, the values are accessed by index. What differentiates lists from arrays is the methods provided to manipulate the state of the collection.

----

### Creating New Lists

Remember, creating objects in your code goes through three expressions that are usually, but not always, contained in the same statement of code.

1. **Declaration** - A declaration associates a new variable name with a defined data type. You've seen this before when working with variables.
2. **Instantiation** - Using `new` creates a new object in-memory and returns the address to it. You've seen `new` with arrays previously, and here it's doing the same thing - making a new object for you to use.
3. **Initialization** - Using a constructor sets some initial values on the created object's instance variables. Essentially, it starts an object off with a certain state.    

The code below shows how to declare and create a new empty list.

{% if book.language === 'Java' %}

```java
List<String> names = new ArrayList<String>();
```

This code creates a new instance of a list of strings.

The `new` keyword sets aside memory to hold this object. `ArrayList<String>` is what is created in memory. You invoke its constructor to make sure it initializes a new list with some default values. The address for this object is saved to the `names` variable.

Note that the data type for the `names` variable looks different.  This isn't a typo. The `names` variable is of type `List<String>`. This is called programming to an interface. It lets you write polymorphic, or interchangeable, code. As you learn more about the language, you'll see that there are other types of lists that could be used. This is allowed because they all share something in common: they implement the `List<T>` interface. This is covered in later chapters. 

If you want, you can create a list of integers: 

```java
List<Integer> ages = new ArrayList<Integer>();
```

Did you notice `Integer` instead of `int` in the above code? There's a reason for this:

> #### Info::There are class versions of primitives
>
> In Java, you can't put a primitive in any of the collection classes. It's a limitation of the language.
>
> But Java didn't want to leave the `int`s out completely, so they made an object version of `int`s called `Integer`. In fact, they did that with all the primitive data types, including `Double`, `Character`, and `Boolean`.
>
> So, you can now make a `List<Integer>` and put `int`s into it whenever you want.
>
> ```java
> List<Integer> myNumbers = new ArrayList<Integer>();
> myNumbers.put(5);
> ```


Or a list of DateTime objects.

```java
List<DateTime> appointments = new ArrayList<DateTime>();
```

Or any other data type, including your own. What if you had a data type that represents a house? 

```java
List<House> neighborhood = new ArrayList<House>();
```

You have created your own list that holds `House` objects.  Don't try typing it in right now - it won't work until you learn how to create your own classes. _With code, anything is possible!_

Take a look back at your list of names.  You can also create lists and initialize them with values if you know them ahead of time.

```java
List<String> names = new ArrayList<String>(Arrays.asList("Luke", "Leia", "Han"));
```

{% elif book.language === 'C#' %}

```csharp
List<string> names = new List<string>();
```

Now, you have created a new instance of a list of strings.

The `new` keyword sets aside memory to hold this object. `List<string>` is what is created in memory. You invoke its constructor to make sure it initializes a new list with some default values. The address for this object is saved to the `names` variable.

If you want, you can create a list of integers:

```csharp
List<int> ages = new List<int>();
```

Or a list of DateTime objects.

```csharp
List<DateTime> appointments = new List<DateTime>();
```

Or any other data type, including your own. What if you had a data type that represents a house? 

```csharp
List<House> neighborhood = new List<House>();
```
You have created your own list that holds `House` objects. Don't try typing it in right now - it won't work until you learn how to create your own classes. _With code, anything is possible!_

Take a look back at your list of names.  You can also create lists and initialize them with values if you know them ahead of time.

```csharp
List<string> names = new List<string>() { "Luke", "Leia", "Han" };
```

{% endif %}

----

### Accessing List Values

{% if book.language === 'Java' %}

Now that you're working with objects and not arrays, you have to get values out of the `List` differently. You do that with the `get()` method.
```java
List<String> names = new ArrayList<String>(Arrays.asList("Luke", "Leia", "Han"));

// The secret children of Darth Vader: Luke and Leia
System.out.println("The secret children of Darth Vader: " + names.get(0) + " and " + names.get(1));
```

{% elif book.language === 'C#' %}

C# lists support index notation as you saw previously with arrays.

```csharp
List<string> names = new List<string>() { "Luke", "Leia", "Han" };

// The secret children of Darth Vader: Luke and Leia
Console.WriteLine("The secret children of Darth Vader: " + names[0] + " and " + names[1]);
```

{% endif %}

Notice that lists are also a 0-based index system, just like arrays.

----

### Adding New Values

{% if book.language === 'Java' %}

Adding new values is easy if you use the `add()` method.

```java
List<String> names = new ArrayList<String>(Arrays.asList("Luke", "Leia", "Han"));
names.add("Chewbacca"); // names now contains Luke, Leia, Han, Chewbacca
```

The `add()` method only accepts arguments of the same type declared between the angle brackets. 

```java
List<Integer> ages = new ArrayList<Integer>();
ages.add("Hello");  //Will not compile
```

Lists also have an overloaded `add()` method that lets you set a value at a specific index in the array.

```java
List<String> names = new ArrayList<String>(Arrays.asList("Luke", "Leia", "Han"));
names.add(0, "Chewbacca");   // names now contains Chewbacca, Luke, Leia, Han
```
{% elif book.language === 'C#' %}

Adding new values is easy if you use the Add method.

```csharp
List<string> names = new List<string>() { "Luke", "Leia", "Han" };
names.Add("Chewbacca"); // names now contains Luke, Leia, Han, Chewbacca
```

The Add method only accepts arguments of the same type declared in `List<T>`. 

```csharp
List<int> ages = new List<int>();
ages.Add("Hello");  //Will not compile
```

Lists also have an Insert method to set a value at a specific index in the array.

```csharp
List<string> names = new List<string>() { "Luke", "Leia", "Han" };
names.Insert(0, "Chewbacca");   // names now contains Chewbacca, Luke, Leia, Han
```

{% endif %}

----

### Removing Values

{% if book.language === 'Java' %}

There are two approaches to removing items from a list. The first uses the `remove()` method, which accepts the value you want to remove.

```java
List<String> names = new ArrayList<String>(Arrays.asList("Luke", "Leia", "Han"));
names.remove("Luke");   // names now contains Leia, Han
```

Alternatively, you could use the `remove()` method if you know the specific index.

```java
List<String> names = new ArrayList<String>(Arrays.asList("Luke", "Leia", "Han"));
names.remove(1);   // names now contains Luke, Han
```

{% elif book.language === 'C#' %}

There are two approaches to removing items from a list. The first uses the Remove method, which accepts the value you want to remove.

```csharp
List<string> names = new List<string>() { "Luke", "Leia", "Han" };
names.Remove("Luke");   // names now contains Leia, Han
```

Alternatively, you could use the RemoveAt method if you know the specific index.

```csharp
List<string> names = new List<string>() { "Luke", "Leia", "Han" };
names.RemoveAt(1);   // names now contains Luke, Han
```
{% endif %}

----

### Iterating Through a List


{% if book.language === 'Java' %}

Collections also support a model of iteration called enumerators. Enumerators are a simple type that allow you to move forward through a set, one item at a time. You can use a new loop called a foreach loop to do this. The definition for the foreach loop looks like:

```java
for(<type> <identifier> : <expression>) {
    <body>
}
```

To do this for your list of strings, you would write the following: 

```java
List<String> names = new ArrayList<String>(Arrays.asList("Luke", "Leia", "Han"));
for(String name : names) {
    System.out.println("Character: " + name);
}
```

You can also use the for loop to move through the list.

```java
List<String> names = new ArrayList<String>(Arrays.asList("Luke", "Leia", "Han"));
for(int i = 0; i < names.size(); i++) {
    System.out.println("Character: " + names.get(i));
}
```

You might have noticed that we used a new method up above on the List. That method is called `size()`. It returns how many things are in the List.

{% elif book.language === 'C#' %}

Collections also support a model of iteration called enumerators. Enumerators are a simple type that allow you to move forward through a set, one item at a time. You can use a new loop called a foreach loop to do this. The definition for the foreach loop looks like:

```csharp
foreach(<type> <identifier> in <expression>) 
{
    <body>
}
```

To do this for your list of strings, you would write the following: 

```csharp
List<string> names = new List<string>() { "Luke", "Leia", "Han" };
foreach(string name in names)
{
    Console.WriteLine("Character: " + name);
}
```

You can also use the for loop to move through the list.

```csharp
List<string> names = new List<string>() { "Luke", "Leia", "Han" };
for(int i = 0; i < names.Count; i++) 
{
    Console.WriteLine("Character: " + names[i]);
}
```

With lists, there is a new Count property. With all collections, you use Count to get the number of items. As confusing as this is, there is good reason for it. Length actually represents the size of the array that has been _allocated_, but it doesn't reflect the number of actual items you've added. This is because collections store all of their data within an internal array.

> #### Caution::A Note About Properties
>Properties work differently than methods. Methods require you to use the `()` syntax to indicate that you are calling the method. Properties are like variables. You can set them, and you can access them. Later on, you'll see it is possible for some properties to support access but not assignment. As an example, the Count property doesn't let you set the value to zero since that doesn't make a lot of sense.
>
>```csharp
>List<string> names = new List<string>() { "Luke", "Leia", "Han" };
>names.Count = 0; // Will not compile
>```

{% endif %}

----

### Other Methods

This chapter covered some of the more common methods that lists contain, but it's important to know that there are many other methods that exist to make your job easier. Explore and experiment with the intellisense to see what other methods you can discover.
