## Queue

Imagine you're working at a deli counter, and there are several anxious customers who want to order their lunch meats and cheeses.  When the employees start working, who is served first? The most common way to deal with this problem is to use a queue. The flow is fairly straightforward:

1. Customer visits the line and takes a number.
2. One at a time, a number (in increasing order) is called for an individual. 
3. You wait for your number. When it is called, you give the employee your order.
4. After you complete your order, the next number is called. 

Queues follow a First-In First-Out(**FIFO**). Each customer to the deli counter receives a number that indicates the order their request was received. As a request is processed, the employee calls for the next customer.

> #### Info::Real World Usage
>
> Queues are used when there's a need for a system that manages a group of objects in the order requests are received.
> 
> 1. A print queue prints documents in the order requests are made.
> 2. A Call Center phone system uses a queue to hold customers in the order they call. Service representatives take the call of the person on hold the longest.

Like you saw with lists, the queue is internally backed with an array. The `Queue` class manages it so that items are read in a particular order and added and removed from a particular position.

### Creating a Queue

{% if book.language === 'Java' %}

Declaring a new queue consists of the same three expressions to create any other object.

```java
Queue<String> names = new LinkedList<String>();
```

{% elif book.language === 'C#' %}

Declaring a new queue consists of the same three expressions to create any other object.

```csharp
Queue<string> names = new Queue<string>();
```

{% endif %}

###  Adding Items to the Queue

When you add items to the queue, it works just like real life. No one can jump the line; they always go to the end of the line. Queues use the Enqueue method that adds an item to the last spot in the queue.

{% if book.language === 'Java' %}

```java
Queue<String> names = new LinkedList<String>();

names.offer("C3PO");  // names now contains C3PO
names.offer("R2-D2"); // names now contains C3PO, R2-D2
names.offer("BB-8");  // names now contains C3PO, R2-D2, BB-8
```

> #### Caution::LinkedList?
>
> What is `LinkedList`? It's the class that you use in Java to create a Queue. Future chapters discuss object-oriented material later. For now, know that a LinkedList object acts like a Queue, and you can use it here that way.

{% elif book.language === 'C#' %}

```csharp
Queue<string> names = new Queue<string>();

names.Enqueue("C3PO");  // names now contains C3PO
names.Enqueue("R2-D2"); // names now contains C3PO, R2-D2
names.Enqueue("BB-8");  // names now contains C3PO, R2-D2, BB-8
```

{% endif %}

### Removing Items from the Queue

The method for removing items is Dequeue. This always removes the item that has been in the queue the longest.

{% if book.language === 'Java' %}

```java
Queue<String> names = new LinkedList<String>();

// ... names now contains C3PO, R2-D2, BB-8

String name = names.poll(); // name contains C3PO and names contains R2-D2, BB-8
```

If you try and call `.poll()` when the queue is empty, you get a `null` returned. It's always best to check the `.size()` property before attempting to poll an item. 

{% elif book.language === 'C#' %}

```csharp
Queue<string> names = new Queue<string>();

// ... names now contains C3PO, R2-D2, BB-8

string name = names.Dequeue(); // name contains C3PO and names contains R2-D2, BB-8
```

If you try and call Dequeue when the queue is empty, an `InvalidOperationException` is thrown. It's always best to check the Count property before attempting to dequeue an item. 

{% endif %}

### Looping Through the Queue

As with stacks, you can use the foreach iterator to go through the contents of a queue. Alternatively, you can dequeue each item as long as there are items in the queue.

{% if book.language === 'Java' %}

```java
Queue<String> names = new LinkedList<String>();

// ... names now contains C3PO, R2-D2, BB-8

while (names.size() > 0)
{
    String name = names.poll(); // the item is removed from the queue
    System.out.println(name);
}
```

{% elif book.language === 'C#' %}


```csharp
Queue<string> names = new Queue<string>();

// ... names now contains C3PO, R2-D2, BB-8

while (names.Count > 0)
{
    string name = names.Dequeue(); // the item is removed from the queue
    Console.WriteLine(name);
}
```
{% endif %}