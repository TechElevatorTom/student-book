# Linear Data Structures

In this chapter, you'll learn:

* about other data types that support collections of data and can shrink and grow
* how code libraries are organized using namespaces (C#) or packages (Java)
* the foreach loop, another looping mechanism to iterate through collections




