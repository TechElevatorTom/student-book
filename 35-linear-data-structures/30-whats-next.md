
## What's Next 

The collections introduced in this section are an example of *linear data structures*. A data structure refers to a particular way the data is organized so that it can be used by the program. A linear data structure is a data structure whose elements have a particular sequence. In addition to these collection classes, arrays are common linear data structures.

If you are interested in looking into other types of linear data structures, take a look at linked lists. They aren't commonly used by junior developers, but they do sometimes come up during an interview.