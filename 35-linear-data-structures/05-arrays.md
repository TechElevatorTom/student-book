## Array Recap

Remember when you first learned about arrays and how they could save you from an excess number of variables? Arrays are one of the most basic collection types available in the {{ book.language }} language. When you declare an array, you define its data type and how many elements it holds.

    //<type>[] names = new <type>[<size>];

    int[] ages = new int[3];            //an array, size 3, of integers
{% if book.language === "C#" %}
    string[] groceries = new string[5]; //an array, size 5, of strings
{% elif book.language === "Java" %}
    String[] groceries = new String[5]; //an array, size 5, of Strings
{% endif %}

Arrays add value to your program, but they come with a significant limitation that you must work around. Once an array is created, its size is fixed. Removing and adding items - at the beginning, middle, or end - isn't easy to do without additional work. In fact, if you wanted to insert a new element into the middle of an array, you would need to: 

* create a new array _B_ that is larger than the existing array  _A_ by 1
* copy all elements from array _A_ to array _B_ up until the place you wish to insert after
* insert the new value into array _B_
* copy the remaining elements from array _A_ to array _B_
* throw out array _A_ 

This doesn't sound bad, but imagine how many applications you interact with where the list allows you to add an infinite amount of items. Some examples include a to-do list app or connections on a social network.

The need to handle collections of unknown size is so common that the {{book.language}} language contains a few special classes to handle this. These classes belong to the Collections Framework. 

In the following sections, you'll learn about the following classes:

* `List` - a general purpose collection
* `Stack` - a collection that enforces first-in last-out behavior
* `Queue` - a collection that implements first-in first-out behavior
