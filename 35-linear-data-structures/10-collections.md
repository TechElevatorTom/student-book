## Collections Classes

Remember that classes are a grouping of variables and methods in a source code file that you can generate objects from. A class definition exists for each data type that you explore. These definitions are the template that you will use to create new objects. You can use a common type, the list to create as many lists as your program requires. Each list contains its own separate values and has methods that make it easy to add, remove, or clear the contents.

{% if book.language === 'Java' %}

In all, the Java environment contains over 4000 classes, and many more can be loaded as third party libraries. You probably won't use them all on the job; in fact, most programmers never see a fraction of them in their whole careers.  Still, it's good to know that if you ever run into a problem, there's probably a class that can fix it.

{% elif book.language === 'C#' %}

Outside of the collections classes, the .NET Framework contains over 9000 other classes. You don't need to memorize them all.  For now, keep in mind that there are several templates and data types to choose from, if you need to.

{% endif %}

In a few chapters, you will see that you can create your own classes when you need something more specific.  Between the framework, creating your own classes, and including classes from other software projects, it can be difficult to manage all of them unless there's a way to organize them.  Thankfully, you can use **namespaces**.

----

### Namespaces

Namespaces are heavily used within {{ book.language }} programs in two ways. The {% if book.language === "C#" %} .NET Framework {% elif book.language === "Java" %} Standard Library {% endif %} classes use namespaces to organize its many classes. Additionally, declaring your own namespaces helps to control the scope of a class and its methods within larger programming projects.

{% if book.language === 'Java' %}

Most Java class files begin with `import` statements. These statements list the namespaces, or *packages* that the file uses, saving the programmer from specifying a fully qualified name every time a method contained within is used.

By including the following line:

```java
import java.util.Scanner;
```

You can write code like this:

```java
Scanner input = new Scanner(System.in);
```

Instead of:

```java
java.util.Scanner input = new java.util.Scanner(System.in);
```

The second way to use packages is to help you to stay organized.  The `package` keyword creates a scope that helps the project organize the classes you create. It also ensures that your new data type's name doesn't conflict with classes created by other programmers.

----

### java.util Package

The `java.util` package contains the different classes and interfaces that you can use to create generic collections. Anytime you want to create a list, a queue, or any of the other collections classes, you'll need to add an `import` statement in your code to this package.

```csharp
import java.util.*;
```

Ordinarily, you don't need to type this in yourself. Eclipse organizes your imports for you if you type `⌘-Shift-O`.

This [link](https://docs.oracle.com/javase/8/docs/api/java/util/package-summary.html) provides full coverage for what the `java.util` package includes.

{% elif book.language === 'C#' %}

Most C# code files begin with a section of `using` directives. This section lists the namespaces that the code file uses. It also saves the programmer from specifying a fully qualified name every time a method contained within is used.

By including the following line:

```csharp
using System;
```

You can write code like this:

```csharp
Console.WriteLine("Hello World!");
```

Instead of:

```csharp
System.Console.WriteLine("Hello World!");
```

The second way to use packages is to help you to stay organized.  The `namespace` keyword creates a scope that helps the project organize the classes you create. It also ensures that your new data type is unique, even for a large project.

----

### System.Collections.Generic Namespace

The `System.Collections.Generic` namespace contains the different classes and interfaces that you'll use to create generic collections. Anytime you want to create a list, a queue, or any of the other collections classes, you'll need to add a `using` directive in your code file to this namespace.

```csharp
using System.Collections.Generic
```

It is used so often that Visual Studio automatically adds it for any new code file that you create.

This [link](https://docs.microsoft.com/en-us/dotnet/api/system.collections.generic?view=netframework-4.7) provides full coverage for what the `System.Collections.Generic` namespace includes.

{% endif %}

