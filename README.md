# Introduction

Welcome to the Student Book for Tech Elevator. This guide is set up to be a supplemental guide, written by Tech Elevator instructors, to help you on this crazy 14 week journey. It is not meant or designed to be a replacement for the lectures, but a review, reference and occasional deep dive into what we talk about in class.

It is a work in progress, but we hope that it can help you on your way. Please let your instructor know about any way you think this could be better or more helpful for you.