# Interfaces

In {{book.language}}, interfaces force each component to expose specific public members that can be used in a specific way. Interfaces provide us with a way to say what classes can do and as such are an alternative way to define an *is a* relationship.

Let's see how an interface is defined.

{% if book.language === 'Java' %}

```java
public interface Alarmable {
    void setAlarm(int hour, int minute);
}
```

{% elif book.language === 'C#' %}

```csharp
public interface IAlarmable
{
    void SetAlarm(int hour, int minute);
}
```

**Note:** Interfaces in C# start with the letter I. Its not required but its a very common coding convention that may get you ostracized from the community if you resist.

{% endif %}

Notice in the definition of our interface, we didn't provide an implementation for our methods. We just provided a method signature (minus the access modifier). That's what an interface is: a declaration of one or more public methods.

After definition, interfaces are implemented by classes. We say that the interface is a contract. If a class wishes to implement an interface, it is required to agree and provide implementations for all methods defined by the interface. A class, if necessary, can implement more than one interface.

{% if book.language === 'Java' %}
```java
public class AlarmClock extends Clock implements Alarmable {    

    public void setAlarm(int hour, int minute) {
        /* implementation for set alarm method on alarm clock*/
    }

}

public class Phone implements Alarmable {
    
    public void setAlarm(int hour, int minute) {
        /* implementation for set alarm method on phone */
    }
}
```
{% elif book.language === 'C#' %}

```csharp
// The AlarmClock class inherits from Clock and implements the IAlarmable interface
public class AlarmClock : Clock, IAlarmable
{    
    public void SetAlarm(int hour, int minute)
    {
        /* implementation for set alarm method on alarm clock*/
    }
}

// The Phone class implements the IAlarmable interface
public class Phone : IAlarmable
{
    public void SetAlarm(int hour, int minute)
    {
        /* implementation for set alarm method on phone */
    }
}
```

{% endif %}

> #### Caution::Interface Instantiation
>
> Interfaces don't get instantiated.

Now that the interface has been defined, it is available as a data type that can be used to reference an existing object. In this sense, we can adjust our Shopkeeper class and eliminate the duplicate methods by programming the set alarm method against an interface and not the concrete classes. 

{% if book.language === 'Java' %}
```java
public class Shopkeeper {
    public void setAlarm(Alarmable alarm) {
        alarm.setAlarm(08, 00); // set the alarm to 8 o'clock
    }
}
```
{% elif book.language === 'C#' %}

```csharp
public class Shopkeeper
{
    public void SetAlarm(IAlarmable alarm)
    {
        alarm.SetAlarm(08, 00); // set the alarm to 8 o'clock
    }
}
```

Our code is back to being loosely coupled. Any existing object (or one that we create) that implements the IAlarmable interface can be passed in as an argument to the SetAlarm method. 

{% endif %}

> #### Note::Polymorphism & Interfaces
>
> With respect to interfaces, polymorphism means *if B is a class that implements interface A and a function can accept A as a parameter, that is can also accept B*.    