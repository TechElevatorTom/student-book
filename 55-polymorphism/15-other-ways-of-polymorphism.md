# Other Ways of Polymorphism

Let's say we wanted our shopkeeper to have the ability to set an alarm. It doesn't make sense for them to set an alarm on a grandfather clock or a cuckoo clock, but it does make sense to set it on an alarm clock. While we're at it, we also need the shopkeeper to set the alarm on their phone.

We'll add the method directly to the AlarmClock class since the alarm feature doesn't apply to all clocks. We also need a separate Phone class that has support for setting an alarm. 

{% if book.language === 'Java' %}
```java
public class AlarmClock extends Clock {    

    public void setAlarm(int hour, int minute) {
        /* implementation for set alarm method on alarm clock*/
    }

}

public class Phone {
    
    public void setAlarm(int hour, int minute) {
        /* implementation for set alarm method on phone */
    }
}
```

{% elif book.language === 'C#' %}
```csharp
public class AlarmClock : Clock
{    
    public void SetAlarm(int hour, int minute)
    {
        /* implementation for set alarm method on alarm clock*/
    }
}

public class Phone
{
    public void SetAlarm(int hour, int minute)
    {
        /* implementation for set alarm method on phone */
    }
}
```
{% endif %}    

When we go to add the ability for our Shopkeeper to set the alarm, we need to set up two separate methods again.

{% if book.language === 'Java' %}

```java
public class Shopkeeper {
    
    public void setAlarm(AlarmClock clock) {
        clock.setAlarm(08, 00); // set the alarm on the alarm clock to 8 o'clock
    }

    public void setAlarm(Phone phone) {
        clock.setAlarm(08, 00); // set the alarm on the phone to 8 o'clock
    }
}
```

{% elif book.language === 'C#' %}

```csharp
public class Shopkeeper
{
    public void SetAlarm(AlarmClock clock)
    {
        clock.SetAlarm(08, 00); // set the alarm on the alarm clock to 8 o'clock
    }

    public void SetAlarm(Phone phone)
    {
        clock.SetAlarm(08, 00); // set the alarm on the phone to 8 o'clock
    }
}
```

{% endif %}

This is similar to the situation we were in before. Neither of these classes appear to share anything in common yet other than the fact that they both can have an alarm set.

> #### Danger::Don't Force Inheritance
>
> You might think adding inheritance is a quick fix to avoid duplicating code. Be careful. If you can't use the phrase "is-a" when talking about two different types, it isn't a good fit. It doesn't make sense to have Phone inherit from AlarmClock. You wouldn't say "a phone *is an* alarm clock."

In reality, we also interact with different objects based on *what they can do* and not *what they are*. With code we can use something called an interface to indicate what a type can do and reference the object by that.

