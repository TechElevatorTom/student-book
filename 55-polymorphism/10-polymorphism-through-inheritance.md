
# Polymorphism & Inheritance

Recall in the last chapter, we added a few subclasses that inherit from our Clock class.

{% if book.language === 'Java' %}
```java
public class Clock {    
    public int hour;
    public int minute;
    public int second;
    
    public void setTime(int hour, int minute) {
        this.hour = hour;
        this.minute = minute;
        this.second = 0;
    }

    /* ... */
}

public class GrandfatherClock extends Clock {
    /* ... Grandfather Clock implementation */    
}

public class CuckooClock extends Clock {
    /* ... Cuckoo Clock implementation */ 
}

public class AlarmClock extends Clock {
    /* ... Alarm Clock implementation */ 
}


```
{% elif book.language === 'C#' %}
```csharp
public class Clock
{    
    public int Hour { get; private set; }
    public int Minute { get; private set; }
    public int Second { get; private set; }
    
    public void SetTime(int hour, int minute)
    {
        this.Hour = hour;
        this.Minute = minute;
        this.Second = 0;
    }

    /* ... */
}

public class GrandfatherClock : Clock
{
    /* ... Grandfather Clock implementation */    
}

public class CuckooClock : Clock
{
    /* ... Cuckoo Clock implementation */ 
}

public class AlarmClock : Clock
{
    /* ... Alarm Clock implementation */ 
}


```
{% endif %}

Suppose you needed to create a Shopkeeper class (clock shops are still a thing, right?) whose responsibility is to reset all of the different clocks to 7am.

{% if book.language === 'Java' %} 

```java
public class Shopkeeper {
    
    public void setTime(GrandfatherClock clock) {
        clock.setTime(07, 00); // Set a grandfather clock to 7 o'clock
    }

    public void setTime(CuckooClock clock) {
        clock.setTime(07, 00); // Set a cuckoo clock to 7 o'clock
    }

    public void setTime(AlarmClock clock) {
        clock.setTime(07, 00); // Set an alarm clock to 7 o'clock
    }    
}
```

{% elif book.language === 'C#' %}

```csharp
public class Shopkeeper
{
    public void SetTime(GrandfatherClock clock)
    {
        clock.SetTime(07,00); // Set a grandfather clock to 7 o'clock
    }

    public void SetTime(CuckooClock clock)
    {
        clock.SetTime(07, 00); // Set a cuckoo clock to 7 o'clock
    }

    public void SetTime(AlarmClock clock) 
    {
        clock.SetTime(07, 00); // Set an alarm clock to 7 o'clock
    }
}
```

{% endif %}

Do you notice a *code smell* here? We're adding a method for every type of Clock. Our code is *tightly coupled*. When we want to add a new Clock type, we'll be adding a new SetTime method for it as well.

> #### Info::Code Smells
>
> A code smell is any symptom in the code of a program that possibly indicates there is a deeper problem, violation of fundamental design principles, or an impact to design quality. Here are some [common code smells](https://blog.codinghorror.com/code-smells/):
> * Duplicate code
> * Conditional complexity
> * Long methods 


**Polymorphic code** allows us to take advantage of the fact that our different clock classes (AlarmClock, GrandfatherClock, and CuckooClock) have an *is-a* relationship with a Clock.

Given that AlarmClock, GrandfatherClock, and CuckooClock are subclasses of Clock, we can write a single function that accepts a reference to a Clock while allowing any class that inherits from it too.

{% if book.language === 'Java' %} 

```java
public class Shopkeeper {

    public void setTime(Clock clock) {
        clock.setTime(07, 00);    // Set any clock to 7 o'clock
    }
}
```

{% elif book.language === 'C#' %}

```csharp
public class Shopkeeper
{
    public void SetTime(Clock clock)
    {
        clock.SetTime(07,00); // Set any clock to 7 o'clock
    }    
}
```

{% endif %}

Now we've written *loosely-coupled code*. The ShopKeeper doesn't differentiate between the different clocks when it comes to setting time. Think about it, how many times have you said "go reset that cuckoo clock to 7 o'clock"? If you are going to say it, you're more likely to point at the cuckoo clock and say "go reset that clock to 7 o'clock".

Our ShopKeeper class is now only dependent on Clock. If we add a new class that inherits from Clock, say a Watch, the code in our ShopKeeper class has no reason to change.

{% if book.language === 'Java' %}

```java
public class Watch extends Clock {
    /* ... Watch implementation */
}
```

{% elif book.language === 'C#' %}

```csharp
public class Watch : Clock
{
    /* ... Watch implementation */
}
```

{% endif %}

> #### Note::Polymorphism & Inheritance
>
> With respect to inheritance, polymorphism simply means *if B is a subclass of A and a function can accept A as a parameter, then it can also accept B*.