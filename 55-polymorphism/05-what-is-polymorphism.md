
# What is Polymorphism?

> *The word polymorphism, derived from the Greek language, means "the ability to have multiple forms".*
>
> *In object-oriented programming, polymorphism is the idea that something can be assigned a different meaning or usage based on the context it is referred to as. This specifically allows variables and objects to take on more than one form.*

That was pretty deep. Let's use an analogy to try and make sense of this term.

Suppose someone told you, "go to the store and buy your favorite food for dinner."  

What happens next depends on some context:
* If the request were made to your 14 year-old self, it might consist of you riding your bike to the store and using the cash in your pocket to pay for a pizza & a 2 liter bottle of pop (soda).
* As an adult, you may *drive to the store* and using the credit card in your wallet, pay for pecan chicken and pinot noir.

In both cases, the person making the request said the same thing, "go to the store and buy your favorite food for dinner", but the result differed, depending on the subject receiving the request.

-----

Often we want to write code that can make a basic request and based on context allow different behaviors to take place. We can write code once that refers to many different things.

As we'll see, polymorphism supports code extensibility. If we write polymorphic code, we have code that functions independent of the object's class type. This allows programmers to incorporate changes through new object types in the system without modifying existing code. 