# Polymorphism

In this chapter you'll learn about the last principle of object-oriented programming, polymorphism.

Specifically we will cover:

* What polymorphism is and how its useful when writing software
* Where inheritance can help us write polymorphic code
* Interfaces and how they are used