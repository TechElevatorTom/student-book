# Building A Real Application

Now we're going to update our bookstore so that our code takes advantage of polymorphism when dealing with products of all different types.

{% if book.language === 'Java' %}

{% video %}https://www.youtube.com/watch?v=-ZTfV6_yCuc {% endvideo %}

{% elif book.language === 'C#' %}

{% video %}https://www.youtube.com/watch?v=5vmNxosOJHo {% endvideo %}

{% endif %}
