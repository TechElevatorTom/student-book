# Temporary Data

Now that you have session, you have a way to persist data across separate HTTP requests. Another common scenario is the need to persist data for a shorter period of time. Consider a ticketing purchase flow:

1. The user navigates to a page and provides the information to purchase a ticket.
1. After the user submits the request, they are taken to a page that displays a summary of their purchase.
3. After completing the purchase, they are given a confirmation number to print and keep for reference.

The challenge that often occurs is having to come up with a way to pass the confirmation number to the confirmation page. If you use session, you will need to remember to clear the data out after you have displayed it. If not, you risk polluting the session with data that is no longer necessary.

Enter **temp data**.

## What Is Temp Data

`TempData` is meant to store short-lived session data. If you find yourself needing data only **for the current and subsequent request**, `TempData` is the perfect solution. It is only useful when you are redirecting the user and want the data to still be available. Once you access the data stored in `TempData`, it is automatically cleared.

To set information in `TempData`, write the following in your Controller before performing a redirect:

```csharp
TempData["Key"] = value; // Where Key is a string and Value is any Object
```

To get information out of `TempData` in the subsequent request:

```csharp
// Get the Value using the same Key
// You may need to cast it
var value = TempData["Key"];
```