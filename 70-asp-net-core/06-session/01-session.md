# Session

Something that is often surprising when first learning web applications is that HTTP is referred to as a *stateless protocol*. This means each HTTP request/response is independent and isolated from previous interactions. Imagine having a conversation with someone at a party who can only remember enough to answer your immediate question.

Depending on your perspective, there are pros and cons with HTTP being stateless. One disadvantage is that it is impossible for a web application to remember anything we ask it to do for an extended period of time (like put items into a shopping cart). However, this can also be advantageous because the less the application needs to remember, the more incoming requests can be served.

That aside, sometimes users need things to be remembered. This is where *session* helps.

## Session

**Session** is the idea of storing information server-side that needs to be persisted throughout a user's interaction with a web application.

Whether we are building an application that needs to store a complex object (e.g. a shopping cart) or a simple object such as a zip code, session can be used. Sessions allow the server to recall information about each client.

Most web applications store session data about their users without the user even acknowledging that this is desired. This occurs with the concept of something called *cookies*. The following outlines the process web application servers follow to recall information about a client on demand:

1. A server responds to a client request. In the response a unique **Session ID** is generated. The Session ID is secure and cannot be easily guessed by other users.
1. The response indicates to the browser that the Session ID should be stored within a *cookie*.
1. For every subsequent request made by the browser, any and all cookies it has for the domain are sent to the server. The Session ID is included in the collection of cookies, and provides enough information for the server to remember the client.

![Session Creation](https://www.whizlabs.com/wp-content/uploads/2015/11/session-tracking.png)


Session state exhibits the following behaviors:

* Cookies are specific to each browser, therefore Session IDs stored in cookies are not shared across browsers.
* Cookies may be deleted when the browser session ends.
* When a request is received with an *expired Session ID* a new session is created.
* The app retains session data for a limited time. When the session expires (times out) the app clears the data in memory so that it can free up space for a new session with another client.
* There is no default mechanism to inform a web application that the client has closed their browser unless the user manually *logs out* of the web application.

## Configuring ASP.NET Core Session

Session isn't enabled by default. To enable it, the `Startup.cs` file needs to be modified.

```csharp
public void ConfigureServices(IServiceCollection services)
{
    services.Configure<CookiePolicyOptions>(options =>
    {
        // This sets if the user needs to "consent" to
        // allow cookies to be saved on their machine.
        // For DEV purposes, set it to false.
        options.CheckConsentNeeded = context => false;
        options.MinimumSameSitePolicy = SameSiteMode.None;
        });

    // Indicates Session should be saved "in memory"
    services.AddDistributedMemoryCache();

    // Sets the options on Session
    services.AddSession(options =>
    {
        // Sets session expiration to 20 minutes
        options.IdleTimeout = TimeSpan.FromMinutes(20);
        options.Cookie.HttpOnly = true;
    });

    services.AddMvc().
        SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
}

public void Configure(IApplicationBuilder app, IHostingEnvironment env)
{
    if (env.IsDevelopment())
    {
        app.UseDeveloperExceptionPage();
    }
    else
    {
        app.UseExceptionHandler("/Error");
        app.UseHsts();
    }

    app.UseHttpsRedirection();
    app.UseStaticFiles();
    app.UseCookiePolicy();

    // Tells our application to use session
    app.UseSession();

    app.UseHttpContextItemsMiddleware();
    app.UseMvc();
}
```

Each of these lines is configuring something called *middleware*.

> #### Info::Middleware
>
> Middleware is software that's assembled into an app pipeline to handle requests and responses.

## Saving and Retrieving Data From Session

Once configured, working with Session is conceptually similar to working with a Dictionary.

To store data in the session, type the following (key and value are both type `string`):

```
HttpContext.Session.SetString(key, value);
```

To retrieve data from session, type the following:

```
string value = HttpContext.Session.GetString(key);
```

These methods are available when you add `using Microsoft.AspNetCore.Http` to the top of the code file.

Of course, there is often information that we want to store that is more complex than a simple string. If we want to store something more complex than a string, say a `ShoppingCart`, then we must *serialize* the object. Serialization is the process of converting an object into its string representation. Deserialization is the process of converting a string back into its object representation. One common way of representing objects as strings is to serialize objects using JSON.

**JSON** stands for JavaScript Object Notation. It is a common way of passing data between applications and is not technology specific (despite the name).

To support the serialization and deserialization of objects in the session using JSON, you will need to add *extension methods* that will make this much easier. You will need to create a  class called `SessionExtensions` and copy/paste the below code into it.

```csharp
public static class SessionExtensions
{
    public static void Set<T>(this ISession session, string key, T value)
    {
        session.SetString(key, JsonConvert.SerializeObject(value));
    }

    public static T Get<T>(this ISession session, string key)
    {
        var value = session.GetString(key);

        return value == null ? default(T) :
            JsonConvert.DeserializeObject<T>(value);
    }
}
```

[Extension methods](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/classes-and-structs/extension-methods) allow you to provide additional methods to existing classes without extending the class. In this case the code is extending session to strongly type the data that it stores, similar to working with `List`, `Dictionary`, `Stack`, and `Queue`. Now you can write:

```csharp
// To store data - key is a string and cart is a ShoppingCart
HttpContext.Session.Set<ShoppingCart>(key, cart);

// To retrieve data
ShoppingCart cart = HttpContext.Session.Get<ShoppingCart>(key);
```

## For More Information

For more information on Session, visit [Microsoft's Session and app State Docs](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/app-state?view=aspnetcore-2.1).
