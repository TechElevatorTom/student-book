# Form Tag Helpers

The `form` tag has a number of various `asp-` helper attributes will be useful.

## Form Tag

The responsibility of the `<form>` tag is to:

1. Indicate the HTTP method that should be used to submit the form data.
1. Identify where the request should be sent.

The `<form>` tag includes the following helpers to assist with this via:

- `asp-controller` - indicates which controller will receive the request
- `asp-action` - indicates which action will process the request
- `asp-route-parameter-name` - this is simlar to `asp-route-{name}` which we worked with hyperlinks.


Here is an example of using the `<form>` tag helpers:

```cshtml
<form asp-controller="Demo" asp-action="Search" method="get">
    <!-- Input and Submit elements -->
</form>
```

Using the helps would generate the following HTML:

```html
<form method="get" action="/Demo/Search">
    <!-- Input and Submit elements -->    
</form>
```

For additional documentation view the [Form Tag Helper Reference](https://docs.microsoft.com/en-us/aspnet/core/mvc/views/working-with-forms?view=aspnetcore-2.1#the-form-tag-helper).

## Input Tag Helper

When working with `<input>` elements, the primary attribute that needs to be set is `name`. `name` indicates what parameter to send the value as in a subsequent request.

The tag helper that is going to be most associated with this element is `asp-for`. Using `asp-for` and binding it to a property on your model will:

* Generate the input `id` and `name` attributes for the `<input>` element.
* Sets the `type` attribute based on the property's data type.
* Generate HTML5 validation attributes to assist with validation.
* Provide strong typing to ensure that in the event of a name change on our property, we update our view or else a compile error will occur.



The `<input>` tag helper sets the HTML type attribute based on the data type of the model property. The following table lists some common .NET types and generated HTML `type` (not every .NET type is listed).

| .NET type | Input Type |
|-----|-----|
|Bool	| type=”checkbox”|
|String	|type=”text”|
|DateTime	|type=”datetime”|
|Byte	|type=”number”|
|Int	|type=”number”|
|Single, Double	|type=”number”|

Given a Register Model class:

```
public class RegisterModel
{
    public string Email { get; set; }
    public string Password { get; set; }
    public DateTime DOB { get; set; }
    public bool ShouldSubscribe { get; set; }
}
```

And a View written like this: 

```
@model RegisterModel

<input asp-for="Email" />
<input asp-for="Password " />
<input asp-for="DOB " />
<input asp-for="ShouldSubscribe " />
```

Then the HTML generated would be similar to the following:

```
<input type="text" id="Email" name="Email" />
<input type="password" id="Password" name="Password" />
<input type="date" id="DOB" name="DOB" />
<input type="checkbox" id="ShouldSubscribe" name="ShouldSubscribe" />
```

For the full documentation view the [Input Tag Helper Reference](https://docs.microsoft.com/en-us/aspnet/core/mvc/views/working-with-forms?view=aspnetcore-2.1#the-input-tag-helper).


## Select Tag Helper

The `<select>` element is used to create drop-down boxes, providing options to the user to select.

The primary work needed for the `<select>` element is:

* identify the `name` attribute to associate the selected value to a parameter
* provide options for the user to select from

As before, the `asp-for` attribute will specify which property should be set from the model for the `<select>` element.

A new attribute `asp-items` is used to provide a list of `option` elements that will be rendered in the dropdown.


Here is a sample usage

```
@model CountryViewModel

<select asp-for="Country" asp-items="Model.Countries"></select> 
```

And a sample model:

```csharp
using Microsoft.AspNetCore.Mvc.Rendering;

public class CountryViewModel
{
    // The selected country
    public string Country { get; set; }

    // The choices the user can select from
    public List<SelectListItem> Countries { get; } = new List<SelectListItem>
    {
        new SelectListItem { Value = "MX", Text = "Mexico" },
        new SelectListItem { Value = "CA", Text = "Canada" },
        new SelectListItem { Value = "US", Text = "USA"  },
    };
}
```

When the final HTML is rendered, this would be displayed:

```html
<select id="Country" name="Country">
    <option value="MX">Mexico</option>
    <option value="CA">Canada</option>
    <option value="US">USA</option>
</select>
```



For further details view the [Select Tag Helper Reference](https://docs.microsoft.com/en-us/aspnet/core/mvc/views/working-with-forms?view=aspnetcore-2.1#the-select-tag-helper).