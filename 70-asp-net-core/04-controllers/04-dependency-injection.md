# Dependency Injection

Until now, you've learned to write code that instantiates other classes. To clarify, when a class such as a controller needs to interact with the database, it might instantiate a DAL whenever and wherever it needs to. 

```
public class MovieController : Controller 
{
    public IActionResult Detail(int id)
    {
        IMovieDao dao = new MovieSqlDao("connection-string-here");
        Movie movie = dao.GetById(id);
        return View(movie);
    }
}
```

Despite how clean this code is, it is not easily testable. 

This line `IMovieDao dao = new MovieSqlDao("connection-string-here");` can cause issues: 

* It is impossible to unit test the controller because it will never fully be isolated. Everytime it is tested, it will need a live database to connect to.
* The controller *does too much work*. Even though database code is contained within the DAO, the fact that the controller instantiates the class (with its connection string) implies that work is being spent writing code that doesn't manage HTTP Requests and Responses (the primary responsibility of the controller). 
* The code is tightly-coupled. When the database needs to change, the controller needs to instantiate a different DAO.

Programmers try to follow a rule called the [Single Responsibility Principle (SRP)](https://en.wikipedia.org/wiki/Single_responsibility_principle).  SRP states that a class should have responsibility **over a single part of the functionality provided by the software**. 

Another principle, the **Dependency Inversion Principle** states that high-level modules should not depend on specific low-level modules. Instead *they should depend on abstractions.* This is where we introduce the concept of dependency injection (DI).

> #### Comment::Dependency Injection
>
> **Dependency injection (DI)** is a technique for achieving loose coupling between objects and their classes they rely on, aka dependencies. Rather than directly instantiating a dependency, the objects a class needs in order to perform its actions are provided to the class as an input. Most often, classes will declare their dependencies via their constructor, allowing other classes to *inject* in a dependency when constructing the object.

What you are more likely to see is the following:

```csharp
public class MovieController : Controller 
{
    private IMovieDao dao;

    // Create a constructor that allows the dependency
    // to be passed in. The dependencies are provided in the
    // form of abstractions so that our MovieController knows
    // as little as possible about it.
    public MovieController(IMovieDao dao)
    {
        this.dao = dao;
    }

    public IActionResult Detail(int id)
    {		
        Movie movie = dao.GetById(id);
        return View(movie);
    }
}
```

Now your `MovieController` is dependent on an `IMovieDao`. This is more generalized. The controller has no idea that it interfaces with a file, a database, or an API. Instead of referencing specific implementations, the class requests an abstractions (typically `interfaces`) which are provided when the controller is constructed.

For further information on the concept of Dependency Injection, Martin Fowler has written a post called [Inversion of Control containers and the Dependency Injection pattern](https://www.martinfowler.com/articles/injection.html).

## DI Containers

When a system is designed to use DI, it's helpful to have a class dedicated to creating these dependencies. These classes are referred to as dependency injection containers. They may also be referred to as **Inversion of Control (IoC)** containers. The name Inversion of Control comes from the fact that the Controller class is no longer in charge of instantiating its dependencies. Those objects are determined outside of the Controller.

A container is a factory that is responsible for various instances of objects upon request. If a class has declared that it has dependencies on abstractions, and the container is configured to create the dependency, then it populates the dependency upon instantiation.

ASP.NET Core includes a built-in container (represented by the `IServiceProvider` interface) that supports constructor injection by default. ASP.NET's container refers to the types it manages as *services*. Throughout the rest of this article, a *service* will refer to an object managed by ASP.NET Core's IoC container. You configure the built-in container's services in the `ConfigureServices` method in your application's `Startup` class.

--------

## Setting Up Dependency Injection

In order to configure your ASP.NET Core MVC application for DI, you need to do the following:

1. **Create abstractions** in the form of interfaces for all of your lower-level modules.
1. **Setup constructors for your higher-level modules** that allow dependencies to be injected.
1. **Configure your DI Container** by mapping an interface to an implementation class.

### Creating Abstractions

Continuing the Movie Example, assume a class already exists called `MovieSqlDao`.

```
public class MovieSqlDao
{
    private string connectionString;
    public MovieSqlDao(string connectionString)
    {
        this.connectionString = connectionString;
    }

    public Movie GetById(int id)
    {
        // open a connection to the database
        using(SqlConnection conn = new SqlConnection(connectionString))
        {
            // Create a command to query the database and return the movie
            // ...
            return movie;
        }
    }
}
```

In order to abstract this, define an `interface` and call it `IMovieDao`.

```
public interface IMovieDao
{
    Movie GetById(int id);
}
```

Now have `MovieSqlDao` implement the `IMovieDao` interface. This way you can say that a `MovieSqlDao` *is-a* `IMovieDao`.

```
public MovieSqlDao : IMovieDao
```

### Adding Constructor Injection

With the abstraction in place, you can declare it as a known dependency in your controller.

```
public class MovieController : Controller 
{
    private IMovieDao dao;

    // Create a constructor that allows the dependency
    // to be passed in. The dependencies are provided in the
    // form of abstractions so that our MovieController knows
    // as little as possible about it.
    public MovieController(IMovieDao dao)
    {
        this.dao = dao;
    }

    public IActionResult Detail(int id)
    {
        Movie movie = dao.GetById(id);
        return View(movie);
    }
}
```

### Configure the DI Container

The last step allows us to tell ASP.NET Core what to do when it encounters a class that is requesting an `IMovieDao`. This step will indicate to ASP.NET that it should always inject an instance of the `MovieSqlDao` class in place of an `IMovieDao`. This change takes place in the `Startup` file within the `ConfigureServices` method.


If the class that you wish to configure as a dependency does not take any constructor arguments then add this line in your `ConfigureServices` method:

```
services.AddTransient<IMovieDao, MovieSqlDao>();
```

If the class does need to be instantiated in a special way (i.e. the `MovieSqlDao` requires a connection string), then you would use this line:

```
// m is arbitrary, call it what you want
services.AddTransient<IMovieDao>(m => new MovieSqlDao("connection-string-here"));
```

------

## Summary

It may seem like excess work to achieve the same result. Ultimately DI allows you to write *cleaner code*. All of the dependencies are managed from one location (`Startup.cs`). Any of the controllers can use this DAO if they need to by requesting the abstraction through its constructor. 

They can even ask for multiple DAOs if necessary:

```csharp
public MovieController(IMovieDao movieDao, IActorDao actorDao, ICategoryDao categoryDao)
```

For a full reference on Dependency Injection within ASP.NET Core MVC visit [Dependency Injection into Controllers](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/dependency-injection).

If you are interested in reading about unit testing controllers, Microsoft has an article written called [Testing Controller Logic in ASP.NET Core](https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/testing).