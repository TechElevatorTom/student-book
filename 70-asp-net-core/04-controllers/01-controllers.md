# ASP.NET Controllers

The next major component of MVC to discuss is the controller. Controllers play a large role in the request/response lifecycle. 

![Action Invoking](https://i.stack.imgur.com/8Fq44.png)

It is through controllers that requests are routed to locate an appropriate action that is capable of generating a response. Actions on controllers are common C# methods that return an `IActionResult` output.


## Actions

Actions have a few general responsibilities:

1. Return an `IActionResult` type using a data model. This is often a view.
1. Delegate work to other services or classes capable of retrieving the data model.
1. Ensure that request data is valid or return a proper error code.

### Returning `IActionResult` Objects

When actions return an `IActionResult`, it is really the object implementing `IActionResult` that is doing the work to generate a response. These types of responses fall into two categories:

#### 1. Results with an Empty Response Body

This type of response is a general HTTP status response:

* 200 - OK
* 400 - Bad Request
* 404 - Not Found
* etc.

In general these responses have no content and only represent a status code.

Because the controller extends the `Controller` class, you are given a number of helper methods. Generating the above outputs would involve writing any of the following below code snippets in your controller action.

* `return Ok()`
* `return BadRequest()`
* `return NotFound()`

Many of these methods will be used when you get into API development in a later module.

#### 2. Results with a Non-Empty Response

This type of response includes content that needs to be sent to the client making the request. It may include any one of the following:

* HTML 
* JSON
* Raw Binary Data
* etc.

In general the output falls within two categories: views and formatted responses.

* **Views** are returned using the `return View(model)` method.

* **Formatted Responses** will be heavily when you need the data to be returned in a structured way.

--------

In general, the controllers you write will act as mediators that will help write cleaner code so that you can separate your models from your views. In the next few sections you'll see how controllers can be used to handle specific types of incoming requests (`GET` vs. `POST`).