# Cross Site Request Forgery (CSRF)

Since `POST` requests are intended to create and update data on a server, there is a vulnerability that can be used to take advantage of a web site's users.

Web Forms are often the victim of something called *cross-site request forgery* attacks. XSRF or CSRF (pronounced *see-surf*) attacks occur when a malicious web application exploits a form that exists in a legitimate web application. The malicious web application impersonates the logged-in user and performs actions on their behalf (that the victim did not intend).

For example:

1. Bob signs into *www.good-banking-site.com*. The server authenticates Bob using his username and password and issues an authentication token for Bob to use upon subsequent requests for identification means. 
1. Bob then visits a malicious site (*www.bad-crook-site.com*) for some reason. The malicious site, *www.bad-crook-site.com*, contains an HTML form is configured to send its data to the vulnerable site. Here is an example of that form:

    ```html
    <h1>Congratulations! You're a Winner!</h1>
    <form action="http://good-banking-site.com/account/withdraw" method="post">        
        <input type="hidden" name="Amount" value="1000000">
        <input type="submit" value="Click to collect your prize!">
    </form>
    ```
3. Bob clicks the button since he can't resist a free prize. Meanwhile the browser submits the form, making a request to *http://good-banking-site.com* asking to withdraw $1,000,000. Since the browser saved his authentication token from before (something you will learn later), it is _conventiently_ sent along for this request to identify Bob.

4. The request runs on the *www.good-banking-site.com* server with Bob's authentication token and performs any action that was asked to perform.

These types of attacks are frequent with financial websites and social media websites.

## Preventing Cross-Site Forgery

There are two ways to prevent this kind of attack:

1. Prompt the user to type their password again before performing the action thereby verifying their identity again.
2. Load some sort of token on the real form page that must be re-submitted with the reuqest to ensure they loaded the real form.

    ```html
    <form action="http://good-banking-site.com/account/withdraw" method="post">        
        <input type="text" name="Amount" value="100">
        <!-- This token in reality is much longer and impossible to guess -->
        <input type="hidden" name="VerifyToken" value="_Ab5de89093850fa13s23decb386" />
        <input type="submit" value="Click to collect your prize!">
    </form>
    ```

When using the `<form>` tag helper along with the `asp-controller` and `asp-action` attributes, ASP.NET MVC automatically adds this hidden `<input>` element to the form. The value is randomly generated per user. The name for this is an _anti-forgery token_.

The anti-forgery token must also be validated by the controller in the subsequent request. This is done using the `[ValidateAntiForgeryToken]` attribute.

```csharp
public AccountController : Controller
{
    // ...

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult Withdraw(int amount)
    {
        // do something to withdraw the money
    }

}
```

The `[ValidateAntiForgeryToken]` attribute will ensure that all request submitted use the same token that was generated for them when loading the empty form. If the token is not supplied then the request will error out, preventing the cross-site request forgery attack.

For more information on XSRF see the [Anti-Request Forgery page](https://docs.microsoft.com/en-us/aspnet/core/security/anti-request-forgery?view=aspnetcore-2.1).