# Building POST Forms with ASP.NET MVC

With HTTP, you learned there are different request methods. So far you've used `GET` which indicates that you are _getting_ a resource. Another type of request that used is an `HTTP POST`.

The `POST` method is used to send data to a server so that it can be _created_ on the server. This may include: 

* uploading an image
* logging in and creating an account
* submitting a post to a bulletin board


Whatever the reason is, data is altered on the site. There are a few things that set `HTTP POST` requests apart from `HTTP GET` requests:

* `POST` requests are never remembered by the browser
* `POST` requests store content in the request body and have no size limit


`POST` requests also create the all too common double post scenario. Ever seen this?

![Double Form Post](https://i.stack.imgur.com/dZhrT.png)

In a later section, you'll see how to prevent this.

## Example POST Form

Using the example from the GET section, the form will be modified to be a signup page instead of a search page.

![Simple Form](resources/SimpleForm.png)

To get to this page you might navigate to `http://www.xyz.com/home/register`.

In order to make the form *post* the data, `method` attribute on the `<form>` tag is updated to `post`. The action indicates that the data should be posted to `Register`.

```html
@model Person

<form method="POST" asp-controller="Home" asp-action="Register">
    <label asp-for="Name"></label>
    <input asp-for="Name" />
    <label asp-for="Email"></label>
    <input asp-for="Email" />
    <input type="submit" />
</form>
```

The controller to support a `GET` and `POST` request looks like this now.

```csharp
public class HomeController : Controller
{
    [HttpGet]
    public IActionResult Register()
    {
        return View();
    }

    [HttpPost]	
    public IActionResult Register(Person input)
    {
        // Using name & email, maybe call a DAL to save the Avenger record
        var person = dal.SavePerson(input.Name, input.Email);

        // Return the Success view indicating the avenger was saved
        return View("Success", person);
    }
}
```

Pay close attention here. You will see two `Register` actions.

1. `public IActionResult Register() { }`
2. `public IActionResult Register(Person input) { }`

These are overloaded methods in C#. The first method has an attribute above it `[HttpGet]` while the other has `[HttpPost]`. These are filters that limit the incoming request to be `GET` or `POST` only. They're used to help ASP.NET MVC indicate where it should route the incoming request.

The user loading the form and submitting it will result in two requests.

1. **Request 1** will be a `GET` request and load the first page containing the empty form.
1. **Request 2** occurs when the form is submitted. A `POST` request sends the data back to the server.


### Post-Redirect-Get

Back to the image at the beginning of the section. Have you ever seen this?

![Double Form Post](https://i.stack.imgur.com/dZhrT.png)

When a web form is submitted via HTTP POST, it is often common that the web user may attempt to refresh the page. This is understandable. Maybe they wish to show updated information on the screen and intend no harm. Certain browsers however, will cause the contents of the original POST request to be resubmitted. This might cause undesirable results (e.g. double signup, double ticketing purchase, etc.).

If the user were to press **continue** then this flow is what would likely occur between the browser and the server.

![Double Form Post Flow](https://upload.wikimedia.org/wikipedia/commons/f/f3/PostRedirectGet_DoubleSubmitProblem.png)

There is a pattern called Post-Redirect-Get (PRG) that can help eliminate this problem. Instead of returning a web page after saving the data, the server responds with a redirect command, telling the user's browser to go directly to another page (i.e. a confirmation page). This allows users to safely refresh the final page without duplicating the POST request.
 
![PRG Pattern](https://upload.wikimedia.org/wikipedia/commons/thumb/3/3c/PostRedirectGet_DoubleSubmitSolution.png/525px-PostRedirectGet_DoubleSubmitSolution.png)


The controller written from above has to be updated. It includes:

1. A new action to support the third request for the `Success` page
1. A `RedirectToAction` command that tells the browser to go to `http://xyz.com/home/success`


```csharp
public class HomeController : Controller
{
    //1. Gets the empty form
    [HttpGet]
    public IActionResult Register()
    {
        return View();
    }

    //2. Handles the Save Form request
    [HttpPost]	
    public IActionResult Register(Person input)
    {
        // Using name & email, maybe call a DAL to save the Avenger record
        var person = dal.SavePerson(input.Name, input.Email);

        // Redirects the user to the Success action
        return RedirectToAction(nameof(Success));
    }

    //3. Load the Success view
    [HttpGet]
    public IActionResult Success()
    {
        return View();
    }
}
```

`RedirectToAction` is a method defined in the base `Controller` class. The method itself generates an HTTP response to instruct the browser that it should go to the next URL. 

> #### Info::There are multiple requests
>
> **REMEMBER** In order for any of these Actions to run, the browser must first submit a request. 3 actions means 3 separate requests. After we redirect the user to the `Success` page though, the user can refresh the page to their heart's content and they'll only refresh the 3rd action, eliminating the double form submission. 

This ends up being what is meant by Post-Redirect-Get.

1. The form is posted.
2. A redirect response is returned.
3. The browser gets the next page.

------

## Comparing `GET` and `POST`

Before moving on, here is a summary of the key differences between `GET` and `POST`.

|   		 | GET 						  |	POST								|
|------------|----------------------------|-------------------------------------|
| Usage		 | Intended for "search" requests | Intended for upload, sending sensitive information, or saving data | 
| Bookmarked | Can be saved as a bookmark | Cannot be bookmarked in the browser |
| Parameters | Parameter data is sent in the URL | Parameter data is sent in the request body |
| BACK button behavior | Requests are re-executed | Browser alerts the user that request data will be re-submitted |
| Restrictions | Only ASCII characters are allowed in the URL | Any kind of character data is allowed |
| Security | Less secure | More secure |
| Form Length | Limited to how many characters can fit in the URL | None |
| Visibility | Visible in the address bar | Parameters are no displayed in the URL |