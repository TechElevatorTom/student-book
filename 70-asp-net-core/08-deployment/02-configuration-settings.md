# Configuration Settings

In most of the DAL code that has been shown, in order to instantiate a DAL (data access layer), a connection string is needed. It is a very normal approach as the connection string is needed in order for the DAL to do its job.

Consider that most companies use separate *environments* (DEV, Quality Assurance, Production, etc.) to run their applications. In theory each time the code is moved from one environment to another, it needs to change. In reality, code should never be modified once it has been signed off and approved on (including changing a simple constant).

Most programs use *configuration files* to address this. A configuration file, or config file, is used to manage the settings for a program. Values such as file paths, server names, database connections, and credentials, are placed in a configuration file. Then as you need to change what connection string to use, you change these values without modifying the code. When the code gets changed there should be a good reason to do so (e.g. added a new feature, fixed bug, etc.).

In a .NET Core project, these settings are found in an `appsettings.json` file.  The default appsettings file is in a format called JSON (JavaScript Object Notation). JSON looks a lot like a dictionary where there are *keys* and *values*.

Here is a sample JSON file:

```JSON
{
  "Logging": {
    "LogLevel": {
      "Default": "Warning"
    },
  },
 "ConnectionStrings": {
	"default": "Data Source=.\SQLExpress; Initial Catalog=Database;..."
   }
  "AllowedHosts": "*",
}
```

In most cases you want to reference these settings from your `Startup.cs` file so you can configure your *services*. The `Startup` class has a property called `Configuration` that is injected during the initialization of our application. We use this property like so:

The following can be used to get the value of a configuration item, such as `AllowedHosts`:

```csharp
string hosts = Configuration["AllowedHosts"];
```

The following can be used to get a nested value (for instance, the default connection string):

```csharp
string connectionString = Configuration["ConnectionStrings:default"]
```

And, to get the LoggingLevel, you would write the following:

```csharp
string level = Configuration["Logging:LogLevel:Default"]
```

This approach should be favored over hard-coding connection strings. We can use it when configuring our Dependency Injection services.

```csharp
string connectionString = Configuration["ConnectionStrings:default"];

services.AddTransient<IMovieDal>(m => new MovieSqlDal(connectionString));
services.AddTransient<IActorDal>(m => new ActorSqlDal(connectionString));
```

## Follow Ups

For a full reference of .NET Configuration using JSON with sample usage, view the [JSON Configuration Reference](https://docs.microsoft.com/en-us/aspnet/core/fundamentals/configuration/?view=aspnetcore-2.1&tabs=basicconfiguration#json-configuration).

If you ever want to try and access the Configuration data from within a Controller, please refer to the blog post [Tip of the Week: How to Access Configuration from a Controller](https://blogs.technet.microsoft.com/dariuszporowski/tip-of-the-week-how-to-access-configuration-from-controller-in-asp-net-core-2-0/).