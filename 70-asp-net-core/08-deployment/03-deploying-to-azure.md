# Deploying To Microsoft Azure

**Disclaimer** Microsoft offers a series of different subscriptions and services, some paid and some un-paid. Every new member signup gets access to a number of services for 12-months (750 hours of virtual machines, 250 GB of Sql Database, 15 GB bandwidth, and [loads more](https://azure.microsoft.com/en-us/free/free-account-faq/)). New sign-ups also receive a $200.00 monthly credit towards other services for 1 year. At the end, users are switched to a *pay-as-you-go* subscription (similar to your electricity bill). The thing you'll most likely need to be aware of is that SQL Databases switch to $5/month for the smallest size.

This is a long tutorial. If it makes it any better, it is better now than it used to be. Before Cloud Computing, deploying web applications took a lot of money, work, and time. Now the tools are heavily integrated with IDEs and with some upfront work you can make deployments automated with a few button clicks.

## Getting Started

This tutorial makes the following assumptions:

1. You have a SQL database with SQL Scripts that you can use to build a database on demand.
1. You have a working web application that communicates to a SQL database.

If you don't have a SQL database your job will be easier, but let's face it, the site will probably be boring.

In this tutorial, you will be:

* creating a new resource group to hold all of our new resources together
* adding a new sql database to the resource group
* adding a new app service to the resource group
* deploying your code from visual studio to the app service

### Create a Resource Group

Within Azure, the term resource group refers to a logical container that holds related resources used for an application. What I would suggest doing, is creating a resource group per application so that you can easily see all of its contained resources.

1. From the left-hand navigation click **Resource Groups**.
![Click Resource Groups in Navigation](resources/Azure1.png)
2. Press **Add** from the top navigation to *Add A New Resource Group*
![Add New Resource Group](resources/Azure2.png)
3. Complete the fiields of the *New Resource Group* form by providing a: *name*, *subscription*, and *resource group location*.
![New Resource Group Fields](resources/Azure3.png)
4. Press **Create**.

### Add a SQL Database Resource

The first resource that you will add to your new resource group will be a SQL Database. If you don't have need for a database, you can skip this section.

1. Click **Create a resource** in the upper left-hand-corner of the Azure portal.
2. Select **Database** from the **New** page, and select **SQL Database**.
![Creating a Sql Database](resources/Azure4.png)
3. Fill out the SQL Database form using the following image to help insert values.
![SQL Database Form](resources/Azure5.png)
4. Under **Server**, click **Configure required settings** and complete the form that creates a logical server to hold your new database.
    * **Server name**: Any globally unique name
    * **Server admin & logon**: Any valid name
    * **Password**: Any password
    * **Subscription**: Any subscription
    * **Resource Group**: Your resource group
    * **Location**: Any valid location
![Creating a New Server](resources/Azure6.png)
5. Press **Select**
6. Select a **Pricing Tier** and select **Basic** and **Apply**
7. Now that you have completed the SQL Database form, click Create to provision the database. Provisioning takes a few minutes.
8. On the toolbar click **Notifications** to monitor the deployment process.

Once the database has been created you will want to build the tables and populate it with any initial data. This tutorial will show how to use the Azure Query Editor to view the contents of your SQL Database.

1. Click **SQL databases** from the left-hand menu and click the database you would like to query.
2. On the SQL database page for your database, find and click **Query editor (preview)** in the left-hand menu.
![Query Editor](https://docs.microsoft.com/en-us/azure/sql-database/media/sql-database-connect-query-portal/find-query-editor.png)
3. Click **Login** and then, when prompted, select **SQL Server authentication** and then provide the server admin login and password you provided when creating the database.
![Query Editor Window](https://docs.microsoft.com/en-us/azure/sql-database/media/sql-database-connect-query-portal/login-menu.png)
4. Click **OK** to login.
5. Paste any queries you want into the editor pane.
![Query Editor View](resources/Azure7.png)

Lastly, you need to get the connection string for this database so that you can have your application connect to it once deployed.

1. Click **Connection Strings**.
![Getting the Connection String](resources/Azure8.png)
2. Copy the connection string to your clipboard our your appsettings.Release.json file.

### Adding a New App Service

App Services are what are used in Azure to host websites, applications, or APIs. Now that you have a database, you're going to create an app service that you can deploy your ASP.NET Core application to.

1. Click **App Services** from the left-hand-menu and click **Create app service**.
![New App Service Window](resources/Azure9.png)
2. Select **Web App** and press **Create**
![Web App](resources/Azure10.png)
3. Complete the form that creates a new app service.
    * **App name**: Enter a unique name that will be the URL for your application
    * **Subscription**: your subscription
    * **Resource Group**: your existing resource group
![New App Service](resources/Azure11.png)
4. Click **Create New** when asked for an app service plan.
    * **App service plan**: Provide a name for the plan
    * **Location**: Select a location for the plan
    * **Pricing Tier**: Select the [pricing tier](https://azure.microsoft.com/en-us/pricing/details/app-service/windows/) for your plan. F1 provides a free tier for development purposes with 60 CPU minutes per day.
![Pricing Tiers](resources/Azure12.png)
5. Press **Apply** to select the plan and **Create** to create the app service.
6. Now that you have completed the SQL Database form, click Create to provision the database. Provisioning takes a few minutes.
7. On the toolbar click **Notifications** to monitor the deployment process.

Once created click on **App Services** and the new app service you created to view the status of the service along with the URL that was set up.
![App Services View](resources/Azure13.png)
![Getting the URL](resources/Azure14.png)

If you have a SQL database that you set up, you'll need to indicate to the Azure App Service what the connection string will be.

1. Select your app service that you created and click **Application settings**
![Application Settings Window](resources/Azure15.png)
2. Scroll down to the **connection strings** section and press **Add new connection string**
![Add Connection String](resources/Azure16.png)
3. Enter the following values in:
    * **Enter a name**: Use the key defined for your connection string in your appsettings.json file. 
    * **Enter a value**: Use the SQL Azure Connection string from your database you created earlier. Don't forget to enter your user id and password credentials in the string.
    * **Type**: Select SQL Azure
4. Press **Save**

### Deploying From Visual Studio to Our New App Service

1. Open your solution that contains the project you want to deploy.
2. Click on the **Build Menu** and then click **Publish *Project-Name***
![Publish Menu](resources/Azure17.png)
3. Press **Start** to publish your app to Azure or another host.
4. Choose **Select existing** Azure app service.
5. Click **Create profile** (to save your settings for later)
6. Select your Azure app service created from the previous step.
7. Press **OK** and then press **Publish**
8. Anxiously wait. Take a deep breath. If it succeeds it'll open a browser window to your URL.


## Glossary

* **App Service** - The compute resource that Azure App provides for hosting a website, web application, api, or mobile app backend.
* **Subscription** - The agreement with MSFT that enables a customer to obtain azure services.
* **Resource Group** - The container in resource manager that holds related resources for an application. Lets you define the components of an entire solution (web app, database, server, third-party service) together.


[Full Glossary](https://docs.microsoft.com/en-us/azure/azure-glossary-cloud-terminology)