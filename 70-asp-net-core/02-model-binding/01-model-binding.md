# Working with Models in ASP.NET MVC

A major benefit of working with an MVC framework is the ability to create a **separation of concerns**. With SOC, applications have the ability to separate the application logic (the models) from the way it is presented (the views).

This provides a number of different benefits:

1. The view isn't involved with code that performs logic. Its sole purpose is to output values of properties and methods into HTML.
1. The model doesn't worry how the information will be presented. It could be used to *Console.WriteLine()* or to generate HTML. It could even be used with a mobile application which uses an entirely different rendering engine. 

Within the same application we can realize the benefit by creating one view that displays a list of model objects while another page displays the details of a singular object. 

> #### Comment::An Analogy
>
> Using Amazon as an example, if you were to search for *C# Books*, you'll see a page that shows a list of books. Clicking on a given book allows you to view that information at a more detailed level. In this case, the same model gets used between both views.


## What is a Model?

In C#, a model is a simple C# class that represents the data of your app. Models often contain properties. They may also contain validation logic that enforces rules for what type of data can be held in the model.

Here is an example model:

```csharp
public class Book
{
    public string ISBN { get; set; }
    public string Title { get; set; }
    public decimal Cost { get; set; }
    public string Author { get; set; }
    public int TotalPages { get; set; }
}
```

### Using a Model

Models are instantiated and populated from within a controller. In most cases when the application uses a database, the controller uses a data access layer (DAL) to populate the model.

Let's say you had a `BooksController`. Inside it includes an `Index` action.

The code for this action would look like:

```csharp
public class BooksController : Controller
{
    public IActionResult Index()
    {
        // create a new data access object
        BooksDAL dal = new BooksDAL();	
        // retrieve all of the books
        IEnumerable<Book> books = dal.GetBooks();	
        // return the books as the model to the view
        return View(books);	
    }
}
```

Whenever the URL (`http://localhost:xxxxx/books/index`) is requested, the MVC framework will invoke this action. Your code inside will instantiate a new data access layer that retrieves the books and returns them as a model to the View. 

> #### Comment::An Analogy
>
> `View()` is a method defined in the base `Controller` class that returns a `ViewResult` data type (this is the thing that implements `IActionResult`).

Within the `Index` view, you will want to indicate that it expects a `List<Book>` or `IEnumerable<Book>` as its model. This is done by adding `@model IEnumerable<Book>` to the top of the file. This is similar to a variable declaration statement.

Anywhere in the same view file that we want to reference the model we use the reserved property `@Model`. The data type always depends upon `@model`. 

Here's how the Razor file will look.

```html
Index.cshtml

@* Bind the model to IEnumerable<Book> *@
@model IEnumerable<Book>

<h1>Book Listing</h1>

<p>There are @Model.Count books that match your search</p>

<div class="books">

@* Use Model to access the collection of books *@
@foreach(Book book in Model)
{
    <div class="book">
        <h2>@book.Title</h2>
        <p>@book.Cost.ToString("C")</p>
        <p>By: @book.Author (pages: @book.TotalPages)</p> 
    </div>
}
</div>
```

Hypothetically, if your view were passed a model with 3 books in it, it would look something like this:

```html
<h1>Book Listing</h1>

<p>There are 3 books that match your search</p>

<div class="books">
    <div class="book">
        <h2>Clean Code</h2>
        <p>$20.99</p>
        <p>By: Robert C. Martin (pages: 415)</p>
    </div>
    <div class="book">
        <h2>The Pragmatic Programmer: From Journeyman to Master</h2>
        <p>$15.94</p>
        <p>By: Andrew Hunt (pages: 387)</p>
    </div>
    <div class="book">
        <h2>Lauren Ipsum</h2>
        <p>$14.99</p>
        <p>By: Carlos Bueno(pages: 215)</p>
    </div>
</div>
```

This allows you to create very powerful templates that can output a massive amount of consistent HTML for all of the objects!

-----

## Summary

Here is a few things to keep in mind about views, controllers, and models.

1. Controllers return any object that implements the `IActionResult` interface. The `ViewResult` is one way of returning an `IActionResult`. 
1. A `ViewResult` is returned by writing `return View()`. By default, the name of the controller action is used for the view to display.
1. A view can have at most one model. This model is passed into the view via `return View(model)`.
1. In the .cshtml file, `@Model` is a reserved property that refers to the object passed into our `ViewResult`.
1. In the .cshtml file, `@model type` is used to indicate what the data type for `@Model` will be.