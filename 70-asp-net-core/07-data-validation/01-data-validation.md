# Form Validation

Before an app stores data in a database, the data must be validated. Data not only needs to be checked to make sure it holds valid values, but also to ensure it doesn't contain anything that may be a security threat. While validation is necessary, it can be tedious to implement. In a web application, validation often happens on both the client and the server.

When it comes to validation, each of the three MVC layers will play a role in ensuring that only valid data makes it into the system:

* The model will identify the validation rules to apply.
* The controller will enforce the validation rules and determine what to return to the user.
* The view will display the validation error messages.

## Model Validation

With ASP.NET MVC, validation attributes are used to indicate what data is acceptable for a given property. It works similar to validation on database columns. Rules can include constraints such as:

* Value must adhere to a specific data type.
* Value is required.
* Value must match a given pattern (e.g. phone, credit card, etc.).
* Value must be within a given range.

Below is a `Movie` model from an app that stores information about movies and TV shows. Most of the properties are required and several string properties have length requirements. Additionally, there's a numeric range restriction in place for the `Price` property that says the price must be at least 0, but less than $1,000. It is worth noting that while there are a number of validation attributes provided by ASP.NET MVC, you are able to custom validation attributes as well.

```csharp
public class Movie
{
    public int Id { get; set; }

    [Required]
    [StringLength(100)]
    public string Title { get; set; }

    [ClassicMovie(1960)] // custom
    [DataType(DataType.Date)]
    public DateTime ReleaseDate { get; set; }

    [Required]
    [StringLength(1000)]
    public string Description { get; set; }

    [Range(0, 999.99)]
    public decimal Price { get; set; }

    [Required]
    public Genre Genre { get; set; }

    public bool Preorder { get; set; }
}
```

Reading through the model reveals the rules about data for this app, making it easier to maintain the code. Below are other built-in validation attributes:

* `[CreditCard]`: Validates the property has a credit card format.
* `[Compare]`: Validates two properties in a model match.
* `[EmailAddress]`: Validates the property has an email format.
* `[Phone]`: Validates the property has a telephone format.
* `[Range]`: Validates the property value falls within the given range.
* `[RegularExpression]`: Validates that the data matches the specified regular expression.
* `[Required]`: Makes a property required.
* `[StringLength]`: Validates that a property has at most the given maximum length.
* `[Url]`: Validates the property has a URL format.

MVC supports any attribute that derives from the `ValidationAttribute` class. Other validation attributes can be found in the `System.ComponentModel.DataAnnotations` namespace.

There may be instances where more features are needed than built-in attributes provide. In these cases, a custom validation attribute can be created by deriving from `ValidationAttribute`. There is a section towards the bottom that explains this.

> #### Important::Notes on [Required]
>
> Non-nullable value types (such as `decimal`, `int`, `float`, and `DateTime`) are inherently required and don't need the `Required` attribute. The app performs no server-side validation checks for non-nullable types that are marked `Required`.
>
>To allow `null` values to be passed in for [non-nullable value types](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/nullable-types/), use `decimal?`, `int?`, `float?`, and `DateTime?`.

## Displaying Validation Errors

Given a form that looks like this:

```html
<form asp-controller="Movie" asp-action="New" method="POST">
    <input asp-for="Title" />
    <input asp-for="ReleaseDate" />
    <input asp-for="Description" />
    <input asp-for="Price" />
    <input type="button" value="Submit" />
</form>
```

In order to display proper error messages, you need to update your HTML to include `asp-validation-for` attributes using `<span>` tags.

```html
<form asp-controller="Movie" asp-action="New" method="POST">
    <input asp-for="Title" />
    <span asp-validation-for="Title"></span>

    <input asp-for="ReleaseDate" />
    <span asp-validation-for="ReleaseDate"></span>

    <input asp-for="Description" />
    <span asp-validation-for="Description"></span>

    <input asp-for="Price" />
    <span asp-validation-for="Price"></span>

    <input type="button" value="Submit" />
</form>
```

These `<span>` tags will create placeholders that render an error message (only in the event that an error occurs). In all other cases, they will be ignored and hidden from the user.


## Enforcing Validation

The last place validation occurs is in the controller. For instance, if the preceding form were submitted without entering any data, the `New(Move movie)` would receive the form `POST`, save the data to the database, and then redirect to the `Success` action method.:

```csharp
public class MovieController : Controller
{
    [HttpGet]
    public IActionResult New()
    {
        return View();
    }

    [HttpPost]
    [ValidateAntiForgeryToken]
    public IActionResult New(Movie movie)
    {
        // save movie to the database
        // movieDal.Create(movie);
        return RedirectToAction("Success");
    }
}
```

Although we defined the validation attributes on the models above, and we decorated the view to display the errors, we are not handling the case when the data is invalid.

By default, ASP.NET automatically validates the model properties. This process happens before each controller action is invoked. However, it is the responsibility of each controller action to responsibly address (or ignore) the errors. This is done by inspecting the `ModelState.IsValid` property. In many cases, the common response is to return the user to the same view with the errors. Otherwise, direct the user on to the next step (via post-redirect-get).

The implementation details for this look like:

```csharp
[HttpPost]
[ValidateAntiForgeryToken]
public IActionResult New(Movie movie)
{
    // See if there are errors
    if (!ModelState.IsValid)
    {
        // Display the New View again
        // with the errors
        return View(movie);
    }

    // otherwise save the movie
    movieDal.Create(movie);
    return RedirectToAction("Success");
}
```

Whenever there is an error the controller will handle the presence of errors and return the user to the original view (along with the original model data). When the page is displayed, it will include the error messages, as well as any data that was input on the form before the request was submitted.

-----


## Custom Validators

If the event that you need to create your own custom validation, you can define these rules as attributes. To do this, inherit from the `ValidationAttribute` class, and override the `IsValid` method. The `IsValid` method accepts two parameters, the first being an object named *value* and the second a `ValidationContext` object named *validationContext*. Value refers to the actual value from the field that our custom validator is validating.

In the following example, a business rule states that users may not set the genre to Classic for a movie released after 1960. The `[ClassicMovie]` attribute checks the genre first, and if it's a Classic, then it checks the release date to ensure that it's later than 1960. If it's released after 1960, validation fails. The attribute accepts an integer parameter representing the year we will use to validate the data. You can capture the value of the parameter in the attribute's constructor, as shown here:

```csharp
public class ClassicMovieAttribute : ValidationAttribute, IClientModelValidator
{
    private int year;

    public ClassicMovieAttribute(int year)
    {
        this.year = year;
    }

    protected override ValidationResult IsValid(object value, ValidationContext validationContext)
    {
        // We can only use this attribute on Movies so cast to a Movie first.
        Movie movie = (Movie)validationContext.ObjectInstance;

        // Once casted, the object can now be checked for the rules.
        if (movie.Genre == Genre.Classic && movie.ReleaseDate.Year > year)
        {
            return new ValidationResult($"Classic movie must be released before {year}");
        }

        return ValidationResult.Success;
    }
}
```

Visit the MSDN docs for a full reference on [ASP.NET Core Model Validation](https://docs.microsoft.com/en-us/aspnet/core/mvc/models/validation?view=aspnetcore-2.1#custom-validation).

https://docs.microsoft.com/en-us/aspnet/core/mvc/controllers/filters?view=aspnetcore-2.1#exception-filters
