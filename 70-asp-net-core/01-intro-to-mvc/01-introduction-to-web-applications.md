# Building Web Applications with ASP.NET Core

When building websites, there are two options:

1. Develop a static site that serves up a single HTML page for every URL.
1. Develop a dynamic web application that generates HTML pages based on the URL the visitor navigates to.

The first option works when your site is sufficiently small and not subject to a lot of change, but it doesn't scale. Imagine how many pages would be needed to support Twitter. Every time a new tweet is published a developer furiously works to write an HTML page allowing other visitors the ability to read it.

The second option, a web application, is used when there is a need to build an up-to-date, always changing website. Web applications consist of application logic (using C#, Java, PHP, etc.) that can both communicate with a database for information retrieval and generate on-the-fly HTML responses. 

The web application framework used depends on the programming language. With C#, the framework is called ASP.NET (Active Server Pages). 

## What is ASP.NET

ASP.NET is a server-side framework developed by Microsoft that allows you to develop dynamic web sites with C#. Web applications developed with the framework follow an established pattern that promotes consistency and agility to quickly develop a web application.

Within ASP.NET there are different types of web application frameworks. One of the most common is ASP.NET MVC.