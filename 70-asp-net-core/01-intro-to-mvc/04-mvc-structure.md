# ASP.NET MVC Project Structure

The MVC Project structure differs from the other types of projects that you have seen so far. There are a few important folders that have been added and **should not be removed**.

## Dependencies

![Dependencies](resources/dependencies.png)

This folder contains all of the dependencies of a project. A dependency is another library that is not included with the initial project template.

Visual Studio uses `NuGet` packages for server-side code dependencies.

> #### Note::NuGet
>
> **NuGet** is a package manager built into Visual Studio. Think of it like an app-store for code where code is accessible and can be imported into your project.

## `wwwroot`

![wwwroot folder](https://csharpcorner-mindcrackerinc.netdna-ssl.com/article/asp-net-core-2-0-project-structure-and-few-important-folders-files/Images/image001.png)

The `wwwroot` folder represents the actual root of the web application when running on a web server. It separates application files like models, views, and controllers from static files.

Any static files (`.css`, `.jpg`, `.png`, `.js`, etc.) which will be sent directly to the browser should be placed inside of wwwroot. Application files (`.cs`, `.cshtml`) are placed outside of wwwroot.

## The `lib` folder inside of wwwroot

This is a library folder which holds common library files used by the front-end of the application. Sometimes this folder may be updated when adding new packages to the project. 

Initially new projects come pre-configured with:
- [**Bootstrap**](https://getbootstrap.com/) - A front-end library to assist with responsive UI development.
- [**jQuery**](https://jquery.com/) - A JavaScript library used on a majority of live websites.
- **jQuery Validation** - An extension of jQuery providing common front-end validation checks.
- **jQuery Validation (Unobtrusive)** - Microsoft's built-in validation library.


## Controllers folder 

By convention, the **Controllers** folder holds all of the MVC controllers that a project references. Every controller meets the following criteria:

1. End with a suffix of `Controller`. Examples of this will be `MovieController`, `HomeController`, and `UserController`.
1. Inherit from `Microsoft.AspNetCore.Mvc.Controller`. This is automatically defined when creating new controllers.

Controllers look like normal classes. They contain methods that (in the context of MVC) are referred to as *Actions*. Every action returns an `IActionResult`. In other words, this can be any instance object whose class implements the `IActionResult` interface.

## Models folder

The **Models** folder holds all of the models that are used by the application. In ASP.NET MVC, models are essentially C# classes, and are often referred to as Plain Old CLR Objects, or POCOs. They often contain properties that might be displayed on a web page.

You are free to organize the contents of this folder however you wish.

## Views folder

The **Views** folder holds all of the views that are created. With ASP.NET MVC, a new syntax called [Razor][razor-syntax] is used. Razor Views have the `.cshtml` file extension. Razor Views allow you to create dynamic web pages that are a combination of HTML and Razor.

Views in this folder are typically stored in 1 of 2 locations:

1. A folder that corresponds with the name of the Controller that uses it. Views returned by the `HomeController` are stored in the *Home* folder.
2. A folder called *Shared* to indicate that the views may be used by multiple controllers.

Views are normally named after the controller action they are associated with. This isn't always the case but it is recommended, as this makes the code you write much easier to read and maintain.

## Other Items

There are a few other items that are included within the default ASP.NET MVC project.

1. `appsettings.json` - The `appsettings.json` file gives us a place to put those.
1. `Program.cs` - This may look familiar, although the content is slightly different than what we are used to. This file runs immediately upon starting the website and instructs ASP.NET how to run our application.
1. `Startup.cs` - This file will be used later on. It provides configuration details for ASP.NET so that features may be enabled/disabled for our use.

> #### Caution::Storing Sensitive Information
>
> Passwords and credentials should NEVER be stored in code or in the appsettings.json file, especially when it is version controlled. Please refer to [Microsoft's documentation](https://docs.microsoft.com/en-us/aspnet/core/security/app-secrets?view=aspnetcore-2.2&tabs=macos)for best practices and strategies for storing sensitive information.

------------

The blog post [What's This and Can I Delete It? Examing a Default ASP.NET Core MVC Project](https://exceptionnotfound.net/whats-this-and-can-i-delete-it-examining-a-default-asp-net-core-mvc-project/) explains many of the items in the default project and goes into further detail about their necessity.

---

[razor-syntax]: (https://docs.microsoft.com/en-us/aspnet/core/mvc/views/razor)