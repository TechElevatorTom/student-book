# Razor Views

The HTML that is generated and returned to the client comes from Views. In ASP.NET MVC these are referred to as Razor Views. Razor views are special HTML files (.cshtml) that are capable of including the syntax referred to as Razor.

## Razor
 
Razor is a syntax loosely based on C#. It lets you reference and use any data type available in C# within your HTML. This can be useful when you want to conditionally (`if/else`) display data, or loop (`for`) through data so it can be displayed on a page. For instance, if you wanted to display all of the items in an array in an HTML list or as rows in a table, you could use a `for` loop to create the list or table rows dynamically.

Razor blocks are identified by the `@` character. This character will precede a Razor expression or a block of code.

### Razor Expressions

Consider the following Razor View that prints out the current date and time. In C# the current date and time can be determined using `DateTime.Now`. That same expression could be used to display the updated date and time every time the page is requested:

```html
Index.cshtml

<h1>The current date and time is @DateTime.Now</h1>
```

Notice the intermix of `<h1>` and `@DateTime.Now`. Razor is intelligent enough to determine the literal text to display and the actual expression to execute.

When the server renders the page the browser would receive the following (adjusted for latest date/time) HTML:

```
<h1>The current date and time is 6/22/2018 5:18 PM</h1>
```

> #### Info::Server Date and Time
>
> This date and time would be the date and time of the server, where the HTML was generated.


### Razor Comments

If you want to write a comment in Razor (similar to a C# comment) you are required to enclose the code between `@*` and `*@`.

```html
Index.cshtml

@* This is a Razor comment *@
<h1>The current date and time is @DateTime.Now</h1>
```

Comments don't get rendered to the page and as such the browser would receive the following HTML:

```
<h1>The current date and time is 6/22/2018 5:18 PM</h1>
```

### Razor Blocks

While Razor expressions are sufficient for in-line values, occasionally you may have cases when you need to execute blocks of code. Razor can be used to open and close a block allowing you to declare variables that can be referenced later in expressions in the same view they are declared in.

For instance, consider the following Razor syntax:

```html
Index.cshtml

@{
    string name = "Al";
}

<h1>Hello @name.ToUpper()</h1>
```

This would render:

```
<h1>Hello Al</h1>
```

The browser requesting the URL never knows Razor is used because the final HTML is generated server-side before being sent back to the browser.

### Control Flows

Razor blocks can be used to create `if/else` logic or any of the loops that are possible in C#. 

```html
Index.cshtml

@{ 
    bool isMorning = (DateTime.Now.Hour < 12);
}

@if (isMorning) 
{
    <h1>Good morning!</h1>
}
else
{
    <h1>Good day!</h1>
}
```

Depending on the time of day the page is requested, the user would see:

```html
<h1>Good morning!</h1>
```

or

```html
<h1>Good day!</h1>
```

> #### Info::Razor Intellisense
>
> Notice how you can combine HTML blocks with Razor code. Intellisense in Visual Studio is usually able to determine when you are writing Razor or HTML syntax. However, sometimes errors do occur. When this happens, try closing and reopening the file. This will give Visual Studio a chance to parse the file again.

Below is an example of creating an unordered HTML list with Razor.

```html
Index.cshtml

<ul>
    @* Loop from 1 to 5 *@
    @for(int i = 1; i <= 5; i++)
    {
        @* Multiple i * 2 and print out the result of the expression *@
        <li>@(i*2)</li>
    }
</ul>
```

This would generate the following HTML:

```html
<ul>
    <li>2</li>
    <li>4</li>
    <li>6</li>
    <li>8</li>
    <li>10</li>
</ul>
```

Clearly, Razor is a powerful markup language that will allow us to create web dynamic web pages. To learn more about how to use Razor, please refer to [Microsoft's documentation for Razor](https://docs.microsoft.com/en-us/aspnet/core/mvc/views/razor?view=aspnetcore-2.1).

------

## Adding Razor Views to a Project

Before a view can be added, a controller and an action need to exist so the user can navigate to the site using a URL. Recall from the previous chapter, if the user visits `http://localhost:xxxxx/`, `http://localhost:xxxxx/home`, or `http://localhost:xxxxx/home/index`, the request will be routed to the `HomeController` and the `Index` action. 

With that, the following will be the first code that will run:

```csharp
public class HomeController : Controller
{
    public IActionResult Index()
    {
        // defaults to the Index view, using the name of the action	
        return View(); 
    }
}
```

You'll go deeper into what's going on inside of the action method later on. For now, add a new view that will be used by the `Index` action. From the solution explorer right-click on **Home > Add > View**.

![Adding a New View](resources/AddingANewView.png)

From the **Add View** window use the following settings:

* **View Name** - _Index_.
* **Template** - _Empty (without model)_.
* **Use a layout page** - _Unchecked_. Layout pages will be covered later.

![New View Screen](resources/NewViewScreen.png)