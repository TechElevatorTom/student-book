# Routing

In MVC apps, the controller does a lot of the work handling the incoming request and determining what needs to be done. Before the controller do its job, _the controller_ that handles the request must be first identified. In ASP.NET MVC, this happens during request **routing**.

ASP.NET uses simple logic to route a request to the correct controller. It starts by parsing the URL into values, referred to as route-data. The three main values identified in a route include:

1. **Controller** - The controller that will process the incoming request.
1. **Action** - The method on the controller (action) that needs to be invoked.
1. **Id** (optional) - The identification data that has been provided to identify a specific resource

URLs follow a pattern so that ASP.NET can identify these route-data values. Given a URL, the pattern is: `http://www.domain.com/controller/action/id`.

ASP.NET MVC pulls each of these pieces out of the URL and uses it to identify which controller and action handle the request. There are some default values that it uses if the URL path doesn't include all of the segments:

- If the controller is not provided, the controller defaults to `Home` (e.g. `http://www.domain.com/`).
- If the action is not provided, the action defaults to `Index` (e.g. `http://www.domain.com/Movies/`).
- The id part of the URL is optional. (e.g. `http://www.domain.com/Movies/List`).

> #### Comment::Server Date and Time
>
> Other parameters provided via the query string (?) may be used. You will see how those work in a later section.

Below is a table demonstrating sample URLs and how the ASP.NET MVC framework would extract values from the URL so that it can be routed.

| URL                                               | Controller          | Action   | Id (optional) |
| ------------------------------------------------- | ------------------- | -------- | ------------- |
| `http://localhost:1234/`                          | `HomeController`    | `Index`  | n/a           |
| `http://localhost:1234/Home/`                     | `HomeController`    | `Index`  | n/a           |
| `http://localhost:1234/Home/Index`                | `HomeController`    | `Index`  | n/a           |
| `http://localhost:1234/Movies/`                   | `MoviesController`  | `Index`  | n/a           |
| `http://localhost:1234/Movies/Detail/5`           | `MoviesController`  | `Detail` | `5`           |
| `http://localhost:1234/Movies/Detail?id=5`        | `MoviesController`  | `Detail` | `5`           |
| `http://localhost:1234/Profile/`                  | `ProfileController` | `Index`  | n/a           |
| `http://localhost:1234/Profile/Detail/j.smith`    | `ProfileController` | `Detail` | `j.smith`     |
| `http://localhost:1234/Profile/Detail?id=j.smith` | `ProfileController` | `Detail` | `j.smith`     |
| `http://localhost:1234/Profile/List?page=3`       | `ProfileController` | `List`   | n/a           |
