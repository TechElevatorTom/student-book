# ASP Attributes

All of the standard HTML elements and attributes work within an ASP.NET Core MVC applications (`<div>`, `<a>`, `<img>`, etc.). In addition to the standard attributes, there are a few special attributes referred to as _ASP.NET attributes_. These utilitarian attributes can assist you with such tasks like generating a dynamic URL, form, or validation message.

These attributes are recognizable because they begin with `asp-*`. To identify which HTML elements support `asp-attributes` look for the `@` in the Razor intellisense dropdown (e.g. `@ a`, `@ form`, `@ div`, etc.).

The following sections identify one of the most common elements that you will use these attributes with.

## Anchor Tag Helper

The Anchor Tag Helper enhances the standard HTML anchor `(<a ... ></a>)` tag by adding new attributes. The rendered anchor element's `href` attribute value can be determined by the values of the `asp-` attributes instead of manually written out.

### asp-controller

The asp-controller attribute assigns the controller used for generating the URL. The following markup can be used to generate a link to `/Speaker`:

```html
<a asp-controller="Speaker">All Speakers</a>
```

The generated HTML would be:

```html
<a href="/Speaker">All Speakers</a>
```

If you want more control over the URL (e.g. `Speaker/Evaluations`) you can use `asp-action`.

### asp-action

The asp-action attribute value represents the action name included in the generated `href` attribute. The following markup sets the generated `href` attribute value to the speaker evaluations page:

```html
<a asp-controller="Speaker"
    asp-action="Evaluations">Speaker Evaluations</a>
```

The generated HTML would be:

```html
<a href="/Speaker/Evaluations">Speaker Evaluations</a>
```

### asp-route-{value}

To add additional values to the URL (most often in the query string `?`), the `asp-route-{value}` attribute is used.

Any value occupying the `{value}` placeholder is interpreted as a potential route parameter and added to the URL.

Consider the following anchor link using the asp-attributes:

```csharp
@model Speaker
<a asp-controller="Speaker"
   asp-action="Detail"
   asp-route-speakerid="12">SpeakerId: 12</a>

<a asp-controller="Profile"
   asp-action="Detail"
   asp-route-email="al@techelevator.com">Email: al@techelevator.com</a>

<a asp-controller="Users"
   asp-action="Detail"
   asp-route-id="1">User: 1</a>
```

The generated HTML would be:

```html
<a href="/Speaker/Detail?speakerid=12">SpeakerId: 12</a>
<a href="/Profile/Detail?email=al@techelevator.com">Email: al@techelevator.com</a>
<a href="/Users/Detail/1">User: 1</a>
```

When using this with any parameter (e.g. `asp-route-word` or `asp-route-count`), the parameter name and value will be appended to the URL as query string parameters (e.g. `?speakerid=12`.

> #### Comment::Server Date and Time
>
> In the following section, ASP.NET Routing, you will learn that `id` is a special route parameter. If you use it in the `asp-route` attribute, it will be added to the end of the URL path instead of the query string.


For the remaining attributes and example usage, see the [Anchor Tag Helper Reference][anchor-tag-helper-reference]

## Other Tag Helpers

The `<form>`, `<input>`, and `<select>` tags all have asp-attributes that will be covered in a later section.

---
[anchor-tag-helper-reference]: https://docs.microsoft.com/en-us/aspnet/core/mvc/views/tag-helpers/built-in/anchor-tag-helper?view=aspnetcore-2.1