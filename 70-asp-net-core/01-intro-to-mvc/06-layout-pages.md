# Layout Pages

Web apps frequently contain similar visual elements. This section covers how to create common layouts so that code can be shared in the many different views of our ASP.NET application.

## What Is a Layout Page

Most sites have a common layout that provides the user with a consistent experience as they navigate from page to page. A layout often includes an app header, navigation or menu elements, location for content, and a footer.

![Common Layout](https://docs.microsoft.com/en-us/aspnet/core/mvc/views/layout/_static/page-layout.png?view=aspnetcore-2.1)

Other HTML structures such as scripts and stylesheets are used by many pages of an app and shouldn't need to be redefined. A layout file provides the ability to define these shared elements so that each view referencing the layout can reuse them. Layouts help us reduce duplicate code in views, following the [Don't Repeat Yourself (DRY) principle](http://deviq.com/don-t-repeat-yourself/).

By convention, a Layout Page in an ASP.NET Application is named `_Layout.cshtml`. If you use the ASP.NET Core MVC project template to create your project, the default Layout Page will be located in the `Views/Shared` folder.

## Example Layout Page

An example Layout Page looks like this:

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome!</title>
</head>

<body>
  <div class="navbar">
    <ul>
      <li><a asp-controller="Home" asp-action="Index">Home</a></li>
      <li><a asp-controller="Home" asp-action="About">About</a></li>
      <li><a asp-controller="Home" asp-action="Contact">Contact</a></li>
    </ul>
  </div>
  <div class="container body-content">

    @RenderBody()

    <hr />

    <footer>
      <p>&copy; 2016 - WebApplication1</p>
    </footer>
  </div>

  @RenderSection("scripts", required: false)

</body>
</html>
```

Notice that this does not include any reference to the main content on the page aside from something called `RenderBody()`. Every Layout Page includes this and it will be covered in an upcoming section.

### Specifying a Layout from a View

In order to understand how a layout page works you need to first revisit the process for handling an incoming request to an ASP.NET MVC application. If a request is sent to `http://localhost:xxxxx/home/why`, the request is mapped to the `HomeController` and the `Why` action.

```csharp
public class HomeController : Controller
{
  public IActionResult Why()
  {
    return View();
  }
}
```

This action returns a `ViewResult` which by default looks for a view file located at `Views/Home/Why.cshtml`. Razor views are similar to objects and as such can have properties. One such property is a `Layout` property. Views specify which layout file to use by setting this property:

```cshtml
@{
  Layout = "_Layout";
}

<h2>Why Us?</h2>

<p>When you work with Acme Co. you're guaranteed to deal with the best
  products your money can by. We always stand by our word to deliver
  on time and on budget.
</p>
```

The layout specified can use a full path (example: `/Views/Shared/_Layout.cshtml`) or a partial name (example: `_Layout`). When a partial name is provided, the Razor view engine will search for the layout file. The controller-associated folder is searched first, followed by the Shared folder.

> #### Info::RenderBody()
>
> By default, every layout must call `RenderBody()`. Wherever the call to RenderBody() is placed, the contents of the view will be rendered.

Using the layout page from before, the full page when generated will have the following HTML:

```html
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Welcome!</title>
</head>

<body>
  <div class="navbar">
    <ul>
      <li><a asp-controller="Home" asp-action="Index">Home</a></li>
      <li><a asp-controller="Home" asp-action="About">About</a></li>
      <li><a asp-controller="Home" asp-action="Contact">Contact</a></li>
    </ul>
  </div>
  <div class="container body-content">

  <!-- RenderBody() was here -->
  <h2>Why Us?</h2>

  <p>When you work with Acme Co. you're guaranteed to deal with the best
    products your money can by. We always stand by our word to deliver
    on time and on budget.
  </p>

  <hr />

  <footer>
    <p>&copy; 2016 - WebApplication1</p>
  </footer>
</div>
</body>
</html>
```

### Layout Sections

Layout pages can optionally reference _layout sections_, by calling `RenderSection`.

```cshtml
@RenderSection("scripts", required: false)
```

Sections provide a way to organize where certain page elements should be placed in the layout while allowing the individual view the ability to specify the content to be rendered.

Individual views specify the content to be rendered within a section using the `@section` Razor syntax.

Each call to `RenderSection` can specify whether that section is required or optional. If a required section isn't found in the view, then an exception is thrown.


An example `section` definition in a View:

```
@section scripts {
  <script type="text/javascript" src="/scripts/main.js"></script>
}
```

In the code above, the script tag is added to the `scripts` section in the Layout Page. The placeholder `@RenderSection("scripts", required: false)` indicates that any View implementing the `scripts` section should place its content here.

-----

For a full reference on Layout Pages, view the [ASP.NET Layout Pages Reference](https://docs.microsoft.com/en-us/aspnet/core/mvc/views/layout).
