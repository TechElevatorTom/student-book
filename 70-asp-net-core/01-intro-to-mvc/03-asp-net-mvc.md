# Working with ASP.NET MVC

Let's look at how we can create a new ASP.NET MVC application using Visual Studio.

From Visual Studio, select **File > New > Project**

![New Project](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/start-mvc/_static/alt_new_project.png)

Complete the **New Project** dialog:

1. In the left pane, click **.NET Core**
1. In the center pane, click **ASP.NET Core Web Application (.NET Core)**
1. Name the project.
	1. **Note:** When naming your project this will be used for all of your namespaces. From experience, it is a bad idea to name a project with hyphens since that is not a recognized character in C#.
1. Click **OK**

Complete the **New ASP.NET Core Web Application** dialog:

1. Select **Web Application(Model-View-Controller)**
1. Click **OK**

![New ASP.NET Core Web Application](https://docs.microsoft.com/en-us/aspnet/core/tutorials/first-mvc-app/start-mvc/_static/new_project22-21.png?view=aspnetcore-2.1)


## Running Your New ASP.NET MVC Application

Visual Studio used a default template for the project you just created. You have a working app now, and you can see it run in a web browser by pressing **`Ctrl+F5`**. It is pretty basic, but it is a great starting place for new projects.

When launching the app, you may notice that the address bar shows `localhost:port#`, not something like `example.com`. This is because `localhost` is the standard name that references your local computer. Visual Studio relies on this to run your application locally. It also selects an available port number (modifiable through the project settings), allowing you the ability to run multiple websites at once.