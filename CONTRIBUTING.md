# Contributing


## Setup

1. Install [**node package manager**](https://www.npmjs.com/).
1. Install GitBook

    Gitbook is needed in order to generate the book from markdown.

    ```
    npm install -g gitbook
    ```
1. Install Gitbook Plugins

    This book uses a number of different packages in order to generate content that is not available out of the box (e.g. Tabs, Callouts, Embedded Videos, etc.).

    ```
    gitbook install
    ```

    Anytime someone adds a new plugin, you'll need to do this.
1. Configuring/Generating the Book

    The book primarily covers concepts that are universal to both C# and Java. Compiling the book generates two separate versions. The `book.json` file is used to configure which version of the book will be generated.

    ```javascript
    "variables": {
        "language": "C#" //"Java"
    }
    ```

    Use this command to generate the book.

    ```
    gitbook serve
    ```

    You can also generate both versions of the book with:

    ```
    npm run build-all
    ```

----- 
## Submitting Updates

When authoring new changes to the book, create a separate branch.

Once ready, changes should be submitted via pull request. The pull request should have one other reviewer involved for peer review purposes. If approved, it can be merged into the master branch. 

Once the change has been merged into the master branch, the book will need to be updated at [http://book.techelevator.com](http://book.techelevator.com). **As of now, this is a manual process due to the fact that a continuous integration process does not exist.**


----- 
**NOTE**: The book primarily covers concepts that are universal to both C# and Java. When the content needs to vary because of terminology or code, you should ensure to cover both cases.

To do this, include the following code snippet throughout your markdown.
```
{% if book.language === 'Java' %}
    Java content goes here
{% elif book.language === 'C#' %}
    C# content goes here
{% endif %}
```



