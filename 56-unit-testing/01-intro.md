# Unit Testing

In this chapter, you'll learn about a best practice in programming: unit testing.

This chapter covers:

* What unit testing is
* Why programmers do it
* Other types of types of testing, who performs them, and how they differ from unit testing
* What makes a good unit test
* Examples of unit tests