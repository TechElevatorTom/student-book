# Creating some real unit tests

Now, you're going to write some real unit tests.

> #### One more thing to discuss - Errors vs Failures
>
> One common issue that often confuses students is the difference between Errors and Failures.
>
> Errors are errors in your code or in the unit test. This is likely to be a runtime error, such as a null reference exception or an array out of bounds exception.
>
> If you have a failure, this means that your unit test failed. So you likely have a logic error - meaning that your code is not doing what you expect it to do, or more appropriately, what the unit test expects it to do.

{% if book.language === 'Java' %}

Here at Tech Elevator, you use a testing framework called JUnit to create and run unit tests.

## Adding Files and Tests

Like all other Java code, unit testing code is defined within a class. Each test class typically contains all of the unit tests for a single "unit" of production code.

If the "unit" is a single class from the production code, then the convention is that the test class belongs to the same package as the class under test. The name of the test class is the name of the production class with "Test" at the end. For example, the test class for the production class "Foo" would be "FooTest".

JUnit includes three main annotations for methods to indicate that they are used in testing:

| Annotation | Description                                                                                                                                                                                                                                         |
| ---------- | --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| @Test      | Indicates the method is a test method. Test methods are always public, return void, and take no arguments.                                                                                                                                           |
| @Before    | Indicates that the method should be executed immediately prior to every test. It is intended to be used when there is a repetitive setup  task that is performed by several tests (for example, "Arrange").                                                  |
| @After     | Indicates that the method should be executed immediately after every test. It is intended to be used when there is a repetitive cleanup task that is performed by several tests (for example, deleting temp files, rolling back database transactions, etc) |

## Viewing Tests

You can view tests in the JUnit window. It pops open automatically once you've run a test, or you can open it yourself by selecting Window->Show View->Other and searching for JUnit.

## Running Tests

You can run all of the tests in a file by either right clicking on the file or clicking the Run button on the top navigation bar and selecting Run As->JUnit Test. You can also run tests from the JUnit window. Once all of the tests in a file have been run once, you have the ability to re-run a single test from the JUnit window.

## Debugging Tests

You can debug a unit test by putting in a break point and then right clicking on the file.  Another way is to click the Debug button on the top navigation bar and select Debug As->JUnit Test. You can also run tests from the JUnit window by right clicking on the test and selecting Debug.

```Java
@Test
public void length_returns_the_number_of_characters_in_a_String() {
    System.out.println("length_returns_the_number_of_characters_in_a_String"); // FOR DEMONSTRATION PURPOSES ONLY, don't do this in your own tests

    /* The assertEquals method validates that two values are equal and
        * fails the test if they are not equal */

    String theString = "Java"; // Arrange
    int length = theString.length(); // Act
    Assert.assertEquals(4, length); // Assert
}
```

{% elif book.language === 'C#' %}

## Creating a Project

Here at Tech Elevator, you use a tool called MSTest to test. The unit tests will be stored in their project on your solution.

1. Open the solution that contains the project that you want to write unit tests for.
2. Add the test project to your solution
   1. Go to File->New->Project
   2. Select .NET Core MSTest Test Project (.NET Core)
   3. Name your project something descriptive - ExistingProject.Tests
3. Add the existing project as a dependency to the new project
   1. Right click on the test project
   2. Select Add->Reference
   3. Click on Projects->Solution
   4. Select the checkbox next to the name of the project
   5. Click on the OK button

After you have your project, you are ready to start adding files to hold your unit test.

## Adding Files and Tests

Tests in the Test Explorer are organized by project name, file name, and test name, so organize your files and tests accordingly. In general, you want to create one file with tests for each .cs file and append Tests to the name. So, for Grader.cs, you create a file called GraderTests.cs. A method can be implemented as a test using the TestMethod attribute.

There are some testing attributes built in to MSTest that you need to be familiar with:

- Before the class name: [TestClass]
- Before a test method: [TestMethod]
- To run a test method multiple times with different inputs: [DataTestMethod]/[DataRow]
- For a comprehensive list of attributes, visit https://www.automatetheplanet.com/mstest-cheat-sheet/

If you have a common setup (Arrange) that needs to be done for a number of tests,  use the [TestInitialize] attribute on a method to indicate that it should be executed before every test.

MSTest has a special Assert method. Be aware that the order matters for the expected value and resulting value, or the message will be very confusing if the test fails. If you type Assert, Intellisense shows you all of your options. If you want to read more, visit https://www.meziantou.net/mstest-v2-exploring-asserts.htm.

You need to make sure to include the packages you are using at the top:

```csharp
using Individual.Exercises.Classes; // or the name of the project that contains the code you want to unit test

using Microsoft.VisualStudio.TestTools.UnitTesting;
```

## Viewing Tests

Unit tests can be viewed and executed from the Test Explorer window. If this is not visible, you can go to the Test menu and select Windows->Test Explorer. Tests are organized by project name, file name, and test name.

## Running Tests

You can run tests by right clicking within the test and selecting run, or from within the Test Explorer window, select Run All. You may also right click on the test and click on Run Selected. 

## Debugging Tests

You can also debug tests. To do so, put a breakpoint in your code and select Debug instead of Run. Selecting a number of tests all at once does not guarantee that they will run in any particular order. This is a common cause of issues.

## An example of the grader test

```csharp
using Individual.Exercises.Classes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Individual.Exercises.Tests
{
    [TestClass]
    class GradeTests
    {
        [TestMethod]
        public void ninetyIsAnA()
        {
            //Arrange
            double score = 90.0;

            //Act
            char result = Grader.getLetterGrade(score);

            //Assert
            Assert.AreEqual('A', result, "90 should be an A");

        }

    }
}
```

Use the [DataTestMethod] and [DataRow] attributes if you are testing the same method with similar input/output expectations:

```csharp
[DataTestMethod]
[DataRow(100.5,'A')]
[DataRow(100, 'A')]
[DataRow(90,'A')]
[DataRow(89.99, 'B')]
[DataRow(80, 'B')]
[DataRow(79.99, 'C')]
[DataRow(70, 'C')]
[DataRow(69.99, 'D')]
[DataRow(60, 'D')]
[DataRow(59.99, 'F')]
[DataRow(0, 'F')]
[DataRow(-5, 'F')]
public void testGraderEdgeCases(double input, char expectedGrade)
{
    //Arrange
    double score = input;

    //Act
    char result = Grader.getLetterGrade(score);

    //Assert
    Assert.AreEqual(expectedGrade, result);
}
```

{% endif %}
