# Arrays and Loops

This chapter will introduce two specific concepts: arrays and loops.

Arrays are a special data type found in almost all programming languages. They make it more convenient to work with values in aggregate rather than a series of individual variables.

Loops are a fundamental concept to managing program control flow, but they go hand in hand with arrays. Loops provide a very convenient way to write a small amount of code that can iterate through all of the values in an array one-by-one.

By the end of this chapter you should have an understanding of:

* Creating arrays, and getting and setting values in an array.
* How to write and use for-loops in general, and more specifically to "walk-through" an array.
* Scoping, and why where variables are declared is important.
* Incrementing and decrementing values using shorthand assignment.

