## Array Basics

When you want to work with collections of values, you use arrays. Rather than creating multiple variables, you create a single variable name that has the possibility to represent multiple values. Instead of worrying whether the test score is Bea's or Sam's (Sam 1 or Sam 2), you interact with the variable as a collection of scores.

### Declaring and Initializing an Array

Here's the syntax for declaring and initializing a new empty array:

```javascript
let testScores = [];
```

This line declares a new variable, called `testScores`, and assigns it an empty array.

If you know the values of your array, you can provide those values at the time of initialization.

```javascript
let testScores = [ 85, 96, 80, 98, 89, 70, 93, 84, 66, 96 ];
```