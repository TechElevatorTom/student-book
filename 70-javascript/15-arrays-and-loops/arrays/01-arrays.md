# Arrays

## What Do Arrays Help Solve?

When you saw variables and datatypes, you learned how to create variables and assign values to them. Using what you learned there, your first inclination might be to write the following bit of code to calculate the average test score for a group of students.

```javascript
let aaronScore = 80;
let beaScore = 98;
let billScore = 89;
let greerScore = 70;
let kyleScore = 93;
let margoScore = 84;
let peteScore = 66;
let zedScore = 96;
let averageScore = (aaronScore + beaScore + billScore + greerScore +
                    kyleScore + margoScore + peteScore + zedScore) / 8.0;
```

This will work, but it is a bit awkward for a couple of reasons. First, there's an awful lot of typing. Second, it's difficult to keep everything straight. What if you forgot to include the scores for the two Sams in the class? You can add them to the code easy enough.

```javascript
let samScore = 85;
let samScore = 96;
.
.
.
let averageScore = (samScore + samScore + aaronScore + beaScore + billScore + greerScore +
                        kyleScore + margoScore + peteScore + zedScore) / 10.0;
```

However, the second `samScore` causes a "duplicate variable" compiler error. But you're a clever programmer and figure out another way -- change the variable names!

```javascript
let student01 = 85;    // Sam
let student02 = 96;    // Sam
let student03 = 80;    // Aaron
let student04 = 98;    // Bea
let student05 = 89;    // Bill
let student06 = 70;    // Greer
let student07 = 93;    // Kyle
let student08 = 84;    // Margo
let student09 = 66;    // Pete
let student10 = 96;    // Zed
let averageScore = (student01 + student02 + student03 + student04 + student05 +
                    student06 + student07 + student08 + student09 + student10) / 10.0;
```

Then again, 10 students sounds like a pretty small class. A more realistic size is probably 25 ... or more. Changing the variable names doesn't really address the issue of too much typing, or lessen the potential confusion caused by so many variables. Having to add 15 more students only exacerbates the problem. Additionally, good naming conventions specifically advise against naming variables sequentially this way.

Fortunately, there is an alternative, an array.