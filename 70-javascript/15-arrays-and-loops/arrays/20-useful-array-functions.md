## Useful Array Functions

JavaScript arrays have many useful built in functions. You can find documentation on them at [the MDN site on Arrays](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array).

### `push()`

The `push()` function is used to add a new element to the end of an array.

``` JavaScript
let numbers = [1, 2, 3];
numbers.push(4);
// numbers is now [1, 2, 3, 4]
```

### `unshift()`

The `unshift()` function is used to add a new element to the beginning of an array.

``` JavaScript
let numbers = [2, 3, 4];
numbers.unshift(1);
// numbers is now [1, 2, 3, 4];
```

### `pop()`

The `pop()` function is related to the `push()` function. `pop()` will remove an element from the end of the array. It will also return that element that was removed.

``` JavaScript
let numbers = [1, 2, 3, 4];
let removedElement = numbers.pop();
// numbers now is [1, 2, 3];
// removedElement is 4;
```

### `shift()`

The `shift()` function is related to the `unshift()` function. `shift()` will remove an element from the beginning of the array. It will also return the element that was removed.

``` JavaScript
let numbers = [1, 2, 3, 4];
let removedElement = numbers.shift();
// numbers now is [2, 3, 4];
// removedElement is 1;
```

### `includes()`

The `includes()` function is used to see if a given value is in an array. It won't tell you how many times it's in the array, but will return a true if it found the value at all or false if it isn't found.

``` JavaScript
let numbers = [1, 2, 3];
let foundThree = numbers.includes(3); // foundThree will be true
let foundFour = numbers.includes(4); // foundFour will be false
```

### `indexOf()`

The `indexOf()` function is used to find the location of a value in an array. It will find the first instance of the value and then return the index to that element. If it can't find the value, it will return `-1`.

``` JavaScript
let numbers = ['one', 'two', 'three', 'four', 'three', 'five', 'three'];
let indexOfThree = numbers.indexOf('three'); // indexOfThree will be 2, the index of the first 'three'
let indexOfFour = numbers.indexOf('six'); // indexOfFour will be -1
```

### `lastIndexOf()`

The `lastIndexOf()` function is used to find the last location of a value in an array. It will find the last instance of a value and the return the index to that element. If it can't find the value, it will return `-1`.

``` JavaScript
let numbers = ['one', 'two', 'three', 'four', 'three', 'five', 'three'];
let indexOfThree = numbers.indexOf('three'); // indexOfThree will be 5, the index of the last 'three'
let indexOfFour = numbers.indexOf('six'); // indexOfFour will be -1
```

### `join()`

The `join()` function will take an array and return a string, putting a value in between every element. The array itself is not changed.

``` JavaScript
let numbers = [1, 2, 3];
let joinedNumbers = numbers.join(';');
// joinedNumbers is "1;2;3"
```

### `reverse()`

The `reverse()` function will reverse the array, making the first element the last and the last element first, and reversing everything in between too. It will reverse the array in place, changing the original.

``` JavaScript
let numbers = [1, 2, 3];
numbers.reverse();
// numbers is [3, 2, 1];
```