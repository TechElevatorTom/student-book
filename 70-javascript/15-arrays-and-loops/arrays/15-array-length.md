## Determining the Length of an Array

What if you wanted to get the last element in the array?

```javascript
    let testScores = [];

    // ... populate the array

    let lastValue = testScores[9];
```

It's cheating though since we know that 9 is going to be the last index. What if someone changed the size of the array and made it smaller? Then `lastValue` would be assigned the value of `undefined` because an array element would no longer exist at index 9.

As a programmer, you should strive to avoid hard-coding numeric values into your program. Fortunately you have a formula for calculating the last index at runtime ,`length - 1`. Most programming languages provide a way to access this value for a given array.

Each array has a property, `.length` that allows you to retrieve the size of the given array.

```javascript
    let size = testScores.length;
```

That syntax is often used when referencing the last value.

```javascript
let testScores = [90, 85, 70, 55, 70, 75, 95, 100];

let lastValue = testScores[testScores.length - 1];
let secondToLastValue = testScores[testScores.length - 2];
```

Remember how expressions are evaluated? The `length - 1` expression evaluates first and evaluates to 9. That 9 is then used to retrieve the actual last value from the array.
