
## Loops

One of the things that are best learned with arrays is the concept of loops. Programs that you write can use any of the control structures below during execution:

1. **A straight line / top-down** - the program runs from top to bottom and every line executes
1. **Applying conditional logic** - using `if/else`, the program conditionally executes code based on a condition being true
1. **Repetitive code flow** - the program continually executes a block of code as long as a condition remains true.

In this next section, you will cover the last point.

There are three different types of loops that are used in programming:

* **for loop** - used when you want code to repeat a defined number of times (e.g. one time for each element in an array)
* **while loop** - used when you want to continually execute a block of code as long as a condition remains true
* **do-while loop** - used when you want to execute a block of code *at least once* and repeat it as long as a condition remains true

The most common loop used is the `for` loop, though for many of the exercises you complete and perform on the job any of the loop choices can be substituted.