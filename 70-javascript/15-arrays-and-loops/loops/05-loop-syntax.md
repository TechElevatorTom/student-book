
### Writing a For Loop

The `for` loop is defined using the following syntax:

```javascript
for (<initializer>; <condition>; <iterator>) {
    <body>
}
```

The loop is made up of three very important pieces:

* *initializer* - the statement (or set of statements) that set the initial state. This is executed one time *before* the loop begins.
* *condition* - the expression evaluated *before* each iteration of the loop.
* *iterator* - a statement (or set of statements) that execute at the end of each pass through the loop

The loop continues forever until the condition evaluates to false.

If you wanted to print "Hello World!" to the console five times using a `for` loop, it would look like this:

```javascript
for(let i = 1; i <= 5; i++) {
    console.log("Hello World!");
}
```

Here's how the three parts match up:

* *initializer* - `let i = 1;` - you declare a local loop variable that is named `i`. Its initial value is 1 and it is used to track which iteration of the loop we are on.
* *condition* - `i <= 5` - before running the body of the loop, you check to make sure `i` is less than 5.
* *iterator* - `i++` - after each iteration of the loop, you increment the value of `i` by 1. This syntax is equivalent to `i = i + 1`. The condition is then evaluated again and, if it remains true, the body executes.

> #### Info::Increment & Decrement Operators
>
> There are many different ways to increment a variable.
>
> ``` javascript
> i = i + 1;
> i += 1;
> i++;
> ```
>
> On the flip-side, there is a short-hand syntax for decrementing a variable as well.
>
> ```
> i = i - 1;
> i -= 1;
> i--;
> ```
