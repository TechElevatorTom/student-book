# Unit Testing Vue Components

When we did unit testing before, we were always testing functions and making sure that what they returned when called with certain parameters is what we expected.

Components are a little different. Now, we will be testing not just the methods of the component, but also data binding and event handling registration to verify that the component is behaving and changing data the way we would expect.

We will be looking here at all the different ways that you can unit test the parts of a component.

## Setting up unit tests

Unit tests can be added to the project when you set up a new project using `vue create`. Just select the Unit Testing option and then the option marked `Mocha and Chai`. Mocha and Chai are the libraries we used to test our normal JavaScript functions, so much of what we are going to use should already be familiar.

This will add a new folder into your project called `tests/unit`. This is the folder where all your testing code will live. Each test file will be named using the convention: `ComponentName.spec.js`. If your component files is called `OrderForm.vue`, then the unit tests that go with it will be called `OrderForm.spec.js`.

At the top of each unit test file, we will have a couple of common lines of code. Following our OrderForm example, they will look like this:

``` JavaScript
import OrderForm from '@/components/OrderForm.vue';
/* eslint-disable-next-line no-unused-vars */
import { shallowMount, Wrapper } from '@vue/test-utils';

import chai from 'chai';
chai.should();

describe('OrderForm', () => {
  /** @type Wrapper */
  let wrapper;
  beforeEach( () => {
    wrapper = shallowMount(OrderForm);
  });

});
```

Let's look at what each line is doing.

### Importing code

First, we need to import some important pieces of code for our tests to run.

First, we will import the component that we want to test:

``` JavaScript
import OrderForm from '@/components/OrderForm.vue';
```

Here, we're importing our OrderForm component from the file where it is defined. The `@` means that we will be looking in our project's `src` folder and then in the `components` folder and the `OrderForm.vue` file. This will be a very common and standard format.

Next, we import some important tools from the Vue Testing Utilities:

``` JavaScript
/* eslint-disable-next-line no-unused-vars */
import { shallowMount, Wrapper } from '@vue/test-utils';
```

This will import the function `shallowMount`, which will allow us to start the component in an isolated environment perfect for testing, and `Wrapper` which is used to poke and prod at the component in our tests. We add a line above this to let VSCode, and ESLint, know not to make this line as an error of one of these variables isn't actually used in our code.

Then, we import and set up our chai assertions:

``` JavaScript
import chai from 'chai';
chai.should();
```

This should look very similar to what you've done before with unit testing.

### Setting up the tests

Then, we are finally ready to set up our tests:

``` JavaScript
describe('OrderForm', () => {
  /** @type Wrapper */
  let wrapper;
  beforeEach( () => {
    wrapper = shallowMount(OrderForm);
  });
```

First, we set up a new `describe()` that we can use to group all of our tests for the `OrderForm` component.

We then want to set up a new variable to hold a `Wrapper` object. The `Wrapper` object, as we will see later, will give us access to our component in what is called a Test Harness. This will allow our component to be isolated from outside factors while also giving us full access to its data and methods so that we can make sure it is behaving as we intend.

`/** @type Wrapper */` tells VS Code that the variable holds a `Wrapper` object. This comment is required if we want VS Code to provide code completion and intellisense for us.

To create a `Wrapper` object, we call `shallowMount(OrderForm)`. `shallowMount()` will take a passed in component, in this case `OrderForm`, and mount it into a test harness, returning the `Wrapper` object that the component is in. We want to do this brand new before each test run so that we can be sure we have a fresh component for every test.

Once we've done that, we're ready to start testing.

## Running our tests

Even though there aren't many tests written yet, you can run all your unit tests by going to your command line and running:

``` bash
npm run test:unit
```

As we write tests, we can keep running this command to make sure that all of our tests are passing and our component is doing what we expect.

## Testing that our component is valid

Our first test could be just to make sure that our component doesn't have any horrible error in it that makes it not work. That test would look like this:

``` JavaScript
it('should be a Vue instance', () => {
    wrapper.isVueInstance().should.be.true;
});
```

This is a test just to make sure that our component is in fact a valid Vue component. We mounted our component in the `wrapper` in our `beforeEach()` and can call the `isVueInstance()` method on that. That should return `true` if the component is set up properly.