# Unit Testing `v-bind`

To test this functionality, we just need to set the data attributes appropriately and then see if the classes appear and disappear from the element. We can check the CSS classes that are on an element by using `find()` to get the element and then `hasClass()` to verify that the class does or doesn't exist.

If we had this in our HTML:

``` HTML
<input type="text" name="firstName" id="firstName" v-model="firstName"
    v-bind:class="{ 'needs-content': firstName == '' }">
```

We could test the toggling of this class with the following tests:

```JavaScript
it('should remove the needs-content css class when firstName has content', () => {
    wrapper.setData({'firstName': 'TEST'});
    wrapper.find('#firstName').hasClass('needs-content').should.be.false;
});

it('should add the needs-content css class when firstName is empty', () => {
    wrapper.setData({'firstName': ''});
    wrapper.find('#firstName').hasClass('needs-content').should.be.true;
});
```

## Testing `v-bind:disabled`

Disabling and enabling a button or element follows the same general principle.

We can unit test this by, again, setting data and then checking to make sure the field is disabled. The disabled attribute on the element can be gotten through the `attributes()` method. Just see if there is a key for `disabled` or not.

If you had the following HTML:

``` HTML
<button type="submit" v-bind:disabled="creditCardNumber == ''">Send Order</button>
```

You could test it with the following tests:

``` JavaScript
it('should disable the submit button when credit card number is blank', () => {
    wrapper.setData({'creditCardNumber': ''});
    wrapper.find('button[type=submit]').attributes().should.include.key('disabled');
});

it('should enable the submit button when credit card number is entered', () => {
    wrapper.setData({'creditCardNumber': '41111111111'});
    wrapper.find('button[type=submit]').attributes().should.not.include.key('disabled');
});
```