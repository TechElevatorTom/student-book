# Testing Components

We've already seen how we can write unit test to test our JavaScript functions. You've also seen that we can write our JavaScript as components--encapsulated pieces of JavaScript, HTML and CSS that combines data and behavior that works on that data.

So, if we have these nicely isolated and encapsulated components, what do we need to do to make sure they're working the way we would expect them to work?

Just like we did for our regular JavaScript, we can write unit tests to check our components. In this section, we will be looking at how unit testing a component will work and how we can write tests that test the component as it would appear on a page.