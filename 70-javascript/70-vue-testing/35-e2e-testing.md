# End-to-end Testing

End-to-end testing is testing that makes sure an application is working the way a user would use it. It's called end-to-end because the test is using the user interface just a like a user of the application would an the tests are testing the full front to back functionality of the site.

These tests are also considered Integration Tests. Integration tests are tests that test a full application all together. For web applications, that means that we want to test our application in an actual browser and make sure that if we click certain buttons and fill out fields in the interface that everything does what it's supposed to. If unit testing is making sure a certain part of our application is working correctly, then integration testing is making sure that they all work together correctly.

We will not be writing end-to-end tests--also called e2e tests--in this class, but Vue CLI does have a built in ability to run e2e tests using an application called Cypress. Feel free to read more about it.

Further Reading:
- [Cypress](https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell)