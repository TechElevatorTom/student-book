# Unit Testing Data Binding

## Testing one-way text binding

If we had somewhere in our component the following HTML:

```HTML
<span id="orderTotalDisplay">{{ orderTotal }}</span>
```

Then we would want to test to make sure that an update to the `orderTotal` data property will update the HTML in the correct place. We can do that pretty easily.

First, we'll define a new test:

```JavaScript
it('should set the Order Total Display when the data property is set', () => {

});
```

First, we'll set the data in the component. We can set data by calling the `setData()` method of the `wrapper` and passing in what the data object should be set to:

``` JavaScript
it('should set the Order Total Display when the data property is set', () => {
    wrapper.setData({'orderTotal': 5.99});

});
```

We can then check the HTML to make sure the display updated properly:

``` JavaScript
it('should set the Order Total Display when the data property is set', () => {
    wrapper.setData({'orderTotal': 5.99});

    wrapper.find('span#orderTotalDisplay').text().should.equal('5.99');
});
```

The `find()` method of the `wrapper` uses standard CSS selectors to find DOM elements of the component. We can then get the `text()` of that element and check that it should equal `'5.99'`. Note that even though we are setting the data as a number, when we get it from the DOM, it will always be a string.

## Testing two-way data binding

You can also test two way binding between a data property and an input element in your component. We'll take a look at how to do that with a couple of different input elements.

### Testing two-way data binding with text inputs

For this test, we want to make sure that whenever we put text into a text input field, that value will be set to the component's data property.

Let's imagine that we have a data property called `firstName` and the following field in our HTML:

```HTML
<input type="text" name="firstName">
```

The first thing that we'll want to do is get a handle on the input element for the `firstName` and set its value to `'TEST'`:

``` JavaScript
wrapper.find('input[name=firstName]').setValue('TEST');
```

Then we just need to make sure that the data property in the component now equals `'TEST'`. We can get the component through the wrapper's `vm` property--`vm` stands for Vue Model in this case--and we can get the component's data properties through properties on the `vm` object.

``` JavaScript
wrapper.vm.firstName.should.equal('TEST');
```

You can test `textarea`s the same way.

### Testing two-way binding with select dropdowns

Select boxes will work a lot like the text boxes. We just need to get a handle on it using `find()` and call `setValue` to one of the available options.

``` JavaScript
wrapper.find('select[name=role]').setValue('admin');
wrapper.vm.role.should.equal('admin');
```

### Testing Radio Buttons and Checkboxes

When testing radio buttons and checkboxes, we can't just set the value of the input field like we did for text boxes. Instead, we'll need to grab a specific element and then set its `checked` property like so:

``` JavaScript
// Get the input element with a name of `gender` and a value of `m`
wrapper.find('input[name=gender][value=m]').setChecked(true);
```

Or, in other words, find the input element with a name of `gender` and a value of `m` and check it. Once you've done that, you can then verify the change in the component's data properties:

``` JavaScript
wrapper.vm.gender.should.equal('m');
```

An important note for checking if a data property equals a certain array. In order to check arrays in an assertion, you'll need to use the `deep` modifier on the assertion to get the testing framework to check each value of the array.

``` JavaScript
wrapper.vm.permissions.should.deep.equal([]);
```

For the permissions checkboxes, the data property is an array and could hold more than one value, if the user selects more than one checkbox. So a good test to check for that will need to use the `deep` modifier:

``` JavaScript
wrapper.find('input[name=permissions][value=x]').setChecked(true);
wrapper.find('input[name=permissions][value=d]').setChecked(true);
wrapper.vm.permissions.should.deep.equal(['x', 'd']);
```

### Verifying contents of table cells

If we want to know if a table cell is properly updated, we'll need to get a handle on that table cell before verifying its contents. We could add an id to every table cell so that we can use `find()` to get the exact one we want, but that could very quickly get messy as we add more and more ids to things in our HTML. What we want to do instead is use `findAll()` to get all the table cells and then select the one we want to check from the results. We'll use that here to get the first cell--using the `at(0)` function on the results--from the row in `tbody` and make sure that it contains `'TEST'`:

``` JavaScript
wrapper.findAll('table tbody tr td').at(0).text().should.equal('TEST');
```

This can be used for any list of elements that we need to look through, including `li` elements and `option` elements.

