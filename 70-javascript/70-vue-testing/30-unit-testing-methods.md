# Unit Testing Component Methods

We can also test our `method`s and `computed` properties from the component to make sure they are doing the right thing as well.

Let's imagine that we had a `method` that looks like the following:

``` JavaScript
closeForm() {
    this.enabled = false;
    this.user = {
        id: 0,
        firstName: '',
        lastName: '',
        title: '',
        email: '',
        enabled: false
    };
}
```

We can call this `closeForm()` method and then check to make sure that all of those values have been successfully set.

``` JavaScript
it('should clear user data when the form is closed', () => {
    let userData = {
        firstName: 'TEST',
        lastName: 'LTEST',
        title: 'SENIOR TESTER',
        email: 'TEST@TEST.TEST',
        enabled: true
    };
    wrapper.setData({
        enabled: true,
        user: userData
    });

    wrapper.vm.closeForm();

    // Check to make sure all the data got set back
    wrapper.vm.enabled.should.be.false;
    wrapper.vm.user.firstName.should.equal('');
    wrapper.vm.user.lastName.should.equal('');
    wrapper.vm.user.title.should.equal('');
    wrapper.vm.user.email.should.equal('');
    wrapper.vm.user.enabled.should.be.false;
    wrapper.vm.user.id.should.equal(0);
});
```

Here, we can just check to make sure the right things were changed after the method is called. Most of the time when testing methods, you will be calling the methods and then checking the side effects that they create to make sure the method worked as designed.