# Unit Testing `v-if`

We can unit test a `v-if` by setting the data attribute to a value and then seeing if the element is there or not. If we had the following `v-if` in our HTML:

```HTML
<fieldset v-if="! sameAddress">
    <legend>Billing Address</legend>
    ...
</fieldset>
```

We can test this by trying to `find()` the element in the DOM and then calling the function `exists()`. `exists()` returns `true` if the element was found and `false` otherwise. We can then verify if we got the value that we expect.

```JavaScript
it('should hide fieldset when sameAddress is true', () => {
    wrapper.setData({'sameAddress': true});
    wrapper.find('#billingAddress').exists().should.be.false;
});

it('should show fieldset when sameAddress is false', () => {
    wrapper.setData({'sameAddress': false});
    wrapper.find('#billingAddress').exists().should.be.true;
});
```