# Extras

<a id="javascript-reserved-keywords"></a>
#### JavaScript Reserved Keywords *(as of ECMAScript 2015)*
|   |   |   |
|---|---|---|
|break | export | super |
|case | extends | switch |
|catch | finally | this |
|class | for | throw |
|const | function | try |
|continue | if | typeof |
|default | in | void |
|delete | instanceof | while |
|do | new | with |
|else | return | yield |

<a id="tech-elevator-uses-google-guides"></a>
#### Tech Elevator uses Google Guides

JavaScript coding styles differ from shop to shop. Tech Elevator has standardized on the [Google JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html).

The key suggestions from the Google style guide:
* Semicolons **are** required,
* Don't use var,
* Arrow functions are preferred,
* Use template strings over concatenation,
* For...of is the preferred type of for-loop,
* Constants should be named in ALL_UPPERCASE separated by underscores,
* One variable per declaration,
* Use single quotation marks, not double.

> #### IMPORTANT::Guidelines enforcement
>
> Just as coding styles vary from shop to shop, so does their enforcement. Depending upon a given development environment, you may, or may not find certain suggested guidelines are more-or-less enforced. Tech Elevator uses a tool named ESLint to enforce many of coding practices listed above. **If you're ever unsure about any of the guidelines, or feel ESLint has gotten things wrong, ask an instructor.**

<a id="comments-for-fun-and-profit"></a>
#### Comments for Fun and Profit

The fun and profit part may have been a bit of an exaggeration, but an argument can be made that the comments you write while the code is still fresh in your head, will make your future self happier when you're fixing a bug, or adding a feature a year later, and you'll have more fun than wondering what in the world you were thinking when you wrote the code way back then. And if you're also able to make the correction or change faster because of some comment, you've saved time and money, so there's the profit.

On the other hand, **there is such a thing as too much commenting**. You don't want to write a comment for every line of code. How you name things like variables, or indent and format your code goes a long way to document how it works, so make that work for you. What you really need to comment are the things that are out of the norm. For instance, why something has an odd name, or an exceptionally long block of code where there's a bit of tricky interplay between variables that might not be obvious on first read. Think of comments as guideposts. They should highlight and alert, not detail every pebble along the trail of code.

There is one more aspect of comments that's worthwhile mentioning. Comments are a useful coding tool. As you're working with code, you may occasionally want to keep some bit of code from running. Perhaps you are trying an alternative, and want to keep the original code around so you can refer to it, or you wish to avoid some unnecessary overhead like a costly calculation that isn't required for your purposes. Since you are using Visual Studio Code as your IDE, you can simply highlight the code you wish to ignore, and then hit `cmd-/` (Mac) or `ctrl-k-c` (PC) to comment it. This is commonly referred to as, "commenting out code". If you wish to "un-comment out" the code, make sure to highlight the same code, and hit `cmd-/` (Mac) or `ctrl-k-c` (PC) to toggle the comment off.

The same technique can be applied to HTML and CSS files. Visual Studio Code correctly toggles comments on-and-off for several well-known file types with the same `cmd-/` (mac) or `ctrl-k-c` (PC) keystrokes. If you have any trouble, please ask an instructor.

### Embedding Special Characters

| Character | Meaning |
|----------|---------- |
| \0 | Null Byte |
| \b | Backspace |
| \f | Form feed |
| \n | New line |
| \r | Carriage return |
| \t | Tab |
| \v | Vertical tab
| \\' | Apostrophe or single quotation mark
| \\" | Double quotation mark
| \\\\ | Backslash character
| \XXX | The character with the Latin-1 encoding specified by up to three octal digits XXX between 0 and 377. For example, \251 is the octal sequence for the copyright symbol.
| \xXX | The character with the Latin-1 encoding specified by the two hexadecimal digits XX between 00 and FF. For example, \xA9 is the hexadecimal sequence for the copyright symbol.


### JavaScript and Unicode

Unicode is the universal character set.

You may embed Unicode into any JavaScript string similarly to the way you can embed other special characters.

| Character | Meaning |
|----------|---------- |
| \uXXXX | The Unicode character specified by the four hexadecimal digits XXXX. For example, \u00A9 is the Unicode sequence for the copyright symbol. See Unicode escape sequences.
| \u{XXXXX} | Unicode code point escapes. For example, \u{2F804} is the same as the simple Unicode escapes \uD87E\uDC04.

<a id='template-literals'></a>
#### Template Literals

Template literals are strings delimited by back-tick (`)(grave accent) characters rather than single or double quotation marks. In addition to regular strings, templates literals can contain placeholders, indicated by a dollar sign and curly braces (${ }).

Properly used, template literals can make your code much more readable than concatenating strings.

```javascript
const firstName = 'Bernice';
const middleInitial = "B";
const lastName = "BillingsBottom";

// Display the fullname using concatenation
console.log("LASTNAME: " + lastName + ", FIRSTNAME: " + firstName + " M.I.: " + middleInitial);

// Display the fullname using template literals
console.log(`LASTNAME: ${lastName}, FIRSTNAME: ${firstName} M.I. ${middleInitial}`);
```

You can even embed arithmetic, and other functionality within a template.

```javascript
console.log(`Total Purchases: ${1.98 + 2.35 + 4.00 + 7.21 + 19.38 + 3.25 + 3.25 + 8.50}`);
console.log(`Half of Infinity is ${Number.POSITIVE_INFINITY / 2}.`);
```

Note the use of Number.POSITIVE_INFINITY. The standard built-in Number object was briefly introduced in the [Number datatype](20-datatypes.html#number-datatype) section earlier in the chapter.

More information on [template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals) can be found at the always helpful Mozilla Developer Network.