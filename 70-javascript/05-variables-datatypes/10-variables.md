# Variables

Software is a set of instructions for the computer to follow, and programming is the art of writing those instructions. But software is more than a collection of instructions, just like programming is more than typing. The purpose of software is to turn data into useful information. In order to do this, the instructions need to be able to access the data. This is the role of variables.

Fundamentally, a variable is simply a *named* location in memory that contains a value that is important to your program.

It might help to think of a memory location as a box with a label. On the label is the name of the variable. Inside the box is the value. In order for an instruction to access the data, you simply give it the name on the label of the box.

For example, and as you'll learn shortly, the code below creates a variable named `firstName`, and sets it value to `'Bernice'`, which is stored in the labeled box in memory. The code then displays the value to the screen (a.k.a. console) simply by referring to the name on the label of the box where it is stored.

![A variable is a box](img/assigning-displaying-firstname-variable.png)

To be absolutely clear, the reason variables are called "variables", is because their values can vary. The following code sets the value of `firstName` to `'Larry'`, and then displays the new value once again on the console.

![The box has a new value](img/changing-displaying-firstname-variable.png)

Only the value inside the box has changed, not the name on the label. For the most part, you don't need to worry about the details such as where the box is in memory, or how the values are put into, or retrieved from the box. JavaScript takes care of that for you.

Now that you know what variables are and do, let's look into how you can make some of your own.

## Variable Names

Programming languages tend to be very particular when it comes to naming things, and JavaScript is no exception. There are certain rules that need to be followed in order to satisfy JavaScript when naming variables. There is also another set of rules, or guidelines, intended for you.

#### JavaScript Variables Naming Rules
* Variable names are comprised of letters `A-Z`, `a-z`, characters `_`, `$`, and digits `0-9`.
* Variable names must start with a letter, `_`, or `$`.
* Variable names are case-sensitive.
* Variables names may be not be a reserved keyword. *(See, [JavaScript Reserved Words](50-extras.html#javascript-reserved-keywords).)*<br>

#### Beyond the JavaScript Rules

> Always give your variables meaningful names.
>
> - *The Golden Rule of Programming*

 A "meaningful" variable name is one that conveys exactly what the data stored in that variable represents. For example, while `money` and `bankAccountBalance` might both be accurate descriptions of a piece of data, the latter is more meaningful because it is very specific and hence there's less ambiguity as to what that piece of data represents. Code that uses meaningful variable names is *much* easier to understand, debug, and modify. However, choosing meaningful names is one of the hardest things about programming (really!). You can only get good at it by practicing, so spend the time and effort to think about what each variable in your programs represents and give them the most meaningful names you can think of.

While variable naming is an art, when it comes to creating names, there are some best practices:

* Follow camel-casing conventions: first word is lowercase, subsequent words have the first letter capitalized
* Constants may optionally appear in all uppercase letters with words separated by an underscore, `_`
* Use pronounceable names for variables
* Prefer names over single characters
* Use names that describe WHAT the variable does not HOW it does it
* Avoid creating multiple variables that are variations of the same name, this creates confusion
* With booleans use names that start with is, has, was, etc. and avoid using a double negative
* Never assume a variable name has an obvious meaning, when in doubt, ask someone else

#### Good / Valid Variable Names
| Name | Reason |
|------|--------|
| `result` | Simple, single word |
| `firstName` | Appropriate use of camelCase |
| `isComplete` | Complete is a boolean, so follows boolean naming conventions |
| `PI` | Pi is a universal constant, use all uppercase letters |
| `number_of_monkeys` | Valid, but not great, doesn't follow camelCase best practice |
| `__grandTotal__` | Special characters can help make names unique |

#### Not So Good / Invalid Variable Names
| Name | Reason |
|------|--------|
| `q` | "Q" is only a name on Star Trek or in James Bond movies, prefer names over a single character |
| `case` | Invalid, `case` is a reserved word in JavaScript |
| `2many` | Invalid, names may not begin with a digit |
| `BuildingBlock` | Doesn't follow the camelCase best practice |
| `notExcludedCharacters` | Double negatives hurt to read and understand, perhaps `includedCharacters` is a better name |
| `helpMe!` | Invalid, `_` and `$` are the only special characters allowed in names |

In addition to the general suggestions listed above, there are dozens of published programming style guides available on the Internet. Besides advice on variable naming, they typically have suggestions on many other topics, from how to layout your code, to what format you should use for documenting what the program does. _(See, [Tech Elevator uses Google Guides](50-extras.html#tech-elevator-uses-google-guides).)_

## Declaring Variables

Now that we know how to name our variables, we're ready to create some. To create a variable, we simply need to declare it.

JavaScript has two forms of declaring a variable. In either case, the basic form is the reserved words, `let`, or `const`, a variable name, and a semi-colon, `;`.

> #### WARNING::var considered harmful
>
> Under previous versions of JavaScript, programmers routinely used the `var` reserved word to declare variables. This reserved word, and an even earlier method of declaring a variable by simply assigning a value to it, are still available in the language in order to run older, legacy code, **but neither should be used in new code**. Stick with `let` and `const` as discussed below.

#### let

You use `let`, when you know the value of the variable needs to be changeable. The basic form is:

```JavaScript
let myChangeableValueVariableName;
```

Glance back to the "box" images at the beginning of the chapter. The `firstName` variable is declared with `let`, since the variable's value is changed from `'Bernice'` in the first image, to `'Larry'` in the second.

```javascript
let firstName = 'Bernice';
```

Additionally, note the variation to the basic form of the `let` declaration in this example. While you are only required to use `let` followed by a variable name, you may "assign" a value to any variable while you're declaring it. The value `'Bernice'` is assigned to the `firstName` variable using the assignment operator, `=`. Additional operators will be covered a bit later in this chapter, but for now, all you need to understand is that the value to the right of the `=` operator is assigned to the variable on the left. That's all there really is to assignment. The value on the right is assigned to the variable on the left.

There is one more variation to declaring using `let` that should be mentioned. Occasionally, you will want to delay assigning a value to a variable until some time after it has been declared. The following is an example of how its done.

```javascript
let firstName;

// ...additional JavaScript code...

firstName = 'Bernice';
```
The `firstName` variable is declared, and several lines later, has `'Bernice'` assigned to it. The assignment operator `=` works exactly same way here as is described above. Once again, the value on the right, `'Bernice'` is assigned to the variable, `firstName` on the left.

> #### IMPORTANT::JavaScript comments
>
> You probably noticed the `// ...additional Javascript code...` line sandwiched between the variable declaration and the assignment in the last code example. This is a comment. JavaScript ignores comments. In other words, you are free to type anything you wish in a comment, JavaScript will not treat it as part of the program code.
>
> Comments come in two flavors in JavaScript. The `//` style is a line comment. Anything you've typed after the two slashes, up to the end of the line, is treated as a comment, and ignored by JavaScript.
>
> The second flavor is a block comment. Block comments use `/* */` as start and end delimiters. Anything between the delimiters is treated as a comment. Block comments typically have multiple lines of text.
>
> ```javascript
> /*
>  * This is a block comment.
>  * Block comments are good for commenting multiple lines, such as when you
>  * are describing the logic of a particularly complex bit of code, and
>  * the truly tricky bits should be highlighted.
>  */
> ```
>
> Comments will be discussed further in a later chapter when the guide covers documenting functions. *(See, [Comments for Fun and Profit](50-extras.html#comments-for-fun-and-profit) for a few more things about comments in this chapter.)*

#### const

The reserved word, `const`, (short for "constant") is the alternative to `let`. You use `const` when the value to the variable **must not be** changed once its been assigned the first time. While this may seem contrary to the nature of variables, there are many times when the values should never change.

```javascript
const PI = 3.14159;
```

Unlike `let` where assignment is not required when the variable is declared, `const` insists on it. JavaScript will raise an error if you attempt to declare a `const`, and do not assign a value at the same time.

> #### IMPORTANT::const preferred over let
>
> As an additional twist to the idea of constant variables, with their unvarying values, is that `const` is the preferred way to declare all variables. `const` makes variables immutable, so you know that no matter where you see the variable in code, the value can't have changed after it was originally assigned. Since there is no possibility of the value being modified by an assignment elsewhere, it can make the code more readable, and easier to understand. Use `let` only in the circumstances when you know the variable's value must be changeable, for instance, when the variable serves as a count of something, or a total sum. Otherwise, default to using `const` when declaring a variable. You can always change it to a `let` if needed.