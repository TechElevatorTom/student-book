# Operators and Arithmetic

There is commonly a need to perform basic arithmetic in the programs you write. Items need to be counted and totaled. Average scores have to be calculated. Expenses need to be subtracted from revenue to determine profit.

Fortunately, JavaScript provides the following arithmetic operators:

Assume: A is equal to 15 and B is equal to 2.

| Operator | Description                                  | Example     |
|----------|----------------------------------------------|-------------|
| `+`      | Adds two operands                            | A + B = 17  |
| `-`      | Subtracts two operands                       | A - B = 13  |
| `*`      | Multiplies two operands                      | A * B = 30  |
| `/`      | Divides two operands                         | A / B = 7.5 |
| `%`      | Finds the remainder after division (modulus) | A % B = 1   |

```javascript
const a = 15;
const b = 2;
let result;

result = a + b;     //expression is evaluated to 17 and copied into result
result = a - b;     //expression is evaluated to 13 and copied into result
result = a * b;     //expression is evaluated to 30 and copied into result
result = a / b;     //expression is evaluated to 7.5 and copied into result
result = a % b;     //expression is evaluated to 1 and copied into result

//In the statement below, a is multiplied with b first. The output is divided
//by the current value of the result variable. This final value is assigned
//into the result value.
result = (a * b) / result;

// Translates to
//  result = (15 * 2) / result;
//      result = 30 / result;
//      result = 30 / 1;
//      result = 30;
```

The order of the operators isn't always from left-to-right.

```javascript
let result;
result = 5 + 7 - 4;     //Result is 8 no matter if we add 5 and 7 first, or minus 4 from 7
result = 5 + 7 * 4;     //Result is 33, not 48. 7 is multiplied by 4, then 5 is added
```

JavaScript, like other programming languages follows an order of operation that supercedes left-to-right. This is commonly referred to as "order of operation", or in the case of JavaScript, "operator precedence". The complete table is actually quite long. Fortunately, you only need to be familiar with a small subset at this time. The operators are listed from highest precedence to lowest. Since some operators share the same level of precedence, they can be executed in order from left-to-right or right-to-left depending upon their *associativity*.

| Operator(s) | Associativity |
|-------------|-------------|
| *, / , % | left-to-right
| +, - | left-to-right |

You can see the full table of [operator precedence](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Operator_Precedence) on the Mozilla Developer Network.

For instance, extending the previous example:

```javascript
let result;
result = 5 + 7 * 4 / 2;     //Result is 19: 7 * 4 = 28, 28 / 2 = 14, 5 + 14 = 19
```
The 7 is multiplied by 4 because the multiplication appeared first, then came the division. If you switch the operators, then the division comes first, and you get a much different answer.

```javascript
let result;
result = 5 + 7 / 4 * 2;     //Result is 8.5: 7 / 4 = 1.75, 1.75 * 2 = 3.5, 5 + 3.5 = 8.5
```

You can alter operator precedence by grouping parts of the operations in parentheses. Once again, using the previous example, you can drastically change the order of operation with a few parentheses.

```javascript
let result;
result = (5 + 7) / (4 * 2); //Result is 1.5: 5 + 7 = 12, 4 * 2 = 8, 12 / 8 = 1.5
```

> #### RECOMMENDATION::always use parentheses when more than one operator is used
>
> Many programmers believe it is a best practice to always use parentheses when there are more than one operator used.
>
> Clearly, no order is needed when there is only one operator. But once more than one is involved, many programmers will find themselves using parentheses, even when they are not strictly needed according to rules of operator precedence. Both `5 + 7 * 4` and `5 + (7 * 4)` produce the same result, but second version shows others that the original programmer not only thought about precedence, but has explicitly coded that this is indeed the order of operation that should occur.

## Shorthand Assignment Operators

Closely associated with basic arithmetic are the shorthand assignments operators. Take a look at the table below. The left column shows the various shorthand operators, the right column what they are a shorthand of.

| Shorthand Operator | Meaning |
| --- | --- |
| x += y | x = x + y |
| x -= y | x = x - y |
| x *= y | x = x * y |
| x /= y | x = x / y |
| x %= y | x = x % y |

The shorthand operators are largely a convenience that saves us a bit of redundant typing.

```javascript
let result = 23;            // Result: 23
result += 7;                // Result: 30
result -= 10;               // Result: 20
result *= 3;                // Result: 60
result /= 12;               // Result: 5
result %= 3;                // Result: 2
result += 5 + 6 / 2;        // Result: 10 - The order operations on the right-side of the assignment still apply
```

And if this level of laziness seems absurd, the laziness of programmers is legend. Just google "laziness of programmers".

## Variable Names and Magic Numbers

Consider the following program where two variables, days and seconds are declared and used later in an arithmetic expression.

```csharp
const days = 2;
const seconds = 40;

// ....

const numberOfHoursAwake = 24 * days;
const numberOfFrames = 24 * seconds;
```

While 24 is the same number in both statements, they serve completely different meaning. In programming we refer to these as *magic numbers*. A magic number in programming is a unique value whose meaning is based on context.

Above, 24 refers to the number of hours in a day and the number of frames per second. When writing a program, it is strongly recommended to create a constant so that we do not have to infer the meaning of 24. This constant allows you to give the number a name so that it may be referenced throughout your code.

Using that same idea above, consider the following program that uses constants to clearly indicate the meaning of 24 in a block of code.


```javascript
const days = 2;
const seconds = 40;
const NumberOfHoursPerDay = 24;
const NumberOfFramesPerSecond = 24;

// ....

const numberOfHoursAwake = NumberOfHoursPerDay * days;
const numberOfFrames = NumberOfFramesPerSecond * seconds;
```