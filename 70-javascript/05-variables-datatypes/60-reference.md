# More Readings and References

## Mozilla Developer Network

The Mozilla Developer Network (MDN) is a seemingly endless source of advice and knowledge. It should be your first port of call for answers.

The following are the MDN pages were used in the writing of this chapter.

[MDN: JavaScript data types and data structures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Data_structures)<br>
[MDN: Grammar and types](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Grammar_and_types)<br>
[MDN: Variable Statement](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/var)<br>
[MDN: Lexical grammar](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Lexical_grammar)<br>
[MDN: Template literals](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Template_literals)
[MDN: Standard built-in objects](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects)


## Others
[Wikipedia: ECMAScript](https://en.wikipedia.org/wiki/ECMAScript)
Good overview and brief history of EMCAScript.<br>
[What is the difference between JavaScript and ECMAScript?](https://stackoverflow.com/questions/912479/what-is-the-difference-between-javascript-and-ecmascript/913064)<br>
Contains a short history of JavaScript and ECMAScript, their share past, and possible future.<br>
[What every JavaScript developer should know about Unicode](https://dmitripavlutin.com/what-every-javascript-developer-should-know-about-unicode/)
A nice introduction to Unicode, and the basic use of it within JavaScript.

[FreeCodeCamp: JavaScript type coercion explained](https://medium.freecodecamp.org/js-type-coercion-explained-27ba3d9a2839)
[Round to at most 2 decimal places only if necessary](https://stackoverflow.com/questions/11832914/round-to-at-most-2-decimal-places-only-if-necessary)

[Google JavaScript Style Guide](https://google.github.io/styleguide/jsguide.html)
[13 Noteworthy Points from Google’s JavaScript Style Guide](https://medium.freecodecamp.org/google-publishes-a-javascript-style-guide-here-are-some-key-lessons-1810b8ad050b)

[Why are shortcuts like x += y considered good practice?](https://softwareengineering.stackexchange.com/questions/134118/why-are-shortcuts-like-x-y-considered-good-practice)
Interesting takes on why short assignment operators are considered best practice.
