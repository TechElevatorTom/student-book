# Datatypes

The variable values shown so far in this chapter have been either numbers, or in the case of the `firstName` variable, quoted text such as `'Bernice'`. It is said that these values have different **datatypes**. JavaScript supports a relatively small set of datatypes.

- Number
- String
- Boolean
- Symbol
- Object
    - Function
    - Array
    - Date
    - RegExp (Regular Expression)
- null
- undefined

Of these, the key datatypes in this chapter are Number, String, Boolean, null, and undefined. The rest will be explored in later chapters. Only Symbol is not discussed in this guide, but was included for the sake of completeness.

> #### IMPORTANT::JavaScript is loosely typed
>
> Depending upon your prior programming experience, you may have noticed the lack of a datatype when declaring variables, especially if you've used C# or Java. Unlike those languages, where variable declaration requires an explicit datatype, JavaScript is a *loosely typed*. Variables are not associated with any particular datatype when declared, and are free to hold any type of value.
>
> ```javascript
> let foo = 42;
> foo = "Steve";
> foo = true;
> ```
>
> Variables can be assigned (and re-assigned) values of any datatype. Note the `let` declaration so `foo` can be reassigned values.

<a id='number-datatype'></a>
### Number

JavaScript supports two ways of storing number datatypes. The first is as 64-bit floating-point values, more commonly known as decimal numbers, i.e. numbers with a decimal point. The second format is once again 64-bit, but this time values are stored as integers, or whole numbers. The two formats can occasionally cause some surprising results in rare circumstances, but for the most part JavaScript silently handles the difference between floating-point and integer values. Basic addition, subtraction, multiplication, and division work as expected. Arithmetic will be covered a bit later in this chapter.

#### Number Formats

As you've already seen, numeric values may be expressed in several different formats.

##### Integer

An integer literal has the following format:
- A sequence of digits `0-9` **without** a leading 0 (zero) which can be signed, (i.e. preceded by `+` or `-`).

```javascript
453
-102
+732123
```

##### Floating-Point

A floating-point literal has the following format:
- A decimal integer which can be signed, (i.e. preceded by `+` or `-`),
- A decimal point (`.`).),
- A fraction (another integer),
- An optional exponent which has the following format:
    - An "e" or "E" followed by an integer which can be signed, (i.e. preceded by `+` or `-`).

```javascript
98.6
-43.45
-2.432e+12
7.01e-8
```

### String

A string is zero or more characters enclosed in double(") or single (') quotation marks.

```javascript
"foo"
'bar'
'1234'
"Rocket J. Squirrel"
```

The end quotation mark must match the one used at the beginning. In other words, JavaScript will not accept:

```javascript
"foo'
'bar"
```

It is possible to embed single quotation (') marks in string enclosed in double quotation (") marks, and double quotation (") marks in a single quotation (') mark string.

```javascript
"Occam's razor"
'Tony said, "Hi"'
```

> #### IMPORTANT::Prefer single quotation marks
>
> While there are times when using double quotations marks to delimit strings makes coding easier, you should strive to always use single quotation marks as string delimiters. *(See, [Tech Elevator uses Google Guides](50-extras.html#tech-elevator-uses-google-guides).)*

#### Special characters in string

Given a preference for single quotation marks as string delimiters, you might be wondering how to show the razor belongs to Occam?

```javascript
'Occam's razor' // Invalid JavaScript: You can not embed a single quotation mark in a single quotation marks delimited string
```

Fortunately, JavaScript allows you to embed special characters into any string. A partial list of these special characters follows:

| Character | Meaning |
|----------|---------- |
| \n | New line |
| \r | Carriage return |
| \t | Tab |
| \\' | Apostrophe or single quotation mark
| \\" | Double quotation mark
| \\\\ | Backslash character

```javascript
"Tony said, \"Hi\""
'Occam\'s razor'
```

#### Concatenation

You can build larger strings from smaller ones in the code you write. This is called "string concatenation", and is accomplished by the adding the strings together with the concatenation operator, `+`.

Let's say your old friend, Occam is packing for an over-night stay. We're already aware of his razor, but what about his toothbrush, and change of socks? Occam is big on simplification, and hates ambiguity. He believes in identifying what's his, and if he's packing for a trip, all his items need to be clearly labeled as belonging to him.

```javascript
const occamRazor = 'Occam\'s ' + 'razor';
const occamBrush = 'Occam\'s ' + 'toothbrush';
const occamSocks = 'Occam\'s ' + 'socks';
```
Note the space after the "s" in the first string literal. Without it, the resulting strings would have been "Occam'srazor", "Occam'stoothbrush", and "Occam'ssocks".

Of course, you're not limited to concatenating literal strings. You can also concatenate string variables. In fact, you can combine variables and string literals in a series of concatenations.

Here's Occam's completed packing list.

```javascript
const occamPackingList = 'Occam\'s packing list: ' + occamRazor + ", " + occamBrush + ", " + occamSocks;
```

### Boolean

Boolean values are perhaps the easiest datatype to describe. They can have one of two values, `true` or `false`.

You'll look more at boolean values in the next chapter when you cover logical branching, or as programmers say, "if-then-else".

### undefined

Undefined is a bit strange. How can a variable have a undefined datatype for a value? The answer is that values live in memory locations, not in variables. Remember the box analogy, the variable simply refers to the label on the box in memory where the value is held. An undefined variable is saying there is a memory location for the variable to refer to, but no value has yet been assigned, or placed in the box. So, when `firstName` is declared, the box in memory exists, but has no datatype until `'Bernice'` is assigned later on in the program.

![Variable value is undefined until assigned](img/variable-with-undefined-value.png)

### null

Although `null` and `undefined` are commonly lumped together, there is a difference. A variable that is undefined still refers to a memory location. You just don't know its datatype since no value has been assigned. A null variable on the other hand, doesn't refer to any memory location, period. Not only is there no value, there's no memory to hold a value.

![Null variable has no value](img/variable-with-null-value.png)

Once `firstName` is assigned `null`, the memory location and any value in it disappears like magic. Well! Actually the memory is garbage collected, but that's a story for another day.

> #### IMPORTANT::mutable vs. immutable
>
> Many programming languages, such as Java and C#, categorize datatypes as either *value* types or *reference* types. This categorization is important in these languages, especially as values are passed in code. Reference types can be inadvertently changed while being passed around, while value types cannot.
>
> JavaScript has a similar categorization, only it is between mutable and immutable. Everything is passed around as values in JavaScript, so there is no reference types to worry about. However, it's the "inadvertently changed" part that's the real problem. Number, String, Boolean, Undefined, and Null are immutable primitive datatypes, and as such, cannot be changed as they are passed around. Objects, Functions, Array, Date, and RegExp are all mutable, and may be unexpectedly changed.
>
> There will be more to say about this in later chapters, but you should be aware so you don't panic if it comes up elsewhere. You can go a long way in JavaScript without ever knowing about the issue, but forewarned is forearmed.