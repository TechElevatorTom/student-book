<!-- 05-variables-datatypes.md -->
# Variables and Datatypes

## Introduction

Welcome to your first steps into JavaScript.

JavaScript is a general-purpose programming language. Unlike HTML and CSS, which are static markup languages used solely for providing the content and style of web pages, JavaScript can be used to accomplish many different tasks. For instance, HTML and CSS can be used to display the weather, but not to forecast it. For that you'd need a programming language like JavaScript. Not only can you use JavaScript to help you forecast the weather, you can also use it to build a game in which the forecast becomes part of the play. And after your spectacular win, you can post your score to the leader board, once again, thanks to JavaScript.

Originally intended as a way to bring dynamic behavior to the static content of HTML, JavaScript has evolved considerably from its earliest days, and is now used in ways not thought of back in the mid-1990's. From the beginning, JavaScript was designed to be an easy-to-learn, forgiving language that was approachable for beginners to pick-up and do something useful. Quickly adopted by the European community, and standardized under the name, "ECMAScript", JavaScript has migrated beyond its traditional place in the browser, and can now be found in back-end applications using NodeJS, embedded in innumerable IoT projects around the world, and used as a sort of universal "glue" code which binds together applications with bits-and-pieces of programming script.

Regardless of where and how its used, variables and datatypes are fundamental to all programming languages, including JavaScript. Understanding them is a key requirement in learning how to program. We'll start there.

#### Objectives

At the end of the chapter, you should be able to:

- Describe what a variable is, and what it does.
- Declare a JavaScript variable, and assign a value to it.
- List, and describe the basic JavaScript datatypes.
- Write arithmetic expressions, and perform simple math in JavaScript.
- Convert from one datatype into another.
- Incorporate JavaScript into a HTML page.
