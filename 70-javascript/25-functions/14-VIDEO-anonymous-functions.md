# VIDEO: Anonymous Functions

This video will show you how to create anonymous functions in JavaScript and how to use them in commonly used array functions.

{% video %}https://www.youtube.com/watch?v=LHgXX9QM1oA{% endvideo %}