# VIDEO: Documenting Functions with JSDoc

This video will show how to document your functions using JSDoc.

{% video %}https://www.youtube.com/watch?v=_sifYGxcSA8{% endvideo %}