# Array Functions Using Anonymous Functions

Arrays in JavaScript have a lot of useful functions themselves that use anonymous functions.

## `forEach`

`forEach` is a function that should look generally familiar. It performs like a `for` loop, running a passed in anonymous function for every element of an array.

```javascript
let numbers = [1, 2, 3, 4];

numbers.forEach( (number) => {
    console.log(`This number is ${number}`);
});
```

## `map`

`map` is a lot like `forEach` but it will return a new array using the return value of the anonymous function as the values in the new array.

```javascript
let numbersToSquare = [1, 2, 3, 4];

let squaredNumbers = numbersToSquare.map( (number) => {
    return number * number;
});

console.log(squaredNumbers);
```

## `filter`

`filter` will take an anonymous function and run each element through it and return a new array. If the function returns `true`, the element will be kept in the new array. If the function returns `false`, the element will be dropped from the new array.

```javascript
let numbersToFilter = [1, 2, 3, 4, 5, 6];

let filteredNumbers = numbersToFilter.filter( (number) => {
    // Only keep numbers divisible by 3
    return number % 3 === 0;
});

console.log(filteredNumbers);
```

## `reduce`

`reduce` will collapse the array down to one single value using the logic of the anonymous function. Here, the anonymous function gets two parameters: the current single value containing the result of all previous elements (typically called the reducer) and the next element to add to that value.

```javascript
let nameParts = ['bosco', 'p.', 'soultrain'];

let fullName = nameParts.reduce( (reducer, part) => {
    return reducer + ' ' + part.substring(0, 1).toLocaleUpperCase() + part.substring(1);
}, ''); // <--- The empty quotes is the value of the reducer for the first element

console.log(fullName.trim());
```