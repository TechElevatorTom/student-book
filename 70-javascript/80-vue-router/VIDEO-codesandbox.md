# CodeSandbox

In this video you will learn what CodeSandbox is and how it can be used to prototype during development without going through the hassle of setting up a new project or a new machine.

{% video %}https://www.youtube.com/watch?v=DpgvcU-Eh24 {% endvideo %}
