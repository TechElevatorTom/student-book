# The Route Object & Router Instance

## Route Object

A route object represents the state of the current active route. It contains parsed information of the current URL and the route records matched by the URL. If you were to log `this.$route` after the Home component was mounted you would see the following info.

![The Route Object](./img/route-object.png)

As you can see this contains information about the route that you will need in different scenarios. In a previous section you saw an example where you had a dynamic route that took in a username variable.

```javascript
export default new Router({
  mode: "history",
  routes: [
    {
      path: "/user-profile/:username",
      name: "user-profile",
      component: UserProfile
    }
  ]
});
```

Now let's say in your user component you needed to make an API call to fetch some data about that user based on their username. This is where the Route Object is very handy and gives you access to information like that. In the created method you will call the method `getUser` and you will need to pass in the username from the current path. To do so you can access the username in the `this.$route.params` object.

```html
<template>
  <div class="user-profile">
    ...
  </div>
</template>

<script>
  export default {
    name: "user-profile",
    data() {
      return {
        user: null,
        API_URL: "https://www.yourapihere.com"
      };
    },
    methods: {
      getUser(username) {
        // ...
      }
    },
    created() {
      this.getUser(this.$route.params.username);
    }
  };
</script>
```
