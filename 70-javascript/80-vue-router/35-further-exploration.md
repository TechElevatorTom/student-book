# Further Exploration

This is considered optional reading but they are solutions to problems that might come up when you start building out larger applications.

## Redirect and alias

Redirecting is also done in the routes configuration.

https://router.vuejs.org/guide/essentials/redirect-and-alias.html

## Route Not Found (404)

There are times where a user might type in an address or you link to a route that doesn't exist. In these cases you will to provide some sort of catch all to let the user know that the route doesn't exist instead of just throwing some random error that they never see.

```js
const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '*', component: NotFoundComponent }
  ]
})
```
