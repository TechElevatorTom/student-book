# Router Link

Now that you learned how to add Vue Router to your project and configure different routes you need to know how to navigate to them. In a traditional web application if you had links to your home and about page they might look something like this.

```html
<a href="index.html">Home</a>
<a href="about.html">About</a>
```

You can use the same code in your Vue applications but this approach comes with a few problems. Now that you have all of your configuration in one place what happens if you decide to change the path from `/about` to `/about/overview/`? Using the approach above you would have to search for every link and update it but if you're using the Router Link component you can update the Vue Router configuration.

Back in your traditional web application you need to let the user know what page they are on. This often takes looking at the path and adding an active class to the current `<a>` link.

```html
<a href="index.html" class="active">Home</a>
<a href="about.html">About</a>
```

With Vue Router you don't have to worry about how to implement this because it works out of the box. The Router Link is just another Vue component but as you will see it gives us a ton of great features out of the box. It is because of this that is preferred over using an anchor tag `<a>`.

## Static Route Matching

You already saw an example of this in the `hello-router` application you saw in the previous lesson but it's a good place to start. If you have a number of routes defined in your router configuration:

```javascript
export default new Router({
  mode: "history",
  routes: [
    { path: "/", component: Home },
    { path: "/about", component: About },
    { path: "/services", component: Services },
    { path: "/products", component: Products },
    { path: "/contact", component: Contact }
  ]
});
```

When you want to direct a user to one of your routes you will use the Router Link component. You can specify the path to the route by using the `to` prop on the `router-link` component. By default `<router-link></router-link>` will be rendered as `<a>` tag by but Router Link is preferred over hard-coded `<a href=""></a>` for the following reasons:

- It works the same way in both HTML5 history mode and hash modes, so if you ever decide to switch mode, or when the router falls back to hash mode in IE9, nothing needs to be changed.
- In HTML5 history mode, router-link will intercept the click event so that the browser doesn't try to reload the page.
- When you are using the base option in HTML5 history mode, you don't need to include it in to prop's URLs.

Let's say you wanted to build a navigation bar that linked to all of your routes. You can use the `<router-link></router-link>` component with the `to` prop being the path you want to link to.

```html
<header>
  <ul>
    <li><router-link to="/">Home</router-link></li>
    <li><router-link to="/about">About</router-link></li>
    <li><router-link to="/services">Services</router-link></li>
    <li><router-link to="/products">Products</router-link></li>
    <li><router-link to="/contact">Contact</router-link></li>
  </ul>
</header>
```

## Named Views

This works great but as I mentioned earlier there is a good reason to use the name property on routes. What happens if you use the path for every link in your application and wanted to change it later on? What if you wanted to change the `/products` path to `/products/overview` and then had a route for each product in your application. Every instance where you used `<router-link></router-link>` would need to get updated.

In that same scenario you could define your router links using a named route like this:

```html
<header>
  <ul>
    <li><router-link v-bind:to="{name: 'home'}">Home</router-link></li>
    <li><router-link v-bind:to="{name: 'about'}">About</router-link></li>
    <li><router-link v-bind:to="{name: 'services'}">Services</router-link></li>
    <li><router-link v-bind:to="{name: 'products'}">Products</router-link></li>
    <li><router-link v-bind:to="{name: 'contact'}">Contact</router-link></li>
  </ul>
</header>
```

Then in your routes config you can go in and change your path and any router link that used the name `products` or `services` would have it's path updated to the new one.

```javascript
export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      component: About
    },
    {
      path: "/products/overview",
      name: "products",
      component: Products
    },
    {
      path: "/services/overview",
      name: "services",
      component: Services
    },
    {
      path: "/contact",
      name: "contact",
      component: Contact
    }
  ]
});
```

Here is a CodeSandbox of that application:

<iframe src="https://codesandbox.io/embed/019ozxz8m0?fontsize=12&view=split" title="router-view-name" style="width:100%; height:600px; border:0; border-radius: 4px; overflow:hidden;" sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>

## Dynamic Route Matching

There are times where you will need to display the same component based off of some dynamic data. For instance you might have a user profile route that will use the UserProfile component but should be rendered with different information for each user id. In vue-router we can use a dynamic segment in the path to achieve that:

```javascript
export default new Router({
  mode: "history",
  routes: [
    {
      path: "/user-profile/:username",
      name: "user-profile",
      component: UserProfile
    }
  ]
});
```

This means that the following URLs will all map to the same route:

- http://localhost:8080/user-profile/frank
- http://localhost:8080/user-profile/mike
- http://localhost:8080/user-profile/josh

A dynamic segment is denoted by a colon `:`. When a route is matched, the value of the dynamic segments will be exposed as `this.$route.params` in every component. You will learn more about this in the route object lesson.

## Router Link Active Class

When building out navigation you will often want to make it apparent to your users what the current route is. This is usually done in the form of CSS and styling the `active` route. Router Link will help us out here and add the class `router-link-active` to the current route.

```html
<a href="/about" class="router-link-exact-active router-link-active">About</a>
```

Now you can add the class to your component and style the active route so the user knows where they are.

```css
li a.router-link-active {
  background-color: white;
  padding: 4px;
  border-radius: 4px;
  color: black;
}
```

![](./img/active-class-home.png)

If you were to click on the home page though you would have a problem.

![](./img/active-class-about.png)

Why are both the home and about routes showing as active?

The default active class matching behavior is inclusive match. For example, `<router-link to="/a">` will get this class applied as long as the current path starts with /a/ or is /a.

One consequence of this is that `<router-link to="/">` will be active for every route! To force the link into "exact match mode", use the exact prop:

```html
<header>
  <ul>
    <li><router-link v-bind:to="{name: 'home'}" exact>Home</router-link></li>
    <li><router-link v-bind:to="{name: 'about'}">About</router-link></li>
    <li><router-link v-bind:to="{name: 'services'}">Services</router-link></li>
    <li><router-link v-bind:to="{name: 'products'}">Products</router-link></li>
    <li><router-link v-bind:to="{name: 'contact'}">Contact</router-link></li>
  </ul>
</header>
```

## Router link as another tag

Sometimes we want `<router-link>` to render as another tag, e.g `<li>` or `<button>`. In that scenario we can use the `tag` prop to specify which tag to render to, and it will still listen to click events for navigation.

```html
<router-link to="/" tag="li">Home</router-link>
<!-- renders as -->
<li>Home</li>

<router-link to="/" tag="button">Home</router-link>
<!-- renders as -->
<button>Home</button>
```
