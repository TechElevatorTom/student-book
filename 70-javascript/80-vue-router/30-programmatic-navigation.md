#Programmatic Navigation

You already learned how to use `<router-link></router-link>` to provide declarative navigation but there are times when you will need to do this programmatically. Say for example you're in a method and after you save a new user you would like to redirect them back to the list of users.

```javascript
methods: {
    saveUser() {
        // save user logic

        // redirect user to /users
    }
}
```

You can't use the Router Link component in a method so Vue gives us access to the same logic programmatically using one of the following methods:

- router.push(location, onComplete?, onAbort?)
- router.replace(location, onComplete?, onAbort?)
- router.go(n)
- router.back()
- router.forward()

You can go through [Vue Router Documentation](https://router.vuejs.org/guide/essentials/navigation.html) to learn more about them but you will see an example of `router.push()` now.

Inside of a Vue instance, you have access to the router instance as `$router`. You can therefore call `this.$router.push`. This is the method called internally when you click a `<router-link>`, so clicking <router-link :to="..."> is the equivalent of calling `router.push(...)`.

The argument can be a string path, or a location descriptor object. Examples:

```javascript
// literal string path
this.$router.push("/");

// object
this.$router.push({ path: "/" });

// named route
this.$router.push({ name: "home" });

// named route with params
this.$router.push({ name: "user", params: { username: "dan" } });

// with query, resulting in /register?plan=private
this.$router.push({ path: "register", query: { plan: "private" } });
```

So going back to our example from above if you wanted to redirect to the list of users after a save you could do the following:

```javascript
methods: {
    saveUser() {
        // save user logic

        // redirect user to /users
        this.$router.push({ name: "users" });
    }
}
```
