# Vue Router Introduction

Until this point you have been building Vue applications that consist of one or two components. You might have the need to add some behavior to a component and as you learned that can be accomplished using events. If you want to react to a user clicking on a button you can listen for the click event and perform some logic each time the button is clicked. If you need to tell other components about that event you can emit an event so that any parent components interested in it can react to it as well.

This has worked out really well until now but what happens when you want to start building larger applications? If you were to create a simple application that had 3 pages (home, about and contact) how would you go about loading an unloading the component that the user was interested in? You could certainly hack something together by looking at the url and using `v-if` or `v-show` to show and hide components but that would become cumbersome, complex and error prone very quickly.

In applications that run on both the front and back end there are ways to handle this particular problem. It usually involves looking at what path the user is requesting and associating that with some code. In Vue you have `vue-router` which is the official and recommended routing provider. Vue Router allows you to define a route ('/', '/about', '/contact') and define the component that you want to load when that route is requested.

In this lesson you will learn:

- How to add Vue Router to a new project using the Vue CLI.
- How to add Vue Router to an existing project
- How to configure Vue Router
- What is The Router Link component and how to use it
- What is The Router View component and how to use it
- The Route Object & Router Instance
- Programmatic Navigation
