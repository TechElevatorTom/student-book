# Router View

The `<router-view></router-view>` component is a functional component that renders the matched component for the given path. A component rendered in `<router-view></router-view>` can also contain its own `<router-view></router-view>`, which will render components for nested paths.

When you add Vue Router to a new or existing project your `App.vue` will look something like this.

```html
<template>
  <div id="app">
    <div id="nav">
      <router-link to="/">Home</router-link>|
      <router-link to="/about">About</router-link>
    </div>
    <router-view />
  </div>
</template>
```

Now that you know what `<router-link></router-link>` is you know that it's to provide navigation to different components. The next part of that is the `<router-view>` component below it. That component will take whatever the current component is for the given path and load it. This one component allows us to say this is where I want my dynamic content to be loaded.


{% video %}https://www.youtube.com/watch?v=RAF3u_cjNsQ {% endvideo %}
