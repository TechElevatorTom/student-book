# Getting Started Video

In this video you will learn how to create a new project that includes the Vue Router. After you learn the different parts that make up the Vue Router you will learn how to add it to an existing project.

{% video %}https://www.youtube.com/watch?v=F9NHGgFulzY {% endvideo %}
