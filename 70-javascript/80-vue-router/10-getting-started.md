# Getting Started with Vue Router

The easiest way to get started with the Vue Router is to add it as a feature when you're creating a new project. It is possible to add it to an existing project and you will see an example of that a little bit later.

## Adding Router to a new project

If you were to create a new project using the Vue CLI `vue create hello-router` and manually select the features you want you would see a screen that looks something like this.

![Vue CLI Select Features](./img/cli-select-features.png)

You will notice that by default Router is not selected. To add the router you will want to arrow down to that option and press the spacebar to select it. From there you can accept the defaults and create a new project. You should end up with a project that looks something like this:

<iframe src="https://codesandbox.io/embed/8jyp2rz2?fontsize=12&view=split" style="width:100%; height:600px; border:0; border-radius: 4px; overflow:hidden;" sandbox="allow-modals allow-forms allow-popups allow-scripts allow-same-origin"></iframe>

> Not familiar with CodeSandbox? Check out this short [video tutorial](VIDEO-codesandbox.md).

If you had a peek at the code and aren't quite sure what it's doing don't worry each of the new files will be covered later in this lesson.

## Existing Projects

What happens if you have an existing project that you want to add the router to? This could be a project you just created and forgot to select the router or a project that has been in development for awhile now.

You could just install the `vue-router` dependency by running the command `npm install vue-router`. While this would bring in the dependencies you need you will still need to do the following to mirror what the CLI does for you:

- create a router.js configuration
- import router into main and pass it to the Vue instance
- create a views folder and add a couple of views
- update App.vue to link to each view and include `<router-view></router-view>`

Nobody wants to do all of this and the good news is you don't have to. The Vue CLI gives us a way to [add plugins/presets](https://cli.vuejs.org/guide/plugins-and-presets.html#allowing-plugin-prompts) using the `vue add` command. If you run the command `vue add router` it will install the dependency for you and run a generator that adds or changes the following files.

- src/router.js
- src/views/About.vue
- src/views/Home.vue
- package-lock.json
- package.json
- src/App.vue
- src/main.js

![Vue Add Router](./img/vue-add-router.png)

## How does the router work?

Now that you have added the router to a project, it is time to understand how it works.

### Router Configuration

The first change was the addition of a new file `router.js`. In this file we create a new router instance and pass in some configuration options to its constructor. There are many options for [customizing the router](https://router.vuejs.org/api/#router-construction-options) but for now you are just going to focus on routes.

```javascript
export default new Router({
  mode: "history",
  routes: [
    {
      path: "/",
      name: "home",
      component: Home
    },
    {
      path: "/about",
      name: "about",
      component: About
    }
  ]
});
```

In the array of routes each route object can contain a [number of properties](https://router.vuejs.org/api/#the-route-object) but at the very least it needs a path and a component. The name is a good idea to include because whenever you want to link to a route you can use the name instead of the path and then if you decide to change the path later you don't have to update that path across your project. When you add new views / pages to your Single Page Application you will need to add a new route here in this configuration.

### The Vue Instance

Now that you have created your router you need to tell your main vue instance about it. If you open up `main.js` you will see that one of the options now being passed to `new Vue()` is the router.

```javascript
import Vue from "vue";
import App from "./App.vue";
import router from "./router";

Vue.config.productionTip = false;

new Vue({
  router,
  render: h => h(App)
}).$mount("#app");
```

### Views

With everything setup you can begin working on your application. You could just start adding new components like you have been doing but there is a better approach.

First thing you need to understand is that both the `src/components` and `src/views` folders contain Vue Components. The main difference is that anything under `src/views` will be views in your application and have a route defined in `router.js`. A component that might be reused or dropped into a view will be defined in `src/components`.

Another difference you will notice is that all of the components in your views folder have one word names:

- Home.vue
- About.vue
- Services.vue
- Products.vue
- etc...

### App.vue

The last part of the router setup to understand is how to display and link to our different views. If you look in `App.vue` you will see the following markup.

```html
<template>
  <div id="app">
    <div id="nav">
      <router-link to="/">Home</router-link>|
      <router-link to="/about">About</router-link>
    </div>
    <router-view />
  </div>
</template>
```

The reason your different views are displayed when asked for is due to the `<router-view></router-view>` component. The `<router-view>` component is a functional component that renders the matched component for the given path.

The `<router-link></router-link>` component will allow you to link to different routes in your application. While an anchor tag `<a href=""/></a>` will suffice, `<router-link></router-link>` offers additional features when paired with the Vue router.



