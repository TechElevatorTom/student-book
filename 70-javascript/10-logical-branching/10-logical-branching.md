# Logical Branching

As a programmer, you will come across situations where your program must decide what to do next based on the data available to it:

* a healthcare portal might display different pages based on the age of the website visitor (senior citizen vs. young adult)
* if a user has not logged in, show them the login page, otherwise allow them to proceed
* if a username is already taken tell the user to select another username

In this section, you will see concepts that are used to enable code to make decisions and determine what to do next.

By the end you should understand how to:

* build boolean expressions using comparison and logical operators
* write conditional code using boolean expressions
* read and understand complex code expressions
* demonstrate an understanding of code blocks and variable scope