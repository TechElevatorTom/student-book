# Blocks and Scope

## Blocks

Each of the sections of code that follow an `if` or an `else` statement are called blocks. Blocks use a pair of braces **`{..}`** to enclose single or multiple lines of code. While they may seem to make life hard for you early on, they are very useful by enhancing code readability and dealing with something called variable scope.

The code below is enclosed in a block that only executes when the condition is true. **Note that good formatting requires that code inside of a block is indented.**

``` javascript
if (condition) {
    //statement or block of code to run when condition is true
}
```

## Scope

You can declare a variable at any point in a block, but you **must** declare it before you use it. Once declared, the variable is *in scope*. Variables are in scope until the end of the block when they are discarded and go *out of scope*.

## Nested Blocks

Whether you have complex conditional logic or you are mixing multiple loops with conditions, blocks are often nested. Each nested block can declare and use its own set of local variables:

```javascript
{ //outer block
    let i;
    { //inner block
        let j;
    }
}
```

The variable `j` has the scope of the inner block. Statements within the inner block can use both `i` and `j`. Statements in the outer block can only use `i`. If the following code was written a compilation error would occur because `j` would be out of scope.

```javascript
{
    let i;
    {
        let j;
    }
    j = 33; //<--- not allowed, variable out of scope
}
```

Javascript will allow a variable in an inner block to have the same name as a variable in an outer block. These are then two seperate variables according to Javascript.

```javascript
{
    let i = 3;
    {
        let i = 29;  // <-- This is a new variable
        console.log(i);  // <-- Will print 29
    }
    console.log(i); // <-- Back to the original variable, will print 3
}
```