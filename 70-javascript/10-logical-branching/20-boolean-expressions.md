# Boolean Expressions

Outside of arithmetic expressions, one of the other common types of expressions is a boolean expression. This type of expression always evaluates to `true` or `false`.

There are two common ways that boolean expressions are built:
* comparison operators (to compare two values)
* logical operators (to create relationships between one or more boolean values)

## Comparison Operators

When you create applications, you often deal with simple arithmetic comparisons to allow our website visitors to make a choice or present them with different options. For example, you may want to:

* calculate if a number is odd or even to create alternating row colors in web pages
* validate that there are enough tickets available before allowing a user to reserve a seat for an event
* check to see if the user has not been to the site within 90 days to show them a reminder

The following comparison operators allow you to make these comparisons.

| Operator | Meaning                    |
|----------|----------------------------|
| `==`     | Equal To                   |
| `!=`     | Not Equal To               |
| `>`      | Greater Than               |
| `<`      | Less Than                  |
| `>=`     | Greater Than or Equal To   |
| `<=`     | Less Than or Equal To      |

These comparison operators when used in an expression always evaluate to a boolean `true` or `false` value. For example:

```javascript
let number = 5;

let isItFive = (number == 5);      //true if number is '5'
let isItEven = (number % 2) == 0;  //true if number is evenly divisible by '2'
let isItNegative = (number < 0);   //true if number is less than but not equal to 0
```

## Logical Operators

The comparison operators are limited in their ability to compare values between two different types. When a boolean expression needs to take two or more conditions into account to evaluate a boolean value (e.g. is the number negative AND is it even) then the logical operators are used.

The logical operators are: AND (`&&`), OR (`||`), XOR (`^`), and NOT (`!`). These operators work with one or more boolean operands to evaluate a boolean expression and yield `true` or `false.`

### NOT `!`

The logical NOT operator when placed in front of a boolean value negates the value of the boolean operand.

### AND `&&`

If both of the operands on each side of the `&&` are `true`, the result of the expression is `true`. If one of them is `false`, the result is `false`. Consider the variable called `width` used in the following expression:

```
(width >= 0.5) && (width <= 5.0)
```

This expression is `true` if the `width` is between 0.5 and 5.0.

### OR `||`

If either of the operands on each side of the `||` are `true`, then the result of the expression is also `true`. The expression is only `false` when both operands are `false`. For example consider the variable called `width` used in the following expression:

```
width < 0.5 || width > 5.0
```

This expression is `true` if the `width` is less than 0.5 or greater than 5.0.

### XOR `^`

If one operand in the expression is `true` and the other operand is `false`, then the result of the expression is `true`. In an exclusive-or whenever both operands have the same value, then the expression is `false`.

This expression is not used as often as the logical AND and logical OR.

### Logical Operator Table

The following table can be used to sum up the different logical operators

| `A`       | `B`       | `!A`  | `A && B` | <code>A &#124;&#124; B</code>      | `A ^ B` |
|-----------|-----------|-------|----------|------------------------------------|---------|
| **TRUE**  | **TRUE**  | FALSE | TRUE     | TRUE                               | FALSE   |
| **TRUE**  | **FALSE** | FALSE | FALSE    | TRUE                               | TRUE    |
| **FALSE** | **TRUE**  | TRUE  | FALSE    | TRUE                               | TRUE    |
| **FALSE** | **FALSE** | TRUE  | FALSE    | FALSE                              | FALSE   |

Using one of your previous hypothetical examples, if you wanted to find out if a number was negative AND even, it could be written like so:

```javascript
let number = 5;

let isItEven = (number % 2 == 0);      //true if number is even
let isItNegative = (number < 0);       //true if number is less than but not equal to 0

let answer = isItEven && isItNegative; //true if isItEven is true and isItNegative is true
```

## Reading Complex Expressions

In programming, it is often likely that you will encounter many different combinations of the above comparison and logical operators used in an expression. Boolean expressions can be combined.

Assume two integers `a` and `b` are used in the following expression.

```javascript
let output = (a >= 10 && a <= 20) || (b >= 10 && b <= 20);
```

In the above expression `output` is only true if `a` or `b` is between 10 and 20. If you give `a` the value 8 and `b` the value of 13, you can see how the expression is simplified step-by-step.

First the value for the variables are replaced within the expressions.

```javascript
let output = (8 >= 10 && 8 <= 20) || (13 >= 10 && 13 <= 20);
```

Next, the following comparison operations are evaluated, simplifying the expression:
* `8 >= 10` is `false`
* `8 <= 20` is `true`
* `13 >= 10` is `true`
* `13 <= 20` is `true`

```javascript
let output = (false && true) || (true && true);
```

After the comparison operations, then the logical expressions are evaluated:
* `false && true` yields `false`
* `true && true` yields `true`.

```javascript
let output = (false) || (true);
```

Lastly `false || true` is evaluated and the final value is `true`. The complex expression from a few steps back is simplified into:

```javascript
let output = true;
```