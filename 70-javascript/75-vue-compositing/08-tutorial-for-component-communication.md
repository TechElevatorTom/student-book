# Building Communicating Components

To see this in action, let's imagine an application that will allow us to search a car manufacturer API. On a search screen, we will need two main features; a search box and a list of results. We could build these in one component and is probably the natural inclination to solve this problem, but let's look at these two functions. Because they are two separate functions. They work together, but they don't have to be together and part of the same component. One of our principles is that a component should be responsible for handling only one job. This example is really doing two different things: Making an API call based on a search string and then showing those results to the user.

So we'll write two different components for these actions. But first, let's define the interfaces of these components, in terms of input and output data.

### Defining the Search Box

Our search box component will have one responsibility, using user input to perform a search via an API.

What is the input, or `props`, this component would have? It won't need to accept any input from other components, but we can make it a very generic component by having the URL to the API added to it as a property. So we'll define a `prop` that can be set on this component as `searchUrl`.

What are the outputs, or custom events? This component will be pulling back search results from the API and then sending an event with those results in them. We'll call this event `search-results`.

### Defining the Results Display

Our results can be shown as a table of results.

What are the input `props` in this component? The search results from the Search Box. That is the data coming in that we want to show in the table.

What is the output as custom events? There are none at the moment. There are some components that are used to receive and show data and don't have any outputs of their own.

## Building the Components

So we have defined two components:

1. a `search-box` component that will take a `searchUrl` as a `props` input property and output a `search-results` event.
2. a `results-display` component that will take a `searchResults` `props` property and use that to display a table of results.

### Building the SearchBox

#### Defining the parts

First, let's define our interface for the user. It'll just be a simple form with a text field and a button:

``` HTML
<template>
    <form>
        <input type="text" name="search" id="search">
        <button type="submit">Search</button>
    </form>
</template>
```

Now let's define our logic. What do we need for our JavaScript? We need a data property to bind to the search box. We need a method to handle the form submission. We need a `props` property to hold the URL we are submitting the search to. Let's create those:

``` JavaScript
export default {
    name: 'search-box',
    data() {
        return {
            // data property to tie to the search box
            search: ''
        };
    },
    props: {
        // props property for accepting the url to the API
        searchUrl: String
    },
    methods: {
        // method to handle the form submission
        performSearch() {
            fetch(this.searchUrl + this.search + '?format=json').then((response) => {
                return response.json();
            }).then((results) => {
                this.$emit('search-results', results);
            });
        }
    }
}
```

Taking a look at that method, we can see our standard API call to get a response as a JavaScript object but then we call something called `$emit`. In order to send our results to other components, we must emit a custom event. We said that our custom event will be called `search-results` and so we call that here and send along all the results that we got back from the API call. We'll see later how to listen for that event in another component.

#### Wiring up the component

Now we need to wire the JavaScript to the HTML using `v-model` for the search field and `v-on` for the form submission.

### Building the ResultsDisplay

Now let's create another component, the `ResultsDisplay` component.

 This component will involve a table that will display search results. This component will need to know how the data is structured and we can get that from the APIs documentation. We will be doing a search off of a Department of Transportation safety database at https://vpic.nhtsa.dot.gov/api/Home.

The resulting manufacturers from this database will be returned in the following format:

``` JSON
{
    "Results": [
        "Make_ID": 474,
        "Make_Name": "Honda",
        "Mfr_Name": "HONDA MOTOR CO., LTD"
    ],
    ...
}
```

#### Defining the parts

First, we'll add our JavaScript, knowing that we will be given a `props` property for the results.

``` JavaScript
export default {
    name: 'results-display',
    props: {
        results: Array
    }
}
```

Now we can build the HTML

``` HTML
<template>
    <table v-if="results">
        <tr>
            <th>ID</th>
            <th>Make Name</th>
            <th>Manufacturer's Name</th>
        </tr>
        <tr v-for="result in results">
            <td>{{ result['Make_ID'] }}</td>
            <td>{{ result['Make_Name'] }}</td>
            <td>{{ result['Mfr_Name'] }}</td>
        </tr>
    </table>
</template>
```

Once that's done, we'll see how to wire up components so that they can talk to each other.

## Setting up the Parent Component

To get two components to talk to each other, they must both be put inside of the same parent component. A parent component in Vue is a component that contains other components and acts as a communication manager between components, routing messages and data to where they need to go.

We'll use `App` as the component that holds both of our search components. There's not much to it at the moment, but we are going to need to import our two components and include them in this component. First, let's import them.

``` JavaScript
import SearchBox from './components/SearchBox.vue';
import ResultsDisplay from './components/ResultsDisplay.vue';
```

Then we define them as child components to the `App` component:

``` JavaScript
export default {
  name: 'app',
  components: {
    SearchBox,
    ResultsDisplay
  }
}
```

And then add them to that component's display. Remember that we use the component's `name`s here for the tags:

``` HTML
<template>
  <div id="app">
    <search-box></search-box>
    <results-display></results-display>
  </div>
</template>
```

Now that they are added, we need to handle their inputs and outputs. The `App` component is now like our switchboard, ready to route the data coming from the components to the other components under its control.

## Handling SearchBox's Inputs and Outputs

First, we need to pass the search url to the `SearchBox` component's `searchUrl` prop. We can do that by just adding an attribute to the tag:

``` HTML
<search-box
    search-url="https://vpic.nhtsa.dot.gov/api/vehicles/getmakeformanufacturer/">
</search-box>
```

Here, we spell the `prop`'s name with a dash, but the `search-url` attribute will be plugged right into the `SearchBox`'s `searchUrl` prop when the component starts. We can pass string data like this to our components' `props` properties whenever we add our components' HTML tag to a page.

The `SearchBox` will also be outputting information in the form of a custom event, so we also need to add a method to `App` that will listen for that event.

``` HTML
<search-box
    search-url="https://vpic.nhtsa.dot.gov/api/vehicles/getmakeformanufacturer/"
    v-on:search-results="handleResults">
</search-box>
```

And then add data and a method to handle that event.

``` JavaScript
  data() {
    return {
      searchResults: { "Results": [] }
    }
  },
  methods: {
    handleResults(newResults) {
      this.searchResults = newResults;
    }
  }
```

So when a new `search-results` event comes in from the `SearchBox`, we handle it with a call to `handleResults`. We take the new results we were given and save them to a data property on the `App` component.

## Handling ResultsDisplay's Input

Now we just need to get that to the `ResultsDisplay` component, which we can do by binding the `searchResults` property to the `ResultsDisplay` component's props property called `results`.

``` HTML
<results-display v-bind:results="searchResults"></results-display>
```

That might have seemed like a lot to keep track of, but just remember about our interfaces. If we know what the inputs and the outputs are to the components, then we can plan out what they will need and what they will be giving out. Then it's just a matter of wiring them together in the parent component.

For inputs to a component, that will be done via `props` attributes in the parent, either hard-coded like the `search-url` in the `search-box` tag or bound to data attributes like the `search-results` in the `results-display` tag.

For outputs, the parent tag will listen for them with a `v-on` just like any other event and then handle the event in a method.

Other than that, the parent component stays pretty dumb. Much like a controller in the MVC architecture, the parent component shouldn't have much logic in it. It's just there to route messages from one component to another.

By defining these interfaces and then focusing on one component at a time, we can now think of our application as specific pieces to be built and not as a whole that has to be kept in mind while building everything else.