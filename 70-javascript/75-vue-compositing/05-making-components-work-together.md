# Making Components Work Together

A popular definition of components is:

> Components are stand-alone, independent parts of the application which are responsible for handling only one job and don't know about each other.

Let's look at the pieces of this definition and start to formulate a plan on how to think about designing components that all work together.

## Stand-Alone and Independent

Components should be built in such a way that they are stand-alone and independent, both from other components and from other things that might not be components running on the site. A component is able to contain all of the data, behavior, UI, and general layout needed to run itself and should need to rely on anything outside of itself to get its own job done.

You can think of this as a form of encapsulation. The component's internals should be protected from other parts of the system, giving components a sort of independence from outside forces.

This independence makes unit testing simple and makes future changes and refactoring much simpler than if the component had to worry about every other piece of functionality on the site. This doesn't mean that the component will never interact with other components on the page, but that those interactions aren't required for the component to function normally. In fact, by not requiring any other component explicitly, we open up the possibility for a component used in different contexts to be used for different purposes.

## Parts of the Application

What makes a component powerful is that it is only one small part of an application. An application or a web page should be made up of many components, all working together to create a complete whole. Much like a car is made up of lots of replaceable and independent pieces, an application should be made of replaceable and independent components.

## Responsible for Handling Only One Job

Each component in the application should only be handling one job. If a component isn't trying to handle an entire dashboard but is instead focused on just one graph on that dashboard, that component is going to be easier to test, easier to maintain, and easier to reuse in another context. We should be trying, when looking at our application, to specify components in as limited a functional context as we can. We can then look at how to make these components work together to build a larger application.

## Don't Know About Each Other

Our components shouldn't know about or rely on other components that might be on the same page or application. That may sound strange at first. How can components work together if they don't know that other components exist?

But this is just the same concept of an application that handles printing. When the application is built, does it know what printers are available? No. In fact, new printers are created all the time that it will have to interact with. But the programmers of that application build an interface and tell the printer manufactures that if they build their printer drivers to support that interface, then their application will work with that printer. The print manager application doesn't really know one printer from another, but as long as the interface is followed then everything will work together.

And that is how we will get our components to work together, by following interfaces between them. JavaScript doesn't have the concept of a programmatic interface like Object Oriented languages do, so we will have to document these interfaces ourselves. To build these interfaces in Vue, we will be looking at two aspects of our components that we haven't touched on yet: custom events and props.

## Custom Events for Outputting Data

Custom Events form our component's output. Whenever we want to send information out of our component to another part of the application, our component will emit an event. We'll look at the mechanics of this later, but for now we just need to understand what a custom event is made of.

All custom events will have two parts to it. The first is the name of the event. This can be whatever we want it to be, but it's usually seen in the form of a noun. Our component has performed an action and it is sending out the results of that action. `search-results`, `product-details`, and `new-message` are all good custom event names.

The second piece of a custom event is the data that is attached to that event. Usually after an action is performed, data is generated that can be sent along with the event. For example, on a `search-results` event, a JavaScript object with the search results contained in it can be sent along with the event. The structure of that data object should be carefully documented with the event so that other programmers will know how to use it in their components.

And where does that event go? That event will go to it's parent component. A parent component is a component that holds other components. A parent component will be developed so that it knows what events to listen to, via `v-on`, and what structure the data is in so that it knows how to handle it. It has no idea how that data was retrieved or exactly what component created that data, only what the *interface* for that information is.

So if a component is outputting data, it will send an event name and a data structure. What about if the component is receiving data?

## Props for Receiving Data

Vue components have several different types of properties; `data`, `computed`, and one that we'll look at now called `props`. `props` are specifically data that is given to the component from outside the component, either from another component or from the mounting of the component. `props` are the way that data gets into our components and then can be used inside the components to perform other actions.

`props` properties can be used inside the components just like any other kind of properties. They can be bound to UI elements using `v-for` or `{{ ... }}`--`props` are almost never two-way bound to input elements because they are changed from outside the component--and can be used inside methods or other computed properties. They are useful to set configuration options on a component so that the components can be reused. Examples of that kind of information would be what API URL it should connect to or what display options should be shown.

They can also be used to pass the main data that a component needs to work, like a table component that needs to show a list of users. The list of users might be created from a search component and then passed to the table component for display.