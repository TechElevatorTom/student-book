# Event Handling in Vue

As we saw in vanilla JavaScript, listening and handling events coming from the user is how we build interactivity into our applications. The way that this was handled out side of Vue was that we had to search for the DOM element we wanted to listen to an event on, add an event listener to that DOM element for a certain event, and then attach a function to that event so that it ran when the event fired off.

As we saw, Vue gives a lot of simple ways to work with the DOM. It also gives us a lot of simple ways to work with events as well. Vue handles attaching event listeners for us using the `v-on` syntax and all event handlers are now just `methods` on our component. We'll look at how to set those up, and listen to all kinds of different events, in the next chapter.