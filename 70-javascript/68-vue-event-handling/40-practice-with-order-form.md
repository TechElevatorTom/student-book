# Practice with the Order Form Component

Let's add a method to our `OrderForm` component from yesterday that will take all the form data and show it in a JavaScript alert box. This method will take all the data properties in the component--the values coming from the data bound input fields--and show them as text in an alert component.

### Structuring Our Data for an API

The first thing we're going to do is refactor how we're handling our data in our component. Mainly, we're going to put all of the data that we want to send to the back-end into its own object. We'll call that object the `order` object.

By sectioning this data off, we can more easily handle it when the time comes to use it. So let's first restructure the `data` section of our component to include an `order` object with all the properties from the form:

``` JavaScript
data() {
    return {
        order: {
            firstName: '',
            lastName: '',
            shippingAddressOne: '',
            shippingAddressTwo: '',
            shippingCity: '',
            shippingState: '',
            shippingZip: '',
            billingAddressOne: '',
            billingAddressTwo: '',
            billingCity: '',
            billingState: '',
            billingZip:'',
            creditCardNumber: '',
            creditCardType: '',
            expDate: ''
        },
        sameAddress: false,
        creditLogoSrc: '../assets/credit.png',
...
```

Now we need to restructure how we use `v-model` in our UI. Instead of linking the first name field to `firstName`, we now just need to link it to `order.firstName`. The same is true for every other field in that interface.

### Using the JSON object to display the data

We're going to use `JSON.stringify()` to format the data to show in an `alert()` call.

``` JavaScript
methods: {
    saveOrder() {

        alert(JSON.stringify(this.order));
        this.message = JSON.stringify(this.order);

    }
}
```

We can also show the information in a `div` on the page as a message. Add a `message` data property to the `data()` function, so that we have someplace for the message to go.

Let's add that data property to the `data()` function and add a `div` at the top of the form to show the message, if it's available.

``` HTML
<form class="order-form">
    <div class="message" v-if="message != ''">{{ message }}</div>
    <div class="form-group">
```

## Using `v-on:submit` to Listen for Submit Events

Now, we want this method to be called when the form is submitted. Normally this would entail getting the form from the DOM and calling a `addEventListener` to it for a `submit` event. But we can use the `v-on` syntax instead to attach the methods to that element:

``` HTML
<form class="order-form" v-on:submit="saveOrder">
```

If we save this and try it, we see that the form still submits, which is not what we want. We need to `preventDefault()` like we did in vanilla JavaScript. But Vue gives us a short cut for this. We can attach the event handler with a special `.prevent` modifier.

``` HTML
<form class="order-form" v-on:submit.prevent="saveOrder">
```

There are a couple of other modifiers we can use on event handlers using `v-on`:

- `.stop` - Will call `stopPropagation()` on the event.
- `.once` - The handler will only be called once and then unregistered.
- `.passive` - Which will act just like adding the passive flag to addEventListener.