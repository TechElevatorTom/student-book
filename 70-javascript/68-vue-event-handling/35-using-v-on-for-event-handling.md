# Using `v-on` for Event Handling

A JavaScript UI wouldn't be useful if it wasn't able to handle events from the user. As we talked about, these front-end frameworks are designed to handle DOM manipulation--as we saw with the `v-bind` and `{{ ... }}` syntax--and make event handling easier. Vue does the later with the `v-on` attribute.

## Adding Behavior to Our Components

Before we dive too far into event handling, we need to talk about how we add behavior to our components. We've said that encapsulation is all about having data and methods that work on that data. Now we will see how we can add these methods to our components.

We already saw that we can add a function to the `computed` section, but that is just to filter or calculate information from existing data. What about making an AJAX call or any complex logic to transform or change data?

For these more complex operations, we have the `methods` section of the component.

`methods` are JavaScript objects that we can add to our component to give it some behavior and logic. The JavaScript functions defined in the `methods` section of your component are there to handle events, modify data, and handle network calls for the component. These `methods` are defined like this in the component:

``` JavaScript
export default {
    name: 'order-form',
    methods: {
        displayFormData() {
            // Method logic goes here
        },
        anotherMethod() {
            // Method logic goes here
        }
    }
};
```

We can then add these methods to elements in our UI, using the `v-on` attribute:

``` HTML
<form v-on:submit="displayFormData">
...
</form>
```

## Using the `v-on` Syntax

`v-on` is an attribute that is added directly to the HTML in the view part of our component, just like `v-bind`. The part after the colon is the event that you want to listen to. To listen for a click on an anchor tag, we could use:

```HTML
<a v-on:click="alertUser">DELETE</a>
```

To listen to change events on a text box, we could use:

``` HTML
<input type="text" name="firstName" v-on:change="updateName" />
```

And so on.

## Getting Special Data on Certain Events

Some events have extra data attached to them. For example, keyboard events have data about what key was pressed and mouse events have data about what mouse button was clicked. When we add our event listeners, we can listen for only specific pieces of data, like a keyboard key, instead of just the event.

### Keyboard Events

For example, if we want to listen for an enter key in a textarea, we can add the following `v-on` listener:

``` HTML
<textarea v-on:keyup.enter="processEnterKey"></textarea>
```

Special keys that you can listen for are:

- `.enter`
- `.tab`
- `.delete` (captures both "Delete" and "Backspace" keys)
- `.esc`
- `.space`
- `.up`
- `.down`
- `.left`
- `.right`

You can also listen for a specific key by using its ASCII code. For instance, if I wanted to listen to a lower case `j` (ASCII code 74), then I could add the following `v-on`:

``` HTML
<input type="text" v-on:keyup.74="handleJ" />
```

You can look up keycodes on [https://keycode.info](keycode.info).

### Mouse Events

We can also listen for which mouse button was clicked using the same sort of trick. We can listen for different mouse buttons that triggered a `click` or `dblclick` event with the following flags:

-   `.left`
-   `.right`
-   `.middle`

For example, we can attach these to a UI element with:

``` HTML
<li v-on:click.left="updateTotal" v-on:click.right="activateContextMenu">Add to Cart</li>
```