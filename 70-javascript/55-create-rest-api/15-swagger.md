## Swagger

Now that we know what information is included in our API docs we need to create them. One thing that I have found in the past is that the easier it is to create documentation, the more likely it is to actually get completed. Swagger is one tool that makes creating API documentation incredibly easy. 

https://swagger.io/

{% video %}https://www.youtube.com/watch?v=QNYwn34-LjQ {% endvideo %}