# Introduction to CORS

Web browsers implement a security concept called [same-origin policy](https://en.wikipedia.org/wiki/Same-origin_policy) that prevents JavaScript code from making requests against a different origin (domain) than the one it originated from. This policy is in place stop some bad things happening but it unfortunately also prevents legitimate requests between trusted origins. 

Cross-Origin Resource Sharing (CORS) is a mechanism that uses additional HTTP headers to tell a browser to let a web application running at one origin (domain) have permission to access selected resources from a server at a different origin. A web application makes a cross-origin HTTP request when it requests a resource that has a different origin (domain, protocol, and port) than its own origin.

## Basic Cross-Origin Request

In a basic example a cross-origin request starts with a client making a GET, POST or HEAD request against a resource on another server. In our example www.domain-a.com is going to be the client and www.domain-b.com is going to pla the role of the server. Say for example that domain-a wants to get a list of users from domain-b. The client (domain-a.com) will make a GET request to domain-b.com and include the origin header that indicates the origin of the client code. 

The Server (domain-b.com) will look at the origin header and determine if it should allow or not allow the request. If the server decides that it should allow the request then it will respond with the list of users along with an Access-Control-Allow-Origin header in the response. This header will notify the client who has access to this resource and if there is a match with the requests Origin, the browser will allow the request. 


```
GET /users/ HTTP/1.1
User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/536.30.1 (KHTML, like Gecko) Version/6.0.5 Safari/536.30.1
Accept: application/json, text/plain, */*
Referer: http://www.domain-a.com
Origin: http://www.domain-a.com
```

The Origin header lets the server know that the client code came from http://www.domain-a.com. The server sends a response with an Access-Control-Allow-Origin that matches our request and therefore the browser will allow the response. 

```
HTTP/1.1 200 OK
Content-Type: application/json;charset=UTF-8
Date: Wed, 20 Nov 2013 19:36:00 GMT
Server: Apache-Coyote/1.1
Content-Length: 35
Connection: keep-alive
Access-Control-Allow-Origin: http://www.domain-a.com

[response payload]
```

## Localhost

We should talk about something that was mentioned earlier that we didn't have a chance to dive into. Earlier when talking about what CORS is we made the following statement:

> A web application makes a cross-origin HTTP request when it requests a resource that has a different origin (domain, protocol, and port) than its own origin.

The domain, protocol and port portion of that statement is very important. Say for instance you're developing a Single Page Application in Vue. In your development environment that application might run on http://localhost:8080/. Now say you were developing the REST API using the Spring Framework and that runs on http://localhost:8081 in development or a .NET REST API that runs on http://localhost:5000/. Those are considered different origins because the port is different and the Vue application would have issues accessing resources from our server application. 

This tends to stump developers because they don't think CORS applies when both applications are running on localhost. I just wanted to point this out as you will undoubtedly come across this scenario sooner rather than later. 

Another local development practice that tends to trip developers up is when you just double click on a file. To view HTML you can just open a file by double clicking on it. This becomes a problem when requesting data cross origin because your origin is a file. The safe bet here is to always run your local files using some type of development server (live server from vs code).

## Further CORS Reading

This was a very basic example and it was used to illustrate the basic concept of CORS and how it works. There are some more advanced concepts to learn and if you want to read up on them check out the [MDN Docs on CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS). 
