# REST API Documentation

I know that writing documentation is usually the last thing we want to do but when it comes time to use an API it's one of the first things we look for. As we have learned, APIs are created for other developers to use. It is going to be very hard for other developers to use your API if they can't understand how it works. 

In that case there are 2 types of documentation. On one hand we might have some reference documentation that explains what our product is and how to use it. We also want to have API documentation that is there to explain how our API works. 

## What's included 

Typically in your API documentation you will include the following information: 

* Resource descriptions
* Endpoints and methods
* Parameters
* Request example
* Response example

On top of these sections your API documentation might include things like getting started, authorization info, status and error codes and more. 

### Resource Descriptions 

Resources are the nouns of our application (students,locations,etc) and is what is returned from our API. You will start with a very brief description on what the resource is used for. Each resource will usually have a number of endpoints that are available to you as the API user. 

Let's say that we were creating an API for Tech Elevator. One resource that we might have is a Location resource. If we wanted to describe our resource to our users we might say something like this.

> Tech Elevator currently has x number of locations and we are always looking to expand. Use the locations API to get a list of locations and information about each location including what tracks that location offers.

### Endpoints and methods

Once we have a description in place for our resource we need to let our consumers know what endpoints are available. The endpoint describes how you access the resource along with the method used to access it. If we were sticking with our locations example we might have a listing like this.

* POST /locations
* GET /locations
* GET /locations/{locationId}
* GET /locations/{locationId}/tracks
* PATCH /locations/{locationId}
* DELETE /locations/{locationId}

### Parameters

Parameters are additional options that you can pass to an endpoint to adjust the response. There are four types of parameters: 

* Header Parameters
* Path Parameters
* Query String Parameters 
* Request Body Parameters

What if we wanted to offer a sorting option in our GET: /locations endpoint. We could allow parameters that do the following: 

* Sort by locations nearest to me
* Sort by location name 
* Sort order ascending or descending 

These are options and we can include them in the documentation as parameters. We would include what parameter type it is as well.

Query Parameters

* Sort By: The field you wish to sort by. Valid options are name,nearest
* Sort Order: The order you want your sort by in. Valid options are asc or desc


### Request Example 

This is what an example request to our endpoint might look like. You can also include an example that includes some parameters but you want to keep this simple. You will also not show off every possible permutation. Sample requests can include code snippets that show the request being used in a variety of languages. 

```
curl -H "Content-Type:application/json" -X GET "https://api.techelevator.com/locations?sortby=name&sortorder=asc"
```

### Response Example

The response example shows what the response will look like if you ran the request example. 

```
{
    "locations": [
        {
            "id": 1,
            "name": "Cleveland Campus",
            "city": "Cleveland",
            "state": "OH"
        },
        {
            "id": 2,
            "name": "Columbus Campus",
            "city": "Columbus",
            "state": "OH"
        },
        {
            "id": 3,
            "name": "Cincinnati Campus",
            "city": "Cincinnati",
            "state": "OH"
        },
        {
            "id": 4,
            "name": "Pittsburgh Campus",
            "city": "Pittsburgh",
            "state": "PA"
        }
    ]
}
```


