# Creating a REST API

We now have a better understanding of what REST services are and how to work with them. In a previous lesson we built a CRUD app in JavaScript using a mockAPI. In this lesson we are going to create our own API using your server side language of choice. 

In this lesson you will learn:

* Creating a REST API
    * How to create a new API project
    * REST Controllers vs Controllers
    * Creating API Resources
    * Testing API Resources
* REST API Documentation
    * What to include in your documentation
    * Swagger
* CORS