# Asynchronous Programming

Prior to talking about web services, all of the programming we have done has been "synchronous". In other words, when we have called a function or method, we have expected to get a result before the flow of execution moves on to the next line of code.

This is similar to the experience of going through a checkout line at a grocery store. You wait in line as the cashier computes the total for each customer and then processes that customer's payment. You need to wait until each person in front of you in line has finished paying for his or her groceries before you can pay for your groceries.

Now, consider what it would be like if restaurants operated in the same fashion. Imagine if a food server came to your table, took your order, walked back to the kitchen to hand the order to the chef, and then waited around for your food to be cooked so that he or she could bring it back to your table and only _then_ went to the next table to take the next order. Needless to say that server would not be receiving very good tips! 

Instead, the server dispatches orders to the kitchen **asynchronously**. The server delivers the order from the first table to the kitchen, and then, while the kitchen is cooking that order, goes out to the dining room and collects orders from more tables. When the kitchen finishes an order, the server is notified and he or she delivers that food to the appropriate table. Cooking a meal is a fairly lengthy process, so rather than waiting around, it's more efficient for the server to simply dispatch a request and then go about doing other useful work until the result of the request is ready (i.e. the meal is cooked).

## Web Service Requests Are _SLOW_

It isn't unusual for a web service request over the Internet to take 50-100ms to make a round trip from your web browser to the server and back. Now, .1 seconds may not seem like much time to you or me, but in computing time it's an _eternity_! Modern CPUs can process 100's of millions of instructions in that amount of time. It would be incredibly inefficient if the code on a web page made a web service call and then waited around for the response before executing any more code on the page. If the page is making enough web service requests, or if those requests are slow enough, it could even lead to the web UI feeling sluggish and unresponsive to the user.

In order to make programming with web services efficient, there are features built into Javascript to allow us to make use of asynchronous programming techniques. Transitioning from synchronous programming to asynchronous programming can be a challenging mental shift, even for experienced programmers, but it's an important concept to master in order to write modern web application interfaces in Javascript.

## Asynchronous vs Synchronous

When we perform some task like sending an email, calling a database or calling another service we have 2 ways to perform that task. When you perform a task **synchronously**, you wait for it to finish before moving on to another task. When you communicate **asynchronously** you can move on to another task before it finishes.

Let's take a look at some pseudo code and look at how it differs between to the 2 paradigms. Each letter below represents a task we must complete to register a new user in our system.

```
A. User fills out registration form
B. User registration form is validated & saved
C. Send new user Registration email to user
D. Send new user registration email to admin
E. Display new user registration success page.
```

## Synchronous 

In **synchronous** we have to finish each step before moving on to the next. The user must wait for the emails to be sent before we will show a success page. 

![Synchronous Request](resources/images/synchronous_diagram.png)

## Asynchronous

In **asynchronous** we can move our longer running, expensive requests like sending an email into an asynchronous request. Now we don't have to wait for those emails to send and the user can be taken to the user success page right away. Why make the user wait for something they don't care about?

![Synchronous Request](resources/images/asynchronous_diagram.png)

```
A. User fills out registration form
B. User registration form is validated & saved
    C. Send new user Registration email to user (Async: Don't wait to complete)
    D. Send new user registration email to admin (Async: Don't wait to complete)
E. Display new user registration success page. (Happens directly after B)
```



