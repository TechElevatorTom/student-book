# Promises

In our [Fetch API examples](31-ws-fetchapi.md), many of the methods we used returned `Promise` objects. This is because many of the methods in the Fetch API are **asynchronous**. In this section we'll explore how we can use the `Promise` object to implement aynchronous code.

As you're familiar with by now, when we call a _synchronous_ method we expect to get the final result back as soon as the method is finished executing and be able to use it immediately. 

```javascript
const salesTaxDue = checkoutService.calculateSalesTax(orderSubTotal); // method returns a number
const orderTotal = orderSubTotal + salesTaxDue; // the result can immediately be used in a calculation
... // some other code that does something useful with orderTotal
```

However, when calling an _asynchronous_ method, the method may finish executing _before_ the result is available. How can we represent a result that isn't available yet, but will _eventually_ be available?

In Javascript, one of the most common ways to handle the results of asynchronous methods is using an object called a `Promise`. When a method returns a `Promise`, you can think of it as saying, "I don't have your answer now, but I _promise_ to get back to you when I do.".

In our code, we can associate handlers (i.e. functions) with the task's eventual success or failure. 

A Promise can be in one of 3 states:

* **Pending**: initial state, neither fulfilled nor rejected.
* **Fulfilled**: meaning that the asynchronous operation completed successfully.
* **Rejected**: meaning that the asynchronous operation failed.

## Programming With Promises

A `Promise` object allows us to specify what should be done when the result of an asynchronous operation has completed by passing a handler function to the [`.then(...)` method](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise/then). For example, if the `calculateSalesTax(...)` method in the example above were asynchronous, it might return a `Promise` that contained the results of the sales tax calculation as illustrated below.

```javascript
checkoutService.calculateSalesTax(orderSubTotal)
    .then( (salesTaxDue) => {
        const orderTotal = orderSubTotal + salesTaxDue; 
        ... // some other code that does something useful with orderTotal
    }); 
```

If we were going to describe this code in English, we might say "Call the `calculateSalesTax` method of the `checkoutService`, wait for the result to be ready, _then_ use the `salesTaxDue` returned to calculate the `orderTotal`."

### Handling Errors

Remember, we said earlier that a method returning a `Promise` object is analogous to that method _promising_ to return some result in the future. Well, what if something goes wrong and the expected result can't be returned (i.e. the promise gets "broken")? For these cases, the `Promise` object provides the `catch(...)` method so that we can specify what should be done in the case of an error. The example below adds some rudimentary error handling to the previous example.

```javascript
checkoutService.calculateSalesTax(orderSubTotal)
    .then( (salesTaxDue) => {
        const orderTotal = orderSubTotal + salesTaxDue; 
        ... // some other code that does something useful with orderTotal

    }).catch( (error) => {
        console.log(error); 
        ... // some additional error handling logic
    }); 
```

### Promises and `fetch()`

Now that we understand a little about `Promise` objects, let's revisit one of our previous [Fetch API examples](31-ws-fetchapi.md).

```javascript
fetch('data.json')                  // sends an HTTP request to the relative path 'todos.json' and returns a Promise
    .then( (response) => {          // the Promise contains the Response object
        return response.json();     // The Response.json() method returns another Promise
    })
    .then( (data) => {              // here we're dealing with the Promise returned by Response.json(), and data is the JSON data contained by that Promise
        
        ... // some code that deals with the JSON data
    });
```

What we are saying here is that when the `Promise` returned by `fetch()` resolves, we are going to call the `Response` object's `json()` method on it. When that `Promise` resolves, we will get the JSON data back and do something with it. This really confuses everyone the first time they see it so please make sure you understand what is going on here.

One of the more confusing bits is `return response.json()`. This returns the `Promise` returned by `response.json()`. We are doing this so that we can "chain" the `then()` methods. Instead of using chaining, we could instead have written the code this way:

```javascript
fetch('data.json')                  
    .then( (response) => {          
        response.json()
            .then( (data) => {        
                    ... // some code that deals with the JSON data
            });     
    });
```

Here, we call `then()` directly on the `Promise` returned by `response.json()`. The two examples are functionally equivalent, it's just a matter of style. However, using chained `then()` methods, as in the former example, generally makes code easier to read and will be the style we will use in examples.