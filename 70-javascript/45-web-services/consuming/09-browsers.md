# Browser Compatibility (Optional Reading)

We need to have a quick talk about browser compatibility when it comes to features in the browser. To do so we need to understand what ECMAScript is and how the different browser vendors work.

## ECMAScript & JavaScript

ECMAScript (or ES) is a trademarked scripting-language specification standardized by Ecma International. It was created to standardize JavaScript, so as to foster multiple independent implementations. 

So ECMAScript is the standard and JavaScript is the implementation. This forces different browsers to adhere to these standards and not implement a feature as they see fit. Without this we would have the wild wild west of functionality across different browsers. 

When you hear things like ES 6 or ES 2015 these are referring to the ECMAScript Standard version that was created. If you want you can read the spec you can as its freely available online. 

https://www.ecma-international.org/ecma-262/6.0/


## Can I Use?

Now that we understand that each browser is implementing JavaScript features based on the ECMAScript standard everything we use should be the same across browsers right? Well that is a nice dream but it just isn't the case. 

Different browsers will implement new ECMAScript features incrementally. So even though the specification says you need to implement these x number of features they don't say in what specific time frame. 

One way you can find out if a browser supports a feature is to use the website http://www.caniuse.com. We learned about Promises and the Fetch API and if you look them up they are both really well supported. 

![Can I Use Promises](../resources/images/caniuse_promises.png)

![Can I Use Fetch](../resources/images/caniuse_fetch.png)

Did you notice IE 11 or Edge 12-13 on the left is red? If so this means that if someone were to open up our fetch and promises demo in one of those browsers they simply wouldn't work. 

## Babel

While we shouldn't be running across a high percentage of IE 11 browsers we will undoubtedly have some using it. We can't simply ignore these users and display a message that says upgrade your browser (though that would be fun to do).

We aren't going to get into in this class but in this instance we can use something like Babel as a "fallback" for people using a browser that doesn't support the features we are using. 

![Babel](../resources/images/babel.png)

Babel is a [transpiler](https://en.wikipedia.org/wiki/Source-to-source_compiler). It rewrites modern JavaScript code into the previous standard. To be a little bit more specific it does 2 things. 

1. A developer runs a transpiler program like Babel on their computer that rewrites code into the older standard. Projects can take advantage of a build system like [webpack](https://webpack.js.org/) to automate this tedious task.

2. The second part is the polyfill. The transpiler rewrites our code so syntax features are covered but for new functions we need something special to implement them. The term polyfill is meant to "fill in" the gaps and add missing implements.

I realize that is a lot but you will see a lot of modern JavaScript projects using Babel and Webpack so you have some time read through the [Babel documentation](https://babeljs.io/). 