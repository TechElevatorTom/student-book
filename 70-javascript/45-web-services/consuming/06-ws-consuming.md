# Consuming Web Services

In this section we are going to look at how we can use JavaScript to "consume", or make use of, Web services. We are going to learn about callbacks, promises and the Fetch API so you can understand the history of Asynchronous development within the JavaScript language. 

We are going to take a look at consuming some simple, local APIs and then move on to more complex public APIs. In this section you will understand:

* Fetch API
    * What is the Fetch API
    * Fetch Syntax
    * Reading a text file
    * Reading JSON
    * Consuming a public API
* Promises
    * What is a Promise
    * Promise Syntax
    * Using Promises with `fetch()`
* Browser Compatibility 
    * Browser Compatibility
    * ECMAScript & JavaScript
    * What *can I use* in my browser
    * Babel