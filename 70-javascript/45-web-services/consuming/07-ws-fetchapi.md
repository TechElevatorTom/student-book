# Fetch API

In order to interact with web services, we will need a way to send HTTP requests and process the results. The most convenient way to do this is to use the Javascript [Fetch API](https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API). The Fetch API provides an interface for accessing and manipulating parts of the HTTP pipeline, such as requests and responses. It also provides a global `fetch()` method that provides an easy, logical way to fetch resources asynchronously across the network.

## Fetch Syntax

If you want to make a request and fetch a resource you can use the [GlobalFetch.fetch](https://developer.mozilla.org/en-US/docs/Web/API/GlobalFetch/fetch) method. 

```javascript
Promise<Response> fetch(input[, init]);
```

Let's breakdown what that method is actually doing. 

* The method is called `fetch()`
* The return type is a [`Promise`](https://developer.mozilla.org/en-US/docs/Web/API/Promise) that resolves to a [`Response`](https://developer.mozilla.org/en-US/docs/Web/API/Response) object which is defined by the fetch API. We'll talk about the `Response` object below, but for the time being, we're going to gloss over the `Promise`. Don't worry, we'll dig into that in the next section.
* The input parameter specifies the resource that you wish to fetch. The resource can be identified by either:
    * A [USVString](https://developer.mozilla.org/en-US/docs/Web/API/USVString) containing the direct URL of the resource you want to fetch.
        * This could be a local resource
        * This could be a remote resource
    * A [Request](https://developer.mozilla.org/en-US/docs/Web/API/Request) object.
* init (optional parameter) An options object containing any custom settings that you want to apply to the request. 

## Using the `.fetch()` Method

In order to get a feel for the mechanics of the Fetch API, we'll start with an example of using the `fetch()` method to retrieve the content of a text file. From the perspective of the client (i.e. the code calling the `fetch()` method), there really isn't any difference between a static text file and calling an actual web service. In both cases, `fetch()` sends an HTTP request and receives character data as a response.

Let's assume that we have an `index.html` file that contains the following:

```html
<!-- 
    filename: index.html
-->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fetch Text File</title>
</head>
<body>

    <h1>Demo</h1>

    <p>Below is the contents of our text file</p>
    <div id="results"></div>
    
    <script src="fetchtxt.js"></script>
</body>
</html>
```

Let's also assume that in the same directory as `index.html`, there is a `demo.txt` file that looks like this.

```
This is a demo txt file.
```

Now, consider the contents of the `fetchtxt.js` file below.

```javascript
// filename: fetchtxt.js
fetch('demo.txt')          // sends an HTTP request to the relative path 'demo.txt'
.then( (response) => {     // this is a bit of magic for now, just know that response is a Response object
    console.log(response)  // this block is where we write code to handle the HTTP response, here we're just logging the response object
});
```

![Fetch Response](../resources/images/txt_response.png)

What we are logging here is a `Response` object and that is important to remember. If we look at the [Response API](https://developer.mozilla.org/en-US/docs/Web/API/Response) there are some helpful methods to get the contents of what we are fetching. In this case we want the text so we can call [**`.text()`**](https://developer.mozilla.org/en-US/docs/Web/API/Body/text).

The most important part about what that is telling us is that the [**`.text()`**](https://developer.mozilla.org/en-US/docs/Web/API/Body/text) method also returns a `Promise`. This is what really confuses people up about the Fetch API because they expect to be able to just do something like this. 

```javascript
// filename: fetchtxt.js
fetch('demo.txt')
.then( (response) => {
    console.log(response.text())
});
```

If you log out `response.text()` you will see that it is indeed another `Promise` and therefore we can't just assume that we will have the text to print out. 

![Response text()](../resources/images/response_text.png)

Again, we're not going to dig into exactly what these mysterious `Promise` objects are just yet, we'll get to that later. For now, just know that we can retrieve the content of the response and update the web page by using the following code.

```javascript
// filename: fetchtxt.js
fetch('demo.txt')                   // sends an HTTP request to the relative path 'demo.txt'
    .then( (response) => {          // magic again...
        return response.text();     // more magic...
    })
    .then( (data) => {              // more magic, but know that the data parameter here contains the text of the response
        document.getElementById('results').innerHTML = data; }          // update the web page with the contents of the file (i.e. text of the HTTP response)
    });
```

## Reading JSON

That was a nice simple example but in the real world we will be calling APIs that return JSON. Before we look into calling a public API, let's look at another example of reading a local file, this time JSON data. 

Let's assume that we had a directory that contained the following files.

* `index.html`
* `todos.js`
* `todos.json`

```html
<!-- filename: index.html -->
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Fetch Example</title>
    <style>
        li.incomplete {
            color:red;
        }
        li.completed {
            color:green
        }
    </style>
</head>
<body>

    <h1>Todo List</h1>
    <ul id="todoList">
        <!-- todo items --> 
    </ul>

    <script src="todos.js"></script>
</body>
</html>
```

```json
// filename: todos.json
[
  {
    "userId": 1,
    "id": 1,
    "title": "This is my 1st todo.",
    "completed": true
  },
  {
    "userId": 1,
    "id": 2,
    "title": "This is my 2nd todo.",
    "completed": false
  },
  {
    "userId": 1,
    "id": 3,
    "title": "This is my 3rd todo.",
    "completed": false
  }
]
```

The Javascript code below is similar to the last example, but notice that this time we're using the [Response](https://developer.mozilla.org/en-US/docs/Web/API/Response) object's [json()](https://developer.mozilla.org/en-US/docs/Web/API/Body/json) method. 


```javascript
// filename: todos.js
fetch('todos.json')                  // sends an HTTP request to the relative path 'todos.json'
    .then( (response) => {           
        return response.json()       // returns a Promise that will eventually yield JSON data
    })
    .then( (data) => {              // data is the JSON data returned in the HTTP response
        console.table(data);        // the .table(...) method formats the JSON data nicely in the Console window
    });
```

The code above will output the following in the console.

![JSON Response](../resources/images/todo_json_response.png)

Now we should have everything we need to output our todo's to our web page. 

```javascript
// filename: todos.js
fetch('data.json')
    .then( (response) => {
        return response.json();
    })
    .then( (json) => {
        const list = document.querySelector("ul");
        // json is our array of todos
        json.forEach( (todo) => {
            let node = document.createElement("li");
            node.appendChild(document.createTextNode(todo.title));
            if(todo.completed) {
                node.classname = "completed";
            } else {
                node.classname = "incomplete";
            }
            list.appendChild(node);
        });
    });
```

## Consuming a Public API

In the previous example we read some JSON data that was stored in a local file. In this example we are going to call an API that returns the same data as before but a lot more of it.

[JSON Placeholder](https://jsonplaceholder.typicode.com/) is a great website for testing and prototyping with some fake data. If you look at the URL below you will see the same todo data that we were working with in the previous example only this time there are 200 of them. 

https://jsonplaceholder.typicode.com/todos

We should be able to change the URL argument in our fetch call to use the public API and everything should just work as is.

```javascript
// filename: todos.js
fetch('https://jsonplaceholder.typicode.com/todos')     // this is the only change to the code
    .then( (response) => {
        return response.json();
    })
    .then( (json) => {
        const list = document.querySelector("ul");
        // json is our array of todos
        json.forEach( (todo) => {
            let node = document.createElement("li");
            node.appendChild(document.createTextNode(todo.title));
            if(todo.completed) {
                node.classname = "completed";
            } else {
                node.classname = "incomplete";
            }
            list.appendChild(node);
        });
    });
```

The point of this example is to show you that there is no difference between reading a local resource and a public API when it comes to working with `fetch()`. Now, as we will find out later, there are some gotchas and things to watch out for when consuming a public API but the process is the same. 
