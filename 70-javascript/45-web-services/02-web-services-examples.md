# Web Services Examples 

We might not realize it but Web Services power a lot of the things we use every single day. This section will take a look at some examples of different products and applications that are taking advantage of Web Services. This is important because it makes the concept of Web Services much more tangible than just a bunch of code sitting on a server somewhere. 

[comment]: # (If this is a stand-alone page a really cool graphic of applications and services that take advantage of Web Services would be really cool here)

## Mobile Applications

If you pull out your phone and open up an application you will most likely be using Web Services without even knowing it. Let's take a look at 3 applications and how they take advantage of Web Services. 

![Mobile Applications](resources/images/mobile_apps_logos.png)

### Banking Application

We can avoid having to actually go into our local bank branch office by taking advantage of some really great features that our banks provide via mobile applications. Most mobile banking applications provide the ability to check account balances, transfer funds and even deposit checks. 

![Mobile Banking Application](resources/images/fifth_third_bank_app.png)

Now, think about all of the data that would live inside of our banking application.

* Balances of any account associated with our login
    * Checking
    * Savings
    * Business
    * Credit Cards
* Detailed list of transactions for each account
* Statements (Reports)
* Loan Information
    * Mortgage
    * Automobile 
    * Personal

Do you think that all of this personal banking information is stored on your device? First off this would be a huge security risk. Second, we also know that all of this information is available by visiting our bank's website. If all of this functionality was on our device we would have to replicate it on the web. 

Instead our bank creates an API that both the mobile and web applications can use to retrieve the data that it needs.

### Amazon

Did you know that Amazon sells almost 563 million products (as of early 2018) online? When we download the Amazon app for our phone it takes about 20 seconds to install, that's pretty fast for 563 million products! They are also adding thousands of products daily, we are going to have to install a ton of updates to keep up with that!

![Amazon Mobile App](resources/images/amazon_app.png)

Of course we know now that when we install the Amazon application that we aren't downloading their entire product library. When we need to search for a product, get product details or look at our account history these are all Web Service calls to Amazon's API. This allows the mobile & web application to use the same service. When a new feature is requested, such as the ability to sort products by best reviewed we only need to make this change in one place. 

### Instagram

Did you know that the most popular person on Instagram is Selena Gomez? Last time we checked she had over 143 Million followers. Can you imagine just for a second how upset she must get when she installs Instagram and waits for all 135M followers to be downloaded onto her phone?  

[comment]: # (I was debating adding this example but everyone uses a social network like Instagram and I feel like it resonates with more people)

![Instagram Mobile App](resources/images/instagram_selena.png)

We of course know now that this isn't what happens. When you visit Selena's profile a Web Service call is made to the API to get a count of her current number of followers. If you want to look at a list of her followers you can but only a few at a time, also known as pagination. 

## Web Services in our daily lives

Beyond just powering mobile applications, Web Services are used in other wayts to improve our lives on a day to day basis.

### Home Automation

Home automation devices are becoming more and more popular these days. This could be anything from automating a light to a camera on your doorbell all the way up to an entire home security system. 

![Ring Video Doorbell](resources/images/ring_video_doorbell.jpg)

Ring Video Doorbell connects to your home Wi-Fi network and sends real-time notifications to your smart phone or tablet when someone is at your door. ... Ring Doorbell can alert you when someone presses the button on your doorbell or when motion is detected.

In this case your phone's application is acting as the API and collects a limited amount of data. If you want to sign up for Ring's monthly service (for a small fee) it will take that data and push it to their API and log much more historical data. 

### Booking a flight

Chances are, when you need a flight for an upcoming vacation, you check some of the major carriers for pricing and availability. Instead of checking each company separately you can use a service like Orbitz. They will check multiple airlines using Web Services and then bring all of the results into a single list that can then be filtered based on what you're looking for.

![Orbitz](resources/images/orbitz.png)

### Currency Conversion

If you have ever taken a trip to another country chances are you have used Google's Currency Converter. This is a public API that allows you to convert money from one country to another. 

![Google Currency Converter](resources/images/currency_converter.png)

### Social Login

Have you ever signed up for a service and been given the option to login using Facebook, LinkedIn, Github or Twitter? One of my favorite services to keep up on programming skills is Exercism.io. When you create an account there you're given the option to signup using your Github account. 

![http://www.exercism.io](resources/images/exercism.png)

### Google Maps

We all might have used Google maps on the web or a mobile device to get directions but the Google Maps API is very popular. If you see a location on a company's website or a list of buildings in an area they are almost always using Google Maps.

![Google Maps](resources/images/maps.png)

### Stripe

Do you want to collect payments on the web? Maybe you have purchased something on the web lately. Chances are you have come across a Stripe payment form. It turns out writing stuff like that well is really hard, but thankfully we have services like this one. If you want to accept payments on the web and you want to be PCI compliant give Stripe a try and take a look at their API. 

![Stripe Payment Form](resources/images/stripe_payments.png)


