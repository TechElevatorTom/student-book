# What are Web Services? 

We know by now that the acronym "UI" stands for "User Interface", and refers to the portion of an application that users interact with in order to make use of its functionality. For web applications, this UI is presented as a web page rendered in the browser. For example, we've all used web sites to look up the weather report for our local area by entering a zip code into a text field in our web browser and clicking a submit button. Well, imagine that we wanted to build an automated concierge website for tourists that would suggest fun things to do around town. It might be useful if this concierge service was able to know what the weather report for the day was going to be so that it could make better suggestions (e.g. don't suggest a trip to the local water park if it's going to be 50 degrees and rainy!). Well, it's not realistic to think that we're going to build our own meteorological system in order to accomplish this, so it would be really nice if we could get that data from an existing weather web site. Fortunately, many systems provide a special interface just for other programs to use. Whereas the User Interface (UI) is provided for users, an Application Programming Interface (API) is provided for other programs to interact with. For web applications, the API is normally provided in the form of a "Web Service".

As defined by [The World Wide Web Consortium (W3C)](https://www.w3.org/TR/ws-arch/), Web services provide a standard means of interoperating between different software applications, running on a variety of platforms and/or frameworks. 

In the information age we live in today, sharing of that information is vital. You might not realize it but everything you use in your day to day life is sharing information with some other system. In this chapter we are going to start out by looking at some examples of Web services to get a better understanding of what they are at a high level. From there we will look at some terms that you might come across and we will dive into consuming a Web Service using the Fetch API.

By the end of this chapter you should have an understanding of:

* What are Web Services
* Examples of Web Services
    * Mobile Applications
    * Every day examples
* APIs
    * What is an API 
    * Web Services vs API
* Asynchronous Programming
    * What is Asynchronous Programming
    * Asynchronous vs Synchronous
* Consuming Web Services in JavaScript
    * The Fetch API
        * How to use the Fetch API
        * Examples
    * Promises
    * Browser Compatibility