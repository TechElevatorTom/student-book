# Why Web Services

As you can see from just a few examples Web Services play a vital role in the things we use every single day. This becomes very important for developers as we will often be tasked with sharing functionality between systems. 

It becomes more important today to build APIs because of all the different devices that can be using your applications. If you wanted to build an application that can be used on web and mobile you might build 2 different user interfaces but you wouldn't want to replicate the back end. 

In the case of the web you could build a Single Page Application (SPA) using a framework such as [Angular](https://angular.io/), [React](https://reactjs.org/) or [Vue](https://vuejs.org/). 

![JavaScript Frameworks](resources/images/javascript_frameworks.png)

In the case of a mobile application you could build a native application using [iOS](https://developer.apple.com/ios/) or [Android](https://developer.android.com/) or build something cross platform using [React Native](https://facebook.github.io/react-native/), [NativeScript](https://www.nativescript.org/) or [Flutter](https://flutter.io/). 

![Mobile Development](resources/images/mobile_icons.png)

These are just the views into your application and all of these views need to get their data and business logic from somewhere and that is where your API comes into play. 