# APIs

An API (Application Programming Interface) is a set of features and rules that exist inside a software program (the application) enabling interaction with it through software - as opposed to a human user interface. The API can be seen as a simple contract (the interface) between the application offering it and other items, such as third party software or hardware.

*Source: https://developer.mozilla.org/en-US/docs/Glossary/API*

## Web Services vs API

All web services are an API but not all APIs are Web services. We can create an API that we don't expose to other developers via a Web Service. 

## Creating a Web Service

When we talk about creating a Web Service it’s us actually creating something new for someone else to use, or consume. We are going to create a class and expose that class to the outside world via an endpoint. We will create our own services later on so this will make a little more sense when we get into that. 

## Consuming a Web Service

When we say we want to consume a Web Service this is just a way of saying that we want to use it. 