# Advanced Data Binding

We've learned how to create simple Vue-based JavaScript Components. But they didn't seem very useful. What about taking data from the user or doing calculations based in user input?

This lesson will look at more advanced features of Vue and how to make more useful JavaScript Components.