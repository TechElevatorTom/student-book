# Computed Properties

You've seen how you can have data properties in your component, but what about more complex pieces of information? How can you bind derived or calculated information to the view?

For example, if you had a component that was keeping track of student scores, you might have a data property set up like this:

```JavaScript
export default {
    name: "student-scores",
    data() {
        return {
            firstName: 'John',
            lastName: 'Mueller',
            scores: [ 90, 80, 70 ]
        }
    }
}
```

There might be a feature that asks you to show the average of the scores. You could calculate that and save it in the data:

```JavaScript
export default {
    name: "student-scores",
    data() {
        return {
            firstName: 'John',
            lastName: 'Mueller',
            scores: [ 90, 80, 70 ],
            scoreAverage: 80
        }
    }
}
```

But then you'll run into a couple of problems:

1. What happens when you add another score? How will you remember to keep updating the `scoreAverage`?
2. You're duplicating data in `scoreAverage`. You already have all the scores, why are you storing that information again as an average.

So you could pull this out as a calculation of the raw data instead of a piece of data itself. This is a common issue to run into, so Vue has a way to do it built into the framework. They call them "computed properties".

## Adding a Computed Property

Computed properties are properties that can be used just like data properties, but are actually calculations instead of a static value.

You can add a computed property to your component in another section parallel to the `data` section, like so:

```JavaScript
export default {
    name: "student-scores",
    data() {
        return {
            firstName: 'John',
            lastName: 'Mueller',
            scores: [ 90, 80, 70 ]
        }
    },
    computed: {

    }
}
```

Computed properties are added as methods in the component code, and then used just like data properties. So to add a new `scoreAverage` property, you'll add a `scoreAverage()` function to `computed`.

```JavaScript
export default {
    name: "student-scores",
    data() {
        return {
            firstName: 'John',
            lastName: 'Mueller',
            scores: [ 90, 80, 70 ]
        }
    },
    computed: {
        scoreAverage() {
            if (this.scores.length === 0) {
                return 0;
            } else {
                return this.scores.reduce((sum, score) => {
                    return sum + score;
                }) / this.scores.length;
            }
        }
    }
}
```

You can now use the computed property just like you would a data property:

```HTML
<span>Your average is: {{ scoreAverage }}</span>
```

Not only that, Vue will also keep this value updated for you. Whenever `scores` changes in any way--an element is added, an element is removed, or an element is changed--Vue will handle updating `scoreAverage` as well.