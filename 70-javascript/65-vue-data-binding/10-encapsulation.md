# Encapsulation

Before we dive back into components, let's talk about encapsulation.

> #### Info::Encapsulation
>
> A language construct that facilitates the bundling of data with the methods (or other functions) operating on that data.

We know that JavaScript is made up of variables and functions that work on those variables, but we've been looking at things on the level of individual variables and individual functions.

Now that we're looking at components, we're going to start thinking in terms of what the component has and what the component can do. Our components will be generally made up of these two pieces; data properties that make up what is has and methods that make up what it can do.

For instance, we will be looking at a component that models an order form. That component will be made up of data--the customer's first name, last name, address, etc.--and methods--verifying that the user filled in all data fields before submitting, showing the proper credit card type based on the credit card number, etc.--that work on that data. The component will contain that data and the methods that act on that data, which is encapsulation. In fact, we would say that the order form component encapsulates the order form's functionality.

Keeping data with the methods that work on that data together as one unit is extremely important in programming and we'll see encapsulation pop up again when we get to Object Oriented Programming.