# Exploring Data Binding

One of the fundamental concepts to understand about component-based development is the idea of Data Binding. In traditional JavaScript, most DOM manipulation is done using the basic functions defined for `document` and `HTMLElement`s. A large number of lines of code are dedicated to just DOM manipulation in most JavaScript applications.

In all the various component-based frameworks, DOM manipulation is handled by the framework itself in the form of Data Binding. Data Binding is the method of linking a data property to a specific spot in the component's view.

In your `first-component` example, we did this with our header. To display a data property as text, we surround it with double curly brackets:

```HTML
<h1>{{message}}</h1>
```

The above code binds the message attribute of the component to the text inside the `<h1>` tag. What shows up in that area? Whatever the value is that has been set to the message data property:

```JavaScript
data() {
    return {
        message: 'Our message from the first component'
    }
}
```

## Data Binding Example

In this example, let's say that you have a new user management component. In the component, you add a new data function that returns an object containing all of the beginning data that the component will start with.

```JavaScript
export default {
    name: "user-management",
    data() {
        return {
            firstName: '',
            lastName: '',
            gender: '',
            disabled: false,
            permissions: [],
            role: '',
            notes: ''
        }
    }
}
```

This object lists all the data properties and their default values. All the data properties that your component has will be defined in this object.

> #### Why do the data properties need to be returned from a function and not just defined as an object?
>
> We have to have a function that returns an object that contains the data properties because if there are two of a component mounted onto the same page, we want each component to have its own set of data properties. If we just have an object defined here, those data properties would be considered shared between both components. By having it be a function that creates the data properties, each component will call that function and get separate data properties.

Now that you have your data properties, you can bind them to the view.

## What is Data Binding?

Data Binding in the above example means that whatever the value of the `message` data property is set to, that's what will show up in the `<h1>`. If the value is ever changed, the view will automatically update with the new value. The data property `message` and the `{{message}}` are bound together and will stay synced.

Using the `{{ dataAttribute }}` syntax is called **one-way data binding**. Data from the data property is bound to the view.

Let's do that for your data properties.

Let's imagine your component has a table at the very bottom that will show what data was entered in the form.

You can do that with the data binding syntax from above. In each table cell, let's bind the display value of the cell to the data property.

```HTML
<tbody>
    <tr>
        <td>{{ firstName }}</td>
        <td>{{ lastName }}</td>
        <td>{{ gender }}</td>
        <td>{{ role }}</td>
        <td>{{ disabled }}</td>
        <td>{{ permissions }}</td>
        <!-- In order to see multiline text from the textarea, wrap in pre -->
        <td><pre>{{ notes }}</pre></td>
    </tr>
</tbody>
```

These table cells are now data bound to the values in the component's data properties. When the data properties' values update, these table cells will show the new value.

## Data Binding with Input Elements

### `v-model` on Input Fields

You can also bind a data property to an input element using an attribute on the HTML elements called `v-model`. This style of data binding can be considered **two-way data binding**. You can do that with text input elements with the `v-model` syntax:

``` HTML
<input type="text" v-model="name"/>
```

This will tie the data property `name` to the value from this text field. If the value of `name` changes, the value in the input field will change. If the user types a new value in the text field, the value of the data property `name` will update to match that value. The data is bound in both directions.

Let's do that now with the First Name and Last Name fields.

```HTML
<input type="text" name="firstName" id="firstName" v-model="firstName">
...
<input type="text" name="lastName" id="lastName" v-model="lastName">
```

By adding a `v-model` attribute to the input elements, you can bind these input fields to the data properties specified.

In fact, you have a chain of bindings now that all work together to keep the UI updated and dynamic. Whenever the value in the input element changes, the value in the data property is automatically updated. When that value updates, the one-way binding in the table cell automatically updates too.

It's important to also note that if the data property value ever changes via some other method (like a function call or API call), then the text in the input box will automatically update to the new value also because the input box is two-way bound to the data property when we use `v-model`.

### `v-model` on radio buttons

You can also bind the radio buttons to a data property with `v-model`. Just bind all the radio buttons to the same data property:

```HTML
<input type="radio" name="gender" value="m" v-model="gender">
...
<input type="radio" name="gender" value="f" v-model="gender">
...
<input type="radio" name="gender" value="o" v-model="gender">
```

Now when a radio button is selected, the data property `gender` is updated to the value of that button.

### `v-model` on selects

To add a data binding to a select element, just add `v-model` on the `select` tag with the data property you want to bind to.

```HTML
<select name="role" v-model="role">
```

This will update the data property with the value from the `option` tag when a new option is selected.

> Vue states that it's good practice to make the first option disabled with a value of the empty string. If you don't, some mobile browsers won't allow a selection to be made by the user.

### `v-model` on single checkbox

You can add a model on a single checkbox to track if it is checked or not with a `true`/`false` value. Just don't give the checkbox a value and bind it to a data property with `v-model`.

``` HTML
<input type="checkbox" name="disabled" id="disabled" v-model="disabled">
```

If the box is checked, the data property `disabled` will be true. If it's unchecked, `disabled` will be false.

### `v-model` on multiple checkboxes

You can also use multiple checkboxes with values to bind to an array of values. Give each checkbox the same name but unique values. Then add `v-model` binding them to the same data property array.

You'll notice that we set up the `permissions` data property to default to an empty array. Let's bind that to the checkboxes for permissions.

``` HTML
<input type="checkbox" name="permissions" value="w" v-model="permissions">
...
<input type="checkbox" name="permissions" value="x" v-model="permissions">
...
<input type="checkbox" name="permissions" value="d" v-model="permissions">
```

As the boxes are checked and unchecked, they will automatically update the permissions data property to be an array filled with the checked values. That means that checking the `"w"` and `"d"` checkboxes will set the permissions data property to `["w", "d"]`.

It's very important that we initialize this data property as an empty array. If we didn't, Vue would have treated the value as a boolean and would have checked and unchecked all the boxes in the list. By initializing the value as an array, we told Vue that we want to treat the checkboxes as individual values that are part of a like group.

### `v-model` on textareas

Textareas work much like input text boxes. Let's link the `notes` data property to the Other Notes textarea.

``` HTML
<textarea name="notes" cols="60" rows="10" v-model="notes"></textarea>
```

Just like with a text box, edits to the textarea can be seen immediately in the table cell.

## `v-model` modifiers

There are some modifiers that we can add to `v-model` too.

### .lazy

If we add .lazy to a field, the data property won't be updated as the user is typing, but will instead wait until the user has left the text field before updating. You can add this to the textarea like this.

``` HTML
<textarea name="notes" cols="60" rows="10" v-model.lazy="notes"></textarea>
```

Now the table cell won't update until you click out of the textarea.

### .number

You can add `.number` to `v-model` to tell it to save the value as a JavaScript number instead of as a string. This is useful for number input elements or anything that you know you'll want as a number and don't want to convert yourself. An example of this would be:

```HTML
<input type="number" name="age" id="age" v-model.number="age">
```

### .trim

You can remove any beginning or ending whitespace that a user might have typed in by putting `.trim` on `v-model`. You can add that to the `firstName` and `lastNames` text boxes.

``` HTML
<input type="text" name="firstName" id="firstName"
    v-model.trim="firstName">
...
<input type="text" name="lastName" id="lastName"
    v-model.trim="lastName">
```