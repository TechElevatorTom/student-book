# Using `v-if` and `v-show` for Conditional Display

In many JavaScript applications, you want to show or hide a section of the screen based on data the user has or hasn't input. Many times in plain JavaScript, this is accomplished with the `display:` CSS class.

We'll be taking a look at further topics using a new component project that models an order form.

## Conditionally Displaying Elements with `v-if`

Vue has a keyword for this already built into the framework. We can add a `v-if` attribute to any element in the template to get this effect. The value of `v-if` is tied to a data attribute or a JavaScript expression and tells the component whether to show the HTML element or not.

You can add this to you component to decide whether to show or not show the billing address. If the checkbox that's labeled "Also use for billing address" is checked, you would want to hide the billing address fields. If it's not checked, you would want to show the billing address fields.

That checkbox is data bound to the `sameAddress` data attribute, so you can look there to see whether or not you want to show the fieldset for the billing address. So let's add a `v-if` to that fieldset:

``` HTML
<fieldset v-if="! sameAddress">
    <legend>Billing Address</legend>
```

`v-if` takes a JavaScript expression stating that if the `sameAddress` data attribute is *not* checked, then show the Billing Address fieldset. If you inspect the DOM after checking this button, you'll see that that fieldset isn't hidden or set invisible, it is completely removed from the DOM.

## Hiding Elements using `v-show`

If you don't want the elements to be removed, but just want them to be hidden but still on the page, you can use `v-show` instead. It works exactly the same way, but will toggle the CSS display of the element rather than remove it from the page.

``` HTML
<fieldset v-show="! sameAddress">
    <legend>Billing Address</legend>
```

## What's the Difference?

So why would you use `v-if` versus `v-show`? Since `v-if` will remove things from the DOM, it is useful to use when you really want things off of the page.

For example, a form field hidden by CSS is still sent when a form is submitted. If you don't want the form field to be submitted when it is hidden, you'll want to use a `v-if` to show and hide it. Then the form field will be removed from the DOM when hidden and readded to the DOM when shown.

Most of the time `v-show` is used more because there are less CPU costs to hiding and showing an element via CSS than completely adding and removing the element and all its children from the DOM. But when you really need the element gone, `v-if` will take care of that for you.