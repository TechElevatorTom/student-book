# Using `v-for` to Render Multiple Elements

There are often times when you have a list of values and you want to render elements based on that list. A table of products, a list of available options for a select, or a list of radio buttons are just some examples.

Vue gives us an easy way to take an array or a JavaScript object and create a series of DOM elements from them using `v-for`.

We're going to do that with an array of credit card types that we can use in the credit card type select box. We may only accept certain credit cards and don't want to hard code that information into our select box's HTML. Instead, we can bind a data property to those options which could be updated dynamically at any time.

We'll start with the data first. So let's create a data property to hold all the available credit card types that will go in the select box. We'll make it an object with key/value pairs.

```JavaScript
expDate: '',
creditLogoSrc: '../assets/credit.png',
availableCardTypes: {
    'visa': 'Visa',
    'mc': 'MasterCard',
    'dc': 'Discover Card'
}
```

We then want to build an option element for every key in that object. We can do that by going to the `creditCardType` select area and adding in a new option tag with the `v-for` attribute included:

```HTML
<select name="creditCardType" id="creditCardType" class="creditCardType" v-model="creditCardType">
    <option value="" disabled>-- Select One --</option>
    <option v-for="(creditCardName, creditCardAbbrev) in availableCardTypes"
        v-bind:value="creditCardAbbrev">{{ creditCardName }}</option>
</select>
```

There's quite a bit going on here, so let's piece it apart.

When using `v-for` on an object, the default action is for the `v-for` to loop through all the values of the properties. So if we had an object that looks like this:

```JavaScript
availableCardTypes: {
    'visa': 'Visa',
    'mc': 'MasterCard',
    'dc': 'Discover Card'
}
```

We can write a `v-for` to loop through those. It would look something like this:

```HTML
v-for="value in availableCardTypes"
```

Using this would pull back the values only.

```
Visa
MasterCard
Discover Card
```

That's because, by default, the `v-for` will only return values and not keys. If you also want the keys, you have to specify that you want the keys stored in a variable for each iteration as well.

```HTML
v-for="(value, key) in availableCardTypes"
```

We ask for a group of variables now, the first one being the value and the _second_ one being the key.

Or, written another way:

```HTML
v-for="(creditCardName, creditCardAbbrev) in availableCardTypes"
```

When this loops through, `creditCardName` will be the value, like `MasterCard`, and `creditCardAbbrev` will be the key, like `mc`.

This will allow us to get both the key and the value of each object property, treating it as a map or dictionary of card types.

For the actual `option` tag, we are getting the `creditCardName` and the `creditCardAbbrev` and then binding the `creditCardAbbrev` to the value of the `option` tag and the `creditCardName` to the display text of the `option` tag.

```JavaScript
<option v-for="(creditCardName, creditCardAbbrev) in availableCardTypes"
    v-bind:value="creditCardAbbrev">{{ creditCardName }}</option>
```

We'll be talking about `v-bind` later, but that's how we bind a value to an HTML element's attributes.

If we add this into the component, we can see that a new option is created for each value in our object. That means that `v-for` duplicates the element it was added to for each entry, in this case the `option` element.

## `v-for` with Arrays

You can also use the `v-for` with a normal array. If we had the following array saved in the `availableCardTypes` data property:

```JavaScript
[
    'Visa',
    'MasterCard',
    'Discover Card'
]
```

Then we could loop through those elements and their indexes with:

```HTML
v-for="(creditCardName, index) in availableCardTypes"
```

Then the second argument is the index of that element in the array, starting at zero.

We don't have an example of this in our component, but it's a good thing to remember.

## Binding a Unique Key for Each Object

One thing you will notice after putting this code into your component is that VSCode is now giving an error saying that you should have a 'v-bind:key' directive. `v-bind:key` is a way for Vue to keep track of which object goes with which DOM element. That way, if the data property is ever changed, Vue will know exactly which DOM element to update and won't need to rerender every DOM element over again. It lets Vue handle the DOM more efficiently than it otherwise would.

To do that, we just want to add another attribute to our `option` tag. The "key" must be a value that is unique to each object in the list, like a primary key or--in our example--the card type abbreviation.

```HTML
<option v-for="(creditCardName, creditCardAbbrev) in availableCardTypes"
    v-bind:value="creditCardAbbrev" v-bind:key="creditCardAbbrev">{{ creditCardName }}</option>
```

This looks like the `v-bind:value` that we added before, but `v-bind:key` is only used internally to Vue and doesn't add a new attribute to the `option` tag. Adding the `v-bind:key` attribute will remove the error, and make our application more efficient.

## Computed Properties and Filtered Lists

Let's do the same for states. We will handle the states a little differently since we only want to ship to certain states and not all of them. States are going to be an array of objects that will hold information on the state's display name, the state's abbreviation and whether we will ship to that state or not.

Since it's an array of states, we won't have keys and don't have any use for the indexes, so we will really only need the value.

The states are saved in the `availableStates` data property. We can use that in the billing address section to populate the state select box. Even though we have an array this time, we can still use the `v-for` syntax to loop through all the states. Then we can use that state object to create our option tag.

```HTML
<option v-for="stateObject in availableStates"
    v-bind:value="stateObject.abbreviation" v-bind:key="stateObject.abbreviation">
    {{stateObject.name}}
</option>
```

Since this is the billing state select box and we will happily accept money from any state in the union, that's all we need to do. You should see all the entries from the `availableStates` array are now options in the dropdown.

For `shippingState`, we only want to list the states that have their `canShip` property set to true. We can't remove the states from the data property for `availableStates` because that will also change the list for `billingState`. We could copy and paste the list, but that's just sounds awful.

Vue, however, gives us a way of making a property in our component that can be calculated from another data property. You could think of it almost like a view of that data. If that data property is ever updated, the computed properties based off of it will also update.

Much like we have our `data` properties, we can also have `computed` properties. We can add a section for this in our component right below the `name`.

We'll create a function in the `computed` section of our component that filters the `availableStates` list so that is only contains states where the `canShip` property is true.

```JavaScript
name: 'order-form',
computed: {
    shippingStates() {
        return this.availableStates.filter( (state) => {
            return state.canShip;
        });
    }
},
```

We can now use `shippingStates` as if it were a data property. That includes using it in creating our options for `shippingState`.

```HTML
<option v-for="stateObject in shippingStates"
    v-bind:value="stateObject.abbreviation" v-bind:key="stateObject.abbreviation">{{stateObject.name}}</option>
```

We could have just filtered the states in the `v-for` ourselves, but by using `computed`, Vue will cache the computed value so that it doesn't need to be calculated every time and will automatically update `shippingStates` if `availableStates` ever changes.
