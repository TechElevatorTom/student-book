# Introduction to the DOM

DOM stands for the Document Object Model and it is the browser's internal representation of the structure of the current web page. This chapter will discuss the DOM, what it is, and how to change it using JavaScript to make our pages more interesting and dynamic.