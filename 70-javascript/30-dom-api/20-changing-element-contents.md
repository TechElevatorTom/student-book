# Changing Elements

Once you've selected an element, you can change it by accessing its properties.

## Changing text with `innerText`

You can change the text inside of an element using the element's `innerText` property.

```javascript
// Get the first list item
let firstListItem = document.querySelector('#todos li');

// Change it to say 'Update Documentation'
firstListItem.innerText = 'Update Documentation';
```

[//]: # (TODO: if this example of innerText overwriting child elements is necessary, there should probably be some image depicting the before and after)

`innerText` gives you the ability to update any text information on the page. If you set `innerText` on an element that has child elements, like our `ul`, those child elements will be destroyed.

```javascript
// Get the list
let todoList = document.getElementById('todos');

// Change list to say 'Update Documentation',
// destroying all line items in the process
todoList.innerText = 'Update Documentation';
```

[//]: # (TODO: will we have talked about HTML entity encoding at this point? If so, it would be better to explain that "special" characters are encoded by innerText)

`innerText` will also put the literal text that you type in into the DOM element. Any HTML added to the text will print to the user and not render as DOM elements.

```javascript
// Get the list
let todoList = document.getElementById('todos');

// User will see '<li>Update Documentation</li>',
// and will not create a new line item
todoList.innerText = '<li>Update Documentation</li>';
```

## Changing HTML with `innerHTML`

[//]: # (TODO: see the previous comment about encoding)

`innerHTML` acts a lot like `innerText`, but *will* render any HTML added as DOM elements.

```javascript
// Get the list
let todoList = document.getElementById('todos');

// User will see 'Update Documentation'
// within a newly created list item
todoList.innerHTML = '<li>Update Documentation</li>';
```

> #### Caution::Using innerHTML could get you hacked!
>
> Anything passed to innerHTML will be read and rendered into the living DOM of the browser. That could be really dangerous! If you ever take input from a user and then use `innerHTML` to put that into an element, you're setting yourself up for what is called a Cross Site Scripting Attack (XSS).
>
> If a user is able to add HTML to your page, that means that they can embed JavaScript into your page using a `<script>` element and *that* means they can use all these methods to completely rewrite your page, including making it look like a login page that sends usernames and passwords to their own site instead of yours.
>
> Never, ever send user inputted data to an `innerHTML` call. When taking user input, always use `innerText` to add their content to a DOM element.


## Getting and setting input element values with `value` and `checked`

You can get the current value of an input element with its `value` property:

```javascript
// Get an input element
let todoInput = document.querySelector('input[name=newTodo]');

// Get the text the user typed into the input field
let newTodoText = todoInput.value;
```

You can also set the value of an input field by assigning a new value to the `value` property:

```javascript
// Empty the text field back out
todoInput.value = '';
```

> #### Warning::Some select boxes have multiple values
>
> While this also works for `select` elements, it is possible for `select` inputs to be configured to have more than one value selected. `value` will only give you back the _first_ selected value. To get them all, access the `selectedOptions` property instead.

For radio and checkbox elements, you can get their status with `checked`:

```javascript
let finishedCheckbox = document.querySelector('input[name=isFinished]');

let isFinished = finishedCheckbox.checked;
```

`checked` will return a boolean, true for checked and false for unchecked.

[//]: # (TODO: this warning is probably useless here since the students won't yet understand what it's saying. This might be something better to point out in the section on Event Handling)

> #### Warning::Setting `value`s and `checked` won't trigger events
>
> We haven't talked about events yet, but you should still be aware that setting values on fields programmatically in JavaScript might bypass event handling code that you want to trigger when a value changes. We'll be talking more about that later and how to get events to trigger after you set a value.
>
> Just keep in mind that setting values in JavaScript can have this effect.

## Adding a removing classes with `classList`

All elements have a variable called `classList` that allows you to see, add, and remove classes from the element. This is the main way that you will change how something looks on the page using JavaScript.

```javascript
// Get the first line item
let firstListItem = document.querySelector('#todos li');

// Add the class `done`
firstListItem.classList.add('done');

// Remove the class `priority`
firstListItem.classList.remove('priority');
```

## Creating new DOM elements with `createElement()`

Instead of supplying HTML to `innerHTML` to create DOM elements, you can create DOM elements directly using `createElement()`.

```javascript
let newListItem = document.createElement('li');
newListItem.innerText = 'Update Documentation';
```

`createElement()` will create a new DOM element and return it. You can then call all the normal functions on that element that you can call on any DOM element, like `innerText`.

This element is not yet on the page however and won't show up to the user until you insert it into the living DOM.

## `insertAdjacentElement()`

We can add new DOM nodes to the end of our list with `insertAdjacentElement('beforeend')`:

```javascript
let newListItem = document.createElement('li');
newListItem.innerText = 'Update Documentation';

let todoList = document.getElementById('todos');
todoList.insertAdjacentElement('beforeend', newListItem);
```

This will add the element as the last child on the element with the id of `'todos'` (before the list ends).

To prepend the element to the list, we can use `insertAdjacentElement('afterbegin')`:

```javascript
let newListItem = document.createElement('li');
newListItem.innerText = 'Update Documentation';

let todoList = document.getElementById('todos');
todoList.insertAdjacentElement('afterbegin', newListItem);
```

This will put the element as the first child of the list (after the list begins).

There are two other position identifiers, `'beforebegin'` and `'afterend'`.

```javascript
let newHeader = document.createElement('h2');
newHeader.innerText = 'Todo List';

let newFooter = document.createElement('h2');
newFooter.innerText = 'End of List';

let todoList = document.getElementById('todos');

// Place the Header before the list
todoList.insertAdjacentElement('beforebegin', newHeader);

// Place the Footer after the list
todoList.insertAdjacentElement('afterend', newFooter);
```

This one function gives a lot of power in where we want to put new DOM elements into our page. For reference:

| Location       | Meaning              |
|----------------|----------------------|
| beforebegin    | Put the element before this one |
| afterbegin     | Put the element inside this one at the top |
| beforeend      | Put the element inside this one at the bottom |
| afterend       | Put the element after this one |

## Traversing the DOM

There may be times when you don't know the exact selector to choose an element with or you need to work with a specific child of an element and need to loop through or walk through a list of elements. For this, we can use some element properties to get an element's children or parent.

### Selecting children with `children` and `childNodes`

If we select an element:

```javascript
let todoList = document.getElementById('todos');
```

We can get all of its immediate children *elements* through the `children` property:

```javascript
let todoItems = todoList.children;
```

`children` will return a `HTMLCollection` object, which you can turn into a real array with access to `map`, `forEach`, and all the other array functions with:

```javascript
let todoItemsArray = Array.from(todoList.children);
```

You can also get children by calling `childNodes`:

```javascript
let todoNodes = todoList.childNodes;
```

This will return a `NodeList` object that will contain all the *nodes* inside that element. You can also pass this to `Array.from()` to get a normal JavaScript array.

> #### Note::What's the difference between `children` and `childNodes`?
>
> So what is the difference between `children` and `childNodes`?
>
> `children` will return **elements** that are children of this element. That means that it will only contain other HTML elements and *not* the text that might be in the element.
>
> `childNodes` will return **nodes** that are children of this element. That *includes* text (including whitespace) and comments that are in the DOM.
>
> For an example, if we had the following DOM structure:
>
> ```html
> <p id="message">
>   This is an <strong>awesome</strong> paragraph.
>   <!-- with a comment -->
> </p>
> ```
>
> Calling `children` on the `p` element will return us just one child, the `strong` element. `children` only returns the children DOM elements and there is technically only one inside the `p`, and that's the `strong` element.
>
> Calling `childNodes` on the `p` element will return five children! The first is a text node containing 'This is an ', the next is a strong element, the next is another text node containing ' paragraph.', then a comment node holding 'with a comment' and finally a text node that holds the newline after the comment.
>
> Which one do you want to use? Depends on what you want to do, but at least you now know how they work.

## Getting an element's `parentNode`

You can access an element's parent using the `parentNode` attribute.

```javascript
let todoList = document.getElementById('todos');
let todoListsParent = todoList.parentNode;
```

## Getting adjacent elements with `nextElementSibling` and `previousElementSibling`

You can select the next and previous sibling--an element that has the same parent--by using the `nextElementSibling` and `previousElementSibling` properties.

```javascript
// Get the first line item of the todo list
let firstTodo = document.querySelector('#todos li');

// Get the next line item
let secondTodo = firstTodo.nextElementSibling;
let thirdTodo = secondTodo.nextElementSibling;
```

## Removing Elements with `removeChild`

You can remove elements with the `removeChild` function.

```javascript
// Get the todo list
let todoList = document.getElementById('todos');

// Remove the first item
todoList.removeChild( todoList.children[0] );
```

Since DOM elements must be removed by calling `removeChild()` on its parent, if you have a handle on the item you want to remove, you can remove it with:

```javascript
// Get the first line item of the todo list
let firstTodo = document.querySelector('#todos li');

// Remove it, using its parent property
firstTodo.parentNode.removeChild(firstTodo);
```

## More Reading

You can learn more about manipulating the DOM at [MDN's Manipulating documents](https://developer.mozilla.org/en-US/docs/Learn/JavaScript/Client-side_web_APIs/Manipulating_documents) page.