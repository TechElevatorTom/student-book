# What is the DOM?

The Document Object Model (DOM) is an internal data structure that browsers use to represent the structure and content of a web page. When the browser loads an HTML document, it needs to translate that into something that it can use to draw a graphical representation of the page.

For instance, we might write the following HTML:

```html
<p>
    This is a paragraph. And it has <strong>bold</strong>
    elements and <em>emphasized</em> elements.
</p>
```

Rendered by a web browser, it would look like this:

![Rendered HTML](img/rendered-dom.png)

## HTML -> DOM

When the browser reads in HTML from a website, it needs to turn that HTML text into an internal representation of the structure that it can work with dynamically. That representation is the DOM, or the Document Object Model.

> #### Info::Document Object Model
>
> The internal, in-memory representation of a web page's structure. Typically stored in RAM as a nested tree of objects that represent the elements of the page.

Understanding the DOM is important. HTML is static. It's only read once when the page loads and then it's converted into a DOM. CSS and JavaScript run against this living DOM and not the static HTML document that you wrote.

![HTML to DOM to Graphical View](img/html-to-dom-to-page.png)

This can be most clearly seen when creating a table.

Most of us write the HTML for a table like this:

```html
<table class="table">
    <tr>
        <td>Tech Elevator</td>
        <td>7100 Euclid Ave.</td>
    </tr>
</table>
```

The browser will read in this HTML and show you exactly what you would expect, a table with one row and two cells.

But if you right click on the page and Inspect Element, you'll see that the DOM looks a little different:

![DOM for a table](img/dom-of-table.png)

You'll notice that there's a `tbody` element in the DOM that doesn't appear in the HTML code. Where did it come from?

It was added by the browser while parsing the HTML code into its DOM representation. All tables have a `tbody`, but it's not required to be included in the HTML code. Browsers will assume that the content should appear in a `tbody` even if you don't specify one. The browser will sometimes do something similar to fix bad HTML. If you forget to close a tag in your HTML, the browser will make a best guess about closing it and change the DOM to fit that best guess.

The above also shows another issue. If we write the following CSS:

```css
table > tr > td {
  background-color: red;
}
```

Nothing happens. The style is not applied to the `td` elements. Based on the HTML code, we would expect this selector to apply to the `td` elements, but **CSS doesn't run on the HTML**, it runs on the *DOM*. And the DOM has a `tbody` element in between the `table` and the `tr`. So, to apply a style to the `td` elements, we could use the following CSS:

```css
table > tbody > tr > td {
  background-color: red;
}
```

## Checking the DOM

So, if the DOM doesn't always match the HTML source code, how can we reliably interact with it using CSS and JavaScript? Fortunately, you can view the DOM in the browser.

To do that, just open the developer tools in Firefox or Chrome and go to the Inspector (Firefox) or the Elements (Chrome) tab. That is a direct HTML-like view of the DOM as it is at that moment. If the DOM changes via JavaScript or CSS, you'll see those changes happen immediately in that view.

This is the best way to understand how your browser is interpreting the HTML source code and how it changes due to live user interaction.

> #### Notice::Always check the browser!
>
> Whenever you are having problems getting a page to behave properly or display correctly, always look at the browser and its DOM representation. Too many new developers go back to their HTML or JavaScript source code to try and figure out what's going on, but the browser is where your code is actually running, so always check there first.