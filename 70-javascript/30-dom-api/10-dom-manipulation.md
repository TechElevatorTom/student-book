# DOM Manipulation with JavaScript

[//]: # (TODO: a practical example of manipulating a page would be good here to illustrate the "adding things, moving things..." stuff)

To create a truly dynamic web application, we'll need to know how to change the DOM after our page has been loaded. Doing things like adding and showing new elements, hiding or removing existing elements, and moving elements around the page will help us create dynamic user experiences. 

And we can do that using a lot of the existing JavaScript knowledge that we've already learned.

## JavaScript and the DOM

JavaScript has many built-in functions that can be used to manipulate the DOM. Below is a list of the most commonly used functions.

> #### Info::I keep hearing about jQuery. What is it?
>
> As you look online at how to do certain things with the DOM, you might run into a library called jQuery that a lot of people are using.
>
> In the early days of the web, browsers implemented their own APIs for working with the DOM, but they were all different. In order to write a JavaScript application that worked in all browsers, you had to use a bunch of `if` statements to figure out which version of a function to call. It was a nightmare and everyone stayed away from JavaScript because of it.
>
> In 2006, a library was released called jQuery that sought to fix that. jQuery gave a standard set of functions that you could call to do DOM manipulation and then *it* would take care of browser compatibility. It was game changing and nearly every website adopted it to add dynamic functionality.
>
> In the years since, browser developers have prioritized cross-browser compatibilty and there aren't nearly as many differences between browsers from a programmer's perspective. jQuery isn't needed near as much as it used to be, *but* a lot of people cut their teeth on it and know it inside and out. That means that a lot of questions get answered as if everyone is using jQuery. If you ask a question online about how to do something in JavaScript, you will almost always get an answer about how to do it with jQuery.
>
> We won't be using jQuery. There is big demand out there now for people that don't use jQuery as a JavaScript crutch and instead know the pure JavaScript way of doing it. In fact, this has a name: Vanilla JavaScript. If you do a search for "Vanilla JavaScript" when searching for a problem, you're much more likely to find a relevant solution.
>
> Want to know the differences between the two? [You Might Not Need jQuery](http://youmightnotneedjquery.com/#add_class) is a good resource to check out.

Now let's look at what functions we can use to manipulate the DOM.