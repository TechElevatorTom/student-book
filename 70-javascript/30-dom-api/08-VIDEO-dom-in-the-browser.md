# VIDEO: Inspecting the DOM in the Browser

This video will show how to inspect the DOM as the browser sees it.

{% video %}https://www.youtube.com/watch?v=r0IIW4-11T4{% endvideo %}