# DOM Selection Functions

## `getElementById()`

The first function we'll look at is `getElementById()`. This function will get a single HTMLElement from the DOM and return a reference to it.

If you had the following HTML:

```html
<h1 id="title">Welcome</h1>
```

Then you could select that from the created DOM with:

```javascript
let titleElement = document.getElementById('title');
```

> #### Warning::IDs on elements should always be unique
>
> If you haven't been doing it already, you should be making all ids unique in your code. There should never be more than one element on a page with the same id. This will be even more important as we start to use more functions in JavaScript that rely on element ids.
>
> In the event that there is more than element on the page that has the same id, the `getElementById()` function will return the first element it finds with the id requested.

## `querySelector()`

`querySelector()` is used to select single elements that don't have ids. `querySelector()` takes a standard CSS selector and returns the first element it finds that matches that selector.

[//]: # (TODO: a more straight-forward example would be better. using a "descendant combinator" selector is uneccessarily complex and using an exmaple that returns the first of multiple possible matches just muddies the waters.)

For instance, if we have the following HTML:

```html
<ul id="todos">
    <li>Walk the dog</li>
    <li>Mow the lawn</li>
    <li>Go shopping</li>
</ul>
```

Then you can select the *first* list item by calling:

```javascript
let firstListItem = document.querySelector('#todos li');
```

You can also call `querySelector()` on any element and it will search within that element. That means you could do the same thing with:

```javascript
let todoList = document.getElementById('todos');
let firstListItem = todoList.querySelector('li');
```

> #### Info::Couldn't I just use `querySelector('#id')` instead of `getElementById('id')`?
>
> Short answer: Yes, you can.
>
> Long answer: If you use `querySelector()` to select on an id, it is much, much slower than just calling `getElementById()`. If you just need to get a single element and it has an id, use `getElementById()`. Use `querySelector()` for selecting single elements without ids.

## `querySelectorAll()`

If we want to get *all* the list items above, we can use `querySelectorAll()` instead. This will return a `NodeList` of all the elements, which you can use as an array.

[//]: # (TODO: it might be useful to demonstrate looping over the returned items to drive home that it's an array)

```javascript
let allListItems = document.querySelectorAll('#todos li');
```

Again, you can also select inside of another element:

```javascript
let todoList = document.getElementById('todos');
let allListItems = todoList.querySelectorAll('li');
```