# VIDEOS: DOM Manipulation Examples

These videos will show how to manipulate the DOM in ways described in the last chapter.

## Changing Text

{% video %}https://www.youtube.com/watch?v=9s5YPhmyNFg{% endvideo %}

## Changing Text Input Elements

{% video %}https://www.youtube.com/watch?v=H075i8WqCrw{% endvideo %}

## Changing Check and Radio Boxes

{% video %}https://www.youtube.com/watch?v=wT5g912GKTY{% endvideo %}