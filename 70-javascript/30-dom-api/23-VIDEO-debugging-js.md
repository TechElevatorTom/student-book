# VIDEO: Debugging JavaScript

In this tutorial you will learn how to debug JavaScript applications using the Chrome Devtools.

{% video %}https://www.youtube.com/watch?v=Qxp0nH07FkQ{% endvideo %}
