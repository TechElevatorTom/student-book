# Unit Testing with the DOM

Now that you know how to unit test your code, you should be able to unit test code that affects the DOM as well. Unit testing code that interacts with the DOM is generally the same as unit testing a function. You'll still arrange your test environment, run the function to act and then assert that the function did what is was supposed to.

But there are some important differences:

- DOM manipulation functions don't generally return anything. Instead of asserting that you're getting the correct value back from the function, you're asserting that the DOM changed in the way you expected.
- You'll need a "fake DOM" or fixture in your test code. Since the tests don't, and shouldn't, run against your actual UI code, you'll need to set up a small simulation of it in your test code. It is best to keep this simulation limited to only the DOM elements you absolutely need to run your tests and to create it using a `beforeEach` function and remove it using an `afterEach` function so that each test has a clean environment to work in.
- You will be doing a lot of DOM manipulation in the tests themselves. You will need to get elements and values from the DOM to pass to your functions and then check those elements after to make sure things were properly changed.

## Process for Testing DOM Manipulation

Some extra steps should be considered in testing DOM manipulation. Think of the simple Arrange, Act, and Assert process like this:

1. Create a test DOM to run the tests against. This is best done in a `beforeEach` and should be attached to a `div` element in the test harness HTML page. The easiest way to create this is by passing a string with all the HTML into a `insertAdjacentHTML` call on that div. This initial setup should be to just create the basic DOM that you'll need for every test.
2. Arrange the DOM for the beginning of the test. If you're testing a todo list's delete function, you'll have to add a todo list item to the DOM before deleting it or add a class to an item before checking a `toggle` on that class.
3. Arrange any data you will need to test with, just like normal tests.
4. Act. Run your function just like you normally would.
5. Grab the DOM elements that you need to assert against to see if the function worked correctly.
6. Assert the DOM is now how you expect it. Have new elements been created? Have values properly updated? Are the proper classes on your elements? Assert that the things your function changes actually changed in your DOM
7. Tear it all down. Empty our your testing `div` using an `afterEach` so that the next test has a clean DOM to work with.

There can be a lot of code behind all of these tests, so take a look at the `tests.js` file in your beginning-exercises. This should give you a good idea of how to design these tests. You won't have to write these tests right away, but you will eventually need to know how to properly test your DOM manipulation functions.