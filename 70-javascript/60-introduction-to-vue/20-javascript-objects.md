# JavaScript Objects

You have already looked at objects in JavaScript. They are data structures that have keys that relate to values:

``` JavaScript
let classGrades = {
    'Jake': 82,
    'Tyler': 92,
    'Connor': 96
};

let connorsGrade = classGrades.Connor;
```

You can also create more complex structures for JavaScript objects by nesting multiple objects into one structure:

``` JavaScript
let classes = {
    'weekone': {
        'dayone': 'Introduction to Command Line',
        'daytwo': 'Variables and Data Types',
        'daythree': 'Logic and Branching',
        'dayfour': 'Loops'
    },
    'weektwo': {
        'dayone': 'Objects and Classes',
        'daytwo': 'String Methods',
        'daythree': 'More String Methods',
        'dayfour': 'Classes and OOP'
    }
};

let className = classes.weektwo.daytwo; // Will be 'String Methods'
```

This lets you build more and more complex structures for your data, structures that very clearly describe the data that is being held. This makes your code more self documenting. When another programmer sees a line of code with `classes.weektwo.daytwo` in it, they can tell exactly what the value in that variable means.

Also remember that you can put any value into a JavaScript object, not just strings or numbers. And functions are values in JavaScript. That means you can build an object like this one:

``` JavaScript
let rectangle = {
    length: 20,
    width: 34,
    area: function() {
        return this.length * this.width;
    },
    perimeter: function() {
        return (2 * this.length) + (2 * this.width);
    }
};

let totalArea = rectangle.area(); // Will equal 680
rectangle.length = 49;
rectangle.width = 82;

totalArea += rectangle.area(); // Will equal 4698
```

This lets our JavaScript object work much like objects in other languages.

> #### Define:JavaScript Objects
>
> JavaScript objects are associative arrays that contain other data and data structures in order to model, or represent, a certain concept in the application. In the example above, the object would model the concept of a rectangle in a mathematics application. By keeping the data and the functions together in one *thing*, you can group together everything you need to represent a rectangle in the system.

This idea of grouping together the data--represented as variables--and behavior--represented as functions--for a certain concept in one place in your code is called Encapsulation.

> #### Define:Encapsulation
>
> Encapsulation is defined as: The bundling of data with the functions that operate on that data. In the example above you have modeled a rectangle that has data--its length and width--bundled with functions that operate on that data--area() and perimeter().

As you will see, when you want to create components in JavaScript--and in Vue--you will be building those components as objects. Those objects will hold the data for that component and the functions that represent actions that the component can use to perform logic and operate on its data.