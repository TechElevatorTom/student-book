# Creating Your First Component

## Creating a Component From Scratch

You're now going to create a simple component. All components that you will be making will be created in their own `.vue` file in the `src/components` directory of your project.

Create a new file in the `components` directory called `FirstComponent.vue`. This is where we will create a simple component and include it into the index page.

First, you'll fill out the content--or the HTML--that you want to show up when the component is rendered.

> #### Note:One Root Element
>
> One key to remember here is that each Vue component must have only one root element. That means that inside the `<template>` tags, there must be an HTML element that holds all the rest of the component's HTML.
>
> Typically that tag is a div.

```HTML
<template>
    <div>
        <h1>{{message}}</h1>
    </div>
</template>
```

This defines your component as a `<div>` tag that contains an `<h1>` tag. When your component is shown, the `<template>` tags themselves won't be included in the display, but everything else that's between them will be shown.

The double curly bracket syntax of `{{message}}` is an example of basic one-way data binding. Data binding is the concept of hooking together a data property in the component to a part of the HTML view. When the value of that property changes, the HTML will automatically update to show the new value. This is an important property of component-based JavaScript that all major frameworks support and you'll be diving into more complex aspects of this tomorrow.

Now that you have our HTML, you can add in some CSS. The order of these sections doesn't really matter, so even though the App component had the CSS last, you can put it second or even first if it makes sense to you.

Add the following CSS:

``` CSS
<style scoped>
    h1 {
        color: blue;
    }
</style>
```

This will set the `<h1>` color to blue, but only the one in the component since you have also `scoped` the CSS to just this component. Scoped means that any CSS you define in this component file will only apply to this component and to nothing else. By defining these styles as scoped you won't be accidentally changing the `h1` style for the entire site.

Now you can add in the Vue JavaScript object that will define the behavior and data for this component:

``` JavaScript
<script>
export default {
    name: 'first-component',
    data() {
        return {
            message: 'Our message from the first component'
        }
    }
}
</script>
```

This section specifies the properties of your component. In this case, you give the component a name and some data properties. The `message:` part of that data function is a data property. This is just a variable that lives in this component. It will contain the message that was data bound in the section above. That means that if the `message` data property value changes, the new message will be shown in the HTML.

As it is now, this means that the HTML section of the component that looks like this:

``` HTML
<h1>{{message}}</h1>
```

Will be show as this to the user:

``` HTML
<h1>Our message from the first component</h1>
```

If you save those three sections into your `FirstComponent.vue` file, that will give you a complete Vue component. Now you'll go into the `App.vue` file and load your new component.

## Loading a Component

In `App.vue`, go to the beginning of the JavaScript section and add in a new import line to import your new component:

``` JavaScript
<script>
import HelloWorld from './components/HelloWorld.vue'
import FirstComponent from './components/FirstComponent.vue'
```

Then add the component to the `components` section of the Vue object:

``` JavaScript
  components: {
    HelloWorld,
    FirstComponent
  }
```

Finally, you add in the tag to the `<template>` section at the top of the `App.vue` component to show your new component on the page.

```HTML
  <div id="app">
    <first-component></first-component>
    <img alt="Vue logo" src="./assets/logo.png">
```

> #### Why do you use the name `first-component`?
>
> The reason the tag is called `first-component` here is because that's the `name` of the component. Whatever you set as the `name` in the component's Vue object is going to be the name of the tag. It's good to make the name a kebab-cased value because, according to the HTML standard, all custom made HTML elements should be all lower case and contain a hyphen to separate them from the standard HTML elements.
>
> As a special note, the auto generated HelloWorld component does *not* follow this standard and should be considered poorly named.

After saving this file, you should see the component show up on your in browser preview. You may need to run `npm run serve` again to start up the live preview.

What you just created in the JavaScript section of the component is commonly called the "Vue object" or a "JavaScript object". JavaScript objects are a very important concept to component-based JavaScript. The next section will take a look at what a JavaScript object is and what JavaScript object are used for.