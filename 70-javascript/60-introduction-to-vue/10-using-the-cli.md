# Using the Command Line to Make a New Project

Before you can fully jump into making a component, you'll need to learn about the tools you will be using.

## Vue.js

The first thing you will need is a framework for building components. JavaScript, by default, does not have a framework for component-based development. In this course, you will be using a popular framework called **Vue.js**.

> #### Note:Component Frameworks
>
> There are other popular component-based Javascript frameworks available. Two of the most popular are Angular and React. Although the syntax and conventions of these frameworks may differ, the terminology and concepts that you'll learn with Vue.js have strong parallels in these other frameworks. So, once you become comfortable using Vue, the learning curve for other component-based Javascript frameworks will be much less steep.

## Installing the Vue.js Tool Chain

The Javascript code you've written so far has been able to be interpreted by a web browser directly. However, Vue.js uses advanced features of the newest versions of Javascript that aren't fully supported by all web browsers yet. Therefore, some steps need to be taken to prepare the source code you write to be run by the browser. Fortunately, like many similar frameworks, Vue provides a "tool chain" of applications that does most of the work for you.

To run this tool chain, you will be using NPM. NPM stands for the "Node Package Manager" and will be used to manage your code's dependencies (i.e. other libraries and tools needed for our project) and to transpile and build our code as well as run our unit tests and development server. You should already have NPM installed as part of your laptop.

> #### Note:What is a transpiler?
>
> Vue components can be written using the latest versions of Javascript. Not all web browsers are fully compatible with all features of these versions, so in order to make sure that Vue components are compatible with any web browser, a program called a "transpiler" or "compiler" (the difference between these terms isn't really important) is used to convert your source code into an older version of Javascript that is more widely compatible with browsers.

## The Vue CLI

The Vue CLI (i.e. Command Line Interface) can be used to simplify some of the grunt work of setting up a project and creating new components. You can check to see if the Vue CLI is already installed on your machine by running the following command:

```shell
vue -V
```

If you don't get back a version number you will need to install it via NPM:

**Windows:**
```shell
npm install -g @vue/cli
```

**Mac:**

```shell
sudo npm install -g @vue/cli
```

This command will install the Vue CLI globally (`-g`) on your machine. The CLI will give you a quick and easy way to get a new Vue project that includes all of the tools you will need.

> Windows Users: There is a known issue with using Git Bash and the Vue CLI. If you're using Git Bash you won't be able to use the up and down arrows to make your selections. There is currently no fix for this and if you want to use the Vue CLI you will need to run it from the Windows Command Prompt.

## Generating a Project

To start a new project, go into your command line and type:

```shell
vue create first-project
```

Where `first-project` is the name of the folder that will be created with your Vue project in it.

This will walk you through picking a number of modules and defaults for your project. The way the interface works is that you use the arrow keys to choose between options, the space to select or deselect an option and the enter key to move to the next screen.

You're going to pick a number of these modules to set up a project structure for your code. Select the following modules:

- Babel - Used so that we can write your code in modern JavaScript but still have it compatible with all major browsers.
- ESLint - to check syntax and common errors in JavaScript and ES6 when the project is built.
- Unit Testing using Mocha and Chai - for an easy to use unit testing framework for your components.
- E2E Testing using Cypress - in order to do black box and integration testing using the Cypress testing framework.

Once you've chosen those, just keep hitting enter on every other screen. You'll be selecting the defaults for every other option. After selecting the last option, it will begin downloading the tools and generating the files that your project will need.

## Project  and Component Structure

Now that you've created a project, you can open it in Visual Studio Code. In your terminal, type:

``` shell
code first-project
```

Again, `first-project` is the name of the folder you told the command line script to use.

There is a lot that gets generated for a new project, but what you're going to focus on right now is what's in the `src` folder. This is where all of the source code for your components is going to go.

Vue components are loaded onto a page using something called a root Vue instance. You can have multiple root Vue instances on one page, but if components are going to communicate with each other, they will need to live inside the same root Vue instance. This Parent/Child component strategy is the standard way that components are architected in Vue.

In your new default project, your root Vue instance is conveniently called `App.vue` and lives in the `src` folder. This is the engine that will start and coordinate your Vue components.

If you open this component, you can see all the basic elements that will make up a Vue component.

Like all component-based frameworks, Vue components will be made up of HTML, CSS and JavaScript. At the top of your component file, surrounded by `<template>` tags, is the HTML that defines the structure and content of the component.

``` HTML
<template>
  <div id="app">
    <img alt="Vue logo" src="./assets/logo.png">
    <HelloWorld msg="Welcome to Your Vue.js App"/>
  </div>
</template>
```

The App component contains HTML for a div, an image, and a `<HelloWorld>` tag. Remember how components are just custom HTML elements? This tag will eventually be replaced by another component that is defined elsewhere. You'll take a look at that later.

Next is the JavaScript inside of `<script>` tags.

``` HTML
<script>
import HelloWorld from './components/HelloWorld.vue'

export default {
  name: 'app',
  components: {
    HelloWorld
  }
}
</script>
```

The JavaScript represents the data and behavior for your component. For the `App` component, its job is to load other components into the page. Here, it is loading in the `HelloWorld` component.

The `<script>` tag defines the Vue object for the component. This object will contain all the configuration needed to create a Vue instance from your component code. You will be going over all of the parts of this as you go through this week.

In the App component, you can see that it's defined with a `name` and what other `components` it contains. A Vue component can be as simple as that.

Next is the CSS.

``` CSS
<style>
#app {
  font-family: 'Avenir', Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  text-align: center;
  color: #2c3e50;
  margin-top: 60px;
}
</style>
```

This CSS defines the default view and layout for the component. The CSS here should only apply to this component and not affect other elements that might also be included on the page. You'll look at ways to do that as you build your own components.

And those are the three pieces of a component. HTML, CSS, and JavaScript.

If you run `npm run serve` in the integrated terminal, this will compile the code and run it using a virtual server for you to view. Run that command and then open your browser to the URL that it shows on the screen once it has complied your project--should be something like `http://localhost:8080/`--and you should see the finished component on the screen.

## Running an existing project

So far you have seen how to create a brand new project, what that new project looks like and how to run it. What happens if you download an existing project though? This is a good time to discuss modern JavaScript projects and dependencies.

Whenever you download or open an existing Vue project (or modern JavaScript project for that matter) and you see a `package.json` this means that your project could have dependencies. Dependencies are stored in a `node_modules` folder and they aren't stored in source control. This means that before your project will work, you need to install all necessary dependencies. If you were to open up a Vue projects package.json you might see something like this:

```javascript
"dependencies": {
    "vue": "^2.5.17"
  },
  "devDependencies": {
    "@vue/cli-plugin-babel": "^3.0.5",
    "@vue/cli-plugin-eslint": "^3.0.5",
    "@vue/cli-plugin-unit-mocha": "^3.0.5",
    "@vue/cli-service": "^3.0.5",
    "@vue/test-utils": "^1.0.0-beta.20",
    "babel-eslint": "^10.0.1",
    "chai": "^4.1.2",
    "eslint": "^5.8.0",
    "eslint-plugin-vue": "^5.0.0-0",
    "vue-template-compiler": "^2.5.17"
  }
```

The dependencies block defines what this project needs to run and the devDependencies block defines what packages you will need to work on this project locally. If you don't see a node_modules folder you can install all of your dependencies by running the following command.

```bash
npm install
```

> If you open up a project that you just downloaded and don't see a node_modules folder and try to  run the application you will get an error. This is usually the first place to look.


