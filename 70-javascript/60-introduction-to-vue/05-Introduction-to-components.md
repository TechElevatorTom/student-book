# Introduction to Components-based JavaScript

The JavaScript you learned last week is how a lot of the web was created. The ideas of having your HTML, CSS, and JavaScript spread across three different files is how a lot of web sites still work today.

But the fundamental uses and methods of JavaScript on the web have changed drastically in the last five years. Websites, and web applications, have gotten a lot more complex. There aren't many static web sites out there anymore. Where you were once able to keep all of your JavaScript in one file, as our front-end applications get bigger and bigger, it has now become too hard to properly separate our code in maintainable modules and properly unit test our code as we write it.

Component-based JavaScript is an approach to JavaScript development that gives you a new way of thinking about your JavaScript code. Instead of thinking of your JavaScript code as separate from your HTML and CSS, injecting itself into your existing web page to manipulate an already functional site, you can start to think of your web page as a collection of components.

## Vanilla JavaScript

Normally, web pages are seen as three separate pieces; one file of HTML, a file of CSS, and a file of JavaScript.

![The monolithic structure of HTML, CSS, and JavaScript](img/monolithic_web.png)

As each of these files gets bigger and bigger and as you start to think of your web pages as more comprised of reusable parts--like headers, footers, navigation, etc--you start to think of ways to break these files up into smaller, more manageable parts.

Many developers have already done this. Most websites have their header and footer information in separate files so they can be reused throughout a site. CSS is often split up into multiple files using a language like SCSS or LESS and compiled into a single CSS file before being deployed to production. Some people do the same thing with their JavaScript, writing it in different files and then minifying it into one file for a production build.

Doing this with CSS helps a lot. You can have a file that contains all the layout rules, a file that contains all the font specifications, a file that defines how forms look, etc.

Does that work the same for JavaScript as it does for CSS? Not really. CSS is a declarative language where each rule is separate from all the other rules. JavaScript is a programming language where a variable, or function, from one file could overwrite a variable from another file. Debugging and troubleshooting a problem like that is not an easy thing to do. JavaScript written in this way is still loaded in the same "program" and one poorly behaving piece of code can ruin the entire application.

## Component-based JavaScript

What you need from JavaScript to support a bigger application is what developers had for years in more structured languages like Java and C#. Those languages are built to leverage good programming principles that will let you reuse your code and make sure that one piece of functionality doesn't affect other pieces when they run together. Essentially, encapsulation and isolation. None of these things are easy in traditional JavaScript, but are built into Component-based JavaScript.

In order to take a look at components, you'll need to reconsider how you look at your site overall. Instead of having all of your code in three separate files, you're going to start thinking of having a main HTML page that defines the main content and structure of our site and then include small components made out of HTML, CSS, and JavaScript to act as one single front-end element.

![Components are just HTML, CSS, and JavaScript working together to perform one piece of functionality on the page](img/component_web.png)

You should now view your web page not as a collection of HTML, CSS, and JavaScript but as a collection of components working together. You can think of components in this model as custom HTML elements. They are a small collection of HTML, CSS, and JavaScript that you include on a page to handle a specific piece of functionality on your site. Which is exactly what a normal HTML element is.

Think about the `<select>` tag. If you include a `<select>` tag in your HTML, that will render to the user in their browser as a GUI dropdown element. If you click the dropdown, you'll see all the `<option>`s that you included in the select element. It has structure, it has data, it has a basic look and feel, and it has behavior. That's a component.

The components you will be making will be more complex than a regular `<select>` element, but the principles are the same. You will be defining new HTML tags that will have structure (HTML), a basic look and feel (CSS), and will contain data and behavior (JavaScript).

There are a number of component-based JavaScript frameworks out there. The two you might hear about are Angular and React. Another popular one, and the one you will be using to learn component-based JavaScript, is Vue. Vue is a very light-weight, but powerful framework for building components in JavaScript using a lot of the skills you already have learned in this course.

So it's time to build your first component.