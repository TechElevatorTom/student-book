# VIDEO: Vue CLI

In this tutorial you will learn how to install and use the Vue CLI. You will also learn what all of the generated files and folders in a Vue project are used for.

{% video %}https://www.youtube.com/watch?v=_TcMK-XWwvg{% endvideo %}
