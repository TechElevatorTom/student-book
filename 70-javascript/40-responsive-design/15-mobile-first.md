# Mobile First

With the shift in internet consumption coming from mobile devices, mobile screens are now the primary means in which users interact with our applications. As such, a term *mobile-first* was coined to indicate that applications should be developed with a "mobile first mindset".

As developers, instead of adding breakpoints into the design as the width of the screen gets smaller, we should create breakpoints in the design when the width of the screen gets larger.

One approach that helps provide a mobile-first approach, is to [simulate mobile devices](https://developers.google.com/web/tools/chrome-devtools/device-mode/) using Chrome or Firefox's Responsive Web Design tools.

A simple strategy to follow for introducing breakpoints? *Start with the small screen first, then expand until it looks bad. At this point it is time to insert a new breakpoint.*



