# Designing for Responsive UI

Applying responsive design consists of addressing 3 key areas:

1. flexible/fluid grid layouts
2. resizable images
3. CSS media queries

As the user switches from an iPad to a laptop, the web application must automatically switch to accomodate the screen's resolution and support for larger image sizes. Even if the user doesn't switch devices, but changes the orientation of the screen from portrait to landscape, we may want our design to accomodate the extra white-space and fill it with content accordingly.

## Flexible Grid Layouts

Responsive sites use fluid grids. A 3-column layout shouldn't specify dimensions as pixels, but rather as proportions in relation to the other columns. For instance, Column 1 should take up half the page, Column 2 take up 30%, and Column 3 takes up the remaining 20%.

Two of the more recent CSS features: [CSS Grid](https://css-tricks.com/snippets/css/complete-guide-grid/) and [CSS Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/) allow us to create these fluid layouts that can shrink and grow proportionately as the screen size changes.

> #### Info::Setting the ViewPort
>
> User's are used to scrolling a web page vertically, not horizontally. As such, HTML5 introduced a method to let web developers take over the viewport (the user's visible area of the web page), via the `<meta>` tag so that the content can scale.
>
> You should include the following `<meta>` tag on all pages.
>
> ```html
> <meta name="viewport" content="width=device-width, initial-scale=1.0">
> ```
>
> The `width=device-width` part sets the width of the page to follow the screen-width of the device (which will vary depending on the device).
>
> The `initial-scale=1.0` part sets the initial zoom level when the page is first loaded by the browser.


## Responsive Images

When working with images, if the `width` property is not set or set to a fixed pixel width, then the image may break the fluid layout as the browser tries to render the image at its full size. 

Instead we should set the width to a percentage that it needs to take up and indicate the height can scale automatically.

```css
img {
    width: 100%;
    height: auto;
}
```

As the user resizes the browser, the container which holds the image will change size and immediately scale the image.

## Media Queries

Even when we design with fluid layouts and responsive images, it may still be necessary to render a completely different layout when there are screen size limitations. Take for example the image we used from [Medium.com](http://medium.com). 

![Medium.com Responsive Layout](img/medium.png)

On the smaller screen, probably the smart phone, the layout is a single-column layout where each of the elements is stacked vertically. Article images are placed on the right and marketing images are hidden due to the fact that they are non-essential. In order to accomodate this, CSS allows us to write conditional CSS in the form of **media queries**.

> #### Info::Setting the ViewPort
>
> Media queries are useful when we want to modify our site depending on a device's characteristics or parameters.   

Media queries can be used to conditionally apply styles using CSS `@media`.

```css
@media only screen and (max-width: 768px) {
    body {
        grid-template-columns: 1fr;
    }
    img { 
        display: none;
    }
}

@media only screen and (min-width: 768px) {
    body {
        grid-template-columns: 5fr 3fr 2fr;
    }
}
```

The above CSS conditionally applies one of two styles:
1. If the viewport's maximum width is under 768px, the grid container only provides 1 column to take up the full width. Images are also hidden.
2. If the viewport's minimum width is 768px or larger, the grid container provides 3 columns (50%, 30%, 20%) to take up the full width.

Other settings are accessible to use within CSS (e.g. `orientation`, `aspect-ratio`, etc.). A full reference is available on [MDN Web Docs - Using media queries](https://developer.mozilla.org/en-US/docs/Web/CSS/Media_Queries/Using_media_queries).


### Breakpoints

When we want certain parts of our page to behave differently, we add what is referred to as a *breakpoint*. A given site, if it supports a number of different layouts, might have multiple breakpoints defined in its stylesheet. Breakpoints are added using media queries.

While it is desirable to have breakpoints for each device we expect, supporting it will become problematic. As such, it is normal to define breakpoints using a variety of ranges that accomodate smartphones, tablets, desktop, and extra-large screens.

You can use these common breakpoints as a reference:

```css
/* Extra small devices (phones, 600px and down) */
@media only screen and (max-width: 600px) {...} 

/* Small devices (portrait tablets and large phones, 600px and up) */
@media only screen and (min-width: 600px) {...} 

/* Medium devices (landscape tablets, 768px and up) */
@media only screen and (min-width: 768px) {...} 

/* Large devices (laptops/desktops, 992px and up) */
@media only screen and (min-width: 992px) {...} 

/* Extra large devices (large laptops and desktops, 1200px and up) */
@media only screen and (min-width: 1200px) {...}
```