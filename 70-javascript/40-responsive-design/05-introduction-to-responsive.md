# What Is Responsive Design?

Day by day the number of devices that our sites need to support grows. With the release of any new device, whether it comes from Apple, Google, Amazon, or Facebook, is a guarantee that things won't remain the same. Apple alone supports iOS on the following screen sizes:

| Device | Native Resolution |
|--------|-------------------|
| iPhone X | 1125 x 2436 |
| iPhone 8 Plus | 1080 x 1920 | 
| iPhone 8 | 730 x 1334 |
| iPhone 7 Plus | 1080 x 1920 |
| iPhone 6s Plus | 1080 x 1920 |
| iPhone 6 Plus | 1080 x 1920 |
| iPhone 7 | 750 x 1334 |
| iPhone 6s | 750 x 1334 |
| iPhone 6 | 750 x 1334 |
| iPhone SE | 640 x 1136 |
| iPad Pro 12.9-inch (2nd generation) | 2048 x 2732 |
| iPad Pro 10.5-inch | 2224 x 1668 |
| iPad Pro (12.9-inch) | 2048 x 2732 |
| iPad Pro (9.7-inch) | 1536 x 2048 |
| iPad Air 2 | 1536 x 2048 |
| iPad Mini 4 | 1536 x 2048 |

This list only shows the latest generation iOS devices and doesn't include the numerous other versions that were once supported by developers.

With all of the possible screen sizes out there, not to mention various laptop and large-size display monitors, the industry will continue to experience a significant amount of fragmentation. No single screen size holds the share of the market and in order to keep up, companies have been forced to create versions of their web applications that render well on any of the possible options.

This began an approach to web design called **responsive design**.

> #### Info::Responsive Design
>
> Responsive web design is the approach that design and development should respond to the user's behavior and environment based on screen size, platform, and orientation.

With responsive design, we are given the goal is to create one site for every screen.

![Medium.com - obtained via https://mediaqueri.es](img/medium.png)

Visit https://mediaqueri.es to see a gallery of common web applications and how they render their site responsively.
