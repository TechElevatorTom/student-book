# Introduction to HTTP Headers

We have learned that we can send a request to a particular resource. We also learned that we can send along an Http Method so that we can instruct the server what this request is meant to do. In this section we are going to learn how to send additional data along with the request.

## HTTP Headers

HTTP headers allow the client and the server to pass additional information with the request or the response. An HTTP header consists of its case-insensitive name followed by a colon ':', then by its value (without line breaks). Leading white space before the value is ignored.

HTTP header fields provide required information about the request or response, or about the object sent in the message body. There are four types of HTTP message headers:

* General-header: These header fields have general applicability for both request and response messages.
* Client Request-header: These header fields have applicability only for request messages.
* Server Response-header: These header fields have applicability only for response messages.
* Entity-header: These header fields define meta information about the entity-body or, if no body is present, about the resource identified by the request.

## Common Headers

This is a list of common headers and what they are used for. 

| Header           | Example                         | Description                                                                                                             |
|------------------|---------------------------------|-------------------------------------------------------------------------------------------------------------------------|
|Accept            | Accept: text/plain              | Content types that are acceptable for the response.                                                                     |
|Accept-Charset    | Accept-Charset: utf-8           | Character sets that are acceptable                                                                                      |
|Accept-Encoding   | Accept-Encoding: gzip, deflate  | List of acceptable encodings                                                                                            |
|Accept-Language   | Accept-Language: en-US          | List of acceptable human languages for response                                                                         |
|Authorization     | Authorization: Basic QWxhZtZQ== | Authentication credentials for HTTP authentication                                                                      |
|Cache-Control     | Cache-Control: no-cache         | Used to specify directives that MUST be obeyed by all caching mechanisms                                                |
|Content-Length    | Content-Length: 348             | The length of the request body in octets (8-bit bytes)                                                                  |
|Content-Type      | Content-Type: application/json  | The MIME type of the body of the request (used with POST and PUT requests)                                              |
|Host              | Host: en.wikipedia.org          | The domain name of the server (for virtual hosting), and the TCP port number on which the server is listening.          |
|User-Agent        | User-Agent: Mozilla/5.0 ...     | The user agent string of the user agent                                                                                 |  
|Cookie            | Cookie: $Version=1; Skin=new;   | An HTTP cookie previously sent by the server with Set-Cookie                                                            | 
|Origin            | Origin: http://www.abc.com      | Initiates a request for cross-origin resource sharing (asks server for an Access-Control-Allow-Origin response header)  |

## All HTTP Headers

There are a lot of HTTP Headers but I just wanted to list out some of the most common ones. If you would like a full list of headers and their possible values please click on the link below. 

https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers