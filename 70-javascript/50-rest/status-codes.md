# Introduction to HTTP Response Status Codes

HTTP response status codes indicate whether a specific HTTP request has been successfully completed. Responses are grouped in five classes: informational responses, successful responses, redirects, client errors, and servers errors.

## Status Code Categories

Status codes are broken down into different categories. In the table below we have labeled a code as 200.x. This means that any status code that falls in the 200 range is a success message. This helps because even if you don't know what that specific message is, you know what type of response it is. 

| Status Code | Description               |
|-------------|---------------------------|
| 100.x       | Information Responses     |
| 200.x       | Successful Responses      |
| 300.x       | Redirection Messages      |
| 400.x       | Client Error Responses    |
| 500.x       | Server Error Responses    | 

Below is a breakdown of each of the status code categories. There are also a few of the common status codes for each category. If you would like a list of all the status codes and their definitions you can visit https://developer.mozilla.org/en-US/docs/Web/HTTP/Status

### Information Responses

Request received, continuing process.

This class of status code indicates a provisional response, consisting only of the Status-Line and optional headers, and is terminated by an empty line. Since HTTP/1.0 did not define any 1xx status codes, servers must not send a 1xx response to an HTTP/1.0 client except under experimental conditions.

| Code                     | Description                                                                                                                                              |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| 100 Continue             | This interim response indicates that everything so far is OK and that the client should continue with the request or ignore it if it is already finished.|
| 101 Switching Protocols  | This code is sent in response to an Upgrade request header by the client, and indicates the protocol the server is switching to. |
| 102 Processing           | This code indicates that the server has received and is processing the request, but no response is available yet.  |

### Successful Responses

This class of status codes indicates the action requested by the client was received, understood, accepted and processed successfully.

| Code                     | Description                                                                                                                                              |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| 200 OK                   | The request has succeeded. The information returned with the response is dependent on the method used in the request.                                    |
| 201 Created              | The request has succeeded and a new resource has been created as a result of it. This is typically the response sent after a POST request, or after some PUT requests. |
| 202 Accepted             | The request has been received but not yet acted upon. It is non-committal, meaning that there is no way in HTTP to later send an asynchronous response indicating the outcome of processing the request. It is intended for cases where another process or server handles the request, or for batch processing.  |
| 204 No Content           | There is no content to send for this request, but the headers may be useful. The user-agent may update its cached headers for this resource with the new ones. |


### Redirection Responses

The client must take additional action to complete the request.

This class of status code indicates that further action needs to be taken by the user agent in order to fulfil the request. The action required may be carried out by the user agent without interaction with the user if and only if the method used in the second request is GET or HEAD. A user agent should not automatically redirect a request more than five times, since such redirections usually indicate an infinite loop.

| Code                     | Description                                                                                                                                              |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| 301 Moved Permanently    | This and all future requests should be directed to the given URI.                                                                                        |
| 304 Not Modified         | Indicates the resource has not been modified since last requested. Typically, the HTTP client provides a header like the If-Modified-Since header to provide a time against which to compare. Using this saves bandwidth and reprocessing on both the server and client, as only the header data must be sent and received in comparison to the entirety of the page being re-processed by the server, then sent again using more bandwidth of the server and client.|


### Client Error Responses

The 4xx class of status code is intended for cases in which the client seems to have erred. Except when responding to a HEAD request, the server should include an entity containing an explanation of the error situation, and whether it is a temporary or permanent condition. These status codes are applicable to any request method. User agents should display any included entity to the user.

| Code                     | Description                                                                                                                                              |
|--------------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------|
| 400 Bad Request          | The request cannot be fulfilled due to bad syntax.                                                                                                       |
| 401 Unauthorized         | Similar to 403 Forbidden, but specifically for use when authentication is possible but has failed or not yet been provided. The response must include a WWW-Authenticate header field containing a challenge applicable to the requested resource. See Basic access authentication and Digest access authentication. |
| 403 Forbidden            | The request was a legal request, but the server is refusing to respond to it. Unlike a 401 Unauthorized response, authenticating will make no difference.|
| 404 Not Found            | The requested resource could not be found but may be available again in the future. Subsequent requests by the client are permissible.|
| 409 Conflict             | Indicates that the request could not be processed because of conflict in the request, such as an edit conflict. |


### Server Error Responses

The server failed to fulfill an apparently valid request.

Response status codes beginning with the digit "5" indicate cases in which the server is aware that it has encountered an error or is otherwise incapable of performing the request. Except when responding to a HEAD request, the server should include an entity containing an explanation of the error situation, and indicate whether it is a temporary or permanent condition. Likewise, user agents should display any included entity to the user. These response codes are applicable to any request method.

| Code                          | Description                                                                                                                                  |
|-------------------------------|----------------------------------------------------------------------------------------------------------------------------------------------|
| 500 Internal Server Error     | The request cannot be fulfilled due to bad syntax.                                                                                           |
| 503 Service Unavailable       | The server is currently unavailable (because it is overloaded or down for maintenance). Generally, this is a temporary state.                |


## Status Code Summary

As we said earlier you're never going to memorize all of the status codes. Just knowing what each category (200.x,400.x,etc) is really going to help you understand what is going on with the request.

All of these status codes are [defined in the protocol](https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html)

