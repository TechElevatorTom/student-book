# Working with a Mock REST API

Right now we are learning all about what REST APIs are and what they consist of. We haven't actually learned how to create an API yet (soon) so it's a little hard to start playing around with them. 

You could do a search for [public REST APIs](https://github.com/toddmotto/public-apis) but generally you need to authenticate with them and most of the time you are just retrieving (GET) information from them. What if we want to practice all of our verbs? 

## mockAPI

[mockAPI](http://www.mockapi.io/projects) is an application that lets you (for free) create a mockAPI that you can then test using one of our fancy new tools we learned about in the last section. 

![mockAPI](resources/images/todoapp.png)

### mockAPI Tutorial

In this short screencast I will show you how to create your own mockAPI, import mine and how to run a quick test in Postman against it. 

{% video %}https://www.youtube.com/watch?v=W1D0EJtze6s{% endvideo %}