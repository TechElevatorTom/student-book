# Introduction to Tools

The easiest way to learn more about REST based APIs is start using them. We know that we can open up a browser window and hit an API endpoint as long as it is it a simple GET request but what about others? In these cases we will want to take advantage of some tools that allow us to test our REST APIs. 

## Browser Tools

In the event that we are ust testing a simple GET request it's nice to have some formatted responses. If you're using Firefox this is already built in and if you're using Chrome there is a really nice extension called [JSON Viewer](https://chrome.google.com/webstore/detail/json-viewer/gbmdgpbipfallnflgajpaliibnhdgobh?hl=en-US). 

When you visit a URL that is going to return JSON instead of seeing a bunch of unformatted JSON we will see a nice looking response like this. 

![JSON Viewer](resources/images/json_viewer.png)

The other nice feature of JSON Viewer is that you can set your own theme for how you would like the response to look. 

{% video %}https://www.youtube.com/watch?v=qwnq8srLSTY{% endvideo %}

## Postman

Postman is one of the most popular HTTP clients around. At the very basic level it allows you to choose a verb, add an endpoint and then configure your request as much as you want. Postman used to be a Chrome extension but is now a standalone application that you can [download here](https://www.getpostman.com/apps). The easiest way to learn more about Postman is to see it in action. 

![Postman](resources/images/postman.png)

{% video %}https://www.youtube.com/watch?v=s99EYYBGu70{% endvideo %}

## Visual Studio Code Extension(s)

If you're already using Visual Studio Code there is a really great extension called [REST Client](https://marketplace.visualstudio.com/items?itemName=humao.rest-client). REST Client allows you to create a file that you will use to test your different endpoints. In the file you can configure the Method, endpoint and options. 

![REST Client](resources/images/rest_client.gif)

Here is a quick tutorial on how to install REST Client and start using it. 

{% video %}https://www.youtube.com/watch?v=Xsj8rkQStJo{% endvideo %}

