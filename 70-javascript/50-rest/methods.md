# Introduction to HTTP Request Methods

We learned what resources are and that we should always use HTTP Verbs when sending a request to the server. An HTTP Verb, also known as an HTTP Request Method tells the server what type of request we are sending it so it knows how to act. 

In the web world we use 2 of these verbs every single day, most of the time without even realizing it. 

* `GET`: When we type in a URL in the browser and hit enter we are performing a GET request on that URL
* `POST`: When we fill out a form online and hit submit, we are sending a POST request.

## Http Methods

This is a list and definition of the most commonly used HTTP Verbs (methods).

| Method  | Definition                                                                                                                                |
|---------|-------------------------------------------------------------------------------------------------------------------------------------------|
| POST    | The POST method is used to submit an entity to the specified resource, often causing a change in state or side effects on the server.     |
| GET     | The GET method requests a representation of the specified resource. Requests using GET should only retrieve data.                         |
| PUT     | The PUT method replaces all current representations of the target resource with the request payload.                                      |
| PATCH   | The PATCH method is used to apply partial modifications to a resource.                                                                    | 
| DELETE  | The DELETE method deletes the specified resource.                                                                                         |

There are a few other HTTP Methods but they aren't used as often. 

* HEAD
* CONNECT
* OPTIONS
* TRACE

## Http Methods for Restful Services

We talked about some of the REST service constraints in a previous section. Http methods play a major role in our "uniform resource" constraint and provide the action counterpart to the noun-based resource. As we discussed earlier in this document the most commonly used HTTP methods are GET, POST, PUT, PATCH and DELETE. These correspond to create, read, update and delete (or CRUD) operations, respectively. 

Below is a table with the Method, Crud Operation and an example request.

| Method   | CRUD   | Example                         |
|----------|--------|---------------------------------|
| POST     | create | http://www.te.com/students/     |
| GET      | read   | http://www.te.com/students/1234 |
| PUT      | update | http://www.te.com/students/1234 |
| DELETE   | create | http://www.te.com/students/1234 |
