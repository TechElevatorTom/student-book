# Introduction to REST based APIs

Now that we have a better understanding of what Web Services are and just how important they are it's time we take a look at REST services. REST stands for Representational State Transfer which is an architecture choice for building your Web Services. 

In this chapter we will learn all about REST and the different building blocks that used to build REST services: 

* What is REST
    * REST Defined
    * History of Web Services
    * RESTful Web Services
* HTTP Request Methods
    * What are HTTP Methods (Verbs)
    * POST,GET,PUT,DELETE,PATCH
* HTTP Request Headers
    * What are headers?
    * Common headers
* HTTP Response Status Codes
    * What are status codes?
    * Status Code Categories
        * Information Response (100.x)
        * Successful Responses (200.x)
        * Redirection Responses (300.x) 
        * Client Error Responses (400.x)
        * Server Error Responses (500.x)
    * Common Status Codes
* Mock REST API
    * Using http://www.mockapi.io
    * Setting up your own Mock API
* Tools
    * Browser Tools
    * Postman
    * Visual Studio Code Extension(s)
