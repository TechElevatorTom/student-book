# History of Web Services

To understand what REST is I think it's important to have a quick history lesson on API development. Knowing where we have come from always helps us appreciate where we are now. 

When API development really hit the scene in the late 90s it was a huge free for all. There were no standards and everyone was writing their own APIs however the saw fit. This was a real big problem because as we learned in a previous chapter one of the big benefits of an API was for applications to talk to each other. If there are no standard to adhere by this becomes a real problem for system to system communication. 

## SOAP

In the wild wild west of API development we needed some type of standard. In 1999 the SOAP specification was released however it did not become a "standard" until version 1.2 when the W3C recommended it in 2003. SOAP stood for `Simple Object Access Protocol` and there was nothing **Simple** about it. 

Take for instance this SOAP web service that will take 2 numbers and add them together. This is what a typical request might look like: 

```
POST /SumNumbers HTTP/1.1
Host: www.techelevator.com
Content-Type: application/soap;  charset="utf-8"
Content-Length: nnnn

<?xml version="1.0">
<SOAP:Envelope
    xmlns:xsi="http://www.w3.org/1999/XMLSchema/instance
    xmlns:xsd="http://www.w3.org/1999/XMLSchema/instance"
    xmlns:SOAP="urn:schemas-xmlsoap-org:soap.v1>
    <SOAP:Body>
        <SumNumbersService xmlns="http://tempuri.org">
            <x>int</x>
            <y>int</y>
        </SumNumbersService>
    </SOAP:Body>
</SOAP:Envelope>
```

And here is what a response might look like. 

```
<?xml version="1.0">
<SOAP:Envelope
    xmlns:xsi="http://www.w3.org/1999/XMLSchema/instance
    xmlns:xsd="http://www.w3.org/1999/XMLSchema/instance"
    xmlns:SOAP="urn:schemas-xmlsoap-org:soap.v1>
    <SOAP:Body>
        <SumNumbersResponse>
            <Sum>10</Sum>
        </SumNumbersResponse>
    </SOAP:Body>
</SOAP:Envelope>
```

If this makes your brain hurt, you're not alone. All of this is put together in accordance with an arbitrary set of guidelines specified in the documentation, which explains the transport bindings, types of data that can be accessed, parameter lists, operation names, and the endpoint URI. And if the call doesn't work, there aren't any HTTP respond codes to nudge you in the right direction.

This is just the request and response objects that are sent between servers. The code needed to produce these objects were even more mind numbing and I am going to save you from having even to see what that code looks like. 

## REST was born

Roy Fielding defined REST in his [2000 PhD dissertation](https://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm) "Architectural Styles and the Design of Network-based Software Architectures" at UC Irvine. In a retrospective look at the development of REST, Fielding said: 

>Throughout the HTTP standardization process, I was called on to defend the design choices of the Web. That is an extremely difficult thing to do within a process that accepts proposals from anyone on a topic that was rapidly becoming the center of an entire industry. I had comments from well over 500 developers, many of whom were distinguished engineers with decades of experience, and I had to explain everything from the most abstract notions of Web interaction to the finest details of HTTP syntax. That process honed my model down to a core set of principles, properties, and constraints that are now called REST.

* Uniform interface
* Stateless
* Client-server
* Cacheable

Another important difference between REST and SOAP is that it's resource-based. So the API accesses nouns (aka URIs), instead of verbs. Then, HTTP verbs are used to access those resources.

It seems like there are a lot of rules, but those rules are universal. It forces the API to be simpler, and makes the learning curve for developers trying to integrate software significantly less steep. Roy Fielding gave the disorganized internet world the gift of a common language through which their software could communicate.

## Resources

* https://en.wikipedia.org/wiki/Representational_state_transfer 
* https://blog.readme.io/the-history-of-rest-apis/ 





