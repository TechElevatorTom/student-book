# RESTful Web Services

Remember earlier when we compared REST to painting? In our painting scenario we talked about how we could go about it any way we wanted but that there was a set way that might be more efficient. What I want to do now is breakdown the architectural constraints that make up a REST service. 

## REST Constraints

There are a few formal REST constraints that we will get to know briefly, as follows:

* Uniform interfaces
* Client-server
* Stateless
* Layered system
* Cacheable
* Self descriptive messages
* Code on demand (optional)
* HATEOS

We are going to break each of these down but I want to be very clear on something here. Understanding the concept is much more important than understanding definitions. If I showed you a list of REST constraints you should be able to explain in a single simple sentence what each of them means. 

### Uniform interfaces

A uniform interface constraint is one of the basics of the design in a REST service in that the uniform interface will simplify and decouple the architecture, which will then enable each part of it independently, it being divided into few more consistent interfaces, which are:

* Resources
* Resource identification

#### Representation and resources

The key abstraction of information in REST is a resource. Any information that can be named can be a resource: a document or image, a temporal service (e.g. "today's weather in Los Angeles"), a collection of other resources, a non-virtual object (e.g. a person), and so on. In other words, any concept that might be the target of an author's hypertext reference must fit within the definition of a resource. A resource is a conceptual mapping to a set of entities, not the entity that corresponds to the mapping at any particular point in time.

#### Resource identification

Resources are identified by uniform resource identifiers, also known as URI's. Take for example if we had a Blog and a REST API for others to consume our content. We might have a resources that allow us to get content 

* Get all Blog Posts `/api/blog/posts`
* Get all Blog Posts where category = ? `/api/blog/posts/java`
* Get Blog Post where id = ? `/api/blog/posts/123`
* Get Blog Post where id = ? and get all comments for this post. `/api/blog/posts/123/comments`

![REST Resources](../resources/images/rest_resources.png)

This also means we *always* use HTTP verbs

* GET
* POST 
* PUT
* PATCH
* DELETE

### Client-Server

In traditional web development the client and server don't know anything about the other and this holds true in a REST architecture. This means that clients aren't concerned with things like data storage. Servers are also not bothered with things like a user interface which makes them a lot simpler and more flexible and scalable. The servers and clients can also be replaced and developed independently as long as the interface between them is not altered in any way.

### Stateless

We must make sure that all client-server communication is stateless. Any request that will be passed will contain in it all the required information to service the request, and thus the sessions state that will be held within the client.

### Layered System

The tiered system that is in place implies that a client cannot tell in an ordinary manner whether it is connected directly to the end server or via an intermediary method. The intermediary solution comes to improve system's scalability by enabling balance load and making use of shared caches that can further imply different security policies.

### Cacheable

In any application that we create these days we should always be looking to see if we can take advantage of caching to improve performance and REST is no different. Good management of the cache will wholly or partially eliminate any client-server interaction that will affect performance and scalability in a positive way.

### Self descriptive messages

There's sufficient information included within each of the messages, allowing us to receive a description of how to process the messages. That means that the data format of the representation must always come with its media type. If you are sending HTML, then you must say it is HTML by sending the media type with the representation. In HTTP this is done by using the content-type header.

### Code on demand (optional)

REST allows client functionality to be extended by downloading and executing code in the form of applets or scripts. This simplifies clients by reducing the number of features required to be pre-implemented. Allowing features to be downloaded after deployment improves system extensibility. However, it also reduces visibility, and thus is only an optional constraint within REST.

### HATEOS

[HATEOAS](https://en.wikipedia.org/wiki/HATEOAS) (Hypermedia as the Engine of Application State) is a constraint of the REST application architecture.A hypermedia-driven site provides information to navigate the site's REST interfaces dynamically by including hypermedia links with the responses. 