# REST: REpresentational State Transfer

When learning something new I always like to compare to something we can all relate to. There is no real right or wrong way to paint a room. There are of course ways that lead to a more efficient process that scales better when we have more painters in the room. We would probably want to cover the floors first and then tape any windows, floor boards, and ceilings. We could then have 1 painter start on all of the trim while another knocks out the big open areas with a roller.

Building a Web Service is similar in that we can choose to build one however we like. What we have found out over the years though is that REST services are simple to use and are efficient as long as you follow some well defined guidelines. We will breakdown what these rules are and how they are used as this chapter progresses.

## Rest Defined 

Representational State Transfer (REST) refers to a group of software architecture design constraints that bring about efficient, reliable, and scalable distributed systems. A system is called RESTful when it adheres to those constraints.

The basic idea of REST is that a resource, e.g. a document, is transferred with its state and relationships (hypertext) via well-defined, standardized operations and formats. Often API's or services call themselves RESTful when they directly modify a type of document as opposed to triggering actions elsewhere.

Because HTTP, the standard protocol behind the Web, also transfers documents and hypertext links, simple HTTP APIs are sometimes colloquially referred to as RESTful APIs, RESTful services, or simply REST services, although they don't necessarily adhere to all REST constraints. Beginners can assume a REST API means an HTTP service that can be called using standard web libraries and tools.

Source: https://developer.mozilla.org/en-US/docs/Glossary/REST
