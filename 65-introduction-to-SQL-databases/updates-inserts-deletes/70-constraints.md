# Constraints on Table Columns

We talked about keys and how those keys can create links between the rows in our tables. We can then join our tables together via those links and create relations between the tables.

But those links also perform another function. They act as a **constraint** on our data and how we're allowed to interact with it.

Constraints do what it sounds like they do, they constrain what we're allowed to do with the data in the database. Whenever we update, insert or delete rows in the database, the DBMS will check that data against constraints that are defined in the tables and reject any changes that conflict with those constraints.

For instance, let's go back to the example we used when talking about keys:

#### countries

| id | name           |
|----|----------------|
| 1  | Canada         |
| 2  | Mexico         |
| 3  | Cuba           |
| 4  | France         |
| 5  | Spain          |
| 6  | Italy          |
| 7  | China          |

#### cities

| id | country_id | name        |
|----|------------|-------------|
| 1  | 6          | Milan       |
| 2  | 6          | Rome        |
| 3  | 7          | Beijing     |
| 4  | 4          | Nice        |
| 5  | 4          | Paris       |
| 6  | 1          | Toronto     |
| 7  | 5          | Seville     |

These two tables both have Primary Keys called `id`, but the cities table also has a Foreign Key called country_id that links to the countries table. On the database, that will look something like this:

``` SQL
ALTER TABLE cities ADD FOREIGN KEY (country_id) REFERENCES countries(id);
```

If I had this data in my database and I wanted to delete Mexico:

``` SQL
DELETE FROM countries WHERE id=2;
```

That would be perfectly fine. We can delete that country because nothing else relies on it. There are no cities for Mexico in the database and nothing preventing Mexico from being deleted.

However, if we tried to delete Canada with:

``` SQL
DELETE FROM countries WHERE id=1;
```

We would get an error:

```
ERROR: update or delete on table "countries" violates foreign key constraint "fk_countryid" on table "cities"
  Detail: Key (id)=(1) is still referenced from table "cities".
```

Oops. That didn't work. But that's because we have a constraint on the cities table called a Foreign Key Constraint. What that says is, if any row in the cities table is pointing to another row in the countries table, that country row is not allowed to be deleted.

These constraints help us keep our data consistent and prevents us from having data that points to something that doesn't exist.

## Other types of constraints

There are other types of constraints that we can put on columns in our database as well. The most obvious one, I suppose, is the data type that is defined on a column. But there are a few more that can help us design our database tables to make sure they never contain data that we don't want them to contain.

| Constraint Name | Description                                        |
|-----------------|----------------------------------------------------|
| UNIQUE          | Verifies the value in this column is unique among all the rows in this table. Good for usernames and email address columns |
| NOT NULL        | Verifies that all rows in this table have a value for this column and the column is never set to NULL |
| DEFAULT value   | If a row is entered and this column isn't given a value, it will get the set default value |
| CHECK           | Verifies that the value in this column meets a programmer defined condition, like (AGE >= 18) |

