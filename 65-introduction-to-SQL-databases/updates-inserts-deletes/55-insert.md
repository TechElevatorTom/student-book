# INSERTing New Rows

Now that we can select data from the database, how do we get it in there in the first place?

## INSERTs

To add a row, we will use an INSERT statement:

``` SQL
INSERT INTO countries (code, name, continent, region)
VALUES ('JOE', 'Amazingland', 'North America', 'Central America');
```

This will create a new row in our countries table and fill in the column of that row with these values.

``` SQL
SELECT * FROM countries WHERE continent='North America';
```

Result:

| code | name           | continent         | region             |
|------|----------------|-------------------|--------------------|
| JOE  | Amazingland    | North America     | Central America    |
| CYM  | Cayman Islands | North America     | Caribbean          |
| ...  | ...            | ...               | ...                |

The format of the INSERT statement is:

``` SQL
INSERT INTO table_name (column1, column2, column3, ...)
VALUES (value1, value2, value3, ...);
```

> #### Note::The columns are optional, but use them
>
> Technically the column list is optional in an INSERT statement and the database will just assume that your VALUES are in the same order that the columns were defined in when the table was created. How do you know what that is? Well, there are ways of looking it up but, honestly, just list out the columns. There is a chance that the columns will be in a different order on dev versus production and other little hiccups, so best practice is to just list out the columns in the INSERT statement so you don't have to worry about it.
>
> Remember: It pays to be explicit.