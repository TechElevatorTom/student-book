# Making Calls in Transactions

One issue we have so far in SQL is that we can really only run one statement at a time and that the statements are separated from each other. If one of the statements fail, it's not easy to reverse anything that came before. For instance, let's say that we want to transfer money from one bank account to another. If we have a table like this:

#### accounts
| account_number | name | balance |
|--------|------|---------|
| 383953 | Bob  | 100.00  |
| 485920 | Jane | 403.00  |

If we want to transfer $25.00 from Bob to Jane, we would need two statements to do that:

``` SQL
UPDATE accounts SET balance = balance - 25 WHERE account_number = 383953;
UPDATE accounts SET balance = balance + 25 WHERE account_number = 485920;
```

Most of the time this will work great, but what if something goes wrong? What if the database crashes right after we take $25 from Bob and the second statement throws an error? Then we would be in an inconsistent state.

We want the transfer to be seen as a single action. The money is either transferred or it's not, it should never be in an in between state. What we want is for this action to be ACID compliant.

## ACID

ACID is an acronym for Atomicity, Consistency, Isolation, and Durability.

|           |  |
|-----------|--------------------------------------------------------------------------------------------------|
| Atomicity | This means that the statements in our transaction above either all happen or none of them happen |
| Consistency | This means that our database should never be in a state where one statement has run and the other hasn't yet. In other words, all statements in the transaction happen at the same time. |
| Isolation | This means that no one else on the database will see the results of the statements until they are all completed |
| Durable | This means that once the statements have run, they are permanent and saved in the database |

Most SQL databases are fully ACID compliant by using transactions.

## Transactions

We can group SQL statements together into a **transaction**. Instead of calling the two statements separately, like we did above, we can instead group them together in an ACID compliant transaction:

```SQL
BEGIN TRANSACTION;
UPDATE accounts SET balance = balance - 25 WHERE account_number = 383953;
UPDATE accounts SET balance = balance + 25 WHERE account_number = 485920;
COMMIT;
```

Now, the first line in our SQL call will start a new transaction. A transaction is kind of like a temporary sandbox that we can use to manipulate the data without actually affecting the data for others. We're in our own little world now and can change and update all we want.

If either of these statements fail or a server dies in the middle, nothing from that transaction gets saved to the database, keeping the transaction atomic and consistent. Since we're in our own little world, the statements are also fully isolated from anyone else on the system.

After running statements in a transaction, you then have two options. Like above, you can `COMMIT` the transaction, which applies the changes to the database in one fell swoop. Or you can `ROLLBACK` which ditches all the changes and keeps the database as if nothing had ever changed.

Transactions aren't used all the time and are never used at all for SELECT statements--since nothing is changing anyway--but are very powerful and useful if you have multiple statements that really should happen as one large change or not at all.