# DELETEing Your Data

And sometimes we want to delete data from the database and get rid of it completely. We do that with a DELETE statement:

``` SQL
DELETE FROM countries
WHERE code = 'JOE';
```

This will delete any row from the database that matches the WHERE clause.

> #### Warning::Be sure of what you're deleting
> 
> As stated with UPDATE statements, if you leave off the WHERE clause on a DELETE, all the rows in your database will disappear. And there is no undo in SQL (at least not yet) so once they're gone, they're gone. And so is your job.
>
> Much better is to write a SELECT statement first, make sure those are the rows you want to delete, and then change it to a DELETE statement after.

The standard format of a DELETE statement is:

``` SQL
DELETE FROM table_name
WHERE condition;
```

> #### Note::In most real world applications, you don't delete often
>
> In most real applications, you actually don't delete a lot of data. Data is valuable and keeping around data that you don't need on the site anymore can have its advantages. If a customer calls up and says that they accidentally deleted something they didn't mean to, you're going to be glad you kept it in the database after all.
>
> So how are deletes handled then? Typically by adding a disabled or deleted_date column to the table and setting that when you want something "deleted". Then all of your SELECT statements can look for that when pulling data from the database.
>
> So then we can pull back all users from a database that haven't been deleted like so:
>
> ``` SQL
> SELECT * FROM users
> WHERE deleted_date IS NULL;
> ```