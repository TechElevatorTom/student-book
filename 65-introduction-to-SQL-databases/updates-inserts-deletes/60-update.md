# UPDATEing Your Data

Data is often changed in a system, for example changing your user profile or updating a blog post, so having a way to update data is important. For SQL databases, you can do that with an update statement.

``` SQL
UPDATE countries SET name = 'Amazia', region = 'New Amazland'
WHERE code = 'JOE';
```

You'll see our old familiar WHERE clause here, which is now selecting which rows we want to update.

> #### Warning::Use your WHERE clause on every UPDATE
>
> What happens if we forget the WHERE clause? The update will happen on every row in the table, changing all of your data. This, typically, is a very, very bad thing.
>
> To make sure that you are going to update the correct rows, write a SELECT statement first:
> 
> ``` SQL
> SELECT * FROM countries
> WHERE code = 'JOE';
> ```
>
> And once you've verified that it is in fact bringing back the rows you want, change it to an UPDATE like above.

So the structure of an UPDATE statement would be:

``` SQL
UPDATE table_name SET column1 = value1, column2 = value2, ...
WHERE condition;
```