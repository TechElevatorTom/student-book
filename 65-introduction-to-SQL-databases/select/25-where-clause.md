# The WHERE Clause

But what if we don't want all the rows that are stored in that table?

If we want to pick and choose which *rows* to pull back from the database, we will need to use a WHERE clause:

    SELECT * FROM countries WHERE continent='North America';

Result:

| code | name           | continent         | region             |
|------|----------------|-------------------|--------------------|
| CYM  | Cayman Islands | North America     | Caribbean          |
| CHL  | Chile          | South America     | South America      |
| CRI  | Costa Rica     | North America     | Central America    |
| DMA  | Dominica       | North America     | Caribbean          |
| DOM  | Dominican Republic | North America     | Caribbean      |
| ...  | ...            | ...               | ...                |

In defining a WHERE clause, we are telling the database which rows we want. In the above example, we're telling the database when only what rows where the data in continent is equal to 'North America'.

> #### Warning::Equality in SQL
>
> In most languages we've seen so far, we typically use double equals or triple equals to see if two things are equal. In SQL, we just use one equal. Just something to remember!

## Conditionals

By specifying conditionals in our WHERE clause, we can narrow down which rows we're selecting from the database. Much like a conditional in an `if` statement, the WHERE clause conditional will only select rows where the conditional evaluates to true.

All of the standard conditionals that we already know and love are also available in SQL:

| Operator              | Numbers           | Characters            |
|-----------------------|-------------------|-----------------------|
| =                     | Equal To          | Equal To              |
| <>, !=                | Not Equal To      | Not Equal To          |
| <                     | Less Than         | Alphabetically Before |
| >                     | Greater Than      | Alphabetically After  |
| <=                    | Less Than or Equal| Alphabetically Before or Equal |
| >=                    | Greater Than      | Alphabetically After or Equal  |
| IN(value1,value2,...) | Is Value in List  | Is Value in List      |
| BETWEEN value1 AND value2 | Is Value Greater Than or Equal to value1 and Less Than or Equal to value2 | Is Value Greater Than or Equal to value1 and Less Than or Equal to value2 |
| IS NULL               | Equal To Null     | Equal To Null         |
| IS NOT NULL           | Not Equal To Null | Not Equal To Null     |
| LIKE '%value1%'       | N/A               | Contains value1       |

> #### Note::NULL Value in SQL
>
> NULL means something a little differently in SQL than in other programming language. In OOP languages, NULL means that a variable doesn't contain an object. In SQL, NULL means that a value hasn't been set on the column when the row was submitted. So a column would be NULL if it does not have a value. For instance, if you had a table of people and needed to put Madonna or Cher in the table, the `last_name` column for those rows would be NULL.

## ANDs and ORs

These conditionals can then be put together in a SELECT statement to get the rows you want.

For example, if we had a `people` table and wanted to get all the rows that represented children, we could write a statement like this:

``` SQL
-- Select all children from the database
SELECT * FROM people WHERE age < 18;
```

We can also combine these conditionals using the keywords `AND` and `OR` to make more complex statements:

``` SQL
-- Select all the first names of children with a last name of Smith from the database
SELECT first_name FROM people WHERE age < 18 AND last_name = 'Smith';
```

## LIKE

LIKE is a special conditional that only works with character data. It's considered a Fuzzy Search operator. If I write the following SQL statement:

``` SQL
-- Get all last names that end in son
SELECT last_name FROM people WHERE last_name LIKE '%son';
```

It will return all rows where the `last_name` ends in `son`. The % is what is called a Wildcard. It matches anything, so the above statement will return names like Erickson, Benson and Danielson, but it won't return sonny or Wilsonn. Because the % is at the beginning, that means that the string must end in `son`.

If I want to only get rows where the `last_name` *begins* with `Ba`:

``` SQL
-- Get all last names that begin with Ba
SELECT last_name FROM people WHERE last_name LIKE 'Ba%';
```

If I want to only get rows where the `last_name` has the string `ski` anywhere in it, including having the string start with or end with `ski`:

``` SQL
-- Get all last names that contain ski anywhere
SELECT last_name FROM people WHERE last_name LIKE '%ski%';
```

You can have as many % in your LIKE as you want in order to do very powerful fuzzy searching in your database.

## Review

So now we've learned a couple of different clauses that we can use to build SQL statements:

- SELECT lets us define which columns we want to pull data from
- FROM lets us define which table we want to pull data from
- WHERE lets us define which rows we want to pull

A lot of SQL statements just use those three clauses, but in the following chapters, we'll learn some other clauses that will help us refine our results further.