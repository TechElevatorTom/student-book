# JOINs

We've seen so far how to select data from a table in the database. We'll now take a look at how to select data from more than one table at a time. This is called **joining** and it's one of the most powerful ways that we can pull information from our databases.

Let's look again at our city and country tables.

#### countries

| id | name           |
|----|----------------|
| 1  | Canada         |
| 2  | Mexico         |
| 3  | Cuba           |
| 4  | France         |
| 5  | Spain          |
| 6  | Italy          |
| 7  | China          |

#### cities

| id | country_id | name        |
|----|------------|-------------|
| 1  | 6          | Milan       |
| 2  | 6          | Rome        |
| 3  | 7          | Beijing     |
| 4  | 4          | Nice        |
| 5  | 4          | Paris       |
| 6  | 1          | Toronto     |
| 7  | 5          | Seville     |

Let's say what we want to select all the cities in France. We could do this:

```SQL
SELECT name FROM cities WHERE country_id=4;
```

That works, but only if we know that the id of France is 4. What if we don't know that? What if we want to select all the cities by only knowing the name of the country?

We could write a statement that looks like this:

```SQL
SELECT name FROM cities WHERE country_id = (SELECT id from countries WHERE name='France');
```

This query uses a **subselect**. A subselect is a way to run a select statement inside another statement. The subselect runs first and pulls back the id of the country whose name is France. We can then use that in our other select statement to pull back all the cities in that country. Now we don't have to hard code ids and have to memorize every id in our database.

But subselects only work for so many things--and they're slow. What we really want to use for this type of query is a JOIN clause.

## JOINing Tables

We can write the above query using a JOIN clause as well.

```SQL
SELECT cities.name FROM cities
JOIN countries ON countries.id = cities.country_id
WHERE countries.name='France';
```

**JOINs** allow us to join up the rows of one table to the rows of another table, essentially giving us a super row of information that we can then select from. So the above join:

```SQL
FROM cities JOIN countries ON countries.id = cities.country_id
```

Will give us rows that look like this:

| cities.id | cities.country_id | cities.name        | countries.id   | countries.name |
|----|------------|-------------|--------------|-----------|
| 1  | 6          | Milan       | 6            | Italy     |
| 2  | 6          | Rome        | 6            | Italy     |
| 3  | 7          | Beijing     | 7            | China     |
| 4  | 4          | Nice        | 4            | France     |
| 5  | 4          | Paris       | 4            | France     |
| 6  | 1          | Toronto     | 1            | Canada     |
| 7  | 5          | Seville     | 5            | Spain     |

We have joined the two tables together where the cities.country_id = countries.id. You can see that the country can be duplicated, like for Milan and Rome, because more than one city has the same country.

Then we can select our columns from this new joined table. Since both cities and countries have a name column, we have to specify *which* name column we want in the SELECT clause:

```SQL
SELECT cities.name FROM cities
JOIN countries ON countries.id = cities.country_id
```

And then we only want the rows that have 'France' in the country:

```SQL
SELECT cities.name FROM cities
JOIN countries ON countries.id = cities.country_id
WHERE countries.name = 'France';
```

Which will give us:

| cities.name |
|-------------|
| Nice        |
| Paris       |

We could also change that statement to:

```SQL
SELECT CONCAT(cities.name, ', ', countries.name) AS display_name FROM cities
JOIN countries ON countries.id = cities.country_id
WHERE countries.name = 'France';
```

With the result being:

| display_name  |
|---------------|
| Nice, France  |
| Paris, France |

## Joining Multiple Tables

Now let's revisit our actors and films example. From that example, let's imagine that we have an `actors` table that looks like this:

#### actors

| id | name       |
|----|------------|
| 1  | Brad Pitt  |
| 2  | Bruce Willis |
| 3  | Chris Evans |
| 4  | Alan Rickman |
| 5  | Ed Norton  |
| 6  | Tom Cruise |

And `films`:

#### films

| id | name       |
|----|------------|
| 1  | Die Hard   |
| 2  | Fight Club |
| 3  | Moonrise Kingdom |
| 4  | 12 Monkeys |

And, finally, the join table:

#### actors_films

| actor_id | film_id  |
|----------|----------|
| 2        | 1        |
| 4        | 1        |
| 5        | 2        |
| 1        | 2        |
| 5        | 3        |
| 2        | 3        |
| 1        | 4        |
| 2        | 4        |

Using this table, we can pull information about both actors and films by joining the tables together. If we wanted to get the name and all of the films from actors whose names start with 'B'.

```SQL
SELECT actors.name, films.name FROM actors
JOIN actors_films ON actors_films.actor_id = actors.id
JOIN films ON films.id = actors_films.film_id
WHERE actors.name LIKE 'B%';
```

| actors.name   | films.name    |
|---------------|---------------|
| Brad Pitt     | Fight Club    |
| Brad Pitt     | 12 Monkeys    |
| Bruce Willis  | Die Hard      |
| Bruce Willis  | Moonrise Kingdom |
| Bruce Willis  | 12 Monkeys    |

You'll see that a lot of the values were duplicated, but each combination is unique. As the database joins the rows of different tables to each other, it will create as many rows as need to join all of the information.

When we select from `actors`, we really only get two rows `Brad Pitt` and `Bruce Willis`. But then we join the `actors_films` table on and the `films` table. Since Brad Pitt has been in two of the films, his name is attached to both of those rows and we get his name duplicated.

But that's okay. Each row has all the information we need from that call.

What if we want to know how many movies each actor has been in? How would we write that query?

Well, we want to pull data from the `actors` table and join that to the `films` table:

```SQL
FROM actors
JOIN actors_films ON actors_films.actor_id = actors.id
JOIN films ON films.id = actors_films.film_id
```

We want to count the films, so if we group the rows by the actor, then we can `COUNT` the number of films for each actor:

```SQL
FROM actors
JOIN actors_films ON actors_films.actor_id = actors.id
JOIN films ON films.id = actors_films.film_id
GROUP BY actors.id
```

And then we can select the actor's name and how many rows were returned for that actor:

```SQL
SELECT actors.name, COUNT(*) AS num_of_films FROM actors
JOIN actors_films ON actors_films.actor_id = actors.id
JOIN films ON films.id = actors_films.film_id
GROUP BY actors.id
```

| actors.name   | num_of_films |
|---------------|--------------|
| Brad Pitt     | 2            |
| Bruce Willis  | 3            |
| Alan Rickman  | 1            |
| Ed Norton     | 2            |

> #### Warning::What happened to Tom Cruise?
>
> You might be wondering what happened to Tom Cruise.
>
> Aren't we all?
>
> In the results, Tom Cruise doesn't show up because he is not linked in the database to any films. Since he isn't joined to any films, when we join the `actors_films` table in, his name disappears from the list. Same with Chris Evans, as a matter of fact.
>
> This is because we're using an INNER JOIN. By just using the `JOIN` keyword, we are using an INNER JOIN by default. An INNER JOIN will only pull back rows if there is a matching row in the joined table. No matching row? Then the SQL call will just drop it like it never existed and poor Tom Cruise is left on the cutting room floor.
>
> There are other types of joins that we can use to prevent that. Take a look at your study materials to see what they are. Most of the time, though, you'll be using this default INNER JOIN.