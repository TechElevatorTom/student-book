# Select Statements

We will first be looking at selecting data from a database. This is one of the main types of statements you'll be using when developing web applications.

For our examples, we are going to assume that we have a database table that looks something like this:

![Our cities table](../resources/sample-table.png)

In our database, this table would have a name. We'll call it the `countries` table. If I want to get all the names of all the countries in the `countries` table, I could run the following statement:

    SELECT name FROM countries;

Result:

| name           |
|----------------|
| Cayman Islands |
| Chile          |
| Cook Islands   |
| Costa Rica     |
| Djibouti       |
| Dominica       |
| Dominican Republic |
| Ecuador        |
| ...            |

This is a SELECT statement. All SELECT statements start with the keyword `SELECT`, followed by which column's data we want to get from the database table. We then have the `FROM` keyword that specifies which table we want the data to be pulled from. This tells the database to give us the `name` of *every* row in the `countries` table.

These are called the SELECT clause and the FROM clause of the the SELECT statement.

> #### Warning::Clause Order
>
> It might seem like we would need to specify the table *before* specifying which columns to select. But SQL is a declarative language and the pieces of the statement aren't written in the order that they run but in an order that reads more like English. Just be sure to remember what order the clauses need to be run in.

We can select more than one column from the table by separating each column name with a comma.

    SELECT name, continent FROM countries;

Result:

| name           | continent         |
|----------------|-------------------|
| Cayman Islands | North America     |
| Chile          | South America     |
| Cook Islands   | Oceania           |
| Costa Rica     | North America     |
| Djibouti       | Africa            |
| Dominica       | North America     |
| Dominican Republic | North America     |
| Ecuador        | South America     |
| ...            | ...               |

Since programmers are lazy, there is also a shortcut for selecting all the columns of a table. That shortcut is to use the `*` symbol, which means *all*.

    SELECT * FROM countries;

Result:

| code | name           | continent         | region             |
|------|----------------|-------------------|--------------------|
| CYM  | Cayman Islands | North America     | Caribbean          |
| CHL  | Chile          | South America     | South America      |
| COK  | Cook Islands   | Oceania           | Polynesia          |
| CRI  | Costa Rica     | North America     | Central America    |
| DJI  | Djibouti       | Africa            | Eastern Africa     |
| DMA  | Dominica       | North America     | Caribbean          |
| DOM  | Dominican Republic | North America     | Caribbean      |
| ECU  | Ecuador        | South America     | South America      |
| ...  | ...            | ...               | ...                |

Using these two clauses allows us to specify what data we want to select from the database. The FROM clause lets us specify what table we want to select from and the SELECT clause lets us specify what columns we want to select from that table.
