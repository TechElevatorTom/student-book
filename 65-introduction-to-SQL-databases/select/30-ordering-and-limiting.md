# Ordering and Limiting Rows

As you have seen, SQL is built out of clauses that we can just add onto statements and continue to refine the results that we get back. Now we'll explore a couple more clauses that will help us refine them further.

## ORDER BY

By default, rows will come back from the database in an unspecified order. It's completely up to the DBMS how it returns rows from a SELECT statement. However, by adding an ORDER BY clause, we can tell the database what order to return our rows in.

``` SQL
SELECT * FROM users ORDER BY age;
```

This will now order our results by the user's age. But in what order? Smallest to greatest or greatest to smallest?

By default, all `ORDER BY` clauses are `ASC` or ascending, starting at the lowest value and going to the highest value. So if you're ordering by a number, the first rows you get back will be the lowest values and the last row you get will have the highest value. For strings, `a` is considered the lowest value, so `ASC` order will go from `a` to `z`, which means it is alphabetical.

Since `ASC` is the default order if one is not specified, the above SQL statement could have been written like this as well:

```SQL
SELECT * FROM users ORDER BY age ASC;
```

Now let's imagine that we have the following rows in our database:


| first_name | last_name    | age  |
|------------|--------------|------|
| Nick       | Smith        | 11   |
| Adam       | Smith        | 10   |
| Beatrice   | Gold         | 8    |
| Zachary    | Claus        | 9    |

Using the statement:

```SQL
SELECT * FROM users ORDER BY age ASC;
```

Will give us the results:

| first_name | last_name    | age  |
|------------|--------------|------|
| Beatrice   | Gold         | 8    |
| Zachary    | Claus        | 9    |
| Adam       | Smith        | 10   |
| Nick       | Smith        | 11   |

And using the statement:


```SQL
SELECT * FROM users ORDER BY age DESC;
```

Will give us the results:

| first_name | last_name    | age  |
|------------|--------------|------|
| Nick       | Smith        | 11   |
| Adam       | Smith        | 10   |
| Zachary    | Claus        | 9    |
| Beatrice   | Gold         | 8    |

### Alphabetical Sorting

Using the statement:

```SQL
SELECT * FROM users ORDER BY last_name ASC;
```

Will give us the results:

| first_name | last_name    | age  |
|------------|--------------|------|
| Zachary    | Claus        | 9    |
| Beatrice   | Gold         | 8    |
| Nick       | Smith        | 11   |
| Adam       | Smith        | 10   |

### Why Aren't Nick and Adam Alphabetical?

We ordered by `last_name` and now all the rows are sorted alphabetically by the `last_name` column. But shouldn't Adam be before Nick? Why would the database not sort that as well?

### Adding Two Order Bys

Because we didn't tell it to. Computers, as we know, only do what you tell them to do. In the SQL statement, we never defined that we cared if the `first_name` was also sorted or not, so the database left it alone. However, we can fix that by telling the database, "First, sort by `last_name`, then sort by `first_name`":

```SQL
SELECT * FROM users ORDER BY last_name ASC, first_name ASC;
```

Will give us the results:

| first_name | last_name | age |
|------------|-----------|-----|
| Zachary    | Claus     | 9   |
| Beatrice   | Gold      | 8   |
| Adam       | Smith     | 10  |
| Nick       | Smith     | 11  |

Adding two columns to the `ORDER BY` clause tells the server that we want it to first sort by the first column and *then* sort by the second column.

In other word, this clause:

``` SQL
ORDER BY last_name ASC, first_name ASC
```

Says to order by the users' last names first and then, *if any of the last names are duplicates*, order by those users' first names.

## LIMITing

We can also add another clause to our SQL to only return a certain number of rows. This can be useful for pulling back the top result of a query or for paging results from an SQL query.

If I want to pull back just the oldest child in the table, I can run this statement:

{% if book.language === 'Java' %}

``` SQL
SELECT * FROM users ORDER BY age DESC LIMIT 1
```

{% elif book.language === 'C#' %}

``` SQL
SELECT TOP 1 * FROM users ORDER BY age DESC
```

{% endif %}

Will give us the result:

| first_name | last_name | age |
|------------|-----------|-----|
| Nick       | Smith     | 11  |

## Review

Now we've added two new clauses to our SELECT statement:

- ORDER BY lets us say in what order we want our results in
- {% if book.language === 'Java' %}LIMIT{% elif book.language === 'C#' %}TOP{% endif %} lets us specify how many rows we want the statement to return

Next, we'll look at turning our data into information using GROUPs and aggregate functions.