# Aggregate Data to Get Information

So far, we've been selecting rows of data from the database and returning it. Sometimes, you might not want all the data from the database, but instead want some *information* that the data can tell you.

For instance, we have our `users` table and we can select all the users from there, but what if we just want to know how many users are in that table? We could select them all and then count them manually, with a simple select statement:

```SQL
SELECT * FROM users;
```

We would get:

| first_name | last_name    | age  |
|------------|--------------|------|
| Nick       | Smith        | 11   |
| Adam       | Smith        | 10   |
| Beatrice   | Gold         | 8    |
| Zachary    | Claus        | 9    |

We can count those and see we have four rows, but with any result bigger than that, it will be a big pain. Thankfully, we have things called **aggregate functions**.

## Aggregate Functions

Instead of counting the rows, we can tell the database to count the rows like this:

```SQL
SELECT COUNT(*) FROM users;
```

Result:

| count |
|-------|
| 4     |

Using these functions, we can collapse our data down into pieces of information. `COUNT(*)` will tell us how many rows are returned from the statement.

A call like this:

``` SQL
SELECT COUNT(*) FROM users WHERE last_name = 'Smith';
```

Will return:

| count |
|-------|
| 2     |

So, `COUNT(*)` counts the number of rows.

There are some other aggregate functions too.

`AVG(column)` will return the average value in a certain column:

``` SQL
SELECT AVG(age) FROM users;
```

| avg |
|-----|
| 9.5 |

`SUM(column)` will add up all the value from a certain column:

``` SQL
SELECT SUM(age) FROM users;
```

| sum |
|-----|
| 38  |

`MIN(column)` will give you the smallest value in that column:

``` SQL
SELECT MIN(age) FROM users;
```

| min |
|-----|
| 8   |

`MAX(column)` will give you the largest value in that column:

```SQL
SELECT MAX(age) FROM users;
```

| max |
|-----|
| 11  |