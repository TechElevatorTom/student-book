# Primary and Foreign Keys

Before we move onto the next part of our SELECT statement, we need to talk a little bit about the relations in our database.

Relational Databases are all about the links that we form between our tables and not just the data in our tables. The power of our databases comes from the fact that we can treat our data as discrete objects of data and then combine them to figure out information about the data.

For a concrete example, imagine we had two tables in our database--a table of cities and a table of countries:

#### countries

| id | name           |
|----|----------------|
| 1  | Canada         |
| 2  | Mexico         |
| 3  | Cuba           |
| 4  | France         |
| 5  | Spain          |
| 6  | Italy          |
| 7  | China          |

#### cities

| id | country_id | name        |
|----|------------|-------------|
| 1  | 6          | Milan       |
| 2  | 6          | Rome        |
| 3  | 7          | Beijing     |
| 4  | 4          | Nice        |
| 5  | 4          | Paris       |
| 6  | 1          | Toronto     |
| 7  | 5          | Seville     |

In the above examples, the `id` columns are what we call **Primary Keys**. Primary key columns are columns that hold a value that is unique for every row in that table. Those are identifiers that we can use to get a specific row of data. If I want to get the row from the countries table that has id 4, I know that I'll be getting back France. Since the `id` column is set up as the primary key--something that we add as a **constraint** on that table when we create it--I also know that each row's id will be unique in that table.

The cities table also has an `id` column, which is the primary key for that table. You'll also see that there is a column called `country_id`. That column is a **Foreign Key**. It references the country table and links a city to a certain country. By creating these primary and foreign keys, we can make our data truly relational and model how these two pieces of data link to each other in the real world.

## Cardinality

We know that cities and countries are related in real life. Obviously, countries contain many cities. And a city is inside one country. There are a few different kinds of relationships that we'll want to model in our database. Here, we'll look at the most common two.

### One-to-Many

In the above example, the relationship between a city and a country is a one to many relationship. It can be expressed as:

> A city is in only one country and a country has many cities.

To model this in our SQL tables, we would have a primary key on both tables and a foreign key on the table that has the *many* part of the relationship--the city in our example.

![Cities and Countries](../resources/cities-and-countries.png)

<!-- http://yuml.me/diagram/nofunky/class/edit/cities and countries, [countries|id:integer;name:varchar]1-*[cities|id:integer;country_id:integer;name:varchar] -->

This makes sense if you think about these columns. Primary keys must be unique, but foreign keys don't. Having a `country_id` on city means that many cities can have the same country. If we put a city_id on the countries table, then a country could only have one city per row, which is not at all what we want. So, because it is one country having many cities, we put the foreign key to countries on the cities table.

### Many-to-Many

But sometimes we want both sides of the relationship to be many. For instance, if we wanted to model films and actors, that would be a many to many relationship. We would say:

> A actor can be in many films and a film can have many actors.

So where does the foreign key go? If we put it on the `films` table, then a film could only have one actor. If we put it on the `actors` table, then an actor could only be in one film. Neither of those scenarios is what we want.

So if we don't want the foreign key on the `films` table and we don't want it on the `actors` table, maybe we should just put it in its own table?

![Actors and Films](../resources/actors-and-films.png)

<!--http://yuml.me/diagram/nofunky/class/edit/actors and films, [actors|id:integer;name:varchar]1-*[actors_films|actor_id:integer;film_id:integer],[actors_films]*-1[films|id:integer;name:varchar]-->

That `actors_films` table is what we call a join table or an associative table. It is a required technique to join two tables together in a many to many relationship. Here, we still have primary keys on the `actors` and `films` tables, but then we put the foreign keys onto a new table called `actors_films`. So the `actors_films` table can have many rows with the same actor and many rows with the same film, satisfying the need to link the `actors` and `films` tables together in a many to many relationship.

> #### Note::Database Table Naming
>
> You might be wondering how we come up with these names for our tables and their rows. It's not random, but it's also not something set in stone either. Wherever you end up, the company will most likely have a naming convention that they already follow. The convention used in this book is commonly known as the Active Record convention.
>
> In the Active Record convention, tables relate to some object that you have in your application, like a City and a Country. Table names are that object pluralized: `cities` and `countries` as examples.
>
> Primary keys are almost always `id` and an integer. Foreign keys are the singular form of the table name with `_id` tacked onto the end, like `city_id` or `country_id`.
>
> Join tables are the two table names concatenated together with a `_` in the middle. The names are arranged alphabetically, like `actors_films` and not `films_actors`.
>
> Again, these aren't requirements and you could see databases that are set up much differently than this. It's better to understand the concepts than to memorize the naming.

So, how do we use these relationships in our SQL statements?