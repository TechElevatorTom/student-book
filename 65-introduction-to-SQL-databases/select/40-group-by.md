# GROUP BY for Extracting Information

What if you wanted to know how many users have the last name of Smith? That's a fairly easy statement to create considering what we've learned so far.

```SQL
SELECT * FROM users;
```

| first_name | last_name    | age  |
|------------|--------------|------|
| Nick       | Smith        | 11   |
| Adam       | Smith        | 10   |
| Beatrice   | Gold         | 8    |
| Zachary    | Claus        | 9    |


``` SQL
SELECT COUNT(*) FROM users WHERE last_name = 'Smith';
```

| count |
|-------|
| 2     |

But what if we want to know how many students have *each* last name? Currently, we would have to run a new statement per last name in the database, which we don't want to do.

If we want to aggregate data by a column in the table, we can do that by grouping rows together based on that column.

``` SQL
SELECT last_name, COUNT(*) FROM users GROUP BY last_name;
```

| last_name | count |
|-----------|-------|
| Smith     | 2     |
| Gold      | 1     |
| Claus     | 1     |

So what does `GROUP BY` do? It groups rows of data together that contain the same value so that you can aggregate them together.

So the statement:

``` SQL
SELECT last_name, COUNT(*) FROM users GROUP BY last_name;
```

Tells the database to group the rows from users (since there is no WHERE clause, it uses all the rows). So first, the database takes all the rows:

| first_name | last_name    | age  |
|------------|--------------|------|
| Nick       | Smith        | 11   |
| Adam       | Smith        | 10   |
| Beatrice   | Gold         | 8    |
| Zachary    | Claus        | 9    |

Groups them by `last_name`:

| first_name | last_name    | age  |
|------------|--------------|------|
| Nick       | Smith        | 11   |
| Adam       | Smith        | 10   |

| first_name | last_name    | age  |
|------------|--------------|------|
| Beatrice   | Gold         | 8    |

| first_name | last_name    | age  |
|------------|--------------|------|
| Zachary    | Claus        | 9    |

And then collapses the rows down to just the `last_name` and the `COUNT(*)` from the SELECT clause, as we saw above in the result.

| last_name | count |
|-----------|-------|
| Smith     | 2     |
| Gold      | 1     |
| Claus     | 1     |

So can we see who all has that `last_name`?

``` SQL
SELECT first_name, last_name, COUNT(*) FROM users GROUP BY last_name;
```

Result:

```
ERROR: selected columns must be in GROUP BY clause
```

We actually can't select the first names because all the first names in our groups are different.

When the Smiths are grouped, then we will have the following rows in that group:

| first_name | last_name    | age  |
|------------|--------------|------|
| Nick       | Smith        | 11   |
| Adam       | Smith        | 10   |

Selecting the `last_name` from that group as one data point is easy, because they all have the same `last_name`, but selecting the `first_name` is harder. Which `first_name` do we choose? Nick or Adam? We can only take one value and the database doesn't know which one you want, so it doesn't even try. However, `COUNT(*)` will take those two rows and turn them into one value, so that works just as we expect and gives us back a `2`.

| last_name | count |
|-----------|-------|
| Smith     | 2     |

So `GROUP BY` is all about taking multiple rows and collapsing them down into one row. How you want them collapsed depends on what information you're trying to see.

## Grouping by Multiple Columns

So how can we get meaningful information from the pieces of data in our tables? Let's take the following `students` table as an example:

| first_name | last_name | state | class |
|------------|-----------|-------|-------|
| Monica     | Carlo     | PA    | Java  |
| Doug       | Marshal   | PA    | C#    |
| Greg       | Bishop    | OH    | C#    |
| Betty      | Raines    | OH    | Java  |
| Ted        | Dorsey    | PA    | Java  |
| Susan      | Granger   | PA    | C#    |

If we wanted to know how many students came from each state, we could write that as:

``` SQL
SELECT state, COUNT(*) FROM students GROUP BY state ORDER BY state;
```

| state | count |
|-------|-------|
| OH    | 2     |
| PA    | 4     |

If we wanted to know how many students are in each class, we could write:

```SQL
SELECT class, COUNT(*) FROM students GROUP BY class ORDER BY class;
```

| class | count |
|--------|------|
| C#    | 3     |
|Java   | 3     |

And that's just great. But what if I wanted to see how many students from each state are in each class? For this, I can group by on two different columns:

``` SQL
SELECT state, class, COUNT(*) FROM students GROUP BY state, class ORDER BY class, state;
```

This will first take the rows and group them by state:

| first_name | last_name | state | class |
|------------|-----------|-------|-------|
| Monica     | Carlo     | PA    | Java  |
| Doug       | Marshal   | PA    | C#    |
| Susan      | Granger   | PA    | C#    |
| Ted        | Dorsey    | PA    | Java  |


| first_name | last_name | state | class |
|------------|-----------|-------|-------|
| Greg       | Bishop    | OH    | C#    |
| Betty      | Raines    | OH    | Java  |

It will then group those groups into classes:

| first_name | last_name | state | class |
|------------|-----------|-------|-------|
| Monica     | Carlo     | PA    | Java  |
| Ted        | Dorsey    | PA    | Java  |

| first_name | last_name | state | class |
|------------|-----------|-------|-------|
| Doug       | Marshal   | PA    | C#    |
| Susan      | Granger   | PA    | C#    |


| first_name | last_name | state | class |
|------------|-----------|-------|-------|
| Greg       | Bishop    | OH    | C#    |

| first_name | last_name | state | class |
|------------|-----------|-------|-------|
| Betty      | Raines    | OH    | Java  |

And then run the select to collapse these groups into one row each:

| state | class | count |
|-------|-------|-------|
| PA    | Java  | 2     |
| PA    | C#    | 2     |
| OH    | C#    | 1     |
| OH    | Java  | 1     |

Finally we sort, first by class:

| state | class | count |
|-------|-------|-------|
| PA    | C#    | 2     |
| OH    | C#    | 1     |
| PA    | Java  | 2     |
| OH    | Java  | 1     |

Then by state:

| state | class | count |
|-------|-------|-------|
| OH    | C#    | 1     |
| PA    | C#    | 2     |
| OH    | Java  | 1     |
| PA    | Java  | 2     |

And there are our results.

## Clause Order

Now that we have seen many of the clauses that we will be using, let's talk a little bit about what order these clauses run in.

In imperative programming, like Java and C#, your statements always run from top to bottom and left to right. But like I said at the beginning of this section on SQL, SQL is a declarative language and does *not* run from top to bottom and left to right. So what order does it run in?

SELECT statement clauses will always be executed--and can be thought through--in the following order:

1. FROM clause - The database needs to know which table you are selecting from first of all
2. WHERE clause - The database then needs to know which rows we are going to work with
3. GROUP BY clause - The database will then group those rows according to our GROUP BY clause
4. SELECT clause - The database will then collapse those rows down and select the columns that we want data from
5. ORDER BY clause - The database will order the rows in the order what we ask for
6. LIMIT / TOP clause - The database will then only return the number of resulting rows that we want