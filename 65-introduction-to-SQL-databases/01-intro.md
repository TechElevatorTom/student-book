# Introduction to Databases

All of the data that we've been generating and using in our programs so far just disappears once the program closes. But in most web applications, data sticks around, or persists, even if we leave the site, log out of the site or close our browsers. That data persistence isn't a feature of Java or C#, but comes from another application, and another language.

This chapter will talk about these databases. You will learn:
- The main components of a relational database management system (RDBMS)
- How to read data from a database using Structured Query Language (SQL)
- How to add, modify and remove data from a database using SQL
- Principles of database design
- How to create database objects using data definition language (DDL)
- How to enforce data integrity with constraints and transactions

