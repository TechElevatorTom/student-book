# Relational Database Management Systems

In order to persist data in an application, that data must be stored in some type of *database*. A database is an organized collection of data that can be accessed,
managed, and updated. Good databases are fast, persistent and consistent. The kind of database we'll be looking at here are *relational databases*.

A relational database is a certain type of database that acts a lot like an Excel spreadsheet. A relational database is made up of tables that contain columns where each column has a name and a data type. You can then insert rows into those tables where each row specifies the data for a certain element.

![A table for Patient data](resources/patient-table.png)

<!-- https://www.ictlounge.com/html/types_of_databases.htm -->

Most databases contain many tables that all relate to each other. Data can then be stored in those tables and retrieved again at a later time.

These databases are managed in Relational Database Management Systems, or RDMBS. An RDBMS is a computer application that manages the definition, storage, retrieval and administration of these databases.

These database systems are used for a number of reasons: 

1. They easily support storing large number of records, often in the millions of records, quickly and efficiently.
2. They enable central storage of all of a company's data.
3. They support a structured query syntax to retrieve, insert and update data.
4. They enforce consistency and integrity of data so data won't get lost or corrupted.

We will also be using relational databases for our applications and we will be using the Structured Query Language, or SQL, to work with them.