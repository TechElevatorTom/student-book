# Introduction to Data Security

Data is a very valuable thing, and so it's very important that we protect the data in our applications from being stolen or lost. Considering that data breaches are a common occurrence, especially in web applications, learning some basic data security skills when working with a database is extremely important.

The things you will learn here are things that are still major issues in a large number of web applications. Many experienced web developers don't know or pay attention to these issues. Major data security breaches have happened in major banking, social media and communications applications over the years. You can see some of the worst in this [Data Breach Visualization][data-breach-visualization] including over half a billion email accounts stolen from Yahoo in 2014 and recently, 383 million credit cards and passport information stolen from Marriott Hotels.

So, how can you protect the information you have stored in an SQL database from outside attacks? There are a couple of ways this is accomplished, some of which we've already taught you.

[data-breach-visualization]: https://informationisbeautiful.net/visualizations/worlds-biggest-data-breaches-hacks/