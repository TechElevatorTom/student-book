# SQL Injection Attacks and Defenses

One of the main ways that data gets stolen or destroyed in web applications is via an attack called an [SQL Injection Attack][sql-injection-attack]. An SQL Injection attack is an attack where a malicious attacker is able to pass a special value into your application that, when put into an SQL statement, will delete or display to the attacker information the attacker should not have access to.

This is possible--as are all injection style attacks--because the value that makes the SQL bad doesn't affect the rest of the application at all.

## Getting chat logs

Let's imagine that you are building a new application that will handle chat logs in an SQL database. Each chat message has the following information:

| to | from | message |
|---|---|---|
| joe | jack | Hey, dude. What's up? |
| mary | joe | Want to hang out Friday night? |
| jack | mary | That Joe guy is a real moron. |
| mary | jack | Yeah, a real moron 5. rofl |

When Joe goes to his page to see his messages. He could even search them and we could imagine an SQL statement like the following would select his messages given his search query.

```SQL
SELECT to, from, message FROM messages WHERE (to='joe' OR from='joe') AND
    message LIKE '%dude%';
```

Now in the DAO, we wouldn't have this written out this way at all. We could have a line like this:

``` Java
"SELECT to, from, message FROM messages WHERE (to='" + username +
    "' OR from='" + username + "') AND message LIKE '%" + search + "%'";
```

If the value of `username` is "joe" and `search` is "dude", this would return these rows:

| to | from | message |
|---|---|---|
| joe | jack | Hey, dude. What's up? |

This application could run just fine for years without anyone knowing about how insecure this is. In fact, you might not even see how insecure it is. But let's imagine that a user has the ability to set that `search` to whatever they want. That's a reasonable expectation.

If Joe set `search` to "night", he would get:

| to | from | message |
|---|---|---|
| mary | joe | Want to hang out Friday night? |

But he could set the search query to anything. If he knows how SQL works, he might send the following search:

``` SQL
O'Malley
```

This will generate the SQL:

``` SQL
SELECT to, from, message FROM messages WHERE (to='joe' OR from='joe') AND
    message LIKE '%O'Malley%';
```

Which will cause a syntax error. It might even print a big ugly error message on the screen. This will tell Joe that there's a problem in the SQL. And now Joe can exploit it.

He could send the following search:

``` SQL
%' OR 1=1 --
```

While this looks like gibberish, it's actually a very careful crafted attack. If that gets put into the SQL via concatenation and then sent to the SQL server, the statement will look like this:

``` SQL
SELECT to, from, message FROM messages WHERE (to='joe' OR from='joe') AND
    message LIKE '%%' OR 1=1 --%';
```

In other words, select messages where the to or from are joe and the message is empty **or select everything**. That `1=1` is always going to be true, which means it will always return the row, which means that it returns every row and Joe gets to see this:

| to | from | message |
|---|---|---|
| joe | jack | Hey, dude. What's up? |
| mary | joe | Want to hang out Friday night? |
| jack | mary | That Joe guy is a real moron. |
| mary | jack | Yeah, a real moron 5. rofl |

The `1=1` attack is a way for attackers to get every single record out of a database, whether they own it or not.

This might make Joe mad. This might make Joe do something drastic. Joe could send this search next:

``` SQL
%'; DELETE FROM messages; --
```

Which will run the following statement:

``` SQL
SELECT to, from, message FROM messages WHERE (to='joe' OR from='joe') AND
    message LIKE '%%'; DELETE FROM messages; --%';
```

Which will empty the entire messages table from the database. And, unless the site has a back up, the data will be gone forever. Even if the site has a back up, it will take hours to get everything back to the way it was and by then, users will no longer trust the site anyway.

All of this is happening because we're using one programming language to write another programming language. Injection attacks are possible because what is harmless in one language is destructive in the other and if the programmer isn't watching for it, it'll come back to bite them.

I can write this in our back-end language and never cause a problem:

``` Java
String search = "%'; DELETE FROM messages; --";
```

But the minute I try to put that in an SQL statement, I've just managed to destroy a large part of my application.

## Preventing SQL injection attacks

As you can see, these kinds of attacks are bad news. So how do you protect yourself from them?

Luckily, you've already been taught how. When you saw how to generate SQL from
{% if book.language === 'Java' %}
Java
{% elif book.language === 'C#' %}
C#
{% endif %}
, you saw what are called [prepared statements or parameterized statements][prepared-statements]. You should never be concatenating strings together to form an SQL statement. Instead, you should always use the prepared statement or parameterized statement functionality of whatever programming language you are working in.


{% if book.language === 'Java' %}

### Spring Database `JdbcTemplate`

In Java, you have the `JdbcTemplate` class to use to make prepared statement calls to your database. By using SQL statements with `?` placeholders in them, you let the SQL database put the values into the SQL statement instead of doing it in Java. The SQL database knows how to merge these two values into a safe SQL statement that won't retrieve unauthorized information or destroy your data.

If you wrote your statement from before as a prepared statement, it would look like this:

``` Java
String sqlSearchMessages = "SELECT to, from, message FROM messages " +
    "WHERE (to=? OR from=?) AND message LIKE ?";
```

And then you could call your SQL statement with the following code:

``` Java
SqlRowSet results = jdbcTemplate.queryForRowSet(sqlSearchMessages, username, username, "%" + search + "%");
```

{% elif book.language === 'C#' %}

In .Net, you have the `SqlCommand` class to use to make parameterized statement calls to your database. By using SQL statements with `@` parameters in them, you let the SQL database put the values into the SQL statement instead of doing it in C#. The SQL database knows how to merge these values into a safe SQL statement that won't retrieve unauthorized information or destroy your data.

If you wrote your statement from before as a parameterized statement, it would look like this:

``` CSharp
private const string SQL_SearchMessages = "SELECT to, from, message FROM messages " +
    "WHERE (to=@username OR from=@username) AND message LIKE @search";
```

And then you could call your SQL statement with the following code:

``` CSharp
cmd.Parameters.AddWithValue("@username", username);
cmd.Parameters.AddWithValue("@search", search);
SqlDataReader reader = cmd.ExecuteReader();
```

{% endif %}

How does this fix the problem? Because now you aren't building the whole SQL statement in your code. Instead, you are making your prepared statement, giving that to the server and then telling the server, "Hey, put these values in the placeholders that are set up." Since the SQL server understands SQL, it's able to insert the values into the statement in a way that treats the value as a string, and not as part of the statement.

Using prepared statements, if an attacker sent the string:

``` SQL
%'; DELETE FROM messages; --
```

The database wouldn't execute the `DELETE` statement, but would instead search for the literal string `%'; DELETE FROM messages; --` in the messages and, not finding any, return nothing.

> ### Always use prepared statements. Never concatenate user provided data onto your SQL statements.

[sql-injection-attack]: https://www.owasp.org/index.php/Top_10-2017_A1-Injection
[prepared-statements]: https://en.wikipedia.org/wiki/Prepared_statement

