
## Why Are There So Many Different Data Structures

Your choice of data structure depends directly on the type of data you are working with. However, there is something else you must consider: _what you are trying to do with the data_.

With all of the collections, you may find yourself performing one of the four following operations:

* adding or inserting items in a particular spot
* removing or deleting an item from the collection
* searching for a particular value
* updating the value

Each data structure performs these operations more or less efficiently than the other. For example, the queue always inserts items at the end and removes them from the front. It doesn't matter if the queue has 1 item or 100,000 items - it takes the same amount of time. 

As a programmer, your goal is to write code generically without hard-coding it to fixed collection sizes. Sometimes your solution takes 1s to execute with 1 item; sometimes it takes 10s to run with 100 items. You may not mean to, but the code you write can inadvertently create a new problem that causes the algorithm time to go up by a factor of _n_,  _n&#178;_, or _2&#8319;_. This is important so that you can identify a worst-case scenario when looking at your program's algorithms.

This way of measuring algorithm complexity is called **Big-O Notation**. 

**Disclaimer**: This next section covers an advanced computer science topic that is often brought up casually during an interview. The intent of the discussion is to challenge the candidate to realize the efficiency of the solution and look for alternative ways of writing the same code. 

Before you dive in, it is important to remember one thing as you begin your journey as a coder: **Premature optimization is the root of all evil.**

![Premature Optimization is the root of all evil. <a href="https://www.explainxkcd.com/wiki/index.php/1691:_Optimization" target="_blank">XKCD Explained</a>](resources/optimization.png)

When it comes to working on a problem, whether for yourself or an interviewer, focus first on creating the working solution. Once it works, look for ways to optimize.

### O(1) - Constant Time

Suppose you were asked to get the first number in an array - a list could work - and say if it was even. The code might look something like this:

{% if book.language === 'Java' %}

```java
public boolean isFirstElementEven(int[] array) {
    return array[0] % 2 == 0;
}
```


{% elif book.language === 'C#' %}

```csharp
public bool IsFirstElementEven(int[] array)
{
    return array[0] % 2 == 0;
}
```
{% endif %}

This algorithm takes the same amount of time to run whether the size of the array is length 1 or length 100,000. The Big O notation, O(1), describes an algorithm that always takes the same time.


### O(N) - Linear Time

Below is the code for a search algorithm that looks through an array seeking a specified value.

{% if book.language === 'Java' %}

```java
public boolean containsValue(int[] array, int someValue) {
    for(int i = 0; i < array.length; i++) {
        if (array[i] == someValue) { 
            return true;
        }
    }

    return false;
}
```


{% elif book.language === 'C#' %}

```csharp
public bool ContainsValue(int[] array, int someValue)
{
    for(int i = 0; i < array.Length; i++) 
    {
        if (array[i] == someValue) 
        { 
            return true;
        }
    }

    return false;
}
```

{% endif %}

This algorithm searches every element in the array to see if it matches `someValue`. In a worst-case scenario, it doesn't exist, but you don't discover this until the end. In a best-case scenario, the first element in the array is what you need.

Because Big O notation always refers to the upper limit or worst-case performance, this algorithm could be described as O(N). Its performance is linearly proportional to the size of the input data set. 

### O(N&#178;) - Quadratic Time

Take a look at another type of algorithm that checks to see if an array contains duplicates.

{% if book.language === 'Java' %}

```java
public boolean containsDuplicates(int[] array) {
    for (int outer = 0; outer < array.length; outer++) {
        for (int inner = 0; inner < array.length; inner++) { 
            // Don't compare with self;
            if (outer == inner) {
                continue;
            }

            if (array[outer] == array[inner]) { 
                return true;
            }
        }
    }

    return false;
}
```


{% elif book.language === 'C#' %}

```csharp
public bool ContainsDuplicates(int[] array) 
{
    for (int outer = 0; outer < array.Length; outer++) 
    {
        for (int inner = 0; inner < array.Length; inner++) 
        { 
            // Don't compare with self;
            if (outer == inner) 
            {
                continue;
            }

            if (array[outer] == array[inner]) 
            { 
                return true;
            }
        }
    }

    return false;
}
```

{% endif %}

This algorithm starts with the first element in the array and searches the remaining elements looking for the same value. If no duplicate is found, the algorithm moves on to the second element and searches the array again.

In a best-case scenario, your duplicate values are the first two elements in the array, and the inner-most part of the for loop runs once or twice. Unfortunately, things don't always work out the way you planned. To be sure, you need to check every value.

You might think an array length 10 is small. If you run this algorithm against an array of that size with no duplicates, the inner-most part of the for loop runs 100 times. An array of size 100 searches the array 1,000,000 times.

This algorithm would be O(N&#178;). This is common with nested loops. If you were to nest another for loop, the algorithm would then be O(N&#179;) and so on.

### Please, Make it Stop!

This material can be interesting - or boring - depending on what motivates you. At this point, it is important for you to realize, but not dwell on, that **there are consequences for the code that you write**. These consequences can be mitigated by choosing the correct data structure.

For your viewing pleasure, through the magic of the Internet and GitHub, here is a poster that demonstrates some Big-O Complexities.

![A poster of common algorithms used in Computer Science. The full version can be found <a target="_blank" href="resources/bigoposter.pdf">here</a>](resources/bigoposter.png)