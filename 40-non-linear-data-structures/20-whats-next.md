
## What's Next

You've reached the end of this section on different data structures and collections classes. Up until this point, you've learned about linear data structures - those with a sequential order like arrays, stacks, and queues - and non-linear data structures, or those with a non-sequential order of data.

This isn't the end of it all. These data structures are the ones used most often, but if you want to explore data structures further:

* **Trees** are often used to maintain a hierarchy of parent, children, and sibling relationships of data, like a file system.
* **Graphs** show how two different pieces of data are connected. Graphs are often seen in social networking and path-finding/GPS algorithms.