

# Non-Linear Data Structures

In this chapter, you'll learn:

* a terminology used to measure algorithmic complexity with arrays, stacks, queues, etc.
* the collection class used to make associations between keys and values, the Map (Java) / Dictionary (C#)
* the set, a collection used to store unique values
