

## Sets

Another type of non-linear data structure is a set. A set is a collection that holds unique values, similar to a list, but in a way that allows you to easily and quickly determine if the object already exists in the set. In C#, it is called a hash set. 

Sets don't use indexes to store values. Instead, they hold hashcodes, which are numbers computed from the values you want to store. 

### Creating a New Set

{% if book.language === 'Java' %}

The syntax for creating a new HashSet is:

```csharp
Set<String> characters = new HashSet<String>();  
```

{% elif book.language === 'C#' %}

The syntax for creating a new HashSet is:

```csharp
HashSet<string> characters = new HashSet<string>();  
```

{% endif %}

### Adding Items to the Set

{% if book.language === 'Java' %}

The `add()` method adds an element to the set.

```java
Set<String> characters = new HashSet<String>();  

characters.add("Luke");     // contains Luke
characters.add("Rey");     // contains Luke, Rey
characters.add("Ben");     // contains Luke, Rey, Ben
characters.add("Luke");     // contains Luke, Rey, Ben
```

The `add()` method returns a boolean indicating if the element was added to the Set. **false** indicates the element is already present. 

{% elif book.language === 'C#' %}

The Add method adds an element to the set.

```csharp
HashSet<string> characters = new HashSet<string>();  

characters.Add("Luke");     // contains Luke
characters.Add("Rey");     // contains Luke, Rey
characters.Add("Ben");     // contains Luke, Rey, Ben
characters.Add("Luke");     // contains Luke, Rey, Ben
```

The Add method returns a boolean indicating if the element was added to the HashSet. **false** indicates the element is already present. 

{% endif %}

### Removing Items from the Set

{% if book.language === 'Java' %}

The `remove()` method removes an element from a Set. The method returns a boolean indicating if the element was removed.

```java
Set<String> characters = new HashSet<String>();  

// ... set contains Luke, Rey, and Ben

characters.remove("Ben");   // set now contains Luke and Rey
```

{% elif book.language === 'C#' %}

The Remove method is used to remove an element from a HashSet. The method returns a boolean indicating if the element was removed.

```csharp
HashSet<string> characters = new HashSet<string>();  

// ... set contains Luke, Rey, and Ben

characters.Remove("Ben");   // set now contains Luke and Rey
```

{% endif %}

### Iterating Through a Set

{% if book.language === 'Java' %}

Sets don't allow you to use an index to access each item. To see each of the elements in a set, you'll need to use the foreach loop.

```java
Set<String> characters = new HashSet<String>();  

// ... set contains Luke, Rey, and Ben

for(String character : characters) {
    // execute code for each item in the set
}
```

{% elif book.language === 'C#' %}

Sets don't allow you to use an index to access each item. To see each of the elements in a set, you'll need to use the foreach loop.

```csharp
HashSet<string> characters = new HashSet<string>();  

// ... set contains Luke, Rey, and Ben

foreach(string character in characters)
{
    // execute code for each item in the set
}
```

{% endif %}

{% if book.language === 'C#' %}

### Other Set Operations

One of the other powerful things that sets can do is perform a union or an intersection with another set.

You will rarely use the Set, except in interviews. If you run into a situation where you have two separate collections and you wish to consolidate them, you can leverage the union and intersection methods.



The UnionWith method modifies the current HashSet to contain all elements that are present in itself and in the specified collection.

```csharp
HashSet<string> old = new HashSet<string>() { "Luke", "Han", "Chewbacca", "Leia", "Darth Vader" };
HashSet<string> new = new HashSet<string>() { "Luke", "Han", "Chewbacca", "Leia", "Rey", "Ben", "Finn" };

old.UnionWith(new);

// old now contains (but guaranteed to be in this order)
// Luke, Han, Chewbacca, Leia, DarthVader, Rey, Ben, and Finn
```

On the other hand, the IntersectWith method allows you to compare two sets and only keep the elements that are present in both.

```csharp
HashSet<string> old = new HashSet<string>() { "Luke", "Han", "Chewbacca", "Leia", "Darth Vader" };
HashSet<string> new = new HashSet<string>() { "Luke", "Han", "Chewbacca", "Leia", "Rey", "Ben", "Finn" };

old.IntersectWith(new);

// old now contains (but guaranteed to be in this order)
// Luke, Han, Chewbacca, Leia
```

{% endif %}
