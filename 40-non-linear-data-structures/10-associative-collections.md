## Associative Collections

Arrays are fast for reading data. If you know your index, the time it takes to access the value is O(1). Linked lists are fast for inserting and removing data. 

Neither are suitable when you need to quickly search for data. Because we don't know what index holds the item we seek, we  need to traverse the entire collection in a worst-case scenario.

The {% if book.language === "C#" %} dictionary {% elif book.language === "Java" %} map {% endif %} offers a distinct advantage over arrays and lists. {% if book.language === "C#" %} Dictionaries {% elif book.language === "Java" %} Maps {% endif %} are comprised of key/value pairs, similar to the way that arrays use indexes to locate a value. With arrays you are not allowed to defined your index and {% if book.language === "C#" %} dictionaries {% elif book.language === "Java" %} maps {% endif %} provide you the ability to create a key of any type.

Here we have a {% if book.language === "C#" %} dictionary {% elif book.language === "Java" %} map {% endif %} where the keys are the names of characters and the value is a boolean indicating whether or not they are a Jedi.

![](resources/dictionary.jpg)

With {% if book.language === "C#" %} dictionaries {% elif book.language === "Java" %} maps {% endif %} it is important to recognize that:

* keys must be unique
* values are not required to be unique

A {% if book.language === "C#" %} dictionary {% elif book.language === "Java" %} map {% endif %} is a type of data structure known as a non-linear data structure. With non-linear data structures, items aren't in a sequential order. Instead relationships are established between items (e.g. our example of a key-value).

### Creating a New {% if book.language === "C#" %} Dictionary {% elif book.language === "Java" %} Map {% endif %}

{% if book.language === 'Java' %}

The syntax for creating a map is

    Map<TKey, TValue> name = new HashMap<TKey, TValue>();

When working with maps, you define what data type your key will be and what data type the value is. If you were to create a map that resembled the above visual, it would look like the code below.

Notice this syntax is similar to how we worked with lists previously.

```java
Map<String, Boolean> people = new HashMap<String, Boolean>();
```

A map can be instantiated and initialized with existing key-value pairs (if known ahead of time):

```java
Map<String, Boolean> people = new HashMap<String, Boolean>();
people.put("Luke", true);
people.put("Han", false);
people.put("Chewbacca", false);
people.put("Yoda", true);
people.put("Leia", false);
```

{% elif book.language === 'C#' %}

The syntax for creating a dictionary is

    Dictionary<TKey, TValue> name = new Dictionary<TKey, TValue>();

When working with dictionaries, you define what data type your key will be and what data type the value is. If you were to create a dictionary that resembled the above visual, it would look like the code below.

Notice this syntax is similar to how we worked with lists previously.

```csharp
Dictionary<string, bool> people = new Dictionary<string, bool>();
```

A dictionary can be instantiated and initialized with existing key-value pairs (if known ahead of time):

```csharp
Dictionary<string, bool> people = new Dictionary<string, bool>() 
{
    { "Luke", true },
    { "Han", false},
    { "Chewbacca", false },
    { "Yoda", true },
    { "Leia", true }
};
```


{% endif %}

### Accessing Dictionary Values

{% if book.language === 'Java' %}

To access a value from a map, you use the same "index" notation you've used when accessing values from a list. Instead of using an integer to represent the index, you use a value that has the same type as the TKey argument:

```java
Map<String, Boolean> people = new HashMap<String, Boolean>(); 

// ... dictionary is populated

boolean isLukeJedi = people.get("Luke");
boolean isHanJedi = people.get("Han");
```

This code retrieves a value from the map using a given key. If you provide a key that does not exist, `null` is returned.

{% elif book.language === 'C#' %}

To access a value from a dictionary, you use the same "index" notation you've used when accessing values from a list. Instead of using an integer to represent the index, you use a value that has the same type as the TKey argument:

```csharp
Dictionary<string, bool> people = new Dictionary<string, bool>(); 

// ... dictionary is populated

bool isLukeJedi = people["Luke"];
bool isHanJedi = people["Han"];
```

This code retrieves a value from the dictionary using a given key. If you provide a key that does not exist, a `KeyNotFoundException` is thrown.

{% endif %}

### Adding Dictionary Values

{% if book.language === 'Java' %}

To add an item to the map, call the `put()` method.

```java
Map<String, Boolean> people = new HashMap<String, Boolean>(); 

people.put("Luke", true);
```

The `put()` method requires two arguments. The first is the key and must match the data type of the key. The second is the value and matches the data type of the value. 

If the key does not exist, it is added. If it does exist, its value will be *overwritten and replaced*.

{% elif book.language === 'C#' %}

To add an item to the dictionary, there are two options. The first uses the Add method:

```csharp
Dictionary<string, bool> people = new Dictionary<string, bool>(); 

people.Add("Luke", true);
```

The Add method requires two arguments. The first is the key and must match the type of TKey. The second is the value and matches the type of TValue. 

Using the Add method comes with a caveat. If you add an item with a key that already exists in the dictionary, an `ArgumentException` is thrown stating the key already exists. As an alternative, you can use the index notation to set values in a dictionary:

```csharp
Dictionary<string, bool> people = new Dictionary<string, bool>(); 

people["Luke"] = true;
```

If the key-value pair does not exist, it is added. If it exists, its value will be updated.

{% endif %}

### Validating the Existing of a Key-Value Pair

{% if book.language === 'Java' %}

A containsKey method exists and can be used to determine whether the Map contains the specified key.

```java
Map<String, Boolean> people = new HashMap<String, Boolean>(); 

// ... map is populated

if (!people.containsKey("Luke")) { 
    people.put("Luke", true);
}
```
{% elif book.language === 'C#' %}

A ContainsKey method exists and can be used to determine whether the dictionary contains the specified key.

```csharp
Dictionary<string, bool> people = new Dictionary<string, bool>(); 

// ... dictionary is populated

if (!people.ContainsKey("Luke")) 
{ 
    people["Luke"] = true;
}
```

{% endif %}

### Removing Values from a Dictionary

{% if book.language === 'Java' %}

A `remove()` method is used to remove a key-value pair from the map. It returns a boolean if the key was successfully found and removed.

```java
Map<String, Boolean> people = new HashMap<String, Boolean>(); 

// ... map is populated with Luke, Han, Chewbacca, Yoda, and Leia

people.remove("Chewbacca"); // Luke, Han, Yoda, and Leia remain
```
{% elif book.language === 'C#' %}

A Remove method is used to remove a key-value pair from the dictionary. It returns a boolean if the pair was successfully found and removed.

```csharp
Dictionary<string, bool> people = new Dictionary<string, bool>(); 

// ... dictionary is populated with Luke, Han, Chewbacca, Yoda, and Leia

people.Remove("Chewbacca"); // Luke, Han, Yoda, and Leia remain
```

{% endif %}

### Iterating Through a Dictionary

{% if book.language === 'Java' %}

Much like lists and other collections, maps can be enumerated. When looping through a map, each individual item is of type `Map.Entry<TKey, TValue>`. To do that, we call the `entrySet()` method.

```java
Map<String, Boolean> people = new HashMap<String, Boolean>(); 

// ... Map is populated with Luke, Han, Chewbacca, Yoda, and Leia

for(Map.Entry<String, Boolean> person : people.entrySet()) {
    if (person.getValue()) {
        System.out.println(person.getKey() + " is a Jedi.");
    } else {
        System.out.println(person.getKey() + " is not a Jedi.");
    }
}
```

This would produce the following output:

    Luke is a Jedi.
    Han is not a Jedi.
    Chewbacca is not a Jedi.
    Yoda is a Jedi.
    Leia is not a Jedi.
    
{% elif book.language === 'C#' %}

Much like lists and other collections, dictionaries can be enumerated. When looping through a dictionary, each individual item is of type `KeyValuePair<TKey, TValue>`.

```csharp
Dictionary<string, bool> people = new Dictionary<string, bool>(); 

// ... dictionary is populated with Luke, Han, Chewbacca, Yoda, and Leia

foreach(KeyValuePair<string, bool> person in people)
{
    if (person.Value)
    {
        Console.WriteLine(person.Key + " is a Jedi.");
    }
    else
    {
        Console.WriteLine(person.Key + " is not a Jedi.");
    }
}
```

This would produce the following output:

    Luke is a Jedi.
    Han is not a Jedi.
    Chewbacca is not a Jedi.
    Yoda is a Jedi.
    Leia is not a Jedi.

{% endif %}